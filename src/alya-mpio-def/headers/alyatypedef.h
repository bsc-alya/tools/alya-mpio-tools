/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#ifndef ALYATYPDEF_H
#define ALYATYPDEF_H

#include <map>
#include <string>

#define ALYA_POINT                              1 // 0D

#define ALYA_BAR02                              2 // 1D
#define ALYA_BAR03                              3 // 1D
#define ALYA_BAR04                              4 // 1D

#define ALYA_TRI03                              10 // 2D
#define ALYA_TRI06                              11 // 2D
#define ALYA_QUA04                              12 // 2D
#define ALYA_QUA08                              13 // 2D
#define ALYA_QUA09                              14 // 2D
#define ALYA_QUA16                              15 // 2D
#define ALYA_TRI10                              16 // 2D

#define ALYA_TET04                              30 // 3D
#define ALYA_TET10                              31 // 3D
#define ALYA_PYR05                              32 // 3D
#define ALYA_PYR14                              33 // 3D
#define ALYA_PEN06                              34 // 3D
#define ALYA_PEN15                              35 // 3D
#define ALYA_PEN18                              36 // 3D
#define ALYA_HEX08                              37 // 3D
#define ALYA_HEX20                              38 // 3D
#define ALYA_HEX27                              39 // 3D
#define ALYA_HEX64                              40 // 3D
#define ALYA_TET20                              41 // 3D
#define ALYA_SHELL                              51 // 3D
#define ALYA_BAR3D                              52 // 3D

#define ALYA_POINT_STRING                       "POINT" // 0D

#define ALYA_BAR02_STRING                       "BAR02" // 1D
#define ALYA_BAR03_STRING                       "BAR03" // 1D
#define ALYA_BAR04_STRING                       "BAR04" // 1D

#define ALYA_TRI03_STRING                       "TRI03" // 2D
#define ALYA_TRI06_STRING                       "TRI06" // 2D
#define ALYA_TRI10_STRING                       "TRI10" // 2D
#define ALYA_QUA04_STRING                       "QUA04" // 2D
#define ALYA_QUA08_STRING                       "QUA08" // 2D
#define ALYA_QUA09_STRING                       "QUA09" // 2D
#define ALYA_QUA16_STRING                       "QUA16" // 2D

#define ALYA_TET04_STRING                       "TET04" // 3D
#define ALYA_TET10_STRING                       "TET10"// 3D
#define ALYA_TET20_STRING                       "TET20"// 3D
#define ALYA_PYR05_STRING                       "PYR05" // 3D
#define ALYA_PYR14_STRING                       "PYR14" // 3D
#define ALYA_PEN06_STRING                       "PEN06" // 3D
#define ALYA_PEN15_STRING                       "PEN15" // 3D
#define ALYA_PEN18_STRING                       "PEN18" // 3D
#define ALYA_HEX08_STRING                       "HEX08" // 3D
#define ALYA_HEX20_STRING                       "HEX20" // 3D
#define ALYA_HEX27_STRING                       "HEX27" // 3D
#define ALYA_HEX64_STRING                       "HEX64" // 3D
#define ALYA_SHELL_STRING                       "SHELL" // 3D shell element
#define ALYA_BAR3D_STRING                       "BAR3D" // 3D bar element

#define ALYA_POINT_NODES                        1 // 0D

#define ALYA_BAR02_NODES                        2 // 1D
#define ALYA_BAR03_NODES                        3 // 1D
#define ALYA_BAR04_NODES                        4 // 1D

#define ALYA_TRI03_NODES                        3 // 2D
#define ALYA_TRI06_NODES                        6 // 2D
#define ALYA_TRI10_NODES                        10// 2D
#define ALYA_QUA04_NODES                        4 // 2D
#define ALYA_QUA08_NODES                        8 // 2D
#define ALYA_QUA09_NODES                        9 // 2D
#define ALYA_QUA16_NODES                        16 // 2D

#define ALYA_TET04_NODES                        4 // 3D
#define ALYA_TET10_NODES                        10 // 3D
#define ALYA_TET20_NODES                        20 // 3D
#define ALYA_PYR05_NODES                        5 // 3D
#define ALYA_PYR14_NODES                        14 // 3D
#define ALYA_PEN06_NODES                        6 // 3D
#define ALYA_PEN15_NODES                        15 // 3D
#define ALYA_PEN18_NODES                        18 // 3D
#define ALYA_HEX08_NODES                        8 // 3D
#define ALYA_HEX20_NODES                        20 // 3D
#define ALYA_HEX27_NODES                        27 // 3D
#define ALYA_HEX64_NODES                        64 // 3D
#define ALYA_SHELL_NODES                        3 // 3D
#define ALYA_BAR3D_NODES                        2 // 3D

#define SHAPE_POINT                             "Point"
#define SHAPE_LINEAR                            "Linear"
#define SHAPE_TRIANGLE                          "Triangle"
#define SHAPE_QUADRILATERAL                     "Quadrilateral"
#define SHAPE_TETRAHEDRON                       "Tetrahedra"
#define SHAPE_PYRAMID                           "Pyramid"
#define SHAPE_PRISM                             "Prism"
#define SHAPE_HEXAHEDRON                        "Hexahedra"

#define ALYA_POINT_SHAPES                       SHAPE_POINT // 0D

#define ALYA_BAR02_SHAPES                       SHAPE_LINEAR // 1D
#define ALYA_BAR03_SHAPES                       SHAPE_LINEAR // 1D
#define ALYA_BAR04_SHAPES                       SHAPE_LINEAR // 1D

#define ALYA_TRI03_SHAPES                       SHAPE_TRIANGLE // 2D
#define ALYA_TRI06_SHAPES                       SHAPE_TRIANGLE // 2D
#define ALYA_TRI10_SHAPES                       SHAPE_TRIANGLE // 2D
#define ALYA_QUA04_SHAPES                       SHAPE_QUADRILATERAL // 2D
#define ALYA_QUA08_SHAPES                       SHAPE_QUADRILATERAL // 2D
#define ALYA_QUA09_SHAPES                       SHAPE_QUADRILATERAL // 2D
#define ALYA_QUA16_SHAPES                       SHAPE_QUADRILATERAL // 2D

#define ALYA_TET04_SHAPES                       SHAPE_TETRAHEDRON // 3D
#define ALYA_TET10_SHAPES                       SHAPE_TETRAHEDRON // 3D
#define ALYA_TET20_SHAPES                       SHAPE_TETRAHEDRON // 3D
#define ALYA_PYR05_SHAPES                       SHAPE_PYRAMID // 3D
#define ALYA_PYR14_SHAPES                       SHAPE_PYRAMID // 3D
#define ALYA_PEN06_SHAPES                       SHAPE_PRISM // 3D
#define ALYA_PEN15_SHAPES                       SHAPE_PRISM // 3D
#define ALYA_PEN18_SHAPES                       SHAPE_PRISM // 3D
#define ALYA_HEX08_SHAPES                       SHAPE_HEXAHEDRON // 3D
#define ALYA_HEX20_SHAPES                       SHAPE_HEXAHEDRON // 3D
#define ALYA_HEX27_SHAPES                       SHAPE_HEXAHEDRON // 3D
#define ALYA_HEX64_SHAPES                       SHAPE_HEXAHEDRON // 3D
#define ALYA_SHELL_SHAPES                       SHAPE_TRIANGLE // 2D
#define ALYA_BAR3D_SHAPES                       SHAPE_LINEAR // 1D

using namespace std;

namespace alya{

    class AlyaTypeDef
    {
    public:
        static void Init();
        static map <unsigned int, unsigned int> Types2nodes;
        static map <unsigned int, string> Types2shapes;
        static map <unsigned int, unsigned int> Nodes2types[];
        static map <string, unsigned int> String2types;
    };

}

#endif // ALYATYPDEF_H
