/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#ifndef MPIOHEADERDEF_H
#define MPIOHEADERDEF_H

#define MPIOHEADER_HEADERSIZE_BYTES             256
#define MPIOHEADER_HEADERSIZE_BITS              MPIOHEADER_HEADERSIZE_BYTES*8

//Header fields

#define MPIOHEADER_MAGICNUMBER                  27093
#define MPIOHEADER_ALIGN_INT32                  0
#define MPIOHEADER_FORMAT                       "MPIAL00"
#define MPIOHEADER_VERSION                      "V000400"
#define MPIOHEADER_ALIGN_CHAR64                 "0000000"

#define MPIOHEADER_OBJ_LNODS                    "LNODS00"
#define MPIOHEADER_OBJ_LTYPE                    "LTYPE00"
#define MPIOHEADER_OBJ_LMAST                    "LMAST00"
#define MPIOHEADER_OBJ_LMATE                    "LMATE00"
#define MPIOHEADER_OBJ_LESUB                    "LESUB00"
#define MPIOHEADER_OBJ_COORD                    "COORD00"
#define MPIOHEADER_OBJ_LNODB                    "LNODB00"
#define MPIOHEADER_OBJ_LTYPB                    "LTYPB00"
#define MPIOHEADER_OBJ_LELBO                    "LELBO00"
#define MPIOHEADER_OBJ_LESET                    "LESET00"
#define MPIOHEADER_OBJ_LBSET                    "LBSET00"
#define MPIOHEADER_OBJ_LNSET                    "LNSET00"
#define MPIOHEADER_OBJ_CODBO                    "CODBO00"
#define MPIOHEADER_OBJ_CODNO                    "CODNO00"
#define MPIOHEADER_OBJ_LNOCH                    "LNOCH00"
#define MPIOHEADER_OBJ_LELCH                    "LELCH00"
#define MPIOHEADER_OBJ_LBOCH                    "LBOCH00"
#define MPIOHEADER_OBJ_XFIEL                    "XFIEL00"
#define MPIOHEADER_OBJ_MPIRA                    "MPIRA00"
#define MPIOHEADER_OBJ_LEINV                    "LEINV00"
#define MPIOHEADER_OBJ_LNINV                    "LNINV00"

#define MPIOHEADER_DIM_SCALAR                   "SCALA00"
#define MPIOHEADER_DIM_VECTOR                   "VECTO00"

#define MPIOHEADER_RES_ELEM                     "NELEM00"
#define MPIOHEADER_RES_POINT                    "NPOIN00"
#define MPIOHEADER_RES_BOUND                    "NBOUN00"
#define MPIOHEADER_RES_WHATE                    "WHATE00"

#define MPIOHEADER_TYPE_INTEGER                 "INTEG00"
#define MPIOHEADER_TYPE_REAL                    "REAL000"

#define MPIOHEADER_SIZE_4BYTES                  "4BYTE00"
#define MPIOHEADER_SIZE_8BYTES                  "8BYTE00"

#define MPIOHEADER_PAR_NO                       "SEQUE00"
#define MPIOHEADER_PAR_YES                      "PARAL00"

#define MPIOHEADER_FIL_YES                      "FILTE00"
#define MPIOHEADER_FIL_NO                       "NOFIL00"

#define MPIOHEADER_SORT_ASC                     "ASCEN00"
#define MPIOHEADER_SORT_DESC                    "DESCE00"
#define MPIOHEADER_SORT_NONE                    "NONE000"

#define MPIOHEADER_ID_YES                       "ID00000"
#define MPIOHEADER_ID_NO                        "NOID000"

#define MPIOHEADER_OPT_EMPTY                    "NONE000"

#define MPIOHEADER_OPT_NODES_PER_ELEMENT        "NPE0000"
#define MPIOHEADER_INDEX_OPT_NODES_PER_ELEMENT  0

#define MPIOHEADER_OPT_BOUND_ELEMENT            "ELEME00"
#define MPIOHEADER_INDEX_OPT_BOUND_ELEMENT      0

#define MPIOHEADER_EMPTY_CHAR8                  "0000000"

#define MPIOHEADER_STRING_SIZE                  8

#define MPIOHEADER_OPTION_SIZE                  10

//Header class parameters to be tested when reading a file

#define MPIO_TYPE_UNKNOWN                       0
#define MPIO_TYPE_INTEGER                       1
#define MPIO_TYPE_FLOAT                         2

#define MPIO_TYPESIZE_4                         4
#define MPIO_TYPESIZE_8                         8

#define MPIO_NCONO                              3
#define MPIO_MCODB                              99

#define PARTITION_DIM                           4
#define PARTITION_ELEME                         0
#define PARTITION_POINT                         1
#define PARTITION_BOUND                         2
#define PARTITION_WHATE                         3

#endif // MPIOHEADERDEF_H
