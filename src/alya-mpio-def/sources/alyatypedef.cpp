/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include <alyatypedef.h>


map <unsigned int, unsigned int> alya::AlyaTypeDef::Types2nodes;
map <unsigned int, unsigned int> alya::AlyaTypeDef::Nodes2types[4];
map <string, unsigned int> alya::AlyaTypeDef::String2types;
map <unsigned int, string> alya::AlyaTypeDef::Types2shapes;

void alya::AlyaTypeDef::Init()
{
    Types2nodes[ALYA_POINT]=ALYA_POINT_NODES;

    Types2nodes[ALYA_BAR02]=ALYA_BAR02_NODES;
    Types2nodes[ALYA_BAR03]=ALYA_BAR03_NODES;
    Types2nodes[ALYA_BAR04]=ALYA_BAR04_NODES;

    Types2nodes[ALYA_TRI03]=ALYA_TRI03_NODES;
    Types2nodes[ALYA_TRI06]=ALYA_TRI06_NODES;
    Types2nodes[ALYA_TRI10]=ALYA_TRI10_NODES;
    Types2nodes[ALYA_QUA04]=ALYA_QUA04_NODES;
    Types2nodes[ALYA_QUA08]=ALYA_QUA08_NODES;
    Types2nodes[ALYA_QUA09]=ALYA_QUA09_NODES;
    Types2nodes[ALYA_QUA16]=ALYA_QUA16_NODES;

    Types2nodes[ALYA_TET04]=ALYA_TET04_NODES;
    Types2nodes[ALYA_TET10]=ALYA_TET10_NODES;
    Types2nodes[ALYA_TET20]=ALYA_TET20_NODES;
    Types2nodes[ALYA_PYR05]=ALYA_PYR05_NODES;
    Types2nodes[ALYA_PYR14]=ALYA_PYR14_NODES;
    Types2nodes[ALYA_PEN06]=ALYA_PEN06_NODES;
    Types2nodes[ALYA_PEN15]=ALYA_PEN15_NODES;
    Types2nodes[ALYA_PEN18]=ALYA_PEN18_NODES;
    Types2nodes[ALYA_HEX08]=ALYA_HEX08_NODES;
    Types2nodes[ALYA_HEX20]=ALYA_HEX20_NODES;
    Types2nodes[ALYA_HEX27]=ALYA_HEX27_NODES;
    Types2nodes[ALYA_HEX64]=ALYA_HEX64_NODES;
    Types2nodes[ALYA_SHELL]=ALYA_SHELL_NODES;
    Types2nodes[ALYA_BAR3D]=ALYA_BAR3D_NODES;

    Nodes2types[0][ALYA_POINT_NODES]=ALYA_POINT;

    Nodes2types[1][ALYA_BAR02_NODES]=ALYA_BAR02;
    Nodes2types[1][ALYA_BAR03_NODES]=ALYA_BAR03;
    Nodes2types[1][ALYA_BAR04_NODES]=ALYA_BAR04;

    Nodes2types[2][ALYA_TRI03_NODES]=ALYA_TRI03;
    Nodes2types[2][ALYA_TRI06_NODES]=ALYA_TRI06;
    Nodes2types[2][ALYA_TRI10_NODES]=ALYA_TRI10;
    Nodes2types[2][ALYA_QUA04_NODES]=ALYA_QUA04;
    Nodes2types[2][ALYA_QUA08_NODES]=ALYA_QUA08;
    Nodes2types[2][ALYA_QUA09_NODES]=ALYA_QUA09;
    Nodes2types[2][ALYA_QUA16_NODES]=ALYA_QUA16;

    Nodes2types[3][ALYA_TET04_NODES]=ALYA_TET04;
    Nodes2types[3][ALYA_TET10_NODES]=ALYA_TET10;
    Nodes2types[3][ALYA_TET20_NODES]=ALYA_TET20;
    Nodes2types[3][ALYA_PYR05_NODES]=ALYA_PYR05;
    Nodes2types[3][ALYA_PYR14_NODES]=ALYA_PYR14;
    Nodes2types[3][ALYA_PEN06_NODES]=ALYA_PEN06;
    Nodes2types[3][ALYA_PEN15_NODES]=ALYA_PEN15;
    Nodes2types[3][ALYA_PEN18_NODES]=ALYA_PEN18;
    Nodes2types[3][ALYA_HEX08_NODES]=ALYA_HEX08;
    Nodes2types[3][ALYA_HEX20_NODES]=ALYA_HEX20;
    Nodes2types[3][ALYA_HEX27_NODES]=ALYA_HEX27;
    Nodes2types[3][ALYA_HEX64_NODES]=ALYA_HEX64;
    Nodes2types[3][ALYA_SHELL_NODES]=ALYA_SHELL;
    Nodes2types[3][ALYA_BAR3D_NODES]=ALYA_BAR3D;

    String2types[ALYA_POINT_STRING]=ALYA_POINT;

    String2types[ALYA_BAR02_STRING]=ALYA_BAR02;
    String2types[ALYA_BAR03_STRING]=ALYA_BAR03;
    String2types[ALYA_BAR04_STRING]=ALYA_BAR04;

    String2types[ALYA_TRI03_STRING]=ALYA_TRI03;
    String2types[ALYA_TRI06_STRING]=ALYA_TRI06;
    String2types[ALYA_TRI10_STRING]=ALYA_TRI10;
    String2types[ALYA_QUA04_STRING]=ALYA_QUA04;
    String2types[ALYA_QUA08_STRING]=ALYA_QUA08;
    String2types[ALYA_QUA09_STRING]=ALYA_QUA09;
    String2types[ALYA_QUA16_STRING]=ALYA_QUA16;

    String2types[ALYA_TET04_STRING]=ALYA_TET04;
    String2types[ALYA_TET10_STRING]=ALYA_TET10;
    String2types[ALYA_TET20_STRING]=ALYA_TET20;
    String2types[ALYA_PYR05_STRING]=ALYA_PYR05;
    String2types[ALYA_PYR14_STRING]=ALYA_PYR14;
    String2types[ALYA_PEN06_STRING]=ALYA_PEN06;
    String2types[ALYA_PEN15_STRING]=ALYA_PEN15;
    String2types[ALYA_PEN18_STRING]=ALYA_PEN18;
    String2types[ALYA_HEX08_STRING]=ALYA_HEX08;
    String2types[ALYA_HEX20_STRING]=ALYA_HEX20;
    String2types[ALYA_HEX27_STRING]=ALYA_HEX27;
    String2types[ALYA_HEX64_STRING]=ALYA_HEX64;
    String2types[ALYA_SHELL_STRING]=ALYA_SHELL;
    String2types[ALYA_BAR3D_STRING]=ALYA_BAR3D;

    Types2shapes[ALYA_POINT]=ALYA_POINT_SHAPES;

    Types2shapes[ALYA_BAR02]=ALYA_BAR02_SHAPES;
    Types2shapes[ALYA_BAR03]=ALYA_BAR03_SHAPES;
    Types2shapes[ALYA_BAR04]=ALYA_BAR04_SHAPES;

    Types2shapes[ALYA_TRI03]=ALYA_TRI03_SHAPES;
    Types2shapes[ALYA_TRI06]=ALYA_TRI06_SHAPES;
    Types2shapes[ALYA_TRI10]=ALYA_TRI10_SHAPES;
    Types2shapes[ALYA_QUA04]=ALYA_QUA04_SHAPES;
    Types2shapes[ALYA_QUA08]=ALYA_QUA08_SHAPES;
    Types2shapes[ALYA_QUA09]=ALYA_QUA09_SHAPES;
    Types2shapes[ALYA_QUA16]=ALYA_QUA16_SHAPES;

    Types2shapes[ALYA_TET04]=ALYA_TET04_SHAPES;
    Types2shapes[ALYA_TET10]=ALYA_TET10_SHAPES;
    Types2shapes[ALYA_TET20]=ALYA_TET20_SHAPES;
    Types2shapes[ALYA_PYR05]=ALYA_PYR05_SHAPES;
    Types2shapes[ALYA_PYR14]=ALYA_PYR14_SHAPES;
    Types2shapes[ALYA_PEN06]=ALYA_PEN06_SHAPES;
    Types2shapes[ALYA_PEN15]=ALYA_PEN15_SHAPES;
    Types2shapes[ALYA_PEN18]=ALYA_PEN18_SHAPES;
    Types2shapes[ALYA_HEX08]=ALYA_HEX08_SHAPES;
    Types2shapes[ALYA_HEX20]=ALYA_HEX20_SHAPES;
    Types2shapes[ALYA_HEX27]=ALYA_HEX27_SHAPES;
    Types2shapes[ALYA_HEX64]=ALYA_HEX64_SHAPES;
    Types2shapes[ALYA_SHELL]=ALYA_SHELL_SHAPES;
    Types2shapes[ALYA_BAR3D]=ALYA_BAR3D_SHAPES;
}
