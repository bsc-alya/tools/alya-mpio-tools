/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include "argumentmanager.h"

alya::ArgumentManager::ArgumentManager(int argc, char **argv, AlyaPtr_MPIOComm communicator):communicator(communicator)
{
    help=false;
    header=false;
    csv=false;
    valid=true;
    me=string(argv[0]);
    for (int i=1; i<argc; i++){
        string str=string(argv[i]);
        if (ARGUMENT(str, "-h", "--help")){
            help=true;
            break;
        }else{
            if (argc<2){
                valid=false;
                return;
            }else if (ARGUMENT(str, "-H", "--header-only")){
                header=true;
            }else if (ARGUMENT(str, "-c", "--csv-header")){
                csv=true;
                header=true;
            }else{
                file=str;
                i++;
            }
        }
    }
}

void alya::ArgumentManager::usage()
{
    Message::Info(communicator->getRank(), "Usage");
    Message::Info(communicator->getRank(), string(me.c_str())+" [OPTIONS] file");
    Message::Info(communicator->getRank(), "Options:");
    Message::Info(communicator->getRank(), "-h | --help         Print usage");
    Message::Info(communicator->getRank(), "-H | --header-only  Print header only");
    Message::Info(communicator->getRank(), "-c | --csv-header   Print header only (csv format)");
}

bool alya::ArgumentManager::getHelp() const
{
    return help;
}

bool alya::ArgumentManager::getHeader() const
{
    return header;
}

string alya::ArgumentManager::getFile() const
{
    return file;
}

bool alya::ArgumentManager::getCsv() const
{
    return csv;
}

bool alya::ArgumentManager::getValid() const
{
    return valid;
}
