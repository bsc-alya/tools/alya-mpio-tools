/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include "dump.h"


alya::Dump::Dump(std::shared_ptr<alya::MPIOComm> communicator, string fileName, bool headerBool, bool csvBool): communicator(communicator), fileName(fileName), headerBool(headerBool), csvBool(csvBool)
{

}

bool alya::Dump::print()
{
    initPartitions();
    if (communicator->isIOWorker()){
        header = AlyaPtr<MPIOHeader>(new MPIOHeader(communicator, fileName));
        STOP_ON_ERROR(readHeader(header))
        STOP_ON_ERROR(printHeader())
        if (headerBool){
            return true;
        }
        if (header->getMpioType()==MPIO_TYPE_INTEGER&&
                header->getIntSize()==MPIO_TYPESIZE_4){
            AlyaPtr<MPIOFile<int32_t> >file(new MPIOFile<int32_t> (communicator, header, fileName));
            STOP_ON_ERROR(readFile(file))
            file->gather();
            STOP_ON_ERROR(printFile(file))
        }else if (header->getMpioType()==MPIO_TYPE_INTEGER&&
            header->getIntSize()==MPIO_TYPESIZE_8){
            AlyaPtr<MPIOFile<int64_t> >file(new MPIOFile<int64_t> (communicator, header, fileName));
            STOP_ON_ERROR(readFile(file))
            file->gather();
            STOP_ON_ERROR(printFile(file))
        }else if (header->getMpioType()==MPIO_TYPE_FLOAT&&
                  header->getIntSize()==MPIO_TYPESIZE_8){
            AlyaPtr<MPIOFile<double> >file(new MPIOFile<double> (communicator, header, fileName));
            STOP_ON_ERROR(readFile(file))
            file->gather();
            STOP_ON_ERROR(printFile(file))
        }else{
            Message::Error(communicator->getRank(), "Type/size not recognized.");
        }
    }
    return true;
}

bool alya::Dump::readHeader(AlyaPtr<alya::MPIOHeader> header)
{
    STOP_ON_ERROR_MSG(header->open(READ),Message::Error("Can not open "+header->getFileName()+" (header)"))
    STOP_ON_ERROR_MSG(header->read(),Message::Error("Problem while reading "+header->getFileName()+" (header)"))
    STOP_ON_ERROR_MSG(header->close(),Message::Error("Can not close "+header->getFileName()+" (header)"))
    return true;
}

bool alya::Dump::initPartitions()
{
    partition=AlyaPtr<MPIOPartition>(new MPIOPartition(communicator));
    STOP_ON_ERROR(partition->computePartition())
    communicator=partition->getCommunicator();
    return true;
}

template<typename T>
bool alya::Dump::readFile(AlyaPtr<alya::MPIOFile<T> > file)
{
    //Message::Info(communicator->getRank(), "Reading "+file->getFileName()+" file", 2);
    STOP_ON_ERROR_MSG(file->open(READ),Message::Critical(communicator->getRank(), "Can not open "+file->getFileName()+" file!"))
    file->setPartition(partition);
    STOP_ON_ERROR_MSG(file->read(),Message::Critical(communicator->getRank(), "Can not read "+file->getFileName()+" file!"))
    STOP_ON_ERROR_MSG(file->close(), Message::Critical(communicator->getRank(), "Can not close "+file->getFileName()+" file!"))
    return true;
}

template<typename T>
bool alya::Dump::printFile(AlyaPtr<alya::MPIOFile<T> > file)
{
    if (communicator->isMasterOrSequential()){
        for (int i=0; i<file->getLines(); i++){
            for (int j=0; j<file->getColumns(); j++){
                cout<<format(file->getData(i,j));
                if (j<file->getColumns()-1){
                    cout<<" ";
                }
            }
            cout<<endl;
        }
    }
    return true;
}

bool alya::Dump::printHeader()
{
    if (communicator->isMasterOrSequential()){
        if (!csvBool){
            cout << "#OBJECT:     " << header->getObject() << endl;
            cout << "#DIMENSION:  " << header->getDimension() << endl;
            cout << "#RESULTS ON: " << header->getResultsOn() << endl;
            cout << "#TYPE:       " << header->getType() << endl;
            cout << "#SIZE:       " << header->getSize() << endl;
            cout << "#PARALLEL:   " << header->getParallel() << endl;
            cout << "#FILTER:     " << header->getFilter() << endl;
            cout << "#SORTING:    " << header->getSorting() << endl;
            cout << "#ID:         " << header->getId() << endl;
            cout << "#COLUMNS:    " << header->getColumns() << endl;
            cout << "#LINES:      " << header->getLines() << endl;
            cout << "#STEP:       " << header->getIttim() << endl;
            cout << "#DOMAINS:    " << header->getNsubd() << endl;
            cout << "#DIVISION:   " << header->getDivi() << endl;
            cout << "#TAG1:       " << header->getTag1() << endl;
            cout << "#TAG2:       " << header->getTag2() << endl;
            cout << "#TIME:       " << header->getTime() << endl;
        }else{
            cout << header->getObject() COMMA
                header->getDimension() COMMA
                header->getResultsOn() COMMA
                header->getType() COMMA
                header->getSize() COMMA
                header->getParallel() COMMA
                header->getFilter() COMMA
                header->getSorting() COMMA
                header->getId() COMMA
                header->getColumns() COMMA
                header->getLines() COMMA
                header->getIttim() COMMA
                header->getNsubd() COMMA
                header->getDivi() COMMA
                header->getTag1() COMMA
                header->getTag2() COMMA
                header->getTime() << endl;
        }
    }
    return true;
}

string alya::Dump::format(double value)
{
    if (!noformat){
        stringstream stream;
        stream<<setprecision(FLOAT_PRECISION*2)<<std::scientific<<value;
        string s=stream.str();
        //find e:
        unsigned long e=0;
        string rad;
        string radsign;
        string exp;
        string expsign;
        bool shift=false;
        if (s[0]=='-'){
            radsign="-";
            s=s.substr(1, s.size()-1);
        }else{
            radsign=" ";
        }
        for (unsigned long i=0; i<s.size(); i++){
            if (s[i]=='e'){
                e=i;
                break;
            }
        }
        rad=s.substr(0, e);
        if (rad[0]!='0'){
            shift=true;
            string radtmp="0."+rad.substr(0,1)+rad.substr(2,rad.size()-2);
            rad=radtmp;
        }
        rad=rad.substr(0,1+1+FLOAT_PRECISION);
        expsign=s[e+1];
        exp=s.substr(e+2, s.substr().size()-(e+2));
        if (shift){
            int eint=atoi(exp.c_str());
            if (expsign[0]=='+'){
                eint++;
            }else if (--eint==0){
                expsign='+';
            }
            exp=to_string(eint);
        }
        while (exp.size()<EXP_SIZE){
            exp.insert(0, "0");
        }
        while (exp.size()>EXP_SIZE){
            if (exp[0]=='0'){
                exp=exp.substr(1, exp.size()-1);
            }else{
                break;
            }
        }
        s=radsign+rad+"E"+expsign+exp;
        return s;
    }else{
        return to_string(value);
    }
}

string alya::Dump::format(int32_t value)
{
    return to_string(value);
}

string alya::Dump::format(int64_t value)
{
    return to_string(value);
}


