/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#ifndef DUMP_H
#define DUMP_H

#include <streampointer.h>
#include <sstream>
#include <iomanip>
#include <cfloat>
#include <limits>
#include <cmath>
#include <alya-mpio-dump_config.h>
#include <mpiocomm.h>
#include <mpioheader.h>
#include <mpiofile.h>
#include <pointer.h>
#include <message.h>
#include <mpiopartition.h>
#include <alyastring.h>
#include <alyareturn.h>

#define COMMA << ", " <<

using namespace std;

namespace alya {

    class Dump
    {
    public:
        Dump(AlyaPtr_MPIOComm communicator, string fileName, bool headerBool, bool csvBool);
        bool print();
    private:
        bool readHeader(AlyaPtr<MPIOHeader>);
        template<typename T>
        bool readFile(AlyaPtr<MPIOFile<T> > file);
        template<typename T>
        bool printFile(AlyaPtr<MPIOFile<T> > file);
        bool printHeader();
        bool initPartitions();
        string format(double value);
        string format(int32_t value);
        string format(int64_t value);
        AlyaPtr_MPIOComm communicator;
        string fileName;
        AlyaPtr<alya::MPIOHeader> header;
        AlyaPtr<MPIOPartition> partition;
        bool headerBool;
        bool csvBool;
        bool noformat=false;
    };

}



#endif // DUMP_H
