/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include "argumentmanager.h"

alya::ArgumentManager::ArgumentManager(int argc, char **argv, AlyaPtr_MPIOComm communicator):communicator(communicator)
{
    help=false;
    fields=false;
    valid=true;
    quiet=false;
    me=string(argv[0]);
    if (argc<2){
        valid=false;
        return;
    }
    for (int i=1; i<argc; i++){
        string str=string(argv[i]);
        if (ARGUMENT(str, "-h", "--help")){
            help=true;
            return;
        }else if (ARGUMENT(str, "-f", "--fields")){
            fields=true;
            return;
        }else if (ARGUMENT(str, "-q", "--quiet")){
            quiet=true;
        }else{
            if (argc<3){
                valid=false;
                return;
            }
            string str2=string(argv[i+1]);
            file1=str;
            file2=str2;
            i++;
        }
    }
}

void alya::ArgumentManager::usage()
{
    Message::Info(communicator->getRank(), "Usage");
    Message::Info(communicator->getRank(), string(me.c_str())+" [OPTIONS] file1 file2");
    Message::Info(communicator->getRank(), "Options:");
    Message::Info(communicator->getRank(), "-f | --fields   Print fields");
    Message::Info(communicator->getRank(), "-q | --quiet    Do no print anything if files are similar");
    Message::Info(communicator->getRank(), "-h | --help     Print usage");
}

bool alya::ArgumentManager::getHelp() const
{
    return help;
}

string alya::ArgumentManager::getFile2() const
{
    return file2;
}

string alya::ArgumentManager::getFile1() const
{
    return file1;
}

bool alya::ArgumentManager::getFields() const
{
    return fields;
}

bool alya::ArgumentManager::getQuiet() const
{
    return quiet;
}

bool alya::ArgumentManager::getValid() const
{
    return valid;
}
