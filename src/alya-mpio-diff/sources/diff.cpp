/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include "diff.h"


alya::Diff::Diff(std::shared_ptr<alya::MPIOComm> communicator, string fileName1, string fileName2): communicator(communicator), fileName1(fileName1), fileName2(fileName2)
{

}

bool alya::Diff::compare()
{
    initPartitions();
    if (communicator->isIOWorker()){
        STOP_ON_ERROR(compareHeaders())
        initStats();
        if (header1->getMpioType()==MPIO_TYPE_INTEGER&&
            header1->getIntSize()==MPIO_TYPESIZE_4){
            AlyaPtr<MPIOFile<int32_t> >file1(new MPIOFile<int32_t> (communicator, header1, fileName1));
            STOP_ON_ERROR(readFile(file1))
            AlyaPtr<MPIOFile<int32_t> >file2(new MPIOFile<int32_t> (communicator, header2, fileName2));
            STOP_ON_ERROR(readFile(file2))
            STOP_ON_ERROR(compare(file1, file2))
        }else if (header1->getMpioType()==MPIO_TYPE_INTEGER&&
            header1->getIntSize()==MPIO_TYPESIZE_8){
            AlyaPtr<MPIOFile<int64_t> >file1(new MPIOFile<int64_t> (communicator, header1, fileName1));
            STOP_ON_ERROR(readFile(file1))
            AlyaPtr<MPIOFile<int64_t> >file2(new MPIOFile<int64_t> (communicator, header2, fileName2));
            STOP_ON_ERROR(readFile(file2))
            STOP_ON_ERROR(compare(file1, file2))
        }else if (header1->getMpioType()==MPIO_TYPE_FLOAT&&
                  header1->getIntSize()==MPIO_TYPESIZE_8){
            AlyaPtr<MPIOFile<double> >file1(new MPIOFile<double> (communicator, header1, fileName1));
            STOP_ON_ERROR(readFile(file1))
            AlyaPtr<MPIOFile<double> >file2(new MPIOFile<double> (communicator, header2, fileName2));
            STOP_ON_ERROR(readFile(file2))
            STOP_ON_ERROR(compare(file1, file2))
        }else{
            Message::Error(communicator->getRank(), "Type/size not recognized.");
        }
    }
    return true;
}

bool alya::Diff::compare(std::shared_ptr<alya::MPIOFile<double> > file1, std::shared_ptr<alya::MPIOFile<double> > file2)
{
    for (int64_t i=0; i<file1->getLines(); i++){
        for (int64_t j=0; j<file1->getColumns(); j++){
            double value1=static_cast<double> (file1->getData(i, j));
            double value2=static_cast<double> (file2->getData(i, j));
            computeStats(value1, value2);
        }
    }
    computeStats();
    return true;
}

bool alya::Diff::compare(std::shared_ptr<alya::MPIOFile<int32_t> > file1, std::shared_ptr<alya::MPIOFile<int32_t> > file2)
{
    for (int64_t i=0; i<file1->getLines(); i++){
        for (int64_t j=0; j<file1->getColumns(); j++){
            double value1=static_cast<double>(file1->getData(i, j));
            double value2=static_cast<double>(file2->getData(i, j));
            computeStats(value1, value2);
        }
    }
    computeStats();
    return true;
}

bool alya::Diff::compare(std::shared_ptr<alya::MPIOFile<int64_t> > file1, std::shared_ptr<alya::MPIOFile<int64_t> > file2)
{
    for (int64_t i=0; i<file1->getLines(); i++){
        for (int64_t j=0; j<file1->getColumns(); j++){
            double value1=static_cast<double>(file1->getData(i, j));
            double value2=static_cast<double>(file2->getData(i, j));
            computeStats(value1, value2);
        }
    }
    computeStats();
    return true;
}

bool alya::Diff::compareHeaders()
{
    header1 = AlyaPtr<MPIOHeader>(new MPIOHeader(communicator, fileName1));
    header2 = AlyaPtr<MPIOHeader>(new MPIOHeader(communicator, fileName2));
    STOP_ON_ERROR(readHeader(header1))
    STOP_ON_ERROR(readHeader(header2))
    STOP_ON_ERROR_MSG((header1->getObject().compare(header2->getObject())==0), Message::Error("Objects are different"))
    STOP_ON_ERROR_MSG((header1->getDimension().compare(header2->getDimension())==0), Message::Error("Dimensions are different"))
    STOP_ON_ERROR_MSG((header1->getResultsOn().compare(header2->getResultsOn())==0), Message::Error("Results are different"))
    STOP_ON_ERROR_MSG((header1->getType().compare(header2->getType())==0), Message::Error("Types are different"))
    STOP_ON_ERROR_MSG((header1->getSize().compare(header2->getSize())==0), Message::Error("Sizes are different"))
    STOP_ON_ERROR_MSG((header1->getParallel().compare(header2->getParallel())==0), Message::Error("Parallels are different"))
    STOP_ON_ERROR_MSG((header1->getFilter().compare(header2->getFilter())==0), Message::Error("Filters are different"))
    STOP_ON_ERROR_MSG((header1->getSorting().compare(header2->getSorting())==0), Message::Error("Sorting are different"))
    STOP_ON_ERROR_MSG((header1->getId().compare(header2->getId())==0), Message::Error("Id are different"))
    STOP_ON_ERROR_MSG((header1->getColumns()==header2->getColumns()), Message::Error("Columns are different"))
    STOP_ON_ERROR_MSG((header1->getLines()==header2->getLines()), Message::Error("Lines are different"))
    //STOP_ON_ERROR_MSG(header1->getIttim()!=header2->getIttim(), Message::Error("Ittim are different"))
    STOP_ON_ERROR_MSG((header1->getNsubd()==header2->getNsubd()), Message::Error("Subdomains are different"))
    //STOP_ON_ERROR_MSG(header1->getTime()!=header2->getTime(), Message::Error("Time are different"))
    return true;
}

bool alya::Diff::readHeader(AlyaPtr<alya::MPIOHeader> header)
{
    STOP_ON_ERROR_MSG(header->open(READ),Message::Error("Can not open "+header->getFileName()+" (header)"))
    STOP_ON_ERROR_MSG(header->read(),Message::Error("Can not read "+header->getFileName()+" (header)"))
    STOP_ON_ERROR_MSG(header->close(),Message::Error("Can not close "+header->getFileName()+" (header)"))
    return true;
}

bool alya::Diff::initPartitions()
{
    partition=AlyaPtr<MPIOPartition>(new MPIOPartition(communicator));
    STOP_ON_ERROR(partition->computePartition())
    communicator=partition->getCommunicator();
    return true;
}

template<typename T>
bool alya::Diff::readFile(AlyaPtr<alya::MPIOFile<T> > file)
{
    Message::Info(communicator->getRank(), "Reading "+file->getFileName()+" file", 2);
        STOP_ON_ERROR_MSG(file->open(READ),Message::Critical(communicator->getRank(), "Can not open "+file->getFileName()+" file!"))
        file->setPartition(partition);
        STOP_ON_ERROR_MSG(file->read(),Message::Critical(communicator->getRank(), "Can not read "+file->getFileName()+" file!"))
        STOP_ON_ERROR_MSG(file->close(), Message::Critical(communicator->getRank(), "Can not close "+file->getFileName()+" file!"))
    return true;
}

void alya::Diff::computeStats(double value1, double value2)
{
    double diff;
    double ratioDiff1;
    double ratioDiff2;
    diff=abs(value1-value2);
    if (diff!=0.0){
        diffNumber++;
        diffAvg+=diff;
        ratioDiff1=(value1!=0.0)?abs(diff/value1):0.0;
        ratioDiff2=(value2!=0.0)?abs(diff/value2):0.0;
        ratioDiff1Avg+=ratioDiff1;
        ratioDiff2Avg+=ratioDiff2;
        diffMax=max(diff, diffMax);
        diffMin=min(diff, diffMin);
        ratioDiff1Max=max(ratioDiff1, ratioDiff1Max);
        ratioDiff2Max=max(ratioDiff2, ratioDiff2Max);
        ratioDiff1Min=min(ratioDiff1, ratioDiff1Min);
        ratioDiff2Min=min(ratioDiff2, ratioDiff2Min);
    }
}

void alya::Diff::computeStats()
{
    communicator->Sum(&diffNumber);
    communicator->Sum(&diffAvg);
    communicator->Sum(&ratioDiff1Avg);
    communicator->Sum(&ratioDiff2Avg);
    if (communicator->isMasterOrSequential()){
        diffAvg/=diffNumber;
        ratioDiff1Avg/=diffNumber;
        ratioDiff2Avg/=diffNumber;
    }
    communicator->Max(&diffMax);
    communicator->Min(&diffMin);
    communicator->Max(&ratioDiff1Max);
    communicator->Max(&ratioDiff2Max);
    communicator->Min(&ratioDiff1Min);
    communicator->Min(&ratioDiff2Min);
    if (diffNumber==0){
        diffAvg=0;
        ratioDiff1Avg=0;
        ratioDiff2Avg=0;
        diffMax=0;
        diffMin=0;
        ratioDiff1Max=0;
        ratioDiff2Max=0;
        ratioDiff1Min=0;
        ratioDiff2Min=0;
    }
}

void alya::Diff::initStats()
{
    diffNumber=0;
    diffAvg=0;
    ratioDiff1Avg=0;
    ratioDiff2Avg=0;
    diffMax=0;
    diffMin=DBL_MAX;
    ratioDiff1Max=0;
    ratioDiff2Max=0;
    ratioDiff1Min=DBL_MAX;
    ratioDiff2Min=DBL_MAX;
}

void alya::Diff::print(bool quiet)
{
    if (communicator->isMasterOrSequential()){
        long elements=header1->getColumns()*header1->getLines();
        if (!(quiet&&diffNumber==0)){
            cout<<fileName1<<", "<<fileName2<<", "<<elements<<", "<<diffNumber<<", "<<static_cast<double>(diffNumber)/static_cast<double>(elements)<<", "<<diffAvg<<", "<<diffMax<<", "<<diffMin<<", "<<ratioDiff1Avg<<", "<<ratioDiff1Max<<", "<<ratioDiff1Min<<", "<<ratioDiff2Avg<<", "<<ratioDiff2Max<<", "<<ratioDiff2Min<<endl;
        }
    }
}
