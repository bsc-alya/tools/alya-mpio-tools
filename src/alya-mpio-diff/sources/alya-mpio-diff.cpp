/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include <iostream>
#include <argumentmanager.h>
#include <message.h>
#include <pointer.h>
#include <diff.h>
#include <mpiocomm.h>
#include <mpiowrapper.h>

using namespace std;
using namespace alya;

int main(int argc, char* argv[])
{
    MPIO::Init(argc, argv);
    AlyaPtr_MPIOComm communicator(new MPIOComm());
    AlyaPtr<ArgumentManager> argumentManager(new ArgumentManager(argc, argv, communicator));
    if (argumentManager->getHelp()){
        argumentManager->usage();
        MPIO::Finalize();
        return RETURN_OK;

    }
    if (argumentManager->getFields()){
        if (communicator->isMasterOrSequential()){
            cout<<"File1, File2, Size, Diff(number), Diff(ratio), Diff(avg), Diff(max), Diff(min), Diff/A(avg), Diff/A(max), Diff/A(min), Diff/B(avg), Diff/B(max), Diff/B(min)"<<endl;
        }
        MPIO::Finalize();
        return RETURN_OK;

    }
    if (!argumentManager->getValid()){
        Message::Critical(communicator->getRank(), "Invalid arguments");
        argumentManager->usage();
        MPIO::Abort();
        return RETURN_ERR;
    }
    AlyaPtr<Diff> diff(new Diff(communicator, argumentManager->getFile1(), argumentManager->getFile2()));
    if (!diff->compare()){
        MPIO::Abort();
        return RETURN_ERR;
    }
    diff->print(argumentManager->getQuiet());
    MPIO::Finalize();
    return RETURN_OK;
}

