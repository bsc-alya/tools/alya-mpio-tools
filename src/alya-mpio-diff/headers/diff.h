/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#ifndef DIFF_H
#define DIFF_H

#include <streampointer.h>
#include <sstream>
#include <iomanip>
#include <cfloat>
#include <limits>
#include <cmath>
#include <alya-mpio-diff_config.h>
#include <mpiocomm.h>
#include <mpioheader.h>
#include <mpiofile.h>
#include <pointer.h>
#include <message.h>
#include <alyastring.h>
#include <alyareturn.h>

using namespace std;

namespace alya {

    class Diff
    {
    public:
        Diff(AlyaPtr_MPIOComm communicator, string fileName1, string fileName2);
        bool compare();
        void print(bool quiet);
    private:
        bool compare(AlyaPtr<MPIOFile<double> >, AlyaPtr<MPIOFile<double> >);
        bool compare(AlyaPtr<MPIOFile<int32_t> >, AlyaPtr<MPIOFile<int32_t> >);
        bool compare(AlyaPtr<MPIOFile<int64_t> >, AlyaPtr<MPIOFile<int64_t> >);
        bool compareHeaders();
        bool readHeader(AlyaPtr<MPIOHeader>);
        bool initPartitions();
        template <typename T> bool readFile(AlyaPtr<MPIOFile<T> > file);
        void computeStats(double value1, double value2);
        void computeStats();
        void initStats();
        AlyaPtr_MPIOComm communicator;
        string fileName1;
        string fileName2;
        AlyaPtr<alya::MPIOHeader> header1;
        AlyaPtr<alya::MPIOHeader> header2;
        AlyaPtr<MPIOPartition> partition;
        int64_t diffNumber;
        double diffAvg;
        double ratioDiff1Avg;
        double ratioDiff2Avg;
        double diffMax;
        double diffMin;
        double ratioDiff1Max;
        double ratioDiff2Max;
        double ratioDiff1Min;
        double ratioDiff2Min;
    };

}



#endif // DIFF_H
