/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#ifndef TXTWRITER_H
#define TXTWRITER_H

#include <postreader.h>
#include <streampointer.h>
#include <argumentmanager.h>
#include <sstream>
#include <iomanip>
#include <alyareturn.h>

#include <alya-mpio2txt_config.h>

using namespace std;

#define PRINT_LINE(n)   if(index){\
                            *output<<n+1<<"\t";\
                        }

namespace alya {

    class TxtWriter
    {
    public:
        TxtWriter(AlyaPtr<alya::PostReader> reader, AlyaPtr_MPIOComm communicator, AlyaPtr<ArgumentManager> argumentManager);
        bool write();
    private:
        bool convertFiles();
        bool convertFile(AlyaPtr<MPIOFile<double> >);
        bool convertFile(AlyaPtr<MPIOFile<int32_t> >);
        bool convertFile(AlyaPtr<MPIOFile<int64_t> >);
        bool mergeOrGather(AlyaPtr<MPIOFile<double> >);
        bool mergeOrGather(AlyaPtr<MPIOFile<int32_t> >);
        bool mergeOrGather(AlyaPtr<MPIOFile<int64_t> >);
        bool convertMesh();
        bool convertMesh4();
        bool convertMesh8();
        bool convertMeshExtended();
        bool convertPost();
        bool discardPost(AlyaPtr<MPIOHeader>);
        AlyaPtr<MPIOFile<int32_t> > getInv4(string resultsOn);
        AlyaPtr<MPIOFile<int64_t> > getInv8(string resultsOn);
        AlyaPtr_ofstream openFile(string mpioName);
        bool closeFile(AlyaPtr_ofstream);
        string format(double value);
        string format(int32_t value);
        string format(int64_t value);
        AlyaPtr<PostReader> reader;
        AlyaPtr<ArgumentManager> argumentManager;
        AlyaPtr_MPIOComm communicator;
        vector<int> iterations;
        int minIteration, maxIteration;
        string caseName;
        bool noformat, fformat, merge, index, exportMesh;
    };



}



#endif // TXTWRITER_H
