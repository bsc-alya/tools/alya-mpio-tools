/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#ifndef ARGUMENTMANAGER_H
#define ARGUMENTMANAGER_H

#include <string>
#include <iostream>
#include <string.h>
#include <stdio.h>
#include <message.h>
#include <mpiocomm.h>
#include <alya-mpio2txt_config.h>

#define ARGUMENT(n, flag, wholeflag) ((n.compare(flag)==0)||(n.compare(wholeflag)==0))

using namespace std;

namespace alya{

    class ArgumentManager
    {
    public:
        ArgumentManager(int argc, char** argv, AlyaPtr_MPIOComm communicator);
        void usage();
        bool getValid() const;
        bool getNoformat() const;
        bool getHelp() const;
        string getCaseName() const;

        vector<int> getIterations() const;

        int getRangeMin() const;

        int getRangeMax() const;

        bool getMerge() const;

        bool getIndex() const;

        bool getFformat() const;

        bool getExportMesh() const;

    private:
        bool valid;
        bool help;
        bool noformat;
        bool fformat;
        bool merge;
        bool index;
        bool exportMesh;
        string me;
        string caseName;
        vector<int> iterations;
        int rangeMin;
        int rangeMax;
        AlyaPtr_MPIOComm communicator;
    };

}

#endif // ARGUMENTMANAGER_H
