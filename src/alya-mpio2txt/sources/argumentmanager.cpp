/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include "argumentmanager.h"

alya::ArgumentManager::ArgumentManager(int argc, char **argv, AlyaPtr_MPIOComm communicator):communicator(communicator)
{
    help=false;
    noformat=false;
    fformat=false;
    merge=false;
    index=false;
    rangeMin=0;
    rangeMax=0;
    exportMesh=true;
    me=string(argv[0]);
    if (argc<2){
        valid=false;
        return;
    }else{
        for (int i=1; i<argc; i++){
            string str=string(argv[i]);
            if (ARGUMENT(str, "-h", "--help")){
                help=true;
                return;
            }else if (ARGUMENT(str, "-nf", "--no-format")){
                noformat=true;
                Message::Info(communicator->getRank(), "Disabling formatting", 2);
            }else if (ARGUMENT(str, "-ff", "--format-float")){
                fformat=true;
                noformat=true;
                Message::Info(communicator->getRank(), "Format only floats", 2);
            }else if (ARGUMENT(str, "-m", "--merge")){
                merge=true;
                Message::Info(communicator->getRank(), "Merging subdomains", 2);
            }else if (ARGUMENT(str, "-id", "--index")){
                index=true;
                Message::Info(communicator->getRank(), "Printing line indices", 2);
            }else if (ARGUMENT(str, "-i", "--iteration")){
                iterations.push_back(atoi(argv[++i]));
                Message::Info(communicator->getRank(), "Adding iteration: "+to_string(iterations[iterations.size()-1]), 2);
            }else if (ARGUMENT(str, "-r", "--range")){
                rangeMin=atoi(argv[++i]);
                rangeMax=atoi(argv[++i]);
                Message::Info(communicator->getRank(), "Exporting fields only", 2);
            }else if (ARGUMENT(str, "-f", "--field-only")){
                exportMesh=false;
            }else if (ARGUMENT(str, "-d", "--dani")){
                exportMesh=false;
                index=true;
                merge=true;
                fformat=true;
                noformat=true;
                Message::Info(communicator->getRank(), "Hola Dani!", 2);
            }else{
                caseName=str;
                Message::Info(communicator->getRank(), "Case: "+caseName, 2);
                valid=true;
            }
        }
    }
    if (caseName.compare("")==0){
        valid=false;
        Message::Error(communicator->getRank(), "You did not specify any case!");
        return;
    }
    if (rangeMax<rangeMin){
        valid=false;
        Message::Error(communicator->getRank(), "Range min is greater than range max");
        return;
    }
}

void alya::ArgumentManager::usage()
{
    Message::Info(communicator->getRank(), "Usage");
    Message::Info(communicator->getRank(), string(me.c_str())+" [OPTIONS] CASE NAME");
    Message::Info(communicator->getRank(), "Options:");
    Message::Info(communicator->getRank(), "-h  | --help              Print usage");
    Message::Info(communicator->getRank(), "-m  | --merge             Merge subdomains");
    Message::Info(communicator->getRank(), "-i  | --iteration [int]   Specify the step to take into account (can be repeated several times)");
    Message::Info(communicator->getRank(), "-r  | --range [int] [int] Specify the step interval to take into account");
    Message::Info(communicator->getRank(), "-id | --index             Print line indices");
    Message::Info(communicator->getRank(), "-nf | --no-format         Print values without formatting");
    Message::Info(communicator->getRank(), "-ff | --format-float      Format float values only");
    Message::Info(communicator->getRank(), "-f  | --fields-only       Export fields only (do not export the mesh)");
    Message::Info(communicator->getRank(), "-d  | --dani              -m -id -ff -f");
}

string alya::ArgumentManager::getCaseName() const
{
    return caseName;
}

int alya::ArgumentManager::getRangeMax() const
{
    return rangeMax;
}

bool alya::ArgumentManager::getMerge() const
{
    return merge;
}

bool alya::ArgumentManager::getIndex() const
{
    return index;
}

bool alya::ArgumentManager::getFformat() const
{
    return fformat;
}

bool alya::ArgumentManager::getExportMesh() const
{
    return exportMesh;
}

int alya::ArgumentManager::getRangeMin() const
{
    return rangeMin;
}

vector<int> alya::ArgumentManager::getIterations() const
{
    return iterations;
}

bool alya::ArgumentManager::getNoformat() const
{
    return noformat;
}

bool alya::ArgumentManager::getHelp() const
{
    return help;
}

bool alya::ArgumentManager::getValid() const
{
    return valid;
}
