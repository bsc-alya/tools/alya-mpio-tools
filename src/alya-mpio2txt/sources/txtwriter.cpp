/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include "txtwriter.h"


alya::TxtWriter::TxtWriter(AlyaPtr<alya::PostReader> reader, AlyaPtr_MPIOComm communicator, AlyaPtr<ArgumentManager> argumentManager):
reader(reader), argumentManager(argumentManager), communicator(communicator)
{
    caseName=argumentManager->getCaseName();
    noformat=argumentManager->getNoformat();
    fformat=argumentManager->getFformat();
    index=argumentManager->getIndex();
    iterations=argumentManager->getIterations();
    minIteration=argumentManager->getRangeMin();
    maxIteration=argumentManager->getRangeMax();
    merge=argumentManager->getMerge();
    exportMesh=argumentManager->getExportMesh();
}

bool alya::TxtWriter::write()
{
    return convertFiles();
}

bool alya::TxtWriter::convertFiles()
{
    STOP_ON_ERROR(convertMesh())
    if (exportMesh){
        STOP_ON_ERROR(convertMeshExtended())
    }
    return convertPost();
}

bool alya::TxtWriter::convertFile(AlyaPtr<MPIOFile<double> > file)
{
    if (communicator->isMasterOrSequential()){
        Message::Info(communicator->getRank(), "Writing "+AlyaString::Format5(file->getHeader()->getObject()), 2);
        AlyaPtr_ofstream output=openFile(file->getFileName());
        if (!output->is_open()){
            return false;
        }
        for (int64_t i=0; i<file->getLines(); i++){
            PRINT_LINE(i)
            for (int64_t j=0; j<file->getColumns(); j++){
                *output<<format(file->getData(i,j));
                if (j<file->getColumns()-1){
                    *output<<" ";
                }
            }
            *output<<endl;
        }
        closeFile(output);
    }
    return true;
}

bool alya::TxtWriter::convertFile(AlyaPtr<MPIOFile<int32_t> > file)
{
    if (communicator->isMasterOrSequential()){
        Message::Info(communicator->getRank(), "Writing "+AlyaString::Format5(file->getHeader()->getObject()), 2);
        AlyaPtr_ofstream output=openFile(file->getFileName());
        if (!output->is_open()){
            return false;
        }
        for (int64_t i=0; i<file->getLines(); i++){
            PRINT_LINE(i)
            for (int64_t j=0; j<file->getColumns(); j++){
                *output<<format(file->getData(i,j));
                if (j<file->getColumns()-1){
                    *output<<" ";
                }
            }
            *output<<endl;
        }
        closeFile(output);
    }
    return true;
}

bool alya::TxtWriter::convertFile(AlyaPtr<MPIOFile<int64_t> > file)
{
    if (communicator->isMasterOrSequential()){
        Message::Info(communicator->getRank(), "Writing "+AlyaString::Format5(file->getHeader()->getObject()), 2);
        AlyaPtr_ofstream output=openFile(file->getFileName());
        if (!output->is_open()){
            return false;
        }
        for (int64_t i=0; i<file->getLines(); i++){
            PRINT_LINE(i)
            for (int64_t j=0; j<file->getColumns(); j++){
                *output<<format(file->getData(i,j));
                if (j<file->getColumns()-1){
                    *output<<" ";
                }
            }
            *output<<endl;
        }
        closeFile(output);
    }
    return true;
}

bool alya::TxtWriter::mergeOrGather(AlyaPtr<MPIOFile<double> > file)
{
    if (merge){
        if (reader->getIntSize()==4){
            AlyaPtr<MPIOFile<int32_t> > inv=getInv4(file->getHeader()->getResultsOn());
            if (inv!=nullptr){
                file->merge(inv);
            }else{
                file->gather();
            }
        }else if (reader->getIntSize()==8){
            AlyaPtr<MPIOFile<int64_t> > inv=getInv8(file->getHeader()->getResultsOn());
            if (inv!=nullptr){
                file->merge(inv);
            }else{
                file->gather();
            }
        }
    }
    else{
        file->gather();
    }
    return true;
}

bool alya::TxtWriter::mergeOrGather(AlyaPtr<MPIOFile<int32_t> > file)
{
    if (merge){
        AlyaPtr<MPIOFile<int32_t> > inv=getInv4(file->getHeader()->getResultsOn());
        if (inv!=nullptr){
            file->merge(inv);
        }else{
            file->gather();
        }
    }
    else{
        file->gather();
    }
    return true;
}

bool alya::TxtWriter::mergeOrGather(AlyaPtr<MPIOFile<int64_t> > file)
{
    if (merge){
        AlyaPtr<MPIOFile<int64_t> > inv=getInv8(file->getHeader()->getResultsOn());
        if (inv!=nullptr){
            file->merge(inv);
        }else{
            file->gather();
        }
    }
    else{
        file->gather();
    }
    return true;
}

bool alya::TxtWriter::convertMesh()
{
    Message::Info(communicator->getRank(), "Processing mesh files", 1);
    STOP_ON_ERROR(reader->readInv());
    if (reader->getIntSize()==4){
        return convertMesh4();
    }else if (reader->getIntSize()==8){
        return convertMesh8();
    }
    return true;
}

bool alya::TxtWriter::convertMesh4()
{
    reader->getLninv4()->gather();
    if (exportMesh){
        convertFile(reader->getLninv4());
    }
    reader->getLeinv4()->gather();
    if (exportMesh){
        convertFile(reader->getLeinv4());
    }
    if (!reader->getNoBoundaries()){
        reader->getLbinv4()->gather();
        if (exportMesh){
            convertFile(reader->getLbinv4());
        }
    }
    if (exportMesh){
        STOP_ON_ERROR(reader->readCoord())
        mergeOrGather(reader->getCoord());
        convertFile(reader->getCoord());
        STOP_ON_ERROR(reader->readLtype())
        mergeOrGather(reader->getLtype4());
        convertFile(reader->getLtype4());
        STOP_ON_ERROR(reader->readLnods())
        if (merge){
            reader->getLnods4()->merge(reader->getLeinv4(), reader->getLninv4());
        }else{
            reader->getLnods4()->gather();
        }
        convertFile(reader->getLnods4());
        STOP_ON_ERROR(reader->readLelbo())
        if (reader->isLelbo()){
            if (merge){
                reader->getLelbo4()->merge(reader->getLbinv4(), reader->getLeinv4());
            }else{
                reader->getLelbo4()->gather();
            }
        }
        if (!reader->getNoBoundaries()){
            STOP_ON_ERROR(reader->readLnodb())
        }
        if (!reader->getNoBoundaries()){
            if (merge){
                reader->getLnodb4()->merge(reader->getLbinv4(), reader->getLninv4());
            }else{
                reader->getLnodb4()->gather();
            }
            convertFile(reader->getLnodb4());
            STOP_ON_ERROR(reader->readLtypb())
            if (reader->getLtypb()){
                mergeOrGather(reader->getLtypb4());
                convertFile(reader->getLtypb4());
            }
        }
    }
    return true;
}

bool alya::TxtWriter::convertMesh8()
{
    reader->getLninv8()->gather();
    if (exportMesh){
        convertFile(reader->getLninv8());
    }
    reader->getLeinv8()->gather();
    if (exportMesh){
        convertFile(reader->getLeinv8());
    }
    if (!reader->getNoBoundaries()){
        reader->getLbinv8()->gather();
        if (exportMesh){
            convertFile(reader->getLbinv8());
        }
    }
    if (exportMesh){
        STOP_ON_ERROR(reader->readCoord())
        mergeOrGather(reader->getCoord());
        convertFile(reader->getCoord());
        STOP_ON_ERROR(reader->readLtype())
        mergeOrGather(reader->getLtype8());
        convertFile(reader->getLtype8());
        STOP_ON_ERROR(reader->readLnods())
        if (merge){
            reader->getLnods8()->merge(reader->getLeinv8(), reader->getLninv8());
        }else{
            reader->getLnods8()->gather();
        }
        convertFile(reader->getLnods8());
        STOP_ON_ERROR(reader->readLelbo())
        if (reader->isLelbo()){
            if (reader->isLelbo()){
                if (merge){
                    reader->getLelbo8()->merge(reader->getLbinv8(), reader->getLeinv8());
                }else{
                    reader->getLelbo8()->gather();
                }
            }
        }
        if (!reader->getNoBoundaries()){
            STOP_ON_ERROR(reader->readLnodb())
        }
        if (!reader->getNoBoundaries()){
            if (merge){
                reader->getLnodb8()->merge(reader->getLbinv8(), reader->getLninv8());
            }else{
                reader->getLnodb8()->gather();
            }
            convertFile(reader->getLnodb8());
            STOP_ON_ERROR(reader->readLtypb())
            if (reader->getLtypb()){
                mergeOrGather(reader->getLtypb8());
                convertFile(reader->getLtypb8());
            }
        }
    }
    return true;
}

bool alya::TxtWriter::convertMeshExtended()
{
    reader->readPostMesh();
    Message::Info(communicator->getRank(), "Processing additional mesh files", 1);
    for (auto pmesh :reader->getPostMeshFloat8()){
        STOP_ON_ERROR(reader->readFile(pmesh))
        mergeOrGather(pmesh);
        convertFile(pmesh);
    }
    for (auto pmesh :reader->getPostMeshInteger4()){
        STOP_ON_ERROR(reader->readFile(pmesh))
        mergeOrGather(pmesh);
        convertFile(pmesh);
    }
    for (auto pmesh :reader->getPostMeshInteger8()){
        STOP_ON_ERROR(reader->readFile(pmesh))
        mergeOrGather(pmesh);
        convertFile(pmesh);
    }
    return true;
}

bool alya::TxtWriter::convertPost()
{
    reader->findSteps();
    if (communicator->isIOWorker()){
        for (const auto &time:reader->getTime(iterations, minIteration, maxIteration)){
            int ittim=time.first;
            Message::Info(communicator->getRank(), "Processing post processed files, step: "+to_string(ittim), 1);
            if (!reader->readPostsByIteration(ittim)){
                return false;
            }
            for (auto rst :reader->getPostFloat8()){
                if (discardPost(rst->getHeader())){
                    continue;
                }
                STOP_ON_ERROR(reader->readFile(rst))
                mergeOrGather(rst);
                convertFile(rst);
            }
            for (auto rst :reader->getPostInteger4()){
                if (discardPost(rst->getHeader())){
                    continue;
                }
                STOP_ON_ERROR(reader->readFile(rst))
                mergeOrGather(rst);
                convertFile(rst);
            }
            for (auto rst :reader->getPostInteger8()){
                if (discardPost(rst->getHeader())){
                    continue;
                }
                STOP_ON_ERROR(reader->readFile(rst))
                mergeOrGather(rst);
                convertFile(rst);
            }
        }
    }
    return true;
}

bool alya::TxtWriter::discardPost(std::shared_ptr<alya::MPIOHeader> header)
{
    if (header->getResultsOn().compare(MPIOHEADER_RES_BOUND)==0&&reader->getNoBoundaries()){
        return true;
    }
    if (header->getObject().compare("XFIEL00")==0&&!exportMesh){
        return true;
    }
    return false;
}

AlyaPtr<alya::MPIOFile<int32_t> > alya::TxtWriter::getInv4(string resultsOn)
{
    if (resultsOn.compare(MPIOHEADER_RES_POINT)==0){
        return reader->getLninv4();
    }else if (resultsOn.compare(MPIOHEADER_RES_ELEM)==0){
        return reader->getLeinv4();
    }else if (resultsOn.compare(MPIOHEADER_RES_BOUND)==0&&!reader->getNoBoundaries()){
        return reader->getLbinv4();
    }else{
        return nullptr;
    }
}

AlyaPtr<alya::MPIOFile<int64_t> > alya::TxtWriter::getInv8(string resultsOn)
{
    if (resultsOn.compare(MPIOHEADER_RES_POINT)==0){
        return reader->getLninv8();
    }else if (resultsOn.compare(MPIOHEADER_RES_ELEM)==0){
        return reader->getLeinv8();
    }else if (resultsOn.compare(MPIOHEADER_RES_BOUND)==0&&!reader->getNoBoundaries()){
        return reader->getLbinv8();
    }else{
        return nullptr;
    }
}

AlyaPtr_ofstream alya::TxtWriter::openFile(string mpioName)
{
    string str=mpioName;
    string old_ext=BIN_EXT;
    string new_ext=TXT_EXT;
    str.replace(str.size()-old_ext.size(),new_ext.size(),new_ext);
    AlyaPtr_ofstream file(new ofstream());
    file->open(str);
    return file;
}

bool alya::TxtWriter::closeFile(AlyaPtr_ofstream file)
{
    file->close();
    return true;
}

string alya::TxtWriter::format(double value)
{
    if (!noformat||fformat){
        stringstream stream;
        stream<<setprecision(FLOAT_PRECISION*2)<<std::scientific<<value;
        string s=stream.str();
        //find e:
        unsigned long e=0;
        string rad;
        string radsign;
        string exp;
        string expsign;
        bool shift=false;
        if (s[0]=='-'){
            radsign="-";
            s=s.substr(1, s.size()-1);
        }else{
            radsign=" ";
        }
        for (unsigned long i=0; i<s.size(); i++){
            if (s[i]=='e'){
                e=i;
                break;
            }
        }
        rad=s.substr(0, e);
        if (rad[0]!='0'){
            shift=true;
            string radtmp="0."+rad.substr(0,1)+rad.substr(2,rad.size()-2);
            rad=radtmp;
        }
        rad=rad.substr(0,1+1+FLOAT_PRECISION);
        expsign=s[e+1];
        exp=s.substr(e+2, s.substr().size()-(e+2));
        if (shift){
            int eint=atoi(exp.c_str());
            if (expsign[0]=='+'){
                eint++;
            }else if (--eint==0){
                expsign='+';
            }
            exp=to_string(eint);
        }
        while (exp.size()<EXP_SIZE){
            exp.insert(0, "0");
        }
        while (exp.size()>EXP_SIZE){
            if (exp[0]=='0'){
                exp=exp.substr(1, exp.size()-1);
            }else{
                break;
            }
        }
        s=radsign+rad+"E"+expsign+exp;
        return s;
    }else{
        return to_string(value);
    }
}

string alya::TxtWriter::format(int32_t value)
{
    if (!noformat){
        return format(static_cast<double>(value));
    }else{
        return to_string(value);
    }
}

string alya::TxtWriter::format(int64_t value)
{
    if (!noformat){
        return format(static_cast<double>(value));
    }else{
        return to_string(value);
    }
}


