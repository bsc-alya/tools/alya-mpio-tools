/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#ifndef MPIOFILE_H
#define MPIOFILE_H

#include <streampointer.h>
#include <mpiowrapper.h>
#include <mpioheader.h>
#include <mpiocomm.h>
#include <alyafile.h>
#include <algorithm>
#include <string>
#include <mpiopartition.h>

using namespace std;

namespace alya{

    template<typename T> class MPIOFile
    {
    public:
        MPIOFile(AlyaPtr_MPIOComm, AlyaPtr_MPIOHeader, string);
        MPIOFile(AlyaPtr_MPIOComm, string);

        bool readHeader();
        bool setHeader(string object, string resultsOn, int64_t columns, int64_t lines, vector<string> options, int64_t subd=1);
        bool setHeader(string object, string resultsOn, int64_t columns, int64_t lines, int64_t ittim=0, double time=0, int64_t divi=0, int64_t tag1=-1, int64_t tag2=-1, vector<string> options=vector<string>(), int64_t subd=-1);
        bool setHeader(AlyaPtr_MPIOHeader header, int64_t lines, int64_t subd);
        bool setHeaderAsField(AlyaPtr_MPIOHeader header, int64_t lines, int64_t tag1, int64_t tag2, int64_t subd);
        bool writeHeader();
        bool open(int flag);
        bool close();
        bool read();
        bool write();
        bool initWriting(bool allocateData=true);
        vector<int64_t> getPartition() const;
        void setPartition(int64_t value);
        void setPartition(const vector<int64_t> &value);
        void setPartition(AlyaPtr<MPIOPartition> partition);
        int64_t getCount() const;
        int64_t getLines() const;
        int64_t getColumns() const;
        bool gather();
        //bool broadcast();
        vector<int64_t> getCounts() const;
        AlyaPtr<alya::MPIOHeader> getHeader() const;
        string getFileName() const;
        bool getGathered() const;
        bool getMerged() const;
        bool isVector() const;
        bool isScalar() const;
        int64_t getOffset() const;

        //Template
        AlyaPtr<T> getData() const;
        T getData(int64_t i);
        T getData(int64_t i, int64_t j);
        T getMaxValue();
        void setData(AlyaPtr<T> data);
        void setData(int64_t i, T value);
        void setData(int64_t i, int64_t j, T value);

        //Specialized
        bool write(T value);
        //bool write(T value[], int64_t size);

        //Deps
        bool merge(AlyaPtr<MPIOFile<int32_t> > inv);
        bool merge(AlyaPtr<MPIOFile<int64_t> > inv);
        bool merge(AlyaPtr<MPIOFile<int32_t> > inv1, AlyaPtr<MPIOFile<int32_t> > inv2);
        bool merge(AlyaPtr<MPIOFile<int64_t> > inv1, AlyaPtr<MPIOFile<int64_t> > inv2);


protected:

        bool setView();
        bool computeOffset();
        bool configDimensions();

        //Template
        bool initData();

        //Specialized
        bool specializedSetView();
        bool specializedFillHeader();
        bool specializedRead();
        bool specializedWrite();

#ifndef NO_MPI
        MPI_File fileMPI;
        MPI_Offset fileSizeMPI;
        MPI_Offset offsetMPI;
#endif
        AlyaPtr_MPIOComm communicator;
        AlyaPtr_MPIOHeader header;
        AlyaPtr_fstream fileSTD;
        long long fileSizeSTD;
        string fileName;
        vector<int64_t> partition;
        AlyaPtr<MPIOPartition> mpioPartition;
        vector<int64_t> counts;
        int64_t offset;
        int64_t count;
        int64_t lines;
        int64_t columns;
        bool gathered;
        bool merged;
        bool broadcasted;
        bool autoPartition;
        int flag;

        //Templatized
        AlyaPtr<T> data;

    };

    template<>
    bool MPIOFile<double>::write(double value);

    template<>
    bool MPIOFile<int32_t>::write(int32_t value);

    template<>
    bool MPIOFile<int64_t>::write(int64_t value);

   // template<>
   // bool MPIOFile<double>::write(double value[], int64_t size);

   // template<>
   // bool MPIOFile<int32_t>::write(int32_t value[], int64_t size);

   // template<>
   // bool MPIOFile<int64_t>::write(int64_t value[], int64_t size);

    template<>
    bool MPIOFile<double>::specializedSetView();

    template<>
    bool MPIOFile<int32_t>::specializedSetView();

    template<>
    bool MPIOFile<int64_t>::specializedSetView();

    template<>
    bool MPIOFile<double>::specializedFillHeader();

    template<>
    bool MPIOFile<int32_t>::specializedFillHeader();

    template<>
    bool MPIOFile<int64_t>::specializedFillHeader();

    template<>
    bool MPIOFile<double>::specializedRead();

    template<>
    bool MPIOFile<int32_t>::specializedRead();

    template<>
    bool MPIOFile<int64_t>::specializedRead();

    template<>
    bool MPIOFile<double>::specializedWrite();

    template<>
    bool MPIOFile<int32_t>::specializedWrite();

    template<>
    bool MPIOFile<int64_t>::specializedWrite();

}

#endif // MPIOFILE_H
