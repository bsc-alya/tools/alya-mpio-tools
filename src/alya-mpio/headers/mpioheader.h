/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#ifndef MPIOHEADER_H
#define MPIOHEADER_H

#include <string>
#include <vector>
#include <streampointer.h>
#include <mpiowrapper.h>
#include <message.h>
#include <mpioheaderdef.h>
#include <mpiocomm.h>
#include <alyafile.h>


using namespace std;

#define CHAR (char*)

#define AlyaPtr_MPIOHeader AlyaPtr<alya::MPIOHeader>

namespace alya{

    class MPIOHeader
    {
    public:
        MPIOHeader(AlyaPtr_MPIOComm, string);

        int64_t getMagicNumber() const;
        void setMagicNumber(const int64_t &value);

        string getAlignChar64() const;
        void setAlignChar64(const string &value);

        string getFormat() const;
        void setFormat(const string &value);

        string getVersion() const;
        void setVersion(const string &value);

        string getObject() const;
        void setObject(const string &value);

        string getDimension() const;
        void setDimension(const string &value);

        string getResultsOn() const;
        void setResultsOn(const string &value);

        string getType() const;
        void setType(const string &value);

        string getSize() const;
        void setSize(const string &value);

        string getParallel() const;
        void setParallel(const string &value);

        string getFilter() const;
        void setFilter(const string &value);

        string getSorting() const;
        void setSorting(const string &value);

        string getId() const;
        void setId(const string &value);

        int64_t getColumns() const;
        void setColumns(const int64_t &value);

        int64_t getLines() const;
        void setLines(const int64_t &value);

        int64_t getIttim() const;
        void setIttim(const int64_t &value);

        int64_t getNsubd() const;
        void setNsubd(const int64_t &value);

        double getTime() const;
        void setTime(double value);

        vector<string> getOptions() const;
        void setOptions(const vector<string> &value);

        int64_t getTag1() const;
        void setTag1(const int64_t &value);

        int64_t getTag2() const;
        void setTag2(const int64_t &value);

        int64_t getDivi() const;
        void setDivi(const int64_t &value);

        int64_t getMpioType() const;

        bool getValid() const;

        bool isVector() const;

        bool isParallel() const;

        bool isFiltered() const;

        bool hasId() const;

        string getFileName() const;

        bool open(int flag);

        bool close();

        bool read();

        bool write();

        long long getFileSizeSTD() const;

        int64_t getIntSize() const;

        bool finalize();

    protected:
        bool computeSize();

        bool check();

        bool broadcast();

        int32_t readInteger4STD();
        int64_t readInteger8STD();
        double readFloat8STD();
        string readStringSTD();
        void write(int32_t value);
        void write(int64_t value);
        void write(double value);
        void write(string value);


    private:
        //Header fields and flags

        int64_t magicNumber;
        string alignChar64;
        string format;
        string version;
        string object;
        string dimension; bool vectorBool;
        string resultsOn;
        string type;
        string size; int64_t intSize;
        string parallel; bool parallelBool;
        string filter; bool filterBool;
        string sorting;
        string id; bool idBool;
        int64_t columns;
        int64_t lines;
        int64_t ittim;
        int64_t nsubd;
        int64_t divi;
        int64_t tag1;
        int64_t tag2;
        vector<string> options;
        double time;

        //Header management

        AlyaPtr_MPIOComm communicator;
        AlyaPtr_fstream fileSTD;
        long long fileSizeSTD;
        string fileName;
        bool valid;
        int64_t mpioType;
        int flag;
        bool finalized;

    };

}

#endif // MPIOHEADER_H
