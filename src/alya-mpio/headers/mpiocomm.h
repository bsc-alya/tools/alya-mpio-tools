/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#ifndef MPIOCOMM_H
#define MPIOCOMM_H

#include <mpiowrapper.h>
#include <pointer.h>
#include <vector>
#include <message.h>
#include <cstring>

#define MPIO_WORKER 0
#define MPIO_NOT_WORKER 1
#define ROOT 0

#define AlyaPtr_MPIOComm AlyaPtr<alya::MPIOComm>

using namespace std;

namespace alya{

    class MPIOComm
    {
    public:
        MPIOComm();
#ifndef NO_MPI
        MPIOComm(MPI_Comm, int color);
        MPI_Comm getCommunicator() const;
        MPI_Comm* getCommunicatorAddress();
        void setCommunicator(const MPI_Comm &value, int color);
#endif
        AlyaPtr_MPIOComm split(int color, int key);
        AlyaPtr_MPIOComm split(int color);
        bool isMaster();
        bool isSlave();
        bool isSequential();
        bool isIOWorker();
        bool isParallelIOWorker();
        bool isMasterOrSequential();
        int getRank() const;
        int getSize() const;
        int getRoot() const;

        void Bcast(bool *buf);
        void Bcast(int32_t *buf);
        void Bcast(int64_t *buf);
        void Bcast(double *buf);
        void Bcast(string *buf);

        void Sum(int32_t *buf);
        void Sum(int64_t *buf);
        void Sum(double *buf);

        void Max(int32_t *buf);
        void Max(int64_t *buf);
        void Max(double *buf);

        void Min(int32_t *buf);
        void Min(int64_t *buf);
        void Min(double *buf);

        void Gather(int64_t send, AlyaPtr<int64_t> recv);

        void Gatherv(AlyaPtr<double> send, AlyaPtr<double> recv, vector<int64_t> size);
        void Gatherv(AlyaPtr<int32_t> send, AlyaPtr<int32_t> recv, vector<int64_t> size);
        void Gatherv(AlyaPtr<int64_t> send, AlyaPtr<int64_t> recv, vector<int64_t> size);

        void Allgather(int64_t send, AlyaPtr<int64_t> recv);

        void Alltoall(AlyaPtr<double> send, int64_t sendcount, AlyaPtr<double> recv, int64_t recvcount);
        void Alltoall(AlyaPtr<int32_t> send, int64_t sendcount, AlyaPtr<int32_t> recv, int64_t recvcount);
        void Alltoall(AlyaPtr<int64_t> send, int64_t sendcount, AlyaPtr<int64_t> recv, int64_t recvcount);

        void Alltoallv(AlyaPtr<double> send, vector<int64_t> sendcount, AlyaPtr<double> recv, vector<int64_t> recvcount);
        void Alltoallv(AlyaPtr<int32_t> send, vector<int64_t> sendcount, AlyaPtr<int32_t> recv, vector<int64_t> recvcount);
        void Alltoallv(AlyaPtr<int64_t> send, vector<int64_t> sendcount, AlyaPtr<int64_t> recv, vector<int64_t> recvcount);

        void Barrier();
    private:
#ifndef NO_MPI
        MPI_Comm communicator;
#endif
        int rank;
        int size;
        int color;
        int root;
        bool sequential;
    };

}

#endif // MPIOCOMM_H
