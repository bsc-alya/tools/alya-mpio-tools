/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include "mpiofile.h"


template<typename T>
alya::MPIOFile<T>::MPIOFile(AlyaPtr_MPIOComm communicator, AlyaPtr_MPIOHeader header, string fileName):
    communicator(communicator), header(header), fileName(fileName), gathered(false), merged(false), broadcasted(false), autoPartition(false)
{

}

template<typename T>
alya::MPIOFile<T>::MPIOFile(AlyaPtr_MPIOComm communicator, string fileName):
    communicator(communicator), header(AlyaPtr_MPIOHeader(new MPIOHeader(communicator, fileName))), fileName(fileName), gathered(false), merged(false), broadcasted(false), autoPartition(false)
{

}

template<typename T>
bool alya::MPIOFile<T>::readHeader()
{
    if (!header->open(READ)){
        return false;
    }
    if (!header->read()){
        return false;
    }
    if (!header->close()){
        return false;
    }
    return true;
}


template<typename T>
bool alya::MPIOFile<T>::writeHeader()
{
    if (!header->open(WRITE)){
        return false;
    }
    if (!header->write()){
        return false;
    }
    if (!header->close()){
        return false;
    }
    return true;
}


template<typename T>
bool alya::MPIOFile<T>::setHeader(string object, string resultsOn, int64_t columns, int64_t lines, vector<string> options, int64_t subd)
{
    return this->setHeader(object, resultsOn, columns, lines, 0, 0, 0, -1, -1, options, subd);
}



template<typename T>
bool alya::MPIOFile<T>::setHeader(string object, string resultsOn,  int64_t columns, int64_t lines, int64_t ittim, double time, int64_t divi, int64_t tag1, int64_t tag2, vector<string> options, int64_t subd)
{
    header->setAlignChar64(MPIOHEADER_ALIGN_CHAR64);
    header->setMagicNumber(MPIOHEADER_MAGICNUMBER);
    header->setFormat(MPIOHEADER_FORMAT);
    header->setVersion(MPIOHEADER_VERSION);
    header->setObject(object);
    header->setResultsOn(resultsOn);
    header->setFilter(MPIOHEADER_FIL_NO);
    header->setSorting(MPIOHEADER_SORT_ASC);
    header->setId(MPIOHEADER_ID_NO);
    header->setColumns(columns);
    header->setLines(lines);
    header->setIttim(ittim);
    header->setTime(time);
    header->setDivi(divi);
    header->setTag1(tag1);
    header->setTag2(tag2);
    if (options.empty()){
        vector<string> o;
        for (int i=0; i<MPIOHEADER_OPTION_SIZE; i++){
            o.push_back(MPIOHEADER_OPT_EMPTY);
        }
        header->setOptions(o);
    }else{
        header->setOptions(options);
    }
    int64_t subdt=communicator->getSize();
    if (subd!=-1){
        subdt=subd;
    }
    if (subdt==1||subd==0){
        header->setNsubd(1);
        header->setParallel(MPIOHEADER_PAR_NO);
    }else{
        header->setNsubd(subdt);
        header->setParallel(MPIOHEADER_PAR_YES);
    }
    header->setDimension(MPIOHEADER_DIM_VECTOR);
    if (header->getColumns()==1){
       header->setDimension(MPIOHEADER_DIM_SCALAR);
    }
    specializedFillHeader();
    header->finalize();
    return true;
}

template<typename T>
bool alya::MPIOFile<T>::setHeader(std::shared_ptr<alya::MPIOHeader> header, int64_t lines, int64_t subd)
{
    string object=header->getObject();
    string resultsOn=header->getResultsOn();
    int64_t col=header->getColumns();
    int64_t ittim=header->getIttim();
    double time=header->getTime();
    int64_t divi=header->getDivi();
    int64_t tag1=header->getTag1();
    int64_t tag2=header->getTag2();
    vector<string> o=header->getOptions();
    return setHeader(object, resultsOn, col, lines, ittim, time, divi, tag1, tag2, o, subd);
}

template<typename T>
bool alya::MPIOFile<T>::setHeaderAsField(std::shared_ptr<alya::MPIOHeader> header, int64_t lines, int64_t tag1, int64_t tag2, int64_t subd)
{
    string object=MPIOHEADER_OBJ_XFIEL;
    string resultsOn=header->getResultsOn();
    int64_t col=header->getColumns();
    int64_t ittim=0;
    double time=0;
    int64_t divi=header->getDivi();
    vector<string> o=header->getOptions();
    return setHeader(object, resultsOn, col, lines, ittim, time, divi, tag1, tag2, o, subd);
}

template<typename T>
bool alya::MPIOFile<T>::open(int flag)
{
    this->flag=flag;
    if (communicator->isSequential()){
        fileSTD=AlyaPtr_fstream(new fstream());
        if (flag == READ){
            fileSTD->open(fileName, std::ios::in | std::ios::binary );
            fileSizeSTD=AlyaFile::Size(fileName);
        } else if (flag == WRITE){
            fileSTD->open(fileName, std::ios::out | std::ios::binary );
        } else if (flag == READWRITE){
            fileSTD->open(fileName, std::ios::in | std::ios::out | std::ios::binary );
            fileSizeSTD=AlyaFile::Size(fileName);
        } else {
            return false;
        }
        if (!fileSTD->is_open()){
            return false;
        }
    }
#ifndef NO_MPI
    if (communicator->isParallelIOWorker()){
        if (flag == READ){
            MPI_Check(MPI_File_open(communicator->getCommunicator(), fileName.c_str(), MPI_MODE_RDONLY, MPI_INFO_NULL, &fileMPI));
        }else if (flag == WRITE){
            MPI_Check(MPI_File_open(communicator->getCommunicator(), fileName.c_str(), MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &fileMPI));
        }else if (flag == READWRITE){
            MPI_Check(MPI_File_open(communicator->getCommunicator(), fileName.c_str(), MPI_MODE_RDWR, MPI_INFO_NULL, &fileMPI));
        }else{
            return false;
        }
        if (flag == READ || flag == READWRITE){
            MPI_Check(MPI_File_get_size(fileMPI, &fileSizeMPI));
        }
        communicator->Barrier();
    }
#endif
    return true;
}

template<typename T>
bool alya::MPIOFile<T>::close()
{
#ifndef NO_MPI
    if (communicator->isParallelIOWorker()){
        communicator->Barrier();
        MPI_Check(MPI_File_close(&fileMPI));
    }
#endif
    if (communicator->isSequential()){
        if (fileSTD->is_open()) fileSTD->close();
    }
    return true;
}

template<typename T>
bool alya::MPIOFile<T>::setView()
{
    if (communicator->isSequential()){
        if (flag == READ){
            fileSTD->seekg(MPIOHEADER_HEADERSIZE_BYTES, ios_base::beg);
        } else if (flag == WRITE){
            fileSTD->seekp(MPIOHEADER_HEADERSIZE_BYTES, ios_base::beg);
        } else if (flag == READWRITE){
            fileSTD->seekg(MPIOHEADER_HEADERSIZE_BYTES, ios_base::beg);
            fileSTD->seekp(MPIOHEADER_HEADERSIZE_BYTES, ios_base::beg);
        }
    }else if (communicator->isIOWorker()){
        specializedSetView();
    }
    return true;
}

template<typename T>
bool alya::MPIOFile<T>::read()
{
    if (!readHeader()){
        return false;
    }
    if (!setView()){
        return false;
    }
    if (!computeOffset()){
        return false;
    }
    if (!initData()){
        return false;
    }
    if (!specializedRead()){
        return false;
    }
    return true;
}

template<typename T>
bool alya::MPIOFile<T>::write()
{
    if (!specializedWrite()){
        return false;
    }
    return true;
}

template<typename T>
bool alya::MPIOFile<T>::initWriting(bool allocateData)
{
    fileSizeSTD=header->getFileSizeSTD();
#ifndef NO_MPI
    fileSizeMPI=MPI_Offset(fileSizeSTD);
    if (communicator->isParallelIOWorker()){
        if (flag == WRITE){
            MPI_Check(MPI_File_set_size(fileMPI, fileSizeMPI));
        }
    }
#endif
    if (!writeHeader()){
        return false;
    }
    if (!setView()){
        return false;
    }
    if (!computeOffset()){
        return false;
    }
    if (allocateData){
        if (!initData()){
            return false;
        }
    }
    return true;
}

template<typename T>
bool alya::MPIOFile<T>::computeOffset()
{
    if (autoPartition){
        partition=mpioPartition->getPartitions(header->getResultsOn());
    }
    offset=0;
    count=0;
    lines=0;
    if (communicator->isIOWorker()){
        if (communicator->isSequential()){
            partition.clear();
            partition.push_back(header->getLines());
            count=header->getLines()*header->getColumns();
            counts.push_back(count);
            lines=header->getLines();
            columns=header->getColumns();
            return true;
        }
#ifndef NO_MPI
        else{
            if (partition.size()==0){
                for (int i=0; i<communicator->getSize(); i++){
                    partition.push_back(header->getLines()/communicator->getSize()+(i<header->getLines()%communicator->getSize()?1:0));
                }
            }else if (partition.size()!=static_cast<unsigned long>(communicator->getSize())){
                Message::Critical(communicator->getRank(),"Partition size ("+to_string(partition.size())+
                                  ") is not equal to worker number ("+to_string(communicator->getSize())+")");
                return false;
            }
            for (int i=0; i<communicator->getRank(); i++){
                offset+=partition[static_cast<unsigned long>(i)];
            }
            for (int i=0; i<communicator->getSize(); i++){
                counts.push_back(partition[static_cast<unsigned long>(i)]*header->getColumns());
            }
            offset*=header->getColumns();
            count=partition[static_cast<unsigned long>(communicator->getRank())]*header->getColumns();
            lines=partition[static_cast<unsigned long>(communicator->getRank())];
            columns=header->getColumns();
        }
        offsetMPI=offset;
#endif
    }
    return true;
}

template<typename T>
bool alya::MPIOFile<T>::configDimensions()
{
    int64_t sumlines=lines;
    if (!merged&&!gathered){
        communicator->Sum(&sumlines);
        AlyaPtr<int64_t> p=AlyaPtr<int64_t>(new int64_t[communicator->getSize()]);
        communicator->Gather(lines, p);
        for (int i=0; i<communicator->getSize(); i++){
            int64_t t;
            if (communicator->isMaster()){
                t=p.get()[i];
            }
            communicator->Bcast(&t);
            partition.push_back(t);
        }
    }else{
        partition.clear();
    }
    header->setLines(sumlines);
    header->setColumns(columns);
    return true;
}

template<typename T>
bool alya::MPIOFile<T>::getMerged() const
{
    return merged;
}

template<typename T>
bool alya::MPIOFile<T>::isVector() const
{
    return !isScalar()&&header->getColumns()>0;
}

template<typename T>
bool alya::MPIOFile<T>::isScalar() const
{
    return header->getColumns()==1;
}

template<typename T>
std::shared_ptr<T> alya::MPIOFile<T>::getData() const
{
    return data;
}

template<typename T>
T alya::MPIOFile<T>::getData(int64_t i)
{
    return data.get()[i];
}

template<typename T>
T alya::MPIOFile<T>::getData(int64_t i, int64_t j)
{
    return data.get()[i*columns+j];
}

template<typename T>
T alya::MPIOFile<T>::getMaxValue()
{
    T maxValue=0;
    if(count>0){
        maxValue=data.get()[0];
    }
    for(int64_t i=1; i<count; i++){
        maxValue=max(maxValue, data.get()[i]);
    }
    return maxValue;
}

template<typename T>
void alya::MPIOFile<T>::setData(std::shared_ptr<T> data)
{
    this->data=data;
}

template<typename T>
void alya::MPIOFile<T>::setData(int64_t i, T value)
{
    data.get()[i]=value;
}

template<typename T>
void alya::MPIOFile<T>::setData(int64_t i, int64_t j, T value)
{
    data.get()[i*columns+j]=value;
}

template<typename T>
bool alya::MPIOFile<T>::merge(std::shared_ptr<MPIOFile<int32_t> > inv)
{
    if (communicator->isParallelIOWorker()){
        if (!inv->getGathered()){
            return false;
        }
        gather();
    }
    if (communicator->isMaster()){
        AlyaPtr<T> temp=AlyaPtr<T>(new T[header->getLines()*header->getColumns()]);
        int64_t m=0;
        for (int64_t i=0; i<lines; i++){
            int64_t invi=inv->getData(i)-1;
            for (int64_t j=0; j<columns; j++){
                temp.get()[invi*columns+j]=data.get()[i*columns+j];
            }
            m=max(invi, m);
        }
        lines=m+1;
        data=AlyaPtr<T>(new T[lines*header->getColumns()]);
        for (int i=0; i<lines*columns; i++){
            data.get()[i]=temp.get()[i];
        }
    }
    merged=true;
    return true;
}

template<typename T>
bool alya::MPIOFile<T>::merge(std::shared_ptr<MPIOFile<int64_t> > inv)
{
    if (communicator->isParallelIOWorker()){
        if (!inv->getGathered()){
            return false;
        }
        gather();
    }
    if (communicator->isMaster()){
        AlyaPtr<T> temp=AlyaPtr<T>(new T[header->getLines()*header->getColumns()]);
        int64_t m=0;
        for (int64_t i=0; i<lines; i++){
            int64_t invi=inv->getData(i)-1;
            for (int64_t j=0; j<columns; j++){
                temp.get()[invi*columns+j]=data.get()[i*columns+j];
            }
            m=max(invi, m);
        }
        lines=m+1;
        data=AlyaPtr<T>(new T[header->getLines()*header->getColumns()]);
        for (int i=0; i<lines*columns; i++){
            data.get()[i]=temp.get()[i];
        }
    }
    merged=true;
    return true;
}

template<typename T>
bool alya::MPIOFile<T>::merge(std::shared_ptr<MPIOFile<int32_t> > inv1, std::shared_ptr<MPIOFile<int32_t> > inv2)
{
    if (std::is_same<T,double>::value){
        return false;
    }
    if (communicator->isParallelIOWorker()){
        if (!inv1->getGathered()){
            return false;
        }
        for (int64_t i=0; i<lines; i++){
            for (int64_t j=0; j<columns; j++){
                int64_t index=i*columns+j;
                T local = data.get()[index];
                if (local > static_cast<T>(0)){
                    data.get()[index]=inv2->getData(local-1);
                }else{
                    break;
                }
            }
        }
    }
    return merge(inv1);
}

template<typename T>
bool alya::MPIOFile<T>::merge(std::shared_ptr<MPIOFile<int64_t> > inv1, std::shared_ptr<MPIOFile<int64_t> > inv2)
{
    if (std::is_same<T,double>::value){
        return false;
    }
    if (std::is_same<T,int32_t>::value){
        return false;
    }
    if (communicator->isParallelIOWorker()){
        if (!inv1->getGathered()){
            return false;
        }
        for (int64_t i=0; i<lines; i++){
            for (int64_t j=0; j<columns; j++){
                int64_t index=i*columns+j;
                T local = data.get()[index];
                if (local > static_cast<T>(0)){
                    data.get()[index]=inv2->getData(local-1);
                }else{
                    break;
                }
            }
        }
    }
    return merge(inv1);
}

template<typename T>
string alya::MPIOFile<T>::getFileName() const
{
    return fileName;
}

template<typename T>
AlyaPtr<alya::MPIOHeader> alya::MPIOFile<T>::getHeader() const
{
    return header;
}

template<typename T>
bool alya::MPIOFile<T>::getGathered() const
{
    return gathered;
}

template<typename T>
vector<int64_t> alya::MPIOFile<T>::getCounts() const
{
    return counts;
}

template<typename T>
int64_t alya::MPIOFile<T>::getColumns() const
{
    return columns;
}

template<typename T>
bool alya::MPIOFile<T>::gather()
{
    if (communicator->isParallelIOWorker()){
        AlyaPtr<T> recv;
        if (communicator->isMaster()){
            recv=AlyaPtr<T>(new T[header->getLines()*header->getColumns()]);
        }else{
            recv=AlyaPtr<T>(new T[1]);
        }
        communicator->Gatherv(data, recv, counts);
        if (communicator->isMaster()){
            data=recv;
            lines=header->getLines();
        }
        gathered=true;
    }
    return true;
}

template<typename T>
int64_t alya::MPIOFile<T>::getLines() const
{
    return lines;
}

template<typename T>
int64_t alya::MPIOFile<T>::getCount() const
{
    return count;
}

template <typename T>
int64_t alya::MPIOFile<T>::getOffset() const
{
    return offset;
}

template<typename T>
vector<int64_t> alya::MPIOFile<T>::getPartition() const
{
    return partition;
}

template<typename T>
void alya::MPIOFile<T>::setPartition(int64_t value)
{
    AlyaPtr<int64_t> partitionPtr(new int64_t[communicator->getSize()]);
    communicator->Allgather(value, partitionPtr);
    partition=vector<int64_t>(partitionPtr.get(), partitionPtr.get()+communicator->getSize());
}

template<typename T>
void alya::MPIOFile<T>::setPartition(const vector<int64_t> &value)
{
    partition = value;
}

template<typename T>
void alya::MPIOFile<T>::setPartition(std::shared_ptr<alya::MPIOPartition> value)
{
    mpioPartition = value;
    autoPartition = true;
}

//Template

template<typename T>
bool alya::MPIOFile<T>::initData()
{
    data=AlyaPtr<T>(new T[count]);
    return true;
}

//Specialized methods

template<>
bool alya::MPIOFile<double>::specializedSetView()
{
#ifndef NO_MPI
    if (communicator->isParallelIOWorker()){
        MPI_Check(MPI_File_set_view(fileMPI, MPIOHEADER_HEADERSIZE_BYTES, MPI_DOUBLE, MPI_DOUBLE, MPIO_NATIVE, MPI_INFO_NULL));
    }
#endif
    return true;
}

template<>
bool alya::MPIOFile<int32_t>::specializedSetView()
{
#ifndef NO_MPI
    if (communicator->isParallelIOWorker()){
        MPI_Check(MPI_File_set_view(fileMPI, MPIOHEADER_HEADERSIZE_BYTES, MPI_INT32_T, MPI_INT32_T, MPIO_NATIVE, MPI_INFO_NULL));
    }
#endif
    return true;
}

template<>
bool alya::MPIOFile<int64_t>::specializedSetView()
{
#ifndef NO_MPI
    if (communicator->isParallelIOWorker()){
        MPI_Check(MPI_File_set_view(fileMPI, MPIOHEADER_HEADERSIZE_BYTES, MPI_INT64_T, MPI_INT64_T, MPIO_NATIVE, MPI_INFO_NULL));
    }
#endif
    return true;
}

template<>
bool alya::MPIOFile<double>::specializedFillHeader()
{
    header->setType(MPIOHEADER_TYPE_REAL);
    header->setSize(MPIOHEADER_SIZE_8BYTES);
    return true;
}

template<>
bool alya::MPIOFile<int32_t>::specializedFillHeader()
{
    header->setType(MPIOHEADER_TYPE_INTEGER);
    header->setSize(MPIOHEADER_SIZE_4BYTES);
    return true;
}

template<>
bool alya::MPIOFile<int64_t>::specializedFillHeader()
{
    header->setType(MPIOHEADER_TYPE_INTEGER);
    header->setSize(MPIOHEADER_SIZE_8BYTES);
    return true;
}

template<>
bool alya::MPIOFile<double>::specializedRead()
{
    if (communicator->isParallelIOWorker()){
#ifndef NO_MPI
        MPI_Status status;
        MPI_Check(MPI_File_read_at_all(fileMPI, offset, data.get(), static_cast<int>(count), MPI_DOUBLE, &status));
#endif
    }else if (communicator->isSequential()){
        fileSTD->read(reinterpret_cast<char*>(data.get()), static_cast<long>(count)*static_cast<long>(sizeof(double)));
    }
    return true;
}

template<>
bool alya::MPIOFile<int32_t>::specializedRead()
{
    if (communicator->isParallelIOWorker()){
#ifndef NO_MPI
        MPI_Status status;
        MPI_Check(MPI_File_read_at_all(fileMPI, offset, data.get(), count, MPI_INT32_T, &status));
#endif
    }else if (communicator->isSequential()){
        fileSTD->read(reinterpret_cast<char*>(data.get()), static_cast<long>(count)*static_cast<long>(sizeof(int32_t)));
    }
    return true;
}

template<>
bool alya::MPIOFile<int64_t>::specializedRead()
{
    if (communicator->isParallelIOWorker()){
#ifndef NO_MPI
        MPI_Status status;
        MPI_Check(MPI_File_read_at_all(fileMPI, offset, data.get(), count, MPI_INT64_T, &status));
#endif
    }else if (communicator->isSequential()){
        fileSTD->read(reinterpret_cast<char*>(data.get()), static_cast<long>(count)*static_cast<long>(sizeof(int64_t)));
    }
    return true;
}

template<>
bool alya::MPIOFile<double>::specializedWrite()
{
    if (communicator->isParallelIOWorker()){
#ifndef NO_MPI
        MPI_Status status;
        MPI_Check(MPI_File_write_at_all(fileMPI, offset, data.get(), count, MPI_DOUBLE, &status));
#endif
    }else if (communicator->isSequential()){
        fileSTD->write(reinterpret_cast<char*>(data.get()), static_cast<long>(count)*static_cast<long>(sizeof(double)));
    }
    return true;
}

template<>
bool alya::MPIOFile<int32_t>::specializedWrite()
{
    if (communicator->isParallelIOWorker()){
#ifndef NO_MPI
        MPI_Status status;
        MPI_Check(MPI_File_write_at_all(fileMPI, offset, data.get(), count, MPI_INT32_T, &status));
#endif
    }else if (communicator->isSequential()){
        fileSTD->write(reinterpret_cast<char*>(data.get()), static_cast<long>(count)*static_cast<long>(sizeof(int32_t)));
    }
    return true;
}

template<>
bool alya::MPIOFile<int64_t>::specializedWrite()
{
    if (communicator->isParallelIOWorker()){
#ifndef NO_MPI
        MPI_Status status;
        MPI_Check(MPI_File_write_at_all(fileMPI, offset, data.get(), count, MPI_INT64_T, &status));
#endif
    }else if (communicator->isSequential()){
        fileSTD->write(reinterpret_cast<char*>(data.get()), static_cast<long>(count)*static_cast<long>(sizeof(int64_t)));
    }
    return true;
}

template<>
bool alya::MPIOFile<double>::write(double value)
{
    if (communicator->isParallelIOWorker()){
#ifndef NO_MPI
        MPI_Status status;
        MPI_Check(MPI_File_write(fileMPI, &value, 1, MPI_DOUBLE, &status));
#endif
    }else if (communicator->isSequential()){
        fileSTD->write(reinterpret_cast<char*>(&value), sizeof(value));
    }
    return true;
}

template<>
bool alya::MPIOFile<int32_t>::write(int32_t value)
{
    if (communicator->isParallelIOWorker()){
#ifndef NO_MPI
        MPI_Status status;
        MPI_Check(MPI_File_write(fileMPI, &value, 1, MPI_INT32_T, &status));
#endif
    }else if (communicator->isSequential()){
        fileSTD->write(reinterpret_cast<char*>(&value), sizeof(value));
    }
    return true;
}

template<>
bool alya::MPIOFile<int64_t>::write(int64_t value)
{
    if (communicator->isParallelIOWorker()){
#ifndef NO_MPI
        MPI_Status status;
        MPI_Check(MPI_File_write(fileMPI, &value, 1, MPI_INT64_T, &status));
#endif
    }else if (communicator->isSequential()){
        fileSTD->write(reinterpret_cast<char*>(&value), sizeof(value));
    }
    return true;
}

//template<>
//bool alya::MPIOFile<double>::write(double value[], int64_t size)
//{
//    if (communicator->isParallelIOWorker()){
//#ifndef NO_MPI
//        MPI_Status status;
//        MPI_Check(MPI_File_write(fileMPI, value, size, MPI_DOUBLE, &status));
//#endif
//    }else if (communicator->isSequential()){
//        fileSTD->write(reinterpret_cast<char*>(&value), sizeof(value));
//    }
//    return true;
//}
//
//template<>
//bool alya::MPIOFile<int32_t>::write(int32_t value[], int64_t size)
//{
//    if (communicator->isParallelIOWorker()){
//#ifndef NO_MPI
//        MPI_Status status;
//        MPI_Check(MPI_File_write(fileMPI, value, size, MPI_INT32_T, &status));
//#endif
//    }else if (communicator->isSequential()){
//        fileSTD->write(reinterpret_cast<char*>(&value), sizeof(value));
//    }
//    return true;
//}
//
//template<>
//bool alya::MPIOFile<int64_t>::write(int64_t value[], int64_t size)
//{
//    if (communicator->isParallelIOWorker()){
//#ifndef NO_MPI
//        MPI_Status status;
//        MPI_Check(MPI_File_write(fileMPI, value, size, MPI_INT64_T, &status));
//#endif
//    }else if (communicator->isSequential()){
//        fileSTD->write(reinterpret_cast<char*>(&value), sizeof(value));
//    }
//    return true;
//}

template class alya::MPIOFile<double>;

template class alya::MPIOFile<int32_t>;

template class alya::MPIOFile<int64_t>;






