/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include "mpioheader.h"

alya::MPIOHeader::MPIOHeader(AlyaPtr_MPIOComm communicator, string fileName):communicator(communicator), fileName(fileName)
{
    flag=WRITE;
    finalized=false;
}

vector<string> alya::MPIOHeader::getOptions() const
{
    return options;
}

void alya::MPIOHeader::setOptions(const vector<string> &value)
{
    options = value;
}

bool alya::MPIOHeader::open(int flag)
{
    this->flag=flag;
    if (communicator->isMasterOrSequential()){
        if (flag == READ){
            fileSTD=AlyaPtr_fstream(new fstream(fileName.c_str(), std::ios::in | std::ios::binary ));
            fileSizeSTD=AlyaFile::Size(fileName);
        } else if (flag == WRITE){
            fileSTD=AlyaPtr_fstream(new fstream(fileName.c_str(), std::ios::out | std::ios::binary ));
        } else if (flag == READWRITE){
            fileSTD=AlyaPtr_fstream(new fstream(fileName.c_str(), std::ios::in | std::ios::out | std::ios::binary ));
            fileSizeSTD=AlyaFile::Size(fileName);
        } else {
            return false;
        }
        if (fileSTD->is_open()){
            return true;
        }else{
            return false;
        }
    }
    return true;
}

bool alya::MPIOHeader::close()
{
    if (communicator->isMasterOrSequential()){
        if (fileSTD->is_open()) fileSTD->close();
    }
    return true;
}

double alya::MPIOHeader::getTime() const
{
    return time;
}

void alya::MPIOHeader::setTime(double value)
{
    time = value;
}

int64_t alya::MPIOHeader::getNsubd() const
{
    return nsubd;
}

void alya::MPIOHeader::setNsubd(const int64_t &value)
{
    nsubd = value;
}

int64_t alya::MPIOHeader::getIttim() const
{
    return ittim;
}

void alya::MPIOHeader::setIttim(const int64_t &value)
{
    ittim = value;
}

int64_t alya::MPIOHeader::getLines() const
{
    return lines;
}

void alya::MPIOHeader::setLines(const int64_t &value)
{
    lines = value;
}

int64_t alya::MPIOHeader::getColumns() const
{
    return columns;
}

void alya::MPIOHeader::setColumns(const int64_t &value)
{
    columns = value;
}

string alya::MPIOHeader::getId() const
{
    return id;
}

void alya::MPIOHeader::setId(const string &value)
{
    id = value;
}

string alya::MPIOHeader::getSorting() const
{
    return sorting;
}

void alya::MPIOHeader::setSorting(const string &value)
{
    sorting = value;
}

string alya::MPIOHeader::getFilter() const
{
    return filter;
}

void alya::MPIOHeader::setFilter(const string &value)
{
    filter = value;
}

string alya::MPIOHeader::getParallel() const
{
    return parallel;
}

void alya::MPIOHeader::setParallel(const string &value)
{
    parallel = value;
}

string alya::MPIOHeader::getSize() const
{
    return size;
}

void alya::MPIOHeader::setSize(const string &value)
{
    size = value;

}

string alya::MPIOHeader::getType() const
{
    return type;
}

void alya::MPIOHeader::setType(const string &value)
{
    type = value;
}

string alya::MPIOHeader::getResultsOn() const
{
    return resultsOn;
}

void alya::MPIOHeader::setResultsOn(const string &value)
{
    resultsOn = value;
}

string alya::MPIOHeader::getDimension() const
{
    return dimension;
}

void alya::MPIOHeader::setDimension(const string &value)
{
    dimension = value;

}

string alya::MPIOHeader::getObject() const
{
    return object;
}

void alya::MPIOHeader::setObject(const string &value)
{
    object = value;
}

string alya::MPIOHeader::getVersion() const
{
    return version;
}

void alya::MPIOHeader::setVersion(const string &value)
{
    version = value;
}

string alya::MPIOHeader::getFormat() const
{
    return format;
}

void alya::MPIOHeader::setFormat(const string &value)
{
    format = value;
}

string alya::MPIOHeader::getAlignChar64() const
{
    return alignChar64;
}

void alya::MPIOHeader::setAlignChar64(const string &value)
{
    alignChar64 = value;
}

int64_t alya::MPIOHeader::getMagicNumber() const
{
    return magicNumber;
}

void alya::MPIOHeader::setMagicNumber(const int64_t &value)
{
    magicNumber = value;
}


int64_t alya::MPIOHeader::getTag1() const
{
    return tag1;
}

void alya::MPIOHeader::setTag1(const int64_t &value)
{
    tag1 = value;
}

int64_t alya::MPIOHeader::getTag2() const
{
    return tag2;
}

void alya::MPIOHeader::setTag2(const int64_t &value)
{
    tag2 = value;
}

bool alya::MPIOHeader::read()
{
    if (communicator->isMasterOrSequential()){
        fileSTD->seekg(ios_base::beg);
        magicNumber=readInteger8STD();
        format=readStringSTD();
        version=readStringSTD();
        object=readStringSTD();
        dimension=readStringSTD();
        resultsOn=readStringSTD();
        type=readStringSTD();
        size=readStringSTD();
        parallel=readStringSTD();
        filter=readStringSTD();
        sorting=readStringSTD();
        id=readStringSTD();
        alignChar64=readStringSTD();
        columns=readInteger8STD();
        lines=readInteger8STD();
        ittim=readInteger8STD();
        nsubd=readInteger8STD();
        divi=readInteger8STD();
        tag1=readInteger8STD();
        tag2=readInteger8STD();
        time=readFloat8STD();
        alignChar64=readStringSTD();
        for (int i=0; i<MPIOHEADER_OPTION_SIZE; i++){
            options.push_back(readStringSTD());
        }
    }
    return finalize();
}

bool alya::MPIOHeader::write()
{
    if (!finalize()){
        return false;
    }
    if (communicator->isMasterOrSequential()){
        fileSTD->seekp(ios_base::beg);
        write(magicNumber);
        write(format);
        write(version);
        write(object);
        write(dimension);
        write(resultsOn);
        write(type);
        write(size);
        write(parallel);
        write(filter);
        write(sorting);
        write(id);
        write(alignChar64);
        write(columns);
        write(lines);
        write(ittim);
        write(nsubd);
        write(divi);
        write(tag1);
        write(tag2);
        write(time);
        write(alignChar64);
        for (unsigned long i=0; i<10; i++){
            write(options[i]);
        }
    }
    return true;
}

bool alya::MPIOHeader::computeSize()
{
    if(flag==WRITE){
        fileSizeSTD=lines*columns*intSize+MPIOHEADER_HEADERSIZE_BYTES;
    }
    return true;
}

bool alya::MPIOHeader::finalize()
{	if (! finalized){
        if (communicator->isIOWorker()){
            broadcast();
            valid=check();
            if (type.compare(MPIOHEADER_TYPE_INTEGER)==0){
                mpioType=MPIO_TYPE_INTEGER;
            }else if (type.compare(MPIOHEADER_TYPE_REAL)==0){
                mpioType=MPIO_TYPE_FLOAT;
            }else{
                mpioType=MPIO_TYPE_UNKNOWN;
            }
            computeSize();
            finalized=valid;
            return valid;
        }
        finalized=true;
        return true;
    }
    return true;
}

bool alya::MPIOHeader::check()
{
    if (magicNumber!=MPIOHEADER_MAGICNUMBER){
        Message::Error(communicator->getRank(), "Wrong magic number: "+to_string(magicNumber)+" instead of "+to_string(MPIOHEADER_MAGICNUMBER));
        return false;
    }
    if (format.compare(MPIOHEADER_FORMAT)!=0){
        Message::Error(communicator->getRank(), "Wrong format: "+format+" instead of "+MPIOHEADER_FORMAT);
        return false;
    }
    if (version.compare(MPIOHEADER_VERSION)!=0){
        Message::Error(communicator->getRank(), "Wrong version: "+version+" instead of "+MPIOHEADER_VERSION);
        return false;
    }
    if (dimension.compare(MPIOHEADER_DIM_SCALAR)==0){
        vectorBool=false;
    }else if (dimension.compare(MPIOHEADER_DIM_VECTOR)==0){
        vectorBool=true;
    }else{
        Message::Error(communicator->getRank(), "Dimension must be scalar or vector");
        return false;
    }
    if (size.compare(MPIOHEADER_SIZE_4BYTES)==0){
        intSize=MPIO_TYPESIZE_4;
    }else if (size.compare(MPIOHEADER_SIZE_8BYTES)==0){
        intSize=MPIO_TYPESIZE_8;
    }else{
        Message::Error(communicator->getRank(), "Type size must be 4 or 8 bytes");
        return false;
    }
    if (parallel.compare(MPIOHEADER_PAR_NO)==0){
        parallelBool=false;
    }else if (parallel.compare(MPIOHEADER_PAR_YES)==0){
        parallelBool=true;
    }else{
        Message::Error(communicator->getRank(), "File must be sequential or parallel");
        return false;
    }
    if (filter.compare(MPIOHEADER_FIL_NO)==0){
        filterBool=false;
    }else{
        filterBool=true;
        Message::Error(communicator->getRank(), "Filtering must be disabled");
        return false;
    }
    if (sorting.compare(MPIOHEADER_SORT_ASC)!=0){
        Message::Error(communicator->getRank(), "Wrong sorting: must be ascending sorting");
        return false;
    }
    if (id.compare(MPIOHEADER_ID_NO)==0){
        idBool=false;
    }else{
        idBool=true;
        Message::Error(communicator->getRank(), "Id must be absent");
        return false;
    }
    if (nsubd==1&&parallelBool){
        Message::Error(communicator->getRank(), "If file has subdomains, nsubd must be > 1");
        return false;
    }
    if (nsubd>1&&!parallelBool){
        Message::Error(communicator->getRank(), "If file has only one subdomain, nsubd must be 1; current value: "+to_string(nsubd));
        return false;
    }
    return true;
}

bool alya::MPIOHeader::broadcast()
{
    communicator->Bcast(&magicNumber);
    communicator->Bcast(&format);
    communicator->Bcast(&version);
    communicator->Bcast(&object);
    communicator->Bcast(&dimension);
    communicator->Bcast(&resultsOn);
    communicator->Bcast(&type);
    communicator->Bcast(&size);
    communicator->Bcast(&parallel);
    communicator->Bcast(&filter);
    communicator->Bcast(&sorting);
    communicator->Bcast(&id);
    communicator->Bcast(&columns);
    communicator->Bcast(&lines);
    communicator->Bcast(&ittim);
    communicator->Bcast(&time);
    communicator->Bcast(&nsubd);
    communicator->Bcast(&divi);
    communicator->Bcast(&tag1);
    communicator->Bcast(&tag2);
    for (unsigned long i=0; i<MPIOHEADER_OPTION_SIZE; i++){
        if (communicator->isSlave()){
            options.push_back(string());
        }
        communicator->Bcast(&(options[i]));
    }
    communicator->Bcast(&time);
    return true;
}

int32_t alya::MPIOHeader::readInteger4STD()
{
    int32_t buf;
    fileSTD->read(reinterpret_cast<char*>(&buf), sizeof(buf));
    return buf;
}

int64_t alya::MPIOHeader::readInteger8STD()
{
    int64_t buf;
    fileSTD->read(reinterpret_cast<char*>(&buf), sizeof(buf));
    return buf;
}

double alya::MPIOHeader::readFloat8STD()
{
    double buf;
    fileSTD->read(reinterpret_cast<char*>(&buf), sizeof(buf));
    return buf;
}

string alya::MPIOHeader::readStringSTD()
{
    char buf[MPIOHEADER_STRING_SIZE];
    fileSTD->read(const_cast<char*>(buf), sizeof(buf));
    string str(buf);
    return str;
}

void alya::MPIOHeader::write(int32_t value)
{
    fileSTD->write(reinterpret_cast<char*>(&value), sizeof(value));
}

void alya::MPIOHeader::write(int64_t value)
{
    fileSTD->write(reinterpret_cast<char*>(&value), sizeof(value));
}

void alya::MPIOHeader::write(double value)
{
    fileSTD->write(reinterpret_cast<char*>(&value), sizeof(value));
}

void alya::MPIOHeader::write(string value)
{
    fileSTD->write(value.c_str(), sizeof(value.c_str()));
}


int64_t alya::MPIOHeader::getIntSize() const
{
    return intSize;
}

long long alya::MPIOHeader::getFileSizeSTD() const
{
    return fileSizeSTD;
}

int64_t alya::MPIOHeader::getDivi() const
{
    return divi;
}

void alya::MPIOHeader::setDivi(const int64_t &value)
{
    divi = value;
}

string alya::MPIOHeader::getFileName() const
{
    return fileName;
}

bool alya::MPIOHeader::hasId() const
{
    return idBool;
}

bool alya::MPIOHeader::isFiltered() const
{
    return filterBool;
}

bool alya::MPIOHeader::isParallel() const
{
    return parallelBool;
}

bool alya::MPIOHeader::isVector() const
{
    return vectorBool;
}

int64_t alya::MPIOHeader::getMpioType() const
{
    return mpioType;
}

bool alya::MPIOHeader::getValid() const
{
    return valid;
}
