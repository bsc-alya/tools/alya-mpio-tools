/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include "mpiocomm.h"

#ifndef NO_MPI
alya::MPIOComm::MPIOComm(MPI_Comm communicator, int color)
{
    sequential=false;
    setCommunicator(communicator, color);
}
#endif

alya::MPIOComm::MPIOComm()
{
#ifndef NO_MPI
    sequential=false;
    setCommunicator(MPI_COMM_WORLD, MPIO_WORKER);
#else
    sequential=true;
    rank=0;
#endif
}

AlyaPtr_MPIOComm alya::MPIOComm::split(int col, int key)
{
    AlyaPtr_MPIOComm newCommunicator;
#ifndef NO_MPI
    MPI_Comm newMPICommunicator;
    if (!sequential){
        MPI_Check(MPI_Comm_split(communicator, col, key, &newMPICommunicator));
        newCommunicator=AlyaPtr_MPIOComm(new MPIOComm(newMPICommunicator, col));
        return newCommunicator;
    }
#endif
    newCommunicator=AlyaPtr_MPIOComm(new MPIOComm());
    return newCommunicator;
}

AlyaPtr_MPIOComm alya::MPIOComm::split(int color)
{
    return split(color, rank);
}

bool alya::MPIOComm::isMaster()
{
    return (!sequential&&color==MPIO_WORKER&&rank==0);
}

bool alya::MPIOComm::isSlave()
{
    return (!sequential&&color==MPIO_WORKER&&rank>0);
}

bool alya::MPIOComm::isSequential()
{
    return sequential;
}

bool alya::MPIOComm::isIOWorker(){
    return sequential||isMaster()||isSlave();
}

bool alya::MPIOComm::isParallelIOWorker()
{
    return isMaster()||isSlave();
}

bool alya::MPIOComm::isMasterOrSequential()
{
    return sequential||isMaster();
}

#ifndef NO_MPI
MPI_Comm alya::MPIOComm::getCommunicator() const
{
    return communicator;
}

MPI_Comm* alya::MPIOComm::getCommunicatorAddress()
{
    return &communicator;
}

void alya::MPIOComm::setCommunicator(const MPI_Comm &value, int col)
{
    if (!sequential){
        communicator = value;
        color=col;
        if (communicator == MPI_COMM_NULL || color == MPI_UNDEFINED){
            rank=-1;
            size=-1;
        }else if(color==MPIO_WORKER){
            MPI_Check(MPI_Comm_rank(communicator, &rank));
            MPI_Check(MPI_Comm_size(communicator, &size));
            if (size==1){
                sequential=true;
                rank=0;
            }
        }else{
            rank=-1;
        }
    }
    root=ROOT;
}
#endif

int alya::MPIOComm::getSize() const
{
    return size;
}

int alya::MPIOComm::getRoot() const
{
    return root;
}

int alya::MPIOComm::getRank() const
{
    return rank;
}

void alya::MPIOComm::Bcast(bool *buf)
{
#ifndef NO_MPI
    if (isParallelIOWorker()){
        MPI_Check(MPI_Bcast(buf, 1, MPI_CXX_BOOL, root, communicator));
    }
#endif
}

void alya::MPIOComm::Bcast(int32_t *buf)
{
#ifndef NO_MPI
    if (isParallelIOWorker()){
        MPI_Check(MPI_Bcast(buf, 1, MPI_INT32_T, root, communicator));
    }
#endif
}

void alya::MPIOComm::Bcast(int64_t *buf)
{
#ifndef NO_MPI
    if (isParallelIOWorker()){
        MPI_Check(MPI_Bcast(buf, 1, MPI_INT64_T, root, communicator));
    }
#endif
}

void alya::MPIOComm::Bcast(double *buf)
{
#ifndef NO_MPI
    if (isParallelIOWorker()){
        MPI_Check(MPI_Bcast(buf, 1, MPI_DOUBLE, root, communicator));
    }
#endif
}

void alya::MPIOComm::Bcast(string *buf)
{
#ifndef NO_MPI
    if (isParallelIOWorker()){
        int bufSize=buf->length() + 1;
        Bcast(&bufSize);
        char * c=new char[bufSize];
        if (rank==root){
            strcpy(c, buf->c_str());
        }
        MPI_Check(MPI_Bcast(c, bufSize, MPI_CHAR, root, communicator));
        if (rank!=root){
            buf->operator =(c);
        }
        delete [] c;
    }
#endif
}

void alya::MPIOComm::Sum(int32_t *buf)
{
#ifndef NO_MPI
    if (isParallelIOWorker()){
        int32_t *bufrcv= new int32_t;
        MPI_Check(MPI_Reduce(buf, bufrcv, 1, MPI_INT32_T, MPI_SUM, root, communicator));
        if (isMaster()){
            *buf=*bufrcv;
        }
    }
#endif
}

void alya::MPIOComm::Sum(int64_t *buf)
{
#ifndef NO_MPI
    if (isParallelIOWorker()){
        int64_t *bufrcv= new int64_t;
        MPI_Check(MPI_Reduce(buf, bufrcv, 1, MPI_INT64_T, MPI_SUM, root, communicator));
        if (isMaster()){
            *buf=*bufrcv;
        }
    }
#endif
}

void alya::MPIOComm::Sum(double *buf)
{
#ifndef NO_MPI
    if (isParallelIOWorker()){
        double *bufrcv= new double;
        MPI_Check(MPI_Reduce(buf, bufrcv, 1, MPI_DOUBLE, MPI_SUM, root, communicator));
        if (isMaster()){
            *buf=*bufrcv;
        }
    }
#endif
}

void alya::MPIOComm::Max(int32_t *buf)
{
#ifndef NO_MPI
    if (isParallelIOWorker()){
        int32_t *bufrcv= new int32_t;
        MPI_Check(MPI_Reduce(buf, bufrcv, 1, MPI_INT32_T, MPI_MAX, root, communicator));
        if (isMaster()){
            *buf=*bufrcv;
        }
    }
#endif
}

void alya::MPIOComm::Max(int64_t *buf)
{
#ifndef NO_MPI
    if (isParallelIOWorker()){
        int64_t *bufrcv= new int64_t;
        MPI_Check(MPI_Reduce(buf, bufrcv, 1, MPI_INT64_T, MPI_MAX, root, communicator));
        if (isMaster()){
            *buf=*bufrcv;
        }
    }
#endif
}

void alya::MPIOComm::Max(double *buf)
{
#ifndef NO_MPI
    if (isParallelIOWorker()){
        double *bufrcv= new double;
        MPI_Check(MPI_Reduce(buf, bufrcv, 1, MPI_DOUBLE, MPI_MAX, root, communicator));
        if (isMaster()){
            *buf=*bufrcv;
        }
    }
#endif
}

void alya::MPIOComm::Min(int32_t *buf)
{
#ifndef NO_MPI
    if (isParallelIOWorker()){
        int32_t *bufrcv= new int32_t;
        MPI_Check(MPI_Reduce(buf, bufrcv, 1, MPI_INT32_T, MPI_MIN, root, communicator));
        if (isMaster()){
            *buf=*bufrcv;
        }
    }
#endif
}

void alya::MPIOComm::Min(int64_t *buf)
{
#ifndef NO_MPI
    if (isParallelIOWorker()){
        int64_t *bufrcv= new int64_t;
        MPI_Check(MPI_Reduce(buf, bufrcv, 1, MPI_INT64_T, MPI_MIN, root, communicator));
        if (isMaster()){
            *buf=*bufrcv;
        }
    }
#endif
}

void alya::MPIOComm::Min(double *buf)
{
#ifndef NO_MPI
    if (isParallelIOWorker()){
        double *bufrcv= new double;
        MPI_Check(MPI_Reduce(buf, bufrcv, 1, MPI_DOUBLE, MPI_MIN, root, communicator));
        if (isMaster()){
            *buf=*bufrcv;
        }
    }
#endif
}

void alya::MPIOComm::Gather(int64_t send, AlyaPtr<int64_t> recv)
{
#ifndef NO_MPI
    if (isIOWorker()){
        if (!sequential){
            MPI_Check(MPI_Gather(&send, 1, MPI_INT64_T, recv.get(), size, MPI_INT64_T, root, communicator));
        }else{
            recv.get()[0]=send;
        }
    }
#endif
}

void alya::MPIOComm::Gatherv(AlyaPtr<int32_t> send, AlyaPtr<int32_t> recv, vector<int64_t> size)
{
#ifndef NO_MPI
    if (isIOWorker()){
        int *size2=new int[size.size()];
        int total=0;
        int *displ=new int[size.size()];
        for (unsigned long i=0; i<size.size(); i++){
            size2[i]=static_cast<int>(size[i]);
            displ[i]=total;
            total+=static_cast<int>(size[i]);
        }
        if (!sequential){
            MPI_Check(MPI_Gatherv(send.get(), size2[getRank()], MPI_INT32_T, recv.get(), size2, displ, MPI_INT32_T, root, communicator));
        }else{
            *recv=*send;
        }
        delete [] size2;
        delete [] displ;
    }
#endif
}

void alya::MPIOComm::Gatherv(AlyaPtr<int64_t> send, AlyaPtr<int64_t> recv, vector<int64_t> size)
{
#ifndef NO_MPI
    if (isIOWorker()){
        int *size2=new int[size.size()];
        int total=0;
        int *displ=new int[size.size()];
        for (unsigned long i=0; i<size.size(); i++){
            size2[i]=static_cast<int>(size[i]);
            displ[i]=total;
            total+=static_cast<int>(size[i]);
        }
        if (!sequential){
            MPI_Check(MPI_Gatherv(send.get(), size2[getRank()], MPI_INT64_T, recv.get(), size2, displ, MPI_INT64_T, root, communicator));
        }else{
            *recv=*send;
        }
        delete [] size2;
        delete [] displ;
    }
#endif
}


void alya::MPIOComm::Gatherv(AlyaPtr<double> send, AlyaPtr<double> recv, vector<int64_t> size)
{
#ifndef NO_MPI
    if (isIOWorker()){
        int *size2=new int[size.size()];
        int total=0;
        int *displ=new int[size.size()];
        for (unsigned long i=0; i<size.size(); i++){
            size2[i]=static_cast<int>(size[i]);
            displ[i]=total;
            total+=static_cast<int>(size[i]);
        }
        if (!sequential){
            MPI_Check(MPI_Gatherv(send.get(), size2[getRank()], MPI_DOUBLE, recv.get(), size2, displ, MPI_DOUBLE, root, communicator));
        }else{
            *recv=*send;
        }
        delete [] size2;
        delete [] displ;
    }
#endif
}

void alya::MPIOComm::Allgather(int64_t send, AlyaPtr<int64_t> recv)
{
#ifndef NO_MPI
    if (isIOWorker()){
        if (!sequential){
            MPI_Check(MPI_Allgather(&send, 1, MPI_INT64_T, recv.get(), 1, MPI_INT64_T, communicator));
        }else{
            *recv=send;
        }
    }
#endif
}

void alya::MPIOComm::Alltoall(AlyaPtr<double> send, int64_t sendcount, AlyaPtr<double> recv, int64_t recvcount)
{
#ifndef NO_MPI
    if (isIOWorker()){
        if (!sequential){
            MPI_Check(MPI_Alltoall(send.get(), sendcount, MPI_DOUBLE, recv.get(), recvcount, MPI_DOUBLE, communicator));
        }else{
            *recv=*send;
        }
    }
#endif
}

void alya::MPIOComm::Alltoall(AlyaPtr<int32_t> send, int64_t sendcount, AlyaPtr<int32_t> recv, int64_t recvcount)
{
#ifndef NO_MPI
    if (isIOWorker()){
        if (!sequential){
            MPI_Check(MPI_Alltoall(send.get(), sendcount, MPI_INT32_T, recv.get(), recvcount, MPI_INT32_T, communicator));
        }else{
            *recv=*send;
        }
    }
#endif
}

void alya::MPIOComm::Alltoall(AlyaPtr<int64_t> send, int64_t sendcount, AlyaPtr<int64_t> recv, int64_t recvcount)
{
#ifndef NO_MPI
    if (isIOWorker()){
        if (!sequential){
            MPI_Check(MPI_Alltoall(send.get(), sendcount, MPI_INT64_T, recv.get(), recvcount, MPI_INT64_T, communicator));
        }else{
            *recv=*send;
        }
    }
#endif
}

void alya::MPIOComm::Alltoallv(AlyaPtr<double> send, vector<int64_t> sendcount, AlyaPtr<double> recv, vector<int64_t> recvcount)
{
#ifndef NO_MPI
    if (isIOWorker()){
        int stotal=0;
        int *scount=new int[sendcount.size()];
        int *sdispls=new int[sendcount.size()];
        int rtotal=0;
        int *rcount=new int[recvcount.size()];
        int *rdispls=new int[recvcount.size()];
        if (sendcount.size()==recvcount.size()){
            for (unsigned long i=0; i<sendcount.size(); i++){
                scount[i]=static_cast<int>(sendcount[i]);
                sdispls[i]=stotal;
                stotal+=scount[i];
                rcount[i]=static_cast<int>(recvcount[i]);
                rdispls[i]=rtotal;
                rtotal+=rcount[i];
            }
        }else{
            for (unsigned long i=0; i<sendcount.size(); i++){
                scount[i]=static_cast<int>(sendcount[i]);
                sdispls[i]=stotal;
                stotal+=scount[i];
            }
            for (unsigned long i=0; i<recvcount.size(); i++){
                rcount[i]=static_cast<int>(recvcount[i]);
                rdispls[i]=rtotal;
                rtotal+=rcount[i];
            }
        }
        if (!sequential){
            MPI_Check(MPI_Alltoallv(send.get(), scount, sdispls, MPI_DOUBLE,
                                    recv.get(), rcount, rdispls, MPI_DOUBLE, communicator));
        }else{
            *recv=*send;
        }
        delete [] scount;
        delete [] sdispls;
        delete [] rcount;
        delete [] rdispls;
    }
#endif
}

void alya::MPIOComm::Alltoallv(AlyaPtr<int32_t> send, vector<int64_t> sendcount, AlyaPtr<int32_t> recv, vector<int64_t> recvcount)
{
#ifndef NO_MPI
    if (isIOWorker()){
        int stotal=0;
        int *scount=new int[sendcount.size()];
        int *sdispls=new int[sendcount.size()];
        int rtotal=0;
        int *rcount=new int[recvcount.size()];
        int *rdispls=new int[recvcount.size()];
        if (sendcount.size()==recvcount.size()){
            for (unsigned long i=0; i<sendcount.size(); i++){
                scount[i]=static_cast<int>(sendcount[i]);
                sdispls[i]=stotal;
                stotal+=scount[i];
                rcount[i]=static_cast<int>(recvcount[i]);
                rdispls[i]=rtotal;
                rtotal+=rcount[i];
            }
        }else{
            for (unsigned long i=0; i<sendcount.size(); i++){
                scount[i]=static_cast<int>(sendcount[i]);
                sdispls[i]=stotal;
                stotal+=scount[i];
            }
            for (unsigned long i=0; i<recvcount.size(); i++){
                rcount[i]=static_cast<int>(recvcount[i]);
                rdispls[i]=rtotal;
                rtotal+=rcount[i];
            }
        }
        if (!sequential){
            MPI_Check(MPI_Alltoallv(send.get(), scount, sdispls, MPI_INT32_T,
                                    recv.get(), rcount, rdispls, MPI_INT32_T, communicator));
        }else{
            *recv=*send;
        }
        delete [] scount;
        delete [] sdispls;
        delete [] rcount;
        delete [] rdispls;
    }
#endif
}

void alya::MPIOComm::Alltoallv(AlyaPtr<int64_t> send, vector<int64_t> sendcount, AlyaPtr<int64_t> recv, vector<int64_t> recvcount)
{
#ifndef NO_MPI
    if (isIOWorker()){
        int stotal=0;
        int *scount=new int[sendcount.size()];
        int *sdispls=new int[sendcount.size()];
        int rtotal=0;
        int *rcount=new int[recvcount.size()];
        int *rdispls=new int[recvcount.size()];
        if (sendcount.size()==recvcount.size()){
            for (unsigned long i=0; i<sendcount.size(); i++){
                scount[i]=static_cast<int>(sendcount[i]);
                sdispls[i]=stotal;
                stotal+=scount[i];
                rcount[i]=static_cast<int>(recvcount[i]);
                rdispls[i]=rtotal;
                rtotal+=rcount[i];
            }
        }else{
            for (unsigned long i=0; i<sendcount.size(); i++){
                scount[i]=static_cast<int>(sendcount[i]);
                sdispls[i]=stotal;
                stotal+=scount[i];
            }
            for (unsigned long i=0; i<recvcount.size(); i++){
                rcount[i]=static_cast<int>(recvcount[i]);
                rdispls[i]=rtotal;
                rtotal+=rcount[i];
            }
        }
        if (!sequential){
            MPI_Check(MPI_Alltoallv(send.get(), scount, sdispls, MPI_INT64_T,
                                    recv.get(), rcount, rdispls, MPI_INT64_T, communicator));
        }else{
            *recv=*send;
        }
        delete [] scount;
        delete [] sdispls;
        delete [] rcount;
        delete [] rdispls;
    }
#endif
}

void alya::MPIOComm::Barrier()
{
#ifndef NO_MPI
    MPI_Check(MPI_Barrier(communicator));
#endif
}
