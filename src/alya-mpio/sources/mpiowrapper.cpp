/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include "mpiowrapper.h"

alya::MPIO::MPIO()
{

}

void alya::MPIO::HandleMPIError(int errcode, char *str)
{
#ifndef NO_MPI
    char msg[MPI_MAX_ERROR_STRING];
    int resultlen;
    MPI_Error_string(errcode, msg, &resultlen);
    alya::Message::Critical(string(str)+": "+string(msg));
    MPI_Abort(MPI_COMM_WORLD, 1);
#endif
}

void alya::MPIO::HandleMPIError(int errcode, string str)
{
#ifndef NO_MPI
    char msg[MPI_MAX_ERROR_STRING];
    int resultlen;
    MPI_Error_string(errcode, msg, &resultlen);
    alya::Message::Critical(str+": "+string(msg));
    MPI_Abort(MPI_COMM_WORLD, 1);
#endif
}

void alya::MPIO::Abort(string str)
{
#ifndef NO_MPI
    Message::Critical(str);
    MPI_Abort(MPI_COMM_WORLD, 1);
    //TODO Manage no MPI
#endif
}

void alya::MPIO::Abort()
{
#ifndef NO_MPI
    MPI_Abort(MPI_COMM_WORLD, 1);
    //TODO Manage no MPI
#endif
}

void alya::MPIO::Init(int argc, char **argv)
{
#ifndef NO_MPI
    MPI_Check(MPI_Init(&argc, &argv));
#endif
}

void alya::MPIO::Finalize()
{
#ifndef NO_MPI
    MPI_Check(MPI_Finalize());
#endif
}




