/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include "mpiopartition.h"



alya::MPIOPartition::MPIOPartition(std::shared_ptr<alya::MPIOComm>communicator, string file):communicator(communicator), file(file), readFile(true)
{

}

alya::MPIOPartition::MPIOPartition(std::shared_ptr<alya::MPIOComm> communicator):communicator(communicator), readFile(false)
{

}

bool alya::MPIOPartition::computePartition()
{
    if (!readFile){
        partitions.clear();
        for (int i=0; i<PARTITION_DIM; i++){
            partitions.push_back(vector<int64_t>());
        }
        return true;
    }
    string line;
    AlyaPtr_ifstream partitionStream;
    if (communicator->isMasterOrSequential()){
        partitionStream=AlyaPtr_ifstream(new ifstream());
        partitionStream->open(file);
        if (!partitionStream->is_open()){
            Message::Error(communicator->getRank(),"Cannot open the partition file "+file);
            return false;
        }
        getline(*partitionStream, line);
        boost::trim_all(line);
        subdomains=atoi(line.c_str());
    }
    communicator->Bcast(&subdomains);
    if (subdomains<communicator->getSize()){
        communicator=communicator->split(communicator->getRank()<subdomains?MPIO_WORKER:MPIO_NOT_WORKER);
    }else if (subdomains>communicator->getSize()){
        Message::Error(communicator->getRank(),"You have launched this application with "+to_string(communicator->getSize())+
                       " processes but the files contain "+ to_string(subdomains)+
                       " subdomains. Relaunch this application with at least "+to_string(subdomains)+" processes.");
        return false;
    }
    partitions.clear();
    for (int i=0; i<PARTITION_DIM; i++){
        partitions.push_back(vector<int64_t>());
    }
    for (unsigned long i=0; i<PARTITION_DIM; i++){
        for (int j=0; j<subdomains; j++){
            (partitions[i]).push_back(0);
        }
    }
    if (communicator->isMasterOrSequential()){
        while (getline(*partitionStream, line)){
            boost::trim_all(line);
            AlyaPtr<boost::tokenizer<boost::escaped_list_separator<char> > > tokens;
            boost::tokenizer<boost::escaped_list_separator<char> >::iterator tokensIterator;
            boost::escaped_list_separator<char> sep('\\',' ','\"');
            tokens=AlyaPtr<boost::tokenizer<boost::escaped_list_separator<char> > >(new boost::tokenizer<boost::escaped_list_separator<char> >(line, sep));
            tokensIterator=tokens->begin();
            string temp=*tokensIterator;
            unsigned long proc = static_cast<unsigned long>(atoi(temp.c_str())-1);//numerotation starts from 1
            for (unsigned long i=0; i<PARTITION_DIM-1; i++){//WHATE is not exported
                tokensIterator++;
                temp=*tokensIterator;
                partitions[i][proc]=atol(temp.c_str());
            }
        }
        partitionStream->close();

    }
    for (unsigned long i=0; i<PARTITION_DIM-1; i++){
        for (unsigned long j=0; j<static_cast<unsigned long>(subdomains); j++){
            int64_t tmp=partitions[i][j];
            communicator->Bcast(&tmp);
            partitions[i][j]=tmp;
        }
    }
    return true;
}

vector<vector<int64_t> > alya::MPIOPartition::getPartitions() const
{
    return partitions;
}

vector<int64_t> alya::MPIOPartition::getPartitions(string resultsOn)
{
    if (!readFile){
        return vector<int64_t>();
    }
    unsigned long d=PARTITION_WHATE;
    if (AlyaString::Equal(resultsOn, MPIOHEADER_RES_ELEM)){
        d=PARTITION_ELEME;
    }else if (AlyaString::Equal(resultsOn, MPIOHEADER_RES_POINT)){
        d=PARTITION_POINT;
    }else if (AlyaString::Equal(resultsOn, MPIOHEADER_RES_BOUND)){
        d=PARTITION_BOUND;
    }else if (AlyaString::Equal(resultsOn, MPIOHEADER_RES_WHATE)){
        d=PARTITION_WHATE;
    }
    return partitions[d];
}

std::shared_ptr<alya::MPIOComm> alya::MPIOPartition::getCommunicator() const
{
    return communicator;
}
