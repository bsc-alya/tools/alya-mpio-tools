/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#ifndef MESSAGE_H
#define MESSAGE_H

#include <iostream>
#include <fstream>
#include <string>
#include <chrono>

//Message level

#ifdef MESSAGE_DEBUG
#define MESSAGE_INFO
#endif

#ifdef MESSAGE_INFO
#define MESSAGE_WARNING
#endif

#ifdef MESSAGE_WARNING
#define MESSAGE_ERROR
#endif

#ifdef MESSAGE_ERROR
#define MESSAGE_CRITICAL
#endif

using namespace std;
using namespace std::chrono;

namespace alya{

    class Message
    {
    public:
        static void Critical(string message);
        static void Debug(string message);
        static void Error(string message);
        static void Info(string message, int Level=0);
        static void Warning(string message);
        static void Progress(int64_t value, int64_t total, int actualization=0);
        static void Critical(int rank, string message);
        static void Debug(int rank, string message);
        static void Error(int rank, string message);
        static void Info(int rank, string message, int Level=0);
        static void Info(int Level=1);
        static void ResetInfo();
        static void Warning(int rank, string message);
        static void Progress(int rank, int64_t value, int64_t total, int actualization);
        static void IAM(int rank, string app);
        static void DONE(int rank);
        static void ARG(int rank);
        static void InitTime();
        static void PrintTime(int rank);
        static void Separator();
        static void Separator(int rank);
    private:
        static int Level;
        static int P;
        static high_resolution_clock::time_point t1;
    };

}

#endif // MESSAGE_H
