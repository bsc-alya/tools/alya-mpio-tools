/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#ifndef FILEREADER_H
#define FILEREADER_H

#include <string>
#include <vector>
#include <streampointer.h>
#include <inputfile.h>
#include <alyastring.h>
#include <message.h>

#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/trim_all.hpp>
#include <boost/tokenizer.hpp>

//#define __DEBUG
#define ALYA_INCLUDE "INCLUDE"

using namespace std;
using namespace boost;

namespace alya {

    class AlyaFileReader
    {
    public:
        AlyaFileReader(bool reader);
        ~AlyaFileReader();
        bool init(string initialFileName);
        bool readLine(bool noInclude=false);
        bool containField(string field);
        string argument(string field, bool reposition=true);
        bool pushFile(string fileName);
        bool popFile();

        string nextString();
        bool lastString();
        int stringNumber();

        std::shared_ptr<InputFile> getCurrentFile() const;

        string getProcessedLine() const;

        string getRawLine() const;

        int64_t getLineNumber() const;

    private:
        vector<AlyaPtr<InputFile> > files;
        AlyaPtr<InputFile> currentFile;
        string processedLine;
        string rawLine;
        string currentString;
        escaped_list_separator<char> sep;
        tokenizer<escaped_list_separator<char> > *tokens;
        tokenizer<escaped_list_separator<char> >::iterator iterator;
        tokenizer<escaped_list_separator<char> >::iterator iterator2;
        tokenizer<escaped_list_separator<char> >::iterator iterator3;
        std::vector<std::string> resultingTokens;
        bool initialized;
        bool reader;
    };

}

#endif // FILEREADER_H
