/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include "alyastring.h"



bool alya::AlyaString::Equal(string a, string b)
{
    boost::replace_all(a, "_","");
    boost::replace_all(b, "_","");
    if (a.length()>5){
        a=a.substr(0,5);
    }
    if (b.length()>5){
        b=b.substr(0,5);
    }
    boost::algorithm::to_upper(a);
    boost::algorithm::to_upper(b);

    return a.compare(b)==0;
}

bool alya::AlyaString::IsNumber(string s)
{
    return !s.empty() && std::find_if(s.begin(), s.end(), [](char c) { return !std::isdigit(c); }) == s.end();
}

bool alya::AlyaString::IsEnabled(string s)
{
    boost::to_upper(s);
    return s.compare("ON")==0||s.compare("YES")==0;
}

string alya::AlyaString::Format5(string s)
{
    if (s.length()>5){
        return s.substr(0,5);
    }else{
        return s;
    }
}

string alya::AlyaString::Integer8(int i)
{
    string it=to_string(i);
    if (it.size()<8){
        it=string(8-it.size(),'0').append(it);
    }
    return it;
}

string alya::AlyaString::Format5Integer8(string s, int i)
{

    string z=Format5(s)+"-"+Integer8(i);
    return z;
}

bool alya::AlyaString::HasSuffix(const string &s, const string &suffix)
{
    return (s.size() >= suffix.size()) && equal(suffix.rbegin(), suffix.rend(), s.rbegin());
}
