/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include "inputfile.h"


alya::InputFile::InputFile(string fileName):fileName(fileName), iline(0), lineNumber(0), lastline(-1)
{
    file=AlyaPtr_ifstream(new ifstream());
}

alya::InputFile::~InputFile()
{
    if (file->is_open()){
        file->close();
    }
}

bool alya::InputFile::open()
{
    file->open(fileName);
    if (!file->is_open()){
        return false;
    }
    return true;
}

bool alya::InputFile::close()
{
    file->close();
    return true;
}

bool alya::InputFile::readLine()
{
    string tline;
    if (iline==lastline){
        return false;
    }
    if (iline==BUFLINES||lineNumber==0){
        lines.clear();
        iline=0;
        for (int i=0; i<BUFLINES; i++){
            if (getline(*file, tline)){
                lines.push_back(tline);
            }else{
                lastline=i;
                break;
            }
        }
    }
    if (lastline==0){
        return false;
    }
    line=lines[static_cast<unsigned long>(iline)];
    iline++;
    lineNumber++;
    return true;
}

string alya::InputFile::getFileName() const
{
    return fileName;
}

string alya::InputFile::getLine() const
{
    return line;
}

int64_t alya::InputFile::getIline() const
{
    return iline;
}

std::shared_ptr<std::ifstream> alya::InputFile::getFile() const
{
    return file;
}

vector<string> alya::InputFile::getLines() const
{
    return lines;
}

int64_t alya::InputFile::getLastline() const
{
    return lastline;
}

int64_t alya::InputFile::getLineNumber() const
{
    return lineNumber;
}
