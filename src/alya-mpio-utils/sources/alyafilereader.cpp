/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include "alyafilereader.h"



alya::AlyaFileReader::AlyaFileReader(bool reader): reader(reader)
{
    if (reader){
        sep=boost::escaped_list_separator<char>('\\',' ','\"');
        tokens=new tokenizer<escaped_list_separator<char> >(processedLine, sep);
    }
}

alya::AlyaFileReader::~AlyaFileReader()
{
    if (reader){
        delete tokens;
    }
}

bool alya::AlyaFileReader::init(string initialFileName)
{
    if (reader){
        return pushFile(initialFileName);
    }else{
        currentFile=nullptr;
        return true;
    }
}

bool alya::AlyaFileReader::readLine(bool noInclude)
{
    if (reader){
        initialized=false;
        if (!currentFile->readLine()){
            if (!popFile()){
                return false;
            }
            return (readLine(noInclude));
        }
        processedLine=currentFile->getLine();
        rawLine=processedLine;
        trim_all(rawLine);
        trim_all(processedLine);
        replace_all(processedLine, "\t", " ");
        replace_all(processedLine, "=", " ");
        replace_all(processedLine, ":", " ");
        replace_all(processedLine, ",", " ");
        trim_all(processedLine);
        if (processedLine[0]=='$' || processedLine.compare("")==0){
            return readLine(noInclude);
        }
    //delete tokens;
        tokens->assign(processedLine, sep);
        iterator=tokens->begin();
        if (!noInclude){
            if (AlyaString::Equal(*iterator, ALYA_INCLUDE)){
                iterator++;
                pushFile(*iterator);
                return readLine(noInclude);
            }
        }
    #ifdef __DEBUG
        cout<<processedLine<<endl;
    #endif
    }
    return true;
}

bool alya::AlyaFileReader::containField(string field)
{
    if (reader){
        iterator2=tokens->begin();
        for (;!iterator2.at_end();iterator2++){
            if (AlyaString::Equal(*iterator2, field)){
                return true;
            }
        }
    }
    return false;
}

string alya::AlyaFileReader::argument(string field, bool reposition)
{
    if (reader){
        iterator2=tokens->begin();
        for (;!iterator2.at_end();iterator2++){
            if (AlyaString::Equal(*iterator2, field)){
                break;
            }
        }
        iterator3=iterator2;
        iterator3++;
        if (iterator2.at_end()){
            return "";
        }else if (iterator3.at_end()){
            return "";
        }else{
            iterator2++;
            if (reposition){
                iterator=iterator2;
            }
        return *iterator2;
        }
    }
    return nullptr;
}

bool alya::AlyaFileReader::pushFile(string fileName)
{
    if (reader){
        Message::Debug("Reading file: "+fileName);
        if (!files.empty()){
            files.pop_back();
            files.push_back(currentFile);
        }
        currentFile=AlyaPtr<InputFile>(new InputFile(fileName));
        if (!currentFile->open()){
            Message::Error("File "+fileName+" not found!");
            return false;
        }
        files.push_back(currentFile);
    }
    return true;
}

bool alya::AlyaFileReader::popFile()
{
    if (reader){
        currentFile->close();
        if (!files.empty()){
            files.pop_back();
        }else{
            Message::Warning("Unexpected end of file!");
            return false;
        }
        if (files.empty()){
            return false;
        }
        *currentFile=*(files[files.size()-1]);
        Message::Debug("Switching back to file: "+currentFile->getFileName());
    }
    return true;
}

string alya::AlyaFileReader::nextString()
{
    if (reader){
        if (initialized){
            iterator++;
        }
        initialized=true;
        currentString=*iterator;
        return currentString;
    }
    return nullptr;
}

bool alya::AlyaFileReader::lastString()
{
    if (reader){
        iterator2=iterator;
        ++iterator2;
        return iterator2.at_end();
    }
    return false;
}

int alya::AlyaFileReader::stringNumber()
{
    if (reader){
        resultingTokens=std::vector<std::string>(tokens->begin(), tokens->end());
        return static_cast<int>(resultingTokens.size());
    }
    return 0;
}

std::shared_ptr<alya::InputFile> alya::AlyaFileReader::getCurrentFile() const
{
    return currentFile;
}

string alya::AlyaFileReader::getProcessedLine() const
{
    return processedLine;
}

string alya::AlyaFileReader::getRawLine() const
{
    return rawLine;
}

int64_t alya::AlyaFileReader::getLineNumber() const
{
    if (reader){
        return currentFile->getLineNumber();
    }
    return 0;
}
