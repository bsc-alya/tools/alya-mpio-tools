/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include "message.h"


void alya::Message::Critical(string message)
{
#ifdef MESSAGE_CRITICAL
    cerr<<"!!!Critical!!!: "<<message<<endl;
#endif
}

void alya::Message::Debug(string message)
{
#ifdef MESSAGE_DEBUG
    cerr<<"Debug: "<<message<<endl;
#endif
}

void alya::Message::Error(string message)
{
#ifdef MESSAGE_ERROR
    cerr<<"!!Error!!: "<<message<<endl;
#endif
}

void alya::Message::Info(string message, int Level)
{
#ifdef MESSAGE_INFO
    Message::Info(Level);
    for (int i=0; i<alya::Message::Level; i++){
        cout<<"---";
    }
    if (alya::Message::Level!=0){
        cout<<" ";
    }
    cout<<message<<endl;
    Message::Info(-Level);
#endif
}

void alya::Message::Warning(string message)
{
#ifdef MESSAGE_WARNING
    cerr<<"!Warning!: "<<message<<endl;
#endif
}

void alya::Message::Progress(int64_t value, int64_t total, int actualization)
{
    int64_t i=(value*100)/total;
    if ((value==1)||(i>static_cast<int64_t>(P+actualization))){
        cout<<to_string(i)<<" %"<<flush;
        P=static_cast<int>(i);
    }
    if (value==total){
        cout<<endl;
        P=0;
    }else{
        cout<<"\r";
    }
}

void alya::Message::Critical(int rank, string message)
{
#ifdef MESSAGE_CRITICAL
    if (rank==0)
        Message::Critical(message);
#endif
}

void alya::Message::Debug(int rank, string message)
{
#ifdef MESSAGE_DEBUG
    if (rank==0)
        Message::Debug(message);
#endif
}

void alya::Message::Error(int rank, string message)
{
#ifdef MESSAGE_ERROR
    if (rank==0)
        Message::Error(message);
#endif
}

void alya::Message::Info(int rank, string message, int Level)
{
#ifdef MESSAGE_INFO
    if (rank==0){
        Message::Info(message, Level);
    }
#endif
}

void alya::Message::Info(int Level)
{
#ifdef MESSAGE_INFO
    alya::Message::Level+=Level;
    if (alya::Message::Level<0) alya::Message::Level=0;
#endif
}

void alya::Message::ResetInfo()
{
#ifdef MESSAGE_INFO
    alya::Message::Level=0;
#endif
}

void alya::Message::Warning(int rank, string message)
{
#ifdef MESSAGE_WARNING
    if (rank==0)
        Message::Warning(message);
#endif
}

void alya::Message::Progress(int rank, int64_t value, int64_t total, int actualization)
{
    if (rank==0)
        Message::Progress(value, total, actualization);
}

void alya::Message::IAM(int rank, string app)
{
    Message::Separator(rank);
    Message::Info(rank, app + " version "+string(__BUILD_VERSION__));
    Message::Separator(rank);
#ifndef NO_MPI
    Message::Info(rank, "Compiled with MPI");
#else
    Message::Info(rank, "Compiled without MPI");
#endif
}

void alya::Message::DONE(int rank)
{
    Message::Separator(rank);
    Message::Info(rank, "Done");
    Message::Separator(rank);
}

void alya::Message::ARG(int rank)
{
    Message::Separator(rank);
    Message::Info(rank, "Reading arguments", 1);
}

void alya::Message::InitTime()
{
    alya::Message::t1 = high_resolution_clock::now();

}

void alya::Message::PrintTime(int rank)
{
    high_resolution_clock::time_point t2 = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>( t2 - alya::Message::t1 ).count();
    Message::Separator(rank);
    Message::Info(rank,"Elapsed time: "+to_string(duration/1000000.0)+" seconds");
    Message::Separator(rank);
}

void alya::Message::Separator()
{
    Message::Info("-----------------------------------------");
}

void alya::Message::Separator(int rank)
{
    if (rank==0){
        Message::Separator();
    }
}

int alya::Message::Level = 0;
int alya::Message::P = 0;
high_resolution_clock::time_point alya::Message::t1;
