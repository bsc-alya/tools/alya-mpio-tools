/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include "domainmanager.h"

alya::DomainManager::DomainManager(AlyaPtr_MPIOComm communicator, AlyaPtr<AlyaFileReader> fileReader, int64_t mcodb): communicator(communicator), fileReader(fileReader), mcodb(mcodb)
{
    AlyaTypeDef::Init();
    boundaryElement=false;
    Message::Info("Domain file: "+fileReader->getCurrentFile()->getFileName(),1);
}

alya::DomainManager::~DomainManager()
{

}

bool alya::DomainManager::read()
{
    state=alya::domain::init;
    sectionLineNumber=0;
    while (fileReader->readLine()){
        currentFileLineNumber=fileReader->getCurrentFile()->getLineNumber();
        currentFileName=fileReader->getCurrentFile()->getFileName();
        switch(state){
            case alya::domain::init:
            {
                string temp=fileReader->nextString();
                if (AlyaString::Equal(temp,DOMAIN_DIMENSIONS)){
                    Message::Info("Reading dimensions", 1);
                    state=alya::domain::dimension;
                }else if (AlyaString::Equal(temp,"STRATEGY")){
                    Message::Info("Reading strategy", 1);
                    state=alya::domain::strategy;
                }
                break;
            }
            case alya::domain::dimension:
            {
                if (!readDimensions()){
                    return false;
                }
                break;
            }
            case alya::domain::strategy:
            {
                if (!readStrategy()){
                    return false;
                }
                if (state==alya::domain::end){
                    return true;
                }
                break;
            }
            case alya::domain::field:
            {
                if (!readFieldDimensions()){
                    return false;
                }
                break;
            }
            case alya::domain::step:
            {
                if (!readFieldSteps()){
                    return false;
                }
                break;
            }
            default:
                break;
        }
    }
    return true;
}

bool alya::DomainManager::readDimensions()
{
    string temp=fileReader->nextString();
    if (!AlyaString::Equal(temp, DOMAIN_END_DIMENSIONS)){
        if (AlyaString::Equal(temp, "NODAL_POINTS")){
            temp=fileReader->nextString();
            nodeCount = atol(temp.c_str());
            Message::Info("Nodal points: "+to_string(nodeCount), 2);
        }
        else if (AlyaString::Equal(temp, "ELEMENTS")){
            temp=fileReader->nextString();
            elementCount = atol(temp.c_str());
            Message::Info("Elements: "+to_string(elementCount), 2);
        }
        else if (AlyaString::Equal(temp, "SPACE_DIMENSIONS")){
            temp=fileReader->nextString();
            dimensions = atol(temp.c_str());
            Message::Info("Dimensions: "+to_string(dimensions), 2);
        }
        else if (AlyaString::Equal(temp, "BOUNDARIES")){
            temp=fileReader->nextString();
            boundaryCount = atol(temp.c_str());
            Message::Info("Boundaries: "+to_string(boundaryCount), 2);
        }
        else if (AlyaString::Equal(temp, "PERIODIC_NODES")){
            temp=fileReader->nextString();
            periodicNodes = atol(temp.c_str());
            Message::Info("Periodic nodes: "+to_string(periodicNodes), 2);
        }
        else if (AlyaString::Equal(temp, "MCODB")){
            temp=fileReader->nextString();
            if (mcodb==0){
                mcodb = atol(temp.c_str());
                Message::Info("MCODB: "+to_string(mcodb), 2);
            }else{
                Message::Warning("MCODB manually forced to "+to_string(mcodb));
            }
        }
        else if (AlyaString::Equal(temp, "TYPES_OF_ELEMENTS")){
            for (unsigned long i=0;!fileReader->lastString();i++){
                temp=fileReader->nextString();
                if (AlyaString::IsNumber(temp)){
                    elementTypes.push_back(atoi(temp.c_str()));
                }else{
                    elementTypes.push_back(static_cast<int>(AlyaTypeDef::String2types[temp]));
                }
                Message::Info("Element type: "+to_string(elementTypes[i]), 2);
            }
            Message::Info("Total element types: "+to_string(elementTypes.size()), 2);
        }
        else if (AlyaString::Equal(temp,"FIELDS")){
                Message::Info("Reading field dimensions", 2);
                state=alya::domain::field;
                temp=fileReader->nextString();
                fieldCount = atol(temp.c_str());
        }else{
            return true;
        }
    }else{
        if (elementTypes.size()==0){
            Message::Error("No type detected");
            return false;
        }else if (elementTypes.size()==1){
            uniqueType=true;
            maxNodes=(AlyaTypeDef::Types2nodes[static_cast<unsigned int>(elementTypes[0])]);
            Message::Info("Max nodes: "+to_string(maxNodes), 2);
        }else{
            uniqueType=false;
            maxNodes=1;
            for (auto const& it: elementTypes){
                maxNodes=max(maxNodes, static_cast<int64_t> (AlyaTypeDef::Types2nodes[static_cast<unsigned int>(it)]));
            }
            Message::Info("Max nodes: "+to_string(maxNodes), 2);
            if (mcodb==0){
                mcodb=MPIO_MCODB;
            }
        }
        state=alya::domain::init;
    }
    return true;
}

bool alya::DomainManager::readStrategy()
{
    string temp=fileReader->nextString();
    if (!AlyaString::Equal(temp, "END_STRATEGY")){
        if (AlyaString::Equal(temp, "BOUNDARY_ELEMENTS")){
            if (AlyaString::IsEnabled(fileReader->nextString())){
                boundaryElement=true;
            }
        }else{
            return true;
        }
    }else{
        state=alya::domain::end;
    }
    return true;
}

bool alya::DomainManager::readFieldDimensions()
{
    string temp=fileReader->nextString();
    if (AlyaString::Equal(temp, "TIMES")){
        state=alya::domain::step;
    }else  if (!AlyaString::Equal(temp, "END_FIELDS")){
        //FIELD:n
        if (fileReader->containField("FIELD")){
            temp=fileReader->argument("FIELD");
            //FILE NUMBER
            int64_t fieldNumber=atoi(temp.c_str());
            int64_t fieldDimension=0;
            string resultsOn="";
            string defaultValue="0";
            int64_t stepNumber=1;
            temp=fileReader->nextString();
            //DIMENSION
            if (fileReader->containField("DIMENSION")){
                //DIMENSION NUMBER
                temp=fileReader->argument("DIMENSION");
                fieldDimension=atoi(temp.c_str());
                //RESULT ON
                resultsOn=fileReader->nextString();
                if (fileReader->containField("DEFAULT")){
                    temp=fileReader->argument("DEFAULT");
                }
                if (fileReader->containField("STEPS")){
                    temp=fileReader->argument("STEPS");
                    stepNumber=atoi(temp.c_str());
                }
            }else{
                return false;
            }
            AlyaPtr<Field> field=AlyaPtr<Field>(new Field(fieldNumber, fieldDimension, resultsOn, stepNumber, defaultValue));
            fields.push_back(field);
        }
    }else{
        state=alya::domain::dimension;
    }
    return true;
}

bool alya::DomainManager::readFieldSteps()
{
    string temp=fileReader->nextString();
    if (!AlyaString::Equal(temp, "END_FIELD")){
        if (!AlyaString::Equal(temp, "END_TIMES")){
            (fields.operator [](fields.size()-1))->addStep(atof(temp.c_str()));
        }else{
            return true;
        }
    }else{
        state=alya::domain::field;
    }
    return true;
}

int64_t alya::DomainManager::getMcodb() const
{
    return mcodb;
}

bool alya::DomainManager::getBoundaryElement() const
{
    return boundaryElement;
}

vector<std::shared_ptr<alya::Field> > alya::DomainManager::getFields()
{
    return fields;
}

int64_t alya::DomainManager::getMaxNodes() const
{
    return maxNodes;
}

bool alya::DomainManager::getUniqueType() const
{
    return uniqueType;
}

int64_t alya::DomainManager::getBoundaryCount() const
{
    return boundaryCount;
}

vector<int> alya::DomainManager::getElementTypes() const
{
    return elementTypes;
}


int64_t alya::DomainManager::getDimensions() const
{
    return dimensions;
}


int64_t alya::DomainManager::getElementCount() const
{
    return elementCount;
}


int64_t alya::DomainManager::getNodeCount() const
{
    return nodeCount;
}
