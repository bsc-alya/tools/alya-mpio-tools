/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include <iostream>
#include <argumentmanager.h>
#include <message.h>
#include <ascii2mpio.h>
#include <domainmanager.h>
#include <alyafilereader.h>
#include <pointer.h>

using namespace std;
using namespace alya;
using namespace std::chrono;

int main(int argc, char* argv[])
{
    Message::InitTime();
    MPIO::Init(argc, argv);
    AlyaPtr_MPIOComm communicator(new MPIOComm());
    int rank=communicator->getRank();
    Message::IAM(rank, "Alya ASCII 2 MPI-IO converter");
    Message::Separator(rank);
    if (communicator->isSequential()){
       Message::Info(rank, "Sequential version");
    }else{
       Message::Info(rank, "Parallel version: "+to_string(communicator->getSize())+" processes");
       Message::Info(rank, "Switching to sequential execution and terminating additional processes");
       communicator=communicator->split(communicator->isMaster()?MPIO_WORKER:MPIO_NOT_WORKER);
       if (!communicator->isIOWorker()){
           MPIO::Finalize();
           return RETURN_OK;
       }
    }
    Message::Separator(rank);
    Message::Info(rank, "Reading arguments", 1);
    Message::Separator(rank);

    AlyaPtr<ArgumentManager> argumentManager(new ArgumentManager(communicator, argc, argv));
    if (argumentManager->getHelp()){
        argumentManager->usage();
        MPIO::Finalize();
        return RETURN_OK;

    }
    if (!argumentManager->getValid()){
        Message::Critical(communicator->getRank(), "Invalid arguments");
        MPIO::Abort();
        return RETURN_ERR_INPUT;
    }
    Message::Separator(rank);
    Message::Info(communicator->getRank(), "Reading domain");
    AlyaPtr<AlyaFileReader> fileReader(new AlyaFileReader(communicator->isMasterOrSequential()));
    if (!fileReader->init(argumentManager->getCaseName()+DOM_EXT)){
        Message::Critical(communicator->getRank(), "Cannot open domain file");
        argumentManager->usage();
        MPIO::Abort();
        return RETURN_ERR_FILES;
    }
    AlyaPtr<DomainManager> domainManager(new DomainManager(communicator, fileReader, argumentManager->getMcodb()));
    domainManager->read();
    Message::Separator(rank);
    Message::Info(communicator->getRank(), "Converting data");
    AlyaPtr<Ascii2mpio> converter(new Ascii2mpio(communicator, fileReader, domainManager, argumentManager->getCaseName(), argumentManager->getIntSize(), argumentManager->getNoProgress()));
    converter->convert();
    Message::DONE(rank);
    Message::PrintTime(rank);
    MPIO::Finalize();
    return RETURN_OK;
}
