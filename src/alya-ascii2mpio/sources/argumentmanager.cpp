/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include "argumentmanager.h"

alya::ArgumentManager::ArgumentManager(AlyaPtr_MPIOComm communicator, int argc, char **argv): caseName(""), communicator(communicator)
{
    intSize=4;
    geometryFile="";
    help=false;
    noProgress=false;
    onlyGeo=false;
    mcodb=0;
    if (argc<2){
        valid=false;
        help=true;
        me=string(argv[0]);
    }else{
        me=string(argv[0]);
        for (int i=1; i<argc; i++){
            string str=string(argv[i]);
            Message::Info();
            if (ARGUMENT(str, "-h", "--help")){
                help=true;
            }else if (ARGUMENT(str, "-i8", "--integers8")){
                intSize=8;
                Message::Info(communicator->getRank(), "Integer size: "+to_string(intSize));
            }else if (ARGUMENT(str, "-i4", "--integers4")){
                intSize=4;
                Message::Info(communicator->getRank(), "Integer size: "+to_string(intSize));
            }else if (ARGUMENT(str, "-np", "--no-progress")){
                noProgress=true;
                Message::Info(communicator->getRank(), "Progress counter disabled");
            }else if (ARGUMENT(str, "--mcodb", "--mcodb")){
                mcodb=atoi(argv[++i]);
                Message::Info(communicator->getRank(), "mcodb: "+to_string(mcodb));
            }else{
                caseName=str;
                Message::Info(communicator->getRank(), "Case name: "+caseName);
                valid=true;
            }
            Message::Info(-1);
        }
    }
    if (caseName.compare("")==0){
        valid=false;
    }
}

void alya::ArgumentManager::usage()
{
    Message::Info(communicator->getRank(), "Usage");
    Message::Info(communicator->getRank(), string(me.c_str())+" [OPTIONS] CASENAME");
    Message::Info(communicator->getRank(), "Options:");
    Message::Info(communicator->getRank(), "\t -h  --help:             print the help");
    Message::Info(communicator->getRank(), "\t -i4 --integers4:        export integers with a size of 4 bytes (default)");
    Message::Info(communicator->getRank(), "\t -i8 --integers8:        export integers with a size of 8 bytes");
    Message::Info(communicator->getRank(), "\t -np --no-progress:      do not print progress counter");
    Message::Info(communicator->getRank(), "\t -mcodb [int]:           force mcodb value");
}

string alya::ArgumentManager::getCaseName() const
{
    return caseName;
}

int alya::ArgumentManager::getIntSize() const
{
    return intSize;
}

string alya::ArgumentManager::getGeometryFile() const
{
    return geometryFile;
}

int alya::ArgumentManager::getMcodb() const
{
    return mcodb;
}

bool alya::ArgumentManager::getOnlyGeo() const
{
    return onlyGeo;
}

bool alya::ArgumentManager::getNoProgress() const
{
    return noProgress;
}

bool alya::ArgumentManager::getHelp() const
{
    return help;
}

bool alya::ArgumentManager::getValid() const
{
    return valid;
}
