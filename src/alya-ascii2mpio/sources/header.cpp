/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include "header.h"

const int64_t alya::Header::H_MagicNumber=MPIOHEADER_MAGICNUMBER;
const char* alya::Header::H_AlignChar64=MPIOHEADER_ALIGN_CHAR64;
const char* alya::Header::H_Format=MPIOHEADER_FORMAT;
const char* alya::Header::H_Version=MPIOHEADER_VERSION;

alya::Header::Header()
{
    for (int i=0; i<MPIOHEADER_OPTION_SIZE; i++){
        h_options[i]=MPIOHEADER_OPT_EMPTY;
    }
}

void alya::Header::writeTo(AlyaPtr_ofstream stream)
{
    stream->seekp(ios_base::beg);
    stream->write(reinterpret_cast<char*>(H_MagicNumber), sizeof(H_MagicNumber));
    stream->write(H_Format, sizeof(H_Format));
    stream->write(H_Version, sizeof(H_Version));
    stream->write(h_object, sizeof(h_object));
    stream->write(h_dimension, sizeof(h_dimension));
    stream->write(h_results_on, sizeof(h_results_on));
    stream->write(h_type, sizeof(h_type));
    stream->write(h_size, sizeof(h_size));
    stream->write(h_parallel, sizeof(h_parallel));
    stream->write(h_filter, sizeof(h_filter));
    stream->write(h_sorting, sizeof(h_sorting));
    stream->write(h_id, sizeof(h_id));
    stream->write(H_AlignChar64, sizeof(H_AlignChar64));
    stream->write(reinterpret_cast<char*>(&h_columns), sizeof(h_columns));
    stream->write(reinterpret_cast<char*>(&h_lines), sizeof(h_lines));
    stream->write(reinterpret_cast<char*>(&h_ittim), sizeof(h_ittim));
    stream->write(reinterpret_cast<char*>(&h_nsubd), sizeof(h_nsubd));
    stream->write(reinterpret_cast<char*>(&h_divi), sizeof(h_divi));
    stream->write(reinterpret_cast<char*>(&h_tag1), sizeof(h_tag1));
    stream->write(reinterpret_cast<char*>(&h_tag2), sizeof(h_tag2));
    stream->write(reinterpret_cast<char*>(&h_time), sizeof(h_time));
    stream->write(H_AlignChar64, sizeof(H_AlignChar64));
    for (int i=0; i<10; i++){
        stream->write(h_options[i], sizeof(h_options[i]));
    }
}
