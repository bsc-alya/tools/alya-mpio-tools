/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include "ascii2mpio.h"


alya::Ascii2mpio::Ascii2mpio(AlyaPtr_MPIOComm communicator, AlyaPtr<AlyaFileReader> fileReader, AlyaPtr<DomainManager> domainManager, string caseName, int intSize, bool noProgress):
    communicator(communicator), fileReader(fileReader), domainManager(domainManager), caseName(caseName), intSize(intSize), noProgress(noProgress), mcodb1(domainManager->getMcodb()+1)
{
    dimensions=domainManager->getDimensions();
    uniqueType=domainManager->getUniqueType();
    AlyaTypeDef::Init();
    defaultValue="0";
    lelbo=false;
}

bool alya::Ascii2mpio::convert()
{
    string temp;
    state=alya::state::init;
    itemNumber=0;
    lineNumber=0;
    previousId=0;
    outputStream=AlyaPtr_ofstream(new ofstream());
    while(fileReader->readLine(previousId!=0)){
        lineNumber=fileReader->getLineNumber();
        switch(state){
            case alya::state::init:
            {
                temp=fileReader->nextString();
                if (AlyaString::Equal(temp,"GEOMETRY")){
                    Message::Info();
                    Message::Info("Section: Geometry");
                    state=alya::state::geometry;
                    outputStream->open(caseName+GEOME_EXT);
                }
                else if (AlyaString::Equal(temp,"SETS")){
                    Message::Info();
                    Message::Info("Section: Sets");
                    state=alya::state::sets;
                    outputStream->open(caseName+SETS_EXT);
                }
                else if (AlyaString::Equal(temp,"BOUNDARY_CONDITIONS")){
                    Message::Info();
                    Message::Info("Section: Bcs");
                    state=alya::state::bcs;
                    outputStream->open(caseName+BCS_EXT);
                }
                else if (AlyaString::Equal(temp,"FIELDS")){
                    Message::Info();
                    Message::Info("Section: Fields");
                    state=alya::state::fields;
                    outputStream->open(caseName+FIELD_EXT);
                }
                break;
            }
            case alya::state::geometry:
            {
                previousId=0;
                Message::Info();
                temp=fileReader->nextString();
                if (AlyaString::Equal(temp, "END_GEOMETRY")){
                    Message::Info(-1);
                    state=alya::state::init;
                    outputStream->close();
                }
                else if (AlyaString::Equal(temp,GEOM_NODES_PER_ELEMENT)){
                    Message::Info("Converting nodes per element");
                    searchDefault();
                    STOP_ON_ERROR(initGenericIntScalars(caseName+LTYPE_EXT, MPIOHEADER_OBJ_LTYPE, MPIOHEADER_RES_ELEM, alya::state::ltypeNPE))
                }
                else if (AlyaString::Equal(temp,GEOM_TYPES)){
                    searchDefault();
                    if (uniqueType||fileReader->containField("ALL")){
                        Message::Warning("Only one type: skipping type conversion");
                    }else if (fileReader->containField("BOUNDARIES")){
                        Message::Info("Converting boundary types");
                        STOP_ON_ERROR(initGenericIntScalars(caseName+LTYPB_EXT, MPIOHEADER_OBJ_LTYPB, MPIOHEADER_RES_BOUND, alya::state::ltypb))
                    }else{
                        Message::Info("Converting types");
                        STOP_ON_ERROR(initGenericIntScalars(caseName+LTYPE_EXT, MPIOHEADER_OBJ_LTYPE, MPIOHEADER_RES_ELEM, alya::state::ltype))
                    }
                }
                else if (AlyaString::Equal(temp,GEOM_ELEMENTS)){
                    Message::Info("Converting elements");
                    searchDefault();
                    STOP_ON_ERROR(initLnods())
                }
                else if (AlyaString::Equal(temp,GEOM_COORDINATES)){
                    Message::Info("Converting coordinates");
                    searchDefault();
                    STOP_ON_ERROR(initCoord())
                }
                else if (AlyaString::Equal(temp,GEOM_BOUNDARIES)){
                    Message::Info("Converting boundaries");
                    searchDefault();
                    bool element=domainManager->getBoundaryElement()||fileReader->containField(GEOM_OPT_BOUND_ELEMENT);
                    STOP_ON_ERROR(initLnodb(element))
                }
                else if (AlyaString::Equal(temp, "MATERIALS")){
                    Message::Info("Converting materials");
                    searchDefault("1");
                    STOP_ON_ERROR(initGenericIntScalars(caseName+LMATE_EXT, MPIOHEADER_OBJ_LMATE, MPIOHEADER_RES_ELEM, alya::state::lmate))
                }
                else if (AlyaString::Equal(temp, "LMAST")){
                    Message::Info("Converting periodic nodes");
                    searchDefault("0");
                    STOP_ON_ERROR(initGenericIntScalars(caseName+LMAST_EXT, MPIOHEADER_OBJ_LMAST, MPIOHEADER_RES_POINT, alya::state::lmast))
                }
                else if (AlyaString::Equal(temp, "PERIODIC")){
                    Message::Warning("PERIODIC is deprecated and will not be converted! Use the periodic2lmast tool to update your geometry file.");
                }else if (AlyaString::Equal(temp, "SUBDOMAIN")){
                    Message::Info("Converting subdomains");
                    searchDefault("1");
                    STOP_ON_ERROR(initGenericIntScalars(caseName+LESUB_EXT, MPIOHEADER_OBJ_LESUB, MPIOHEADER_RES_ELEM, alya::state::lesub))
                }else if (AlyaString::Equal(temp, "LELBO")){
                    Message::Info("Converting boundary elements");
                    searchDefault();
                    STOP_ON_ERROR(initLelbo())
                }else{
                    *outputStream<<fileReader->getRawLine()<<std::endl;
                }
                Message::Info(-1);
                break;
            }
            case alya::state::sets:
            {
                previousId=0;
                Message::Info();
                temp=fileReader->nextString();
                if (AlyaString::Equal(temp,"END_SETS")){
                    Message::Info(-1);
                    state=alya::state::init;
                    outputStream->close();
                }else if (AlyaString::Equal(temp,"ELEMENTS")){
                    Message::Info("Converting element sets");
                    searchDefault();
                    STOP_ON_ERROR(initGenericIntScalars(caseName+LESET_EXT, MPIOHEADER_OBJ_LESET, MPIOHEADER_RES_ELEM, alya::state::leset))
                }else if (AlyaString::Equal(temp,"BOUNDARIES")){
                    searchDefault();
                    Message::Info("Converting boundary sets");
                    STOP_ON_ERROR(initGenericIntScalars(caseName+LBSET_EXT, MPIOHEADER_OBJ_LBSET, MPIOHEADER_RES_BOUND, alya::state::lbset))
                }else if (AlyaString::Equal(temp,"NODES")){
                    Message::Info("Converting node sets");
                    searchDefault();
                    if (fileReader->containField("OLD_FORMAT")){
                        Message::Critical("Old node set format detected");
                        return false;
                    }
                    STOP_ON_ERROR(initGenericIntScalars(caseName+LNSET_EXT, MPIOHEADER_OBJ_LNSET, MPIOHEADER_RES_POINT, alya::state::lnset))
                }
                Message::Info(-1);
                break;
            }
            case alya::state::bcs:
            {
                previousId=0;
                Message::Info();
                temp=fileReader->nextString();
                if (AlyaString::Equal(temp,"END_BOUNDARY_CONDITIONS")){
                    Message::Info(-1);
                    state=alya::state::init;
                    outputStream->close();
                }else if (AlyaString::Equal(temp,"ON_BOUNDARIES")){
                    Message::Info("Converting boundary conditions (boundaries)");
                    searchDefault();
                    STOP_ON_ERROR(initGenericIntScalars(caseName+CODBO_EXT, MPIOHEADER_OBJ_CODBO, MPIOHEADER_RES_BOUND, alya::state::codbo))
                }else if (AlyaString::Equal(temp,"ON_NODES")){
                    Message::Info("Converting boundary conditions (nodes)");
                    searchDefault(to_string(mcodb1));
                    STOP_ON_ERROR(initCodno())
                }
                Message::Info(-1);
                break;
            }
            case alya::state::fields:
            {
                previousId=0;
                Message::Info();
                temp=fileReader->nextString();
                if (AlyaString::Equal(temp,"END_FIELDS")){
                    Message::Info(-1);
                    state=alya::state::init;
                    outputStream->close();
                }else if (AlyaString::Equal(temp,"FIELD")){
                    temp=fileReader->nextString();
                    currentFieldNumber=atoi(temp.c_str());
                    Message::Info("Converting field "+to_string(currentFieldNumber));
                    STOP_ON_ERROR(initField(currentFieldNumber))
                }
                Message::Info(-1);
                break;
            }
            case alya::state::steps:
            {
                previousId=0;
                Message::Info();
                temp=fileReader->nextString();
                if (AlyaString::Equal(temp,"END_FIELD")){
                    state=alya::state::fields;
                    outputStream->close();
                }else if (AlyaString::Equal(temp,"STEP")){
                    temp=fileReader->nextString();
                    int step=atoi(temp.c_str());
                    Message::Info();
                    Message::Info("Converting step "+to_string(step));
                    Message::Info(-1);
                    STOP_ON_ERROR(initField(currentFieldNumber, step))
                }
                Message::Info(-1);
                break;
            }

            //GEO STATES

            case alya::state::coord:
                STOP_ON_ERROR(writeCoord())
                break;
            case alya::state::ltype:
                STOP_ON_ERROR(writeLtype())
                break;
            case alya::state::ltypeNPE:
                STOP_ON_ERROR(writeLtypeNPE())
                break;
            case alya::state::lnods:
                STOP_ON_ERROR(writeLnods())
                break;
            case alya::state::lnodb:
                STOP_ON_ERROR(writeLnodb())
                break;
            case alya::state::ltypb:
                STOP_ON_ERROR(writeIntegers("END_TYPES", domainManager->getBoundaryCount(), alya::state::geometry))
                break;
            case alya::state::lmate:
                STOP_ON_ERROR(writeIntegers("END_MATERIALS", domainManager->getElementCount(), alya::state::geometry))
                break;
            case alya::state::lmast:
                STOP_ON_ERROR(writeIntegers("END_LMAST", domainManager->getNodeCount(), alya::state::geometry))
                break;
            case alya::state::lesub:
                STOP_ON_ERROR(writeIntegers("END_SUBDOMAIN", domainManager->getElementCount(), alya::state::geometry))
                break;
            case alya::state::lelbo:
                STOP_ON_ERROR(writeIntegers("END_LELBO", domainManager->getBoundaryCount(), alya::state::geometry))
                break;

            //CHARACTERISTICS

            case alya::state::lnoch:
                STOP_ON_ERROR(writeIntegers("END_CHARACTERISTICS", domainManager->getNodeCount(), alya::state::geometry))
                break;

            case alya::state::lelch:
                STOP_ON_ERROR(writeIntegers("END_CHARACTERISTICS", domainManager->getElementCount(), alya::state::geometry))
                break;

            case alya::state::lboch:
                STOP_ON_ERROR(writeIntegers("END_CHARACTERISTICS", domainManager->getBoundaryCount(), alya::state::geometry))
                break;

            //SET STATES

            case alya::state::leset:
                STOP_ON_ERROR(writeIntegers("END_ELEMENT", domainManager->getElementCount(), alya::state::sets))
                break;
            case alya::state::lbset:
                STOP_ON_ERROR(writeIntegers("END_BOUNDARIES", domainManager->getBoundaryCount(), alya::state::sets))
                break;
            case alya::state::lnset:
                STOP_ON_ERROR(writeIntegers("END_NODES", domainManager->getNodeCount(), alya::state::sets))
                break;

            //BCS STATES

            case alya::state::codbo:
                STOP_ON_ERROR(writeIntegers("END_ON_BOUNDARIES", domainManager->getBoundaryCount(), alya::state::bcs))
                break;
            case alya::state::codno:
                STOP_ON_ERROR(writeIntegers("END_ON_NODES", domainManager->getNodeCount(), alya::state::bcs))
                break;

            //FIELD STATES

            case alya::state::field:
                STOP_ON_ERROR(writeFloats("END_FIELD", currentCount, alya::state::fields))
                break;
            case alya::state::step:
                STOP_ON_ERROR(writeFloats("END_STEP", currentCount, alya::state::steps))
                break;

            default:
                //nothing
                break;
        }
    }
    return true;
}


bool alya::Ascii2mpio::initCoord()
{
    string file=string(caseName+COORD_EXT);
    itemNumber=0;
    float8file=AlyaPtr<MPIOFile<double> >(new MPIOFile<double> (communicator, file));
    STOP_ON_ERROR_MSG(float8file->setHeader(MPIOHEADER_OBJ_COORD, MPIOHEADER_RES_POINT, dimensions, domainManager->getNodeCount()), Message::Critical("File "+file+": Cannot set the header"))
    STOP_ON_ERROR_MSG(float8file->open(WRITE), Message::Critical("File "+file+": Cannot open"))
    STOP_ON_ERROR_MSG(float8file->initWriting(false), Message::Critical("File "+file+": Cannot start writing"))
    state=alya::state::coord;
    return true;
}

bool alya::Ascii2mpio::initLnods()
{
    string file=string(caseName+LNODS_EXT);
    itemNumber=0;
    if (intSize==4){
        int4file=AlyaPtr<MPIOFile<int32_t> >(new MPIOFile<int32_t> (communicator, file));
        STOP_ON_ERROR_MSG(int4file->setHeader(MPIOHEADER_OBJ_LNODS, MPIOHEADER_RES_ELEM, domainManager->getMaxNodes(), domainManager->getElementCount()), Message::Critical("File "+file+": Cannot set the header"))
        STOP_ON_ERROR_MSG(int4file->open(WRITE), Message::Critical("File "+file+": Cannot open"))
        STOP_ON_ERROR_MSG(int4file->initWriting(false), Message::Critical("File "+file+": Cannot start writing"))
    }else if (intSize==8){
        int8file=AlyaPtr<MPIOFile<int64_t> >(new MPIOFile<int64_t> (communicator, file));
        STOP_ON_ERROR_MSG(int8file->setHeader(MPIOHEADER_OBJ_LNODS, MPIOHEADER_RES_ELEM, domainManager->getMaxNodes(), domainManager->getElementCount()), Message::Critical("File "+file+": Cannot set the header"))
        STOP_ON_ERROR_MSG(int8file->open(WRITE), Message::Critical("File "+file+": Cannot open"))
        STOP_ON_ERROR_MSG(int8file->initWriting(false), Message::Critical("File "+file+": Cannot start writing"))
    }
    state=alya::state::lnods;
    return true;
}

bool alya::Ascii2mpio::initLnodb(bool element)
{
    string file=string(caseName+LNODB_EXT);
    itemNumber=0;
    if (intSize==4){
        int4file=AlyaPtr<MPIOFile<int32_t> >(new MPIOFile<int32_t> (communicator, file));
        STOP_ON_ERROR_MSG(int4file->setHeader(MPIOHEADER_OBJ_LNODB, MPIOHEADER_RES_BOUND, domainManager->getMaxNodes()-1, domainManager->getBoundaryCount()), Message::Critical("File "+file+": Cannot set the header"))
        STOP_ON_ERROR_MSG(int4file->open(WRITE), Message::Critical("File "+file+": Cannot open"))
        STOP_ON_ERROR_MSG(int4file->initWriting(false), Message::Critical("File "+file+": Cannot start writing"))
    }else if (intSize==8){
        int8file=AlyaPtr<MPIOFile<int64_t> >(new MPIOFile<int64_t> (communicator, file));
        STOP_ON_ERROR_MSG(int8file->setHeader(MPIOHEADER_OBJ_LNODB, MPIOHEADER_RES_BOUND, domainManager->getMaxNodes()-1, domainManager->getBoundaryCount()), Message::Critical("File "+file+": Cannot set the header"))
        STOP_ON_ERROR_MSG(int8file->open(WRITE), Message::Critical("File "+file+": Cannot open"))
        STOP_ON_ERROR_MSG(int8file->initWriting(false), Message::Critical("File "+file+": Cannot start writing"))
    }
    if (lelbo){
        Message::Error("LELBO defined twice!");
        return false;
    }
    if (element){
        lelbo=true;
        string file=string(caseName+LELBO_EXT);
        if (intSize==4){
            lelbo4file=AlyaPtr<MPIOFile<int32_t> >(new MPIOFile<int32_t> (communicator, file));
            STOP_ON_ERROR_MSG(lelbo4file->setHeader(MPIOHEADER_OBJ_LELBO, MPIOHEADER_RES_BOUND, 1, domainManager->getBoundaryCount()), Message::Critical("File "+file+": Cannot set the header"))
            STOP_ON_ERROR_MSG(lelbo4file->open(WRITE), Message::Critical("File "+file+": Cannot open"))
            STOP_ON_ERROR_MSG(lelbo4file->initWriting(false), Message::Critical("File "+file+": Cannot start writing"))
        }else if (intSize==8){
            lelbo8file=AlyaPtr<MPIOFile<int64_t> >(new MPIOFile<int64_t> (communicator, file));
            STOP_ON_ERROR_MSG(lelbo8file->setHeader(MPIOHEADER_OBJ_LELBO, MPIOHEADER_RES_BOUND, 1, domainManager->getBoundaryCount()), Message::Critical("File "+file+": Cannot set the header"))
            STOP_ON_ERROR_MSG(lelbo8file->open(WRITE), Message::Critical("File "+file+": Cannot open"))
            STOP_ON_ERROR_MSG(lelbo8file->initWriting(false), Message::Critical("File "+file+": Cannot start writing"))
        }
    }
    state=alya::state::lnodb;
    return true;
}

bool alya::Ascii2mpio::initLelbo()
{
    lelbo=true;
    STOP_ON_ERROR(initGenericIntScalars(caseName+LELBO_EXT, MPIOHEADER_OBJ_LELBO, MPIOHEADER_RES_BOUND, alya::state::lelbo))
    return true;
}

bool alya::Ascii2mpio::initCodno()
{
    string file=string(caseName+CODNO_EXT);
    itemNumber=0;
    if (intSize==4){
        int4file=AlyaPtr<MPIOFile<int32_t> >(new MPIOFile<int32_t> (communicator, file));
        STOP_ON_ERROR_MSG(int4file->setHeader(MPIOHEADER_OBJ_CODNO, MPIOHEADER_RES_POINT, MPIO_NCONO, domainManager->getNodeCount()), Message::Critical("File "+file+": Cannot set the header"))
        STOP_ON_ERROR_MSG(int4file->open(WRITE), Message::Critical("File "+file+": Cannot open"))
        STOP_ON_ERROR_MSG(int4file->initWriting(false), Message::Critical("File "+file+": Cannot start writing"))
    }else if (intSize==8){
        int8file=AlyaPtr<MPIOFile<int64_t> >(new MPIOFile<int64_t> (communicator, file));
        STOP_ON_ERROR_MSG(int8file->setHeader(MPIOHEADER_OBJ_CODNO, MPIOHEADER_RES_POINT, MPIO_NCONO, domainManager->getNodeCount()), Message::Critical("File "+file+": Cannot set the header"))
        STOP_ON_ERROR_MSG(int8file->open(WRITE), Message::Critical("File "+file+": Cannot open"))
        STOP_ON_ERROR_MSG(int8file->initWriting(false), Message::Critical("File "+file+": Cannot start writing"))
    }
    state=alya::state::codno;
    return true;
}

bool alya::Ascii2mpio::initField(int64_t number)
{
    itemNumber=0;
    currentField=domainManager->getFields().at(static_cast<unsigned long>(number-1));
    if (currentField->getSteps()==1){
        STOP_ON_ERROR(initField(number, 1))
        state=alya::state::field;
    }else{
        state=alya::state::steps;
    }
    return true;
}

bool alya::Ascii2mpio::initField(int64_t fieldNumber, int64_t fieldStep)
{
    defaultValue=currentField->getDefaultValue();
    string file=string(caseName+"-XFIEL."+AlyaString::Integer8(static_cast<int>(fieldNumber))+"."+AlyaString::Integer8(static_cast<int>(fieldStep))+MPIO_EXT);
    string on=currentField->getResultsOn();
    if (AlyaString::Equal("NODE",on)||AlyaString::Equal("NODES",on)){
        on=MPIOHEADER_RES_POINT;
        currentCount=domainManager->getNodeCount();
    }else if(AlyaString::Equal("ELEMENTS",on)){
        on=MPIOHEADER_RES_ELEM;
        currentCount=domainManager->getElementCount();
    }else if(AlyaString::Equal("BOUNDARIES",on)){
        on=MPIOHEADER_RES_BOUND;
        currentCount=domainManager->getBoundaryCount();
    }else{
        Message::Critical("Unknown dimension: "+on);
        return false;
    }
    itemNumber=0;
    float8file=AlyaPtr<MPIOFile<double> >(new MPIOFile<double> (communicator, file));
    STOP_ON_ERROR_MSG(float8file->setHeader(MPIOHEADER_OBJ_XFIEL, on, currentField->getDimension(), currentCount, 0, fieldStep>0?currentField->getStepList().at(static_cast<unsigned long>(fieldStep-1)):0, 0, fieldNumber, fieldStep), Message::Critical("File "+file+": Cannot set the header"))
    STOP_ON_ERROR_MSG(float8file->open(WRITE), Message::Critical("File "+file+": Cannot open"))
    STOP_ON_ERROR_MSG(float8file->initWriting(false), Message::Critical("File "+file+": Cannot start writing"))
    state=alya::state::step;
    return true;
}

bool alya::Ascii2mpio::initGenericIntScalars(string file, string obj, string res, alya::state::States st)
{
    itemNumber=0;
    int64_t genericCount=0;
    if (AlyaString::Equal(res,MPIOHEADER_RES_POINT)){
        genericCount=domainManager->getNodeCount();
    }else if (AlyaString::Equal(res,MPIOHEADER_RES_ELEM)){
        genericCount=domainManager->getElementCount();
    }else if (AlyaString::Equal(res,MPIOHEADER_RES_BOUND)){
        genericCount=domainManager->getBoundaryCount();
    }else{
        Message::Critical("Bad Results On");
        return false;
    }
    if (intSize==4){
        int4file=AlyaPtr<MPIOFile<int32_t> >(new MPIOFile<int32_t> (communicator, file));
        STOP_ON_ERROR_MSG(int4file->setHeader(obj, res, 1, genericCount), Message::Critical("File "+file+": Cannot set the header"))
        STOP_ON_ERROR_MSG(int4file->open(WRITE), Message::Critical("File "+file+": Cannot open"))
        STOP_ON_ERROR_MSG(int4file->initWriting(false), Message::Critical("File "+file+": Cannot start writing"))
    }else if (intSize==8){
        int8file=AlyaPtr<MPIOFile<int64_t> >(new MPIOFile<int64_t> (communicator, file));
        STOP_ON_ERROR_MSG(int8file->setHeader(obj, res, 1, genericCount), Message::Critical("File "+file+": Cannot set the header"))
        STOP_ON_ERROR_MSG(int8file->open(WRITE), Message::Critical("File "+file+": Cannot open"))
        STOP_ON_ERROR_MSG(int8file->initWriting(false), Message::Critical("File "+file+": Cannot start writing"))
    }
    state=st;
    return true;
}

bool alya::Ascii2mpio::writeCoord()
{
    string temp=fileReader->nextString();
    if (!AlyaString::Equal(temp,GEOM_END_COORDINATES)){
        double val = atof(temp.c_str());
        if (!noProgress){
            Message::Progress(static_cast<int64_t>(val), domainManager->getNodeCount());
        }
        if (static_cast<int64_t>(val)<=previousId){
            Message::Critical("Line "+ to_string(lineNumber) + ": items are not sorted");
            return false;
        }
        previousId=static_cast<int64_t>(val);
        int64_t i;
        for (i=0;!fileReader->lastString();i++){
            string temp=fileReader->nextString();
            double dval = atof(temp.c_str());
            if (!float8file->write(dval)){
                Message::Critical("Line "+ to_string(lineNumber) + ": cannot write COORD file");
                return false;
            }
        }
        if (dimensions!=i){
            Message::Critical("Line "+ to_string(lineNumber) + ": no consistent dimensions");
            return false;
        }
        itemNumber++;
    }else{
        if (float8file->getHeader()->getLines()!=itemNumber){
            Message::Critical("Node number not coherent with domain file");
            return false;
        }
        float8file->close();
        state=alya::state::geometry;
        previousId=0;
    }
    return true;
}

bool alya::Ascii2mpio::writeLtypeNPE()
{
    string temp=fileReader->nextString();
    if (!AlyaString::Equal(temp,GEOM_END_NODES_PER_ELEMENT)){
        int64_t val = atol(temp.c_str());
        if (!noProgress){
            Message::Progress(val, domainManager->getElementCount());
        }
        if (val<=previousId){
            Message::Critical("Line "+ to_string(lineNumber) + ": items are not sorted");
            return false;
        }
        previousId=val;
        temp=fileReader->nextString();
        val = AlyaTypeDef::Nodes2types[dimensions][static_cast<unsigned int>(atoi(temp.c_str()))];
        if (intSize==8){
            int8file->write(val);
        }else{
            int32_t valCast=static_cast<int32_t>(val);
            int4file->write(valCast);
        }
        itemNumber++;
    }else{
        if (intSize==8?int8file->getHeader()->getLines()!=itemNumber:int4file->getHeader()->getLines()!=itemNumber){
            Message::Critical("Type number not coherent with domain file");
            return false;
        }
        if (intSize==8){
            int8file->close();
        }else{
            int4file->close();
        }
        state=alya::state::geometry;
        previousId=0;
    }
    return true;
}

bool alya::Ascii2mpio::writeLtype()
{
    string temp=fileReader->nextString();
    if (!AlyaString::Equal(temp,GEOM_END_TYPES)){
        int64_t val = atol(temp.c_str());
        if (!noProgress){
            Message::Progress(val, domainManager->getElementCount());
        }
        if (val<=previousId){
            Message::Critical("Line "+ to_string(lineNumber) + ": items are not sorted");
            return false;
        }
        previousId=val;
        temp=fileReader->nextString();
        val = atol(temp.c_str());
        if (intSize==8){
            int8file->write(val);
        }else{
            int32_t valCast=static_cast<int32_t>(val);
            int4file->write(valCast);
        }
        itemNumber++;
    }else{
        if (intSize==8?int8file->getHeader()->getLines()!=itemNumber:int4file->getHeader()->getLines()!=itemNumber){
            Message::Critical("Type number not coherent with domain file");
            return false;
        }
        if (intSize==8){
            int8file->close();
        }else{
            int4file->close();
        }
        state=alya::state::geometry;
        previousId=0;
    }
    return true;
}

bool alya::Ascii2mpio::writeLnods()
{
    string temp=fileReader->nextString();
    if (!AlyaString::Equal(temp,GEOM_END_ELEMENTS)){
        int64_t val = atoi(temp.c_str());
        if (!noProgress){
            Message::Progress(val, domainManager->getElementCount());
        }
        if (val<=previousId){
            Message::Critical("Line "+ to_string(lineNumber) + ": items are not sorted");
            return false;
        }
        previousId=val;
        int64_t i;
        for (i=0;!fileReader->lastString();i++){
            if (intSize==8?i>int8file->getHeader()->getColumns():i>int4file->getHeader()->getColumns()){
                Message::Critical("Line "+ to_string(lineNumber) + ": too many nodes:" + to_string(i) + ", max value: " + to_string(intSize==8?int8file->getHeader()->getColumns():int4file->getHeader()->getColumns()));
                return false;
            }
            temp=fileReader->nextString();
            val = atol(temp.c_str());
            if (intSize==8){
                int8file->write(val);
            }else{
                int32_t valCast=static_cast<int32_t>(val);
                int4file->write(valCast);
            }
            if (val>domainManager->getNodeCount()){
                Message::Warning("Line "+ to_string(lineNumber) + ": node "+ to_string(i) + " ("+to_string(val)+") is greater than the node count");
            }
            if (val==0){
                Message::Warning("Line "+ to_string(lineNumber) + ": node "+ to_string(i) + " ("+to_string(val)+") is equal to 0");
            }
        }
        bool testNodes=false;
        for (auto it: domainManager->getElementTypes()){
            if (i==AlyaTypeDef::Types2nodes[static_cast<unsigned int>(it)]){
                testNodes=true;
            }
        }
        if (testNodes==false){
            Message::Critical("Line "+ to_string(lineNumber) + ": element number not coherent with domain file");
            return false;
        }
        for (;intSize==8?i<int8file->getHeader()->getColumns():i<int4file->getHeader()->getColumns();i++){
            val=0;
            if (intSize==8){
                int8file->write(val);
            }else{
                int32_t valCast=static_cast<int32_t>(val);
                int4file->write(valCast);
            }
        }
        itemNumber++;
    }else{
        if (intSize==8?int8file->getHeader()->getLines()!=itemNumber:int4file->getHeader()->getLines()!=itemNumber){
            Message::Critical("Line "+ to_string(lineNumber) + ": Element number not coherent with domain file");
            return false;
        }
        if (intSize==8){
            int8file->close();
        }else{
            int4file->close();
        }
        state=alya::state::geometry;
        previousId=0;
    }
    return true;
}

bool alya::Ascii2mpio::writeLnodb()
{
    string temp=fileReader->nextString();
    if (!AlyaString::Equal(temp,GEOM_END_BOUNDARIES)){
        int64_t val = atoi(temp.c_str());
        if (!noProgress){
            Message::Progress(val, domainManager->getBoundaryCount());
        }
        if (val<=previousId){
            Message::Critical("Line "+ to_string(lineNumber) + ": items are not sorted");
            return false;
        }
        previousId=val;
        int64_t i;
        for (i=1;!fileReader->lastString();i++){
            if ((!lelbo&&(intSize==8?i>int8file->getHeader()->getColumns():i>int4file->getHeader()->getColumns()))||
                    (lelbo&&(intSize==8?i>int8file->getHeader()->getColumns()+1:i>int4file->getHeader()->getColumns()+1))){
                Message::Critical("Line "+ to_string(lineNumber) + ": too many items:" + to_string(i) + ", max value: " + to_string(intSize==8?int8file->getHeader()->getColumns():int4file->getHeader()->getColumns()));
                return false;
            }
            temp=fileReader->nextString();
            val = atol(temp.c_str());
            if (lelbo&&i==fileReader->stringNumber()-1){
                if (intSize==8){
                    lelbo8file->write(val);
                }else{
                    int32_t valCast=static_cast<int32_t>(val);
                    lelbo4file->write(valCast);
                }
            }else{
                if (intSize==8){
                    int8file->write(val);
                }else{
                    int32_t valCast=static_cast<int32_t>(val);
                    int4file->write(valCast);
                }
            }
        }
        if (lelbo){
            i--;
        }
        for (;intSize==8?i<int8file->getHeader()->getColumns()+1:i<int4file->getHeader()->getColumns()+1;i++){
            val=0;
            if (intSize==8){
                int8file->write(val);
            }else{
                int32_t valCast=static_cast<int32_t>(val);
                int4file->write(valCast);
            }
        }
        itemNumber++;
    }else{
        if (intSize==8?int8file->getHeader()->getLines()!=itemNumber:int4file->getHeader()->getLines()!=itemNumber){
            Message::Critical("Boundary number not coherent with domain file");
            return false;
        }
        if (intSize==8){
            int8file->close();
        }else{
            int4file->close();
        }
        if (lelbo){
            if (intSize==8){
                lelbo8file->close();
            }else{
                lelbo4file->close();
            }
        }
        state=alya::state::geometry;
        previousId=0;
    }
    return true;
}


bool alya::Ascii2mpio::writeFloats(string end, int64_t count, alya::state::States previousState)
{
    string temp=fileReader->nextString();
    if (!AlyaString::Equal(temp,end)){
        int64_t val = atol(temp.c_str());
        if (!noProgress){
            Message::Progress(val, count);
        }
        if (val<=previousId){
            Message::Critical("Line "+ to_string(lineNumber) + ": items are not sorted");
            return false;
        }
        int64_t i;
        while(val>previousId+1){
            previousId++;
            itemNumber++;
            for (i=0; i<float8file->getColumns(); i++){
                float8file->write(atof(defaultValue.c_str()));
            }
        }
        previousId=val;
        for (i=0; !fileReader->lastString(); i++){
            temp=fileReader->nextString();
            double dval = atof(temp.c_str());
            float8file->write(dval);
        }
        for (; i<float8file->getColumns(); i++){
            float8file->write(0.0);
        }
        itemNumber++;
    }else{
        while (float8file->getHeader()->getLines()>itemNumber){
             itemNumber++;
             for (int64_t i=0; i<float8file->getColumns(); i++){
                float8file->write(atof(defaultValue.c_str()));
             }
        }
        float8file->close();
        state=previousState;
        previousId=0;
    }
    return true;
}

bool alya::Ascii2mpio::writeIntegers(string end, int64_t count, alya::state::States previousState)
{
    string temp=fileReader->nextString();
    if (!AlyaString::Equal(temp,end)){
        int64_t val = atol(temp.c_str());
        if (!noProgress){
            Message::Progress(val, count);
        }
        if (val<=previousId){
            Message::Critical("Line "+ to_string(lineNumber) + ": items are not sorted");
            return false;
        }
        int64_t i;
        while(val>previousId+1){
            previousId++;
            itemNumber++;
            for (i=0;intSize==8?i<int8file->getHeader()->getColumns():i<int4file->getHeader()->getColumns();i++){
                if (intSize==8){
                    int8file->write(atol(defaultValue.c_str()));
                }else{
                    int4file->write(atoi(defaultValue.c_str()));
                }
            }
        }
        previousId=val;
        for (i=0; !fileReader->lastString(); i++){
            temp=fileReader->nextString();
            val = atol(temp.c_str());
            if (intSize==8){
                int8file->write(val);
            }else{
                int32_t valCast=static_cast<int32_t>(val);
                int4file->write(valCast);
            }
        }
        for (;intSize==8?i<int8file->getHeader()->getColumns():i<int4file->getHeader()->getColumns();i++){
            if (intSize==8){
                int8file->write(0);
            }else{
                int4file->write(0);
            }
        }
        itemNumber++;
    }else{
        while (intSize==8?int8file->getHeader()->getLines()>itemNumber:int4file->getHeader()->getLines()>itemNumber){
             itemNumber++;
             for (int64_t i=0;intSize==8?i<int8file->getHeader()->getColumns():i<int4file->getHeader()->getColumns();i++){
                 if (intSize==8){
                     int8file->write(atol(defaultValue.c_str()));
                 }else{
                     int4file->write(atoi(defaultValue.c_str()));
                 }
             }
        }
        if (intSize==8){
            int8file->close();
        }else{
            int4file->close();
        }
        state=previousState;
        previousId=0;
    }
    return true;
}

bool alya::Ascii2mpio::searchDefault(string def)
{
    if (fileReader->containField("DEFAULT")){
        defaultValue=fileReader->argument("DEFAULT", false);
        Message::Info();
        Message::Info("Found default value: " + defaultValue);
        Message::Info(-1);
    }else{
        defaultValue=def;
    }
    return true;
}
