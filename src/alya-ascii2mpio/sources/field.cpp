/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include "field.h"

alya::Field::Field()
{

}

alya::Field::Field(int64_t number, int64_t dimension, string resultsOn, int64_t steps, string defaultValue):number(number), dimension(dimension), resultsOn(resultsOn), steps(steps), defaultValue(defaultValue)
{
    if (steps==1){
        addStep(0.0);
    }else if (steps==0){
        this->steps=1;
        addStep(0.0);
    }
}

string alya::Field::getResultsOn() const
{
    return resultsOn;
}

void alya::Field::setResultsOn(const string &value)
{
    resultsOn = value;
}

void alya::Field::addStep(double step)
{
    if (stepList.size()<static_cast<unsigned long>(steps)){
        stepList.push_back(step);
    }else{
        Message::Warning("Trying to add more steps than defined for this field! Time step ignored");
    }
}


vector<double> alya::Field::getStepList() const
{
    return stepList;
}

void alya::Field::setStepList(const vector<double> &value)
{
    stepList = value;
}

string alya::Field::getDefaultValue() const
{
    return defaultValue;
}

void alya::Field::setDefaultValue(const string &value)
{
    defaultValue = value;
}

int64_t alya::Field::getDimension() const
{
    return dimension;
}

void alya::Field::setDimension(const int64_t &value)
{
    dimension = value;
}

int64_t alya::Field::getSteps() const
{
    return steps;
}

void alya::Field::setSteps(const int64_t &value)
{
    steps = value;
    if (steps==1){
        addStep(0.0);
    }else if (steps==0){
        steps=1;
        addStep(0.0);
    }
}

int64_t alya::Field::getNumber() const
{
    return number;
}

void alya::Field::setNumber(const int64_t &value)
{
    number = value;
}

