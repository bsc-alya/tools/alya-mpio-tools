/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#ifndef HEADER_H
#define HEADER_H

#include <cstdint>
#include <streampointer.h>
#include <alya-ascii2mpio_config.h>

using namespace std;

namespace alya {

    class Header
    {
    public:
        Header();
        void writeTo(AlyaPtr_ofstream stream);
    public:
        static const int64_t H_MagicNumber;
        static const char* H_AlignChar64;
        static const char* H_Format;
        static const char* H_Version;
        const char* h_object;
        const char* h_dimension;
        const char* h_results_on;
        const char* h_type;
        const char* h_size;
        const char* h_parallel;
        const char* h_filter;
        const char* h_sorting;
        const char* h_id;
        int64_t h_columns=0;
        int64_t h_lines=0;
        int64_t h_ittim=0;
        int64_t h_nsubd=1;//Should be always equal to 1
        int64_t h_divi=0;
        int64_t h_tag1=-1;
        int64_t h_tag2=-1;
        double h_time=0.0;
        const char* h_options[MPIOHEADER_OPTION_SIZE];
    };


}



#endif // HEADER_H
