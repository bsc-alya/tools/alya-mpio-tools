/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#ifndef DOMAINMANAGER_H
#define DOMAINMANAGER_H

#include <string>
#include <vector>
#include <streampointer.h>
#include <alya-ascii2mpio_config.h>
#include <message.h>
#include <alyastring.h>
#include <alyatypedef.h>
#include <mpiocomm.h>
#include <alyafilereader.h>
#include <field.h>

#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/trim_all.hpp>
#include <boost/algorithm/string/replace.hpp>
#include <boost/tokenizer.hpp>

using namespace std;
using namespace boost;

namespace alya{

    namespace domain {
        enum States{init, dimension, strategy, field, step, end};
    }

    class DomainManager
    {
    public:
        DomainManager(AlyaPtr_MPIOComm communicator, AlyaPtr<AlyaFileReader> fileReader, int64_t mcodb);
        ~DomainManager();
        bool read();
        int64_t getNodeCount() const;
        int64_t getElementCount() const;
        int64_t getDimensions() const;
        vector<int> getElementTypes() const;
        int64_t getBoundaryCount() const;

        bool getUniqueType() const;
        int64_t getMaxNodes() const;


        vector<std::shared_ptr<Field> > getFields();
        
        bool getBoundaryElement() const;

        int64_t getMcodb() const;

    private:
        bool readDimensions();
        bool readStrategy();
        bool readFieldDimensions();
        bool readFieldSteps();


        AlyaPtr_MPIOComm communicator;
        AlyaPtr<AlyaFileReader> fileReader;
        int64_t nodeCount;
        int64_t elementCount;
        int64_t dimensions;
        vector<int> elementTypes;
        int64_t boundaryCount;
        int64_t materialCount;
        int64_t periodicNodes;
        int64_t mcodb;
        int64_t fieldCount;
        bool boundaryElement;
        alya::domain::States state;
        int64_t sectionLineNumber;
        string currentFileName;
        int64_t currentFileLineNumber;
        tokenizer<escaped_list_separator<char> >::iterator tokensIterator;
        bool uniqueType;
        int64_t maxNodes;
        vector<AlyaPtr<Field> > fields;
    };

}

#endif // DOMAINMANAGER_H
