/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#ifndef ASCII2MPIO_H
#define ASCII2MPIO_H

#include <fstream>
#include <iostream>
#include <string>
#include <message.h>
#include <pointer.h>
#include <alyatypedef.h>
#include <domainmanager.h>
#include <alyastring.h>
#include <mpiocomm.h>
#include <mpioheader.h>
#include <alyareturn.h>

#include <mpiofile.h>

using namespace std;

namespace alya {


    namespace state {
        enum States{init, strategy, geometry, sets, fields, bcs, coord,
                    ltype, ltypeNPE, lnods, lmate, lmast, lesub,
                    lnodb, ltypb, lelbo, leset, lbset, lnset, codbo, codno,
                    lnoch, lelch, lboch, field, steps, step, error};
    }

    class Ascii2mpio
    {
    public:
        Ascii2mpio(AlyaPtr_MPIOComm communicator, AlyaPtr<AlyaFileReader> fileReader, AlyaPtr<DomainManager> domainManager, string caseName, int intSize, bool noProgress=false);
        bool convert();
    private:
        bool initCoord();
        bool initTypes(bool nodesPerElement=false);
        bool initLnods();
        bool initLnodb(bool element=false);
        bool initLelbo();
        bool initCodno();
        bool initField(int64_t number);
        bool initField(int64_t number, int64_t step);

        bool initGenericIntScalars(string file, string obj, string res, alya::state::States st);

        bool writeCoord();
        bool writeLtype();
        bool writeLtypeNPE();
        bool writeLnods();
        bool writeLnodb();
        bool writeFloats(string end, int64_t count, alya::state::States previousState);
        bool writeIntegers(string end, int64_t count, alya::state::States previousState);

        bool searchDefault(string def="0");

    private:
        AlyaPtr_MPIOComm communicator;
        AlyaPtr<AlyaFileReader> fileReader;
        AlyaPtr<DomainManager> domainManager;
        int64_t dimensions;
        bool uniqueType;
        alya::state::States state;
        string line;
        int64_t lineNumber;
        int64_t itemNumber;
        int64_t previousId;
        bool lelbo;
        string caseName;
        int intSize;
        bool noProgress;
        int64_t mcodb1;
        AlyaPtr<Field> currentField;
        int currentFieldNumber;
        int64_t currentCount;
        string defaultValue;
        AlyaPtr<MPIOFile<double> > float8file;
        AlyaPtr<MPIOFile<int32_t> > int4file;
        AlyaPtr<MPIOFile<int64_t> > int8file;
        AlyaPtr<MPIOFile<int32_t> > lelbo4file;
        AlyaPtr<MPIOFile<int64_t> > lelbo8file;

        AlyaPtr_ofstream outputStream;
    };

}

#endif // ASCII2MPIO_H
