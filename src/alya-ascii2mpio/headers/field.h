/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#ifndef FIELD_H
#define FIELD_H

#include <vector>
#include <string>
#include <message.h>

using namespace std;

namespace alya{

    class Field
    {
    public:
        Field();
        Field(int64_t number, int64_t dimension, string resultsOn, int64_t steps, string defaultValue="0");

        int64_t getNumber() const;
        void setNumber(const int64_t &value);

        int64_t getSteps() const;
        void setSteps(const int64_t &value);

        int64_t getDimension() const;
        void setDimension(const int64_t &value);

        string getResultsOn() const;
        void setResultsOn(const string &value);

        void addStep(double step);

        vector<double> getStepList() const;
        void setStepList(const vector<double> &value);

        string getDefaultValue() const;
        void setDefaultValue(const string &value);

    private:
        int64_t number;
        int64_t dimension;
        string resultsOn;
        int64_t steps;
        string defaultValue;
        vector<double> stepList;
    };

}

#endif // FIELD_H
