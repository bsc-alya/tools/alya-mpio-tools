/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#ifndef ALYAASCII2MPIO_CONFIG_H
#define ALYAASCII2MPIO_CONFIG_H

#ifndef __STDC_IEC_559__
#warning "Requires IEEE 754 floating point!"
#endif

#ifndef __BUILD_VERSION__
#define __BUILD_VERSION__ "Unknown"
#endif

#include <mpioheaderdef.h>

#define RETURN_OK                   0
#define RETURN_ERR                  1
#define RETURN_ERR_INPUT            2
#define RETURN_ERR_FILES            3
#define RETURN_ERR_PAR              4


#define ELEMENT_ALIGN_DEFAULT       100
#define CASE_NAME_DEFAULT           "case"


#define DOM_EXT                     ".dom.dat"

#define MPIO_EXT                    ".mpio.bin"

#define COORD_EXT                   "-COORD"+MPIO_EXT
#define LTYPE_EXT                   "-LTYPE"+MPIO_EXT
#define LNODS_EXT                   "-LNODS"+MPIO_EXT
#define LNODB_EXT                   "-LNODB"+MPIO_EXT
#define LTYPB_EXT                   "-LTYPB"+MPIO_EXT
#define LELBO_EXT                   "-LELBO"+MPIO_EXT
#define LESET_EXT                   "-LESET"+MPIO_EXT
#define LBSET_EXT                   "-LBSET"+MPIO_EXT
#define LNSET_EXT                   "-LNSET"+MPIO_EXT
#define CODBO_EXT                   "-CODBO"+MPIO_EXT
#define CODNO_EXT                   "-CODNO"+MPIO_EXT
#define LMAST_EXT                   "-LMAST"+MPIO_EXT
#define LMATE_EXT                   "-LMATE"+MPIO_EXT
#define LESUB_EXT                   "-LESUB"+MPIO_EXT
#define LNOCH_EXT                   "-LNOCH"+MPIO_EXT
#define LELCH_EXT                   "-LELCH"+MPIO_EXT
#define LBOCH_EXT                   "-LBOCH"+MPIO_EXT

#define GEOME_EXT                   ".mpio.geo"
#define SETS_EXT                    ".mpio.set"
#define BCS_EXT                     ".mpio.fix"
#define FIELD_EXT                   ".mpio.fie"

#define GEOM_NODES_PER_ELEMENT      "NODES_PER_ELEMENT"
#define GEOM_END_NODES_PER_ELEMENT  "END_NODES_PER_ELEMENT"

#define GEOM_TYPES                  "TYPES"
#define GEOM_END_TYPES              "END_TYPES"

#define GEOM_ELEMENTS               "ELEMENTS"
#define GEOM_END_ELEMENTS           "END_ELEMENTS"

#define GEOM_COORDINATES            "COORDINATES"
#define GEOM_END_COORDINATES        "END_COORDINATES"

#define GEOM_BOUNDARIES             "BOUNDARIES"
#define GEOM_END_BOUNDARIES         "END_BOUNDARIES"

#define GEOM_OPT_BOUND_ELEMENT      "ELEMENT"
#define GEOM_OPT_BOUND_ELEMENTS     "ELEMENTS"

#define SETS_ELEMENTS               "ELEMENTS"
#define SETS_END_ELEMENTS           "END_ELEMENTS"

#define SETS_BOUNDARIES             "BOUNDARIES"
#define SETS_END_BOUNDARIES         "END_BOUNDARIES"

#define DOMAIN_DIMENSIONS           "DIMENSIONS"
#define DOMAIN_END_DIMENSIONS       "END_DIMENSIONS"

#define DOMAIN_GEOMETRY             "GEOMETRY"
#define DOMAIN_END_GEOMETRY         "END_GEOMETRY"

#define DOMAIN_SETS                 "SETS"
#define DOMAIN_END_SETS             "END_SETS"

#define DOMAIN_BOUCO                "BOUNDARY_CONDITIONS"
#define DOMAIN_END_BOUCO            "END_BOUNDARY_CONDITIONS"

#define DOMAIN_FIELDS               "FIELDS"
#define DOMAIN_END_FIELDS           "END_FIELDS"

#define DOMAIN_INCLUDE              "INCLUDE"




#endif // ALYAASCII2MPIO_CONFIG_H
