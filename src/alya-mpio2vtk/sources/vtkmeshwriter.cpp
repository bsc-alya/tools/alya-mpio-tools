/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include "vtkmeshwriter.h"


alya::VTKMeshWriter::VTKMeshWriter(AlyaPtr<alya::MeshReader> meshReader, AlyaPtr_MPIOComm communicator, AlyaPtr<ArgumentManager> argumentManager):
    meshReader(meshReader), communicator(communicator), argumentManager(argumentManager)
{
    caseName=argumentManager->getCaseName();
    AlyaTypeDef::Init();
    VtkTypeDef::Init();
    hex27Map.init();
    pen18Map.init();
    tet20Map.init();
    types=NULL;
}

alya::VTKMeshWriter::~VTKMeshWriter()
{
    delete[] types;
}

bool alya::VTKMeshWriter::write()
{
    if (!convertMesh()){
        return false;
    }
    if (!createGrid()){
        return false;
    }
    if (!createOutputDir()){
        return false;
    }
    return writeFiles();
}

bool alya::VTKMeshWriter::convertMesh()
{
    Message::Info(communicator->getRank(), "Gathering data", 2);
    meshReader->getCoord()->gather();
    if (meshReader->getIntSize()==4){
        meshReader->getLtype4()->gather();
        meshReader->getLnods4()->gather();
    }else{
        meshReader->getLtype8()->gather();
        meshReader->getLnods8()->gather();
    }
    if (communicator->isMasterOrSequential()){
        //COORD
        points = vtkSmartPointer<vtkPoints>::New();
        Message::Info(communicator->getRank(), "Converting points", 2);
        if (!convertPoints(points)){
            return false;
        }

        //LTYPE+LNODS
        if (meshReader->getIntSize()==4){
            types=new int[meshReader->getLtype4()->getLines()];
        }else{
            types=new int[meshReader->getLtype8()->getLines()];
        }
        cellArray = vtkSmartPointer<vtkCellArray>::New();

        Message::Info(communicator->getRank(), "Converting cells", 2);
        if (!convertCells(types, cellArray)){
            return false;
        }
    }
    return true;
}

bool alya::VTKMeshWriter::createGrid()
{
    if (communicator->isMasterOrSequential()){
        Message::Info(communicator->getRank(), "Creating unstructured grid", 2);
        unstructuredGrid = vtkSmartPointer<vtkUnstructuredGrid>::New();
        unstructuredGrid->SetPoints(points);
        unstructuredGrid->SetCells(types, cellArray);
    }
    return true;
}

bool alya::VTKMeshWriter::writeFiles()
{
    if (communicator->isMasterOrSequential()){
        // Write file
        Message::Info(communicator->getRank(), "Writing vtk file", 2);
        vtkSmartPointer<vtkXMLUnstructuredGridWriter> writer = vtkSmartPointer<vtkXMLUnstructuredGridWriter>::New();
        string outputFileName=caseName;
        outputFileName+=".";
        outputFileName+=writer->GetDefaultFileExtension();
        outputFileName=outputDir+DIR_SEP+outputFileName;
        writer->SetFileName(outputFileName.c_str());
#if VTK_MAJOR_VERSION <= 5
        writer->SetInput(unstructuredGrid);
#else
        writer->SetInputData(unstructuredGrid);
#endif
        writer->Write();
    }
    return true;
}

bool alya::VTKMeshWriter::createOutputDir()
{
    outputDir="vtk";
    struct stat info;

    if (communicator->isMasterOrSequential()){
        if( stat(outputDir.c_str(), &info ) == 0 ){
            // S_ISDIR() doesn't exist on my windows
            for (int i=0; i<1000; i++){
                string outputDirTmp=outputDir+"_"+to_string(i);
                if( stat(outputDirTmp.c_str(), &info ) != 0 ){
                    outputDir=outputDirTmp;
                    break;
                }
            }
        }
        const int dir_err = mkdir(outputDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
        if (-1 == dir_err)
        {
            Message::Error("Cannot create the output directory");
            return false;
        }
    }
    communicator->Bcast(&outputDir);
    return true;
}

bool alya::VTKMeshWriter::convertPoints(vtkSmartPointer<vtkPoints> points)
{
    if (meshReader->getCoord()->getColumns()==2){
        return convertPoints2D(points);
    }else if (meshReader->getCoord()->getColumns()==3){
        return convertPoints3D(points);
    }
    if (argumentManager->getDoublePoints()){
       points->SetDataTypeToDouble();
    }
    return true;
}

/*VTK 2D*/
bool alya::VTKMeshWriter::convertPoints2D(vtkSmartPointer<vtkPoints> points)
{
    for (int i=0; i<meshReader->getCoord()->getLines(); i++){
        points->InsertNextPoint(meshReader->getCoord()->getData(i, 0),
                                meshReader->getCoord()->getData(i, 1),
                                0.0);
    }
    return true;
}


bool alya::VTKMeshWriter::convertPoints3D(vtkSmartPointer<vtkPoints> points)
{
    for (int i=0; i<meshReader->getCoord()->getLines(); i++){
        points->InsertNextPoint(meshReader->getCoord()->getData(i, 0),
                                meshReader->getCoord()->getData(i, 1),
                                meshReader->getCoord()->getData(i, 2));
    }
    return true;
}

bool alya::VTKMeshWriter::convertCells(int types[], vtkSmartPointer<vtkCellArray> cellArray)
{
    int64_t lines;
    if (meshReader->getIntSize()==4){
        lines=meshReader->getLtype4()->getLines();
    }else{
        lines=meshReader->getLtype8()->getLines();
    }
    for (int64_t i=0; i<lines; i++){
        unsigned int type;
        if (meshReader->getIntSize()==4){
            type=static_cast<unsigned int>(meshReader->getLtype4()->getData(i));
        }else{
            type=static_cast<unsigned int>(meshReader->getLtype8()->getData(i));
        }
        switch(type) {
            case ALYA_TRI03 :   {
                                    if (!convertCell(types, cellArray, i, vtkSmartPointer<vtkTriangle>::New())) return false;
                                    break;
                                }
            case ALYA_TRI06 :   {
                                    if (!convertCell(types, cellArray, i, vtkSmartPointer<vtkQuadraticTriangle>::New())) return false;
                                    break;
                                }
            case ALYA_TRI10 :   {
                                    if (!convertCell(types, cellArray, i, vtkSmartPointer<vtkLagrangeTriangle>::New())) return false;
                                    break;
                                }
            case ALYA_QUA04 :   {
                                    if (!convertCell(types, cellArray, i, vtkSmartPointer<vtkQuad>::New())) return false;
                                    break;
                                }
            case ALYA_QUA09 :   {
                                    if (!convertCell(types, cellArray, i, vtkSmartPointer<vtkBiQuadraticQuad>::New())) return false;
                                    break;
                                }
            case ALYA_TET04 :   {
                                    if (!convertCell(types, cellArray, i, vtkSmartPointer<vtkTetra>::New())) return false;
                                    break;
                                }
            case ALYA_TET10 :   {
                                    if (!convertCell(types, cellArray, i, vtkSmartPointer<vtkQuadraticTetra>::New())) return false;
                                    break;
                                }
            case ALYA_TET20 :   {
                                    if (!convertHOCell(types, cellArray, i, vtkSmartPointer<vtkLagrangeTetra>::New())) return false;
                                    break;
                                }
            case ALYA_PYR05 :   {
                                    if (!convertCell(types, cellArray, i, vtkSmartPointer<vtkPyramid>::New())) return false;
                                    break;
                                }
            case ALYA_PEN06 :   {
                                    if (!convertCell(types, cellArray, i, vtkSmartPointer<vtkWedge>::New())) return false;
                                    break;
                                }
            case ALYA_PEN18 :   {
                                    if (!convertHOCell(types, cellArray, i, vtkSmartPointer<vtkBiQuadraticQuadraticWedge>::New())) return false;
                                    break;
                                }
            case ALYA_HEX08 :   {
                                    if (!convertCell(types, cellArray, i, vtkSmartPointer<vtkHexahedron>::New())) return false;
                                    break;
                                }
            case ALYA_HEX27 :   {
                                    if (!convertHOCell(types, cellArray, i, vtkSmartPointer<vtkTriQuadraticHexahedron>::New())) return false;
                                    break;
                                }
            case ALYA_SHELL :   {
                                    if (!convertCell(types, cellArray, i, vtkSmartPointer<vtkTriangle>::New())) return false;
                                    break;
                                }
            case ALYA_BAR3D :   {
                                    if (!convertCell(types, cellArray, i, vtkSmartPointer<vtkLine>::New())) return false;
                                    break;
                                }
            default :           {
                                    Message::Error("Type is not implemented: element="+to_string(i)+", type="+to_string(type));
                                    return false;
                                }
    }
    }
    return true;
}

template<typename T>
bool alya::VTKMeshWriter::convertCell(int types[], vtkSmartPointer<vtkCellArray> cellArray, int64_t i, vtkSmartPointer<T> cell)
{
    unsigned int type;
    int64_t elements;
    if (meshReader->getIntSize()==4){
        type=static_cast<unsigned int> (meshReader->getLtype4()->getData(i));
        elements=meshReader->getLnods4()->getColumns();
    }else{
        type=static_cast<unsigned int> (meshReader->getLtype8()->getData(i));
        elements=meshReader->getLnods8()->getColumns();
     }
     if ( type == ALYA_TRI10 ){
          cell->GetPoints()->SetNumberOfPoints(10);
          cell->GetPointIds()->SetNumberOfIds(10);
          cell->Initialize();
     }

    for (int64_t j=0; j<elements; j++){
        int64_t point;
        if (meshReader->getIntSize()==4){
            point=static_cast<int64_t> (meshReader->getLnods4()->getData(i,j));
        }else{
            point=static_cast<int64_t> (meshReader->getLnods8()->getData(i,j));
        }
        if (point!=0){
            cell->GetPointIds()->SetId(j,point-1);
        }else{
            break;
        }
    }
    types[i]=alya::VtkTypeDef::Types2vtk[type];
    cellArray->InsertNextCell(cell);
    return true;
}

template<typename T>
bool alya::VTKMeshWriter::convertHOCell(int types[], vtkSmartPointer<vtkCellArray> cellArray, int64_t i, vtkSmartPointer<T> cell)
{
    unsigned int type;
    int64_t elements;
    if (meshReader->getIntSize()==4){
        type=static_cast<unsigned int> (meshReader->getLtype4()->getData(i));
        elements=meshReader->getLnods4()->getColumns();
    }else{
        type=static_cast<unsigned int> (meshReader->getLtype8()->getData(i));
        elements=meshReader->getLnods8()->getColumns();
    }

    if ( type == ALYA_TET20 ){
          cell->GetPoints()->SetNumberOfPoints(20);
          cell->GetPointIds()->SetNumberOfIds(20);
          cell->Initialize();
     }

    for (int64_t j=0; j<elements; j++){
        int64_t point;
        switch(type) {
            case ALYA_PEN18 :   {
                if (meshReader->getIntSize()==4){
                    point=static_cast<int64_t> (meshReader->getLnods4()->getData(i, pen18Map.get(j)));
                }else{
                    point=static_cast<int64_t> (meshReader->getLnods8()->getData(i, pen18Map.get(j)));
                }
                break;
            }
            case ALYA_TET20 :   {
                if (meshReader->getIntSize()==4){
                    point=static_cast<int64_t> (meshReader->getLnods4()->getData(i, tet20Map.get(j)));
                }else{
                    point=static_cast<int64_t> (meshReader->getLnods8()->getData(i, tet20Map.get(j)));
                }
                break;
            }
            case ALYA_HEX27 :   {
                if (meshReader->getIntSize()==4){
                    point=static_cast<int64_t> (meshReader->getLnods4()->getData(i, hex27Map.get(j)));
                }else{
                    point=static_cast<int64_t> (meshReader->getLnods8()->getData(i, hex27Map.get(j)));
                }
                break;
            }
            default :   {
                return false;
            }
        }
        if (point!=0){
            cell->GetPointIds()->SetId(j,point-1);
        }else{
            break;
        }
    }
    types[i]=alya::VtkTypeDef::Types2vtk[type];
    cellArray->InsertNextCell(cell);
    return true;
}

