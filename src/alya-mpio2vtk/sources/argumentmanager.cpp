/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include "argumentmanager.h"
#include "boundingbox.h"

alya::ArgumentManager::ArgumentManager(int argc, char **argv, AlyaPtr_MPIOComm communicator):communicator(communicator)
{
    help=false;
    me=string(argv[0]);
    convertTypeStr=CONVERT_TYPE_STRING_ALL;
    rangeMin=0;
    rangeMax=0;
    more=false;
    doublePoints=false;
    isboundingbox=false;
    boundingbox=AlyaPtr<BoundingBox>(new BoundingBox());
    if (argc<2){
        valid=false;
        return;
    }else{
        for (int i=1; i<argc; i++){
            string str=string(argv[i]);
            if (ARGUMENT(str, "-h", "--help")){
                help=true;
                return;
            }else if (ARGUMENT(str, "-t", "--type")){
                convertTypeStr=string(argv[++i]);
                Message::Info(communicator->getRank(), "Type: "+convertTypeStr, 2);
            }else if (ARGUMENT(str, "-m", "--mesh")){
                convertTypeStr=CONVERT_TYPE_STRING_MESH;
                Message::Info(communicator->getRank(), "Type: "+convertTypeStr, 2);
            }else if (ARGUMENT(str, "-p", "--post")){
                convertTypeStr=CONVERT_TYPE_STRING_POST;
                Message::Info(communicator->getRank(), "Type: "+convertTypeStr, 2);
            }else if (ARGUMENT(str, "-a", "--all")){
                convertTypeStr=CONVERT_TYPE_STRING_ALL;
                Message::Info(communicator->getRank(), "Type: "+convertTypeStr, 2);
            }else if (ARGUMENT(str, "-d", "--double")){
                doublePoints= true;
                Message::Info(communicator->getRank(), "Points are exported using double format", 2);
            }else if (ARGUMENT(str, "--more", "--more")){
                more=true;
                Message::Info(communicator->getRank(), "Exporting additional mesh files", 2);
            }else if (ARGUMENT(str, "-b", "--boundingbox")){
                isboundingbox=true;
                double xmin=atof(argv[++i]);
                double xmax=atof(argv[++i]);
                double ymin=atof(argv[++i]);
                double ymax=atof(argv[++i]);
                double zmin=atof(argv[++i]);
                double zmax=atof(argv[++i]);
                boundingbox->setCoord(xmin, xmax, ymin, ymax, zmin, zmax);
                Message::Info(communicator->getRank(), "Bounding Box enabled: ", 2);
                //Message::Info(communicator->getRank(), to_string(xmin) + ","+ to_string(xmax) + ","+ to_string(ymin) + ","+ to_string(ymax) + ","+ to_string(zmin) + ","+ to_string(zmax), 3);
                Message::Info(communicator->getRank(), to_string(boundingbox->getXmin()) + ","+ to_string(boundingbox->getXmax()) + ","+ to_string(boundingbox->getYmin()) + ","+ to_string(boundingbox->getYmax()) + ","+ to_string(boundingbox->getZmin()) + ","+ to_string(boundingbox->getZmax()), 3);
            }else if (ARGUMENT(str, "-i", "--iteration")){
                iterations.push_back(atoi(argv[++i]));
                Message::Info(communicator->getRank(), "Adding iteration: "+to_string(iterations[iterations.size()-1]), 2);
            }else if (ARGUMENT(str, "-r", "--range")){
                rangeMin=atoi(argv[++i]);
                rangeMax=atoi(argv[++i]);
                Message::Info(communicator->getRank(), "Iteration interval: "+to_string(rangeMin)+" - "+to_string(rangeMax), 2);
            }else{
                caseName=str;
                Message::Info(communicator->getRank(), "Case: "+caseName, 2);
                valid=true;
            }
        }
    }
    if (caseName.compare("")==0){
        valid=false;
        Message::Error(communicator->getRank(), "You did not specify any case!");
        return;
    }
    if (convertTypeStr==CONVERT_TYPE_STRING_MESH){
        convertType=CONVERT_TYPE_MESH;
        Message::Info(communicator->getRank(), "Operation: original mesh conversion",1);
    }else if (convertTypeStr==CONVERT_TYPE_STRING_POST){
        convertType=CONVERT_TYPE_POST;
        Message::Info(communicator->getRank(), "Operation: post processed mesh conversion",1);
    }else if (convertTypeStr==CONVERT_TYPE_STRING_ALL){
        convertType=CONVERT_TYPE_ALL;
        Message::Info(communicator->getRank(), "Operation: post processed mesh + fields conversion",1);
    }else{
        valid=false;
        Message::Error(communicator->getRank(), "This operation is not implememented");
    }
    if (rangeMax<rangeMin){
        valid=false;
        Message::Error(communicator->getRank(), "Range min is greater than range max");
    }
}

void alya::ArgumentManager::usage()
{
    Message::Info(communicator->getRank(), "Usage");
    Message::Info(communicator->getRank(), string(me.c_str())+" [OPTIONS] CASE NAME");
    Message::Info(communicator->getRank(), "Options:");
    Message::Info(communicator->getRank(), "-h | --help                                             Print help");
    Message::Info(communicator->getRank(), "-m | --mesh                                             Convert original mesh");
    Message::Info(communicator->getRank(), "-p | --post                                             Convert only post processed mesh");
    Message::Info(communicator->getRank(), "-a | --all                                              Convert all post processed files (default)");
    Message::Info(communicator->getRank(), "-d | --double                                           Points are exported using doubles instead of floats");
    Message::Info(communicator->getRank(), "-i | --iteration [int]                                  Only export the specified step (can be repeated several times)");
    Message::Info(communicator->getRank(), "-r | --range [int] [int]                                Only export the steps contained in this interval");
    Message::Info(communicator->getRank(), "-b | --boundingbox [int] [int] [int] [int] [int] [int]  Only export subdomains that are contained in the specified bounding box (xmin xmax ymin ymax zmin zmax");
    Message::Info(communicator->getRank(), "--more                                                  Export additional post processed mesh files");
}

string alya::ArgumentManager::getCaseName() const
{
    return caseName;
}

int alya::ArgumentManager::getConvertType() const
{
    return convertType;
}

vector<int> alya::ArgumentManager::getIterations() const
{
    return iterations;
}

int alya::ArgumentManager::getRangeMax() const
{
    return rangeMax;
}

bool alya::ArgumentManager::getMore() const
{
    return more;
}

bool alya::ArgumentManager::getIsboundingbox() const
{
    return isboundingbox;
}

bool alya::ArgumentManager::getDoublePoints() const
{
    return doublePoints;
}

AlyaPtr<alya::BoundingBox> alya::ArgumentManager::getBoundingbox() const
{
    return boundingbox;
}

int alya::ArgumentManager::getRangeMin() const
{
    return rangeMin;
}

bool alya::ArgumentManager::getHelp() const
{
    return help;
}

bool alya::ArgumentManager::getValid() const
{
    return valid;
}
