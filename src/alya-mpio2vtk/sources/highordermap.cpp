#include "highordermap.h"

int64_t alya::HighOrderMap::get(int64_t i)
{
    if ( ho.find(i) == ho.end() ) {
        return i;
    } else {
        return ho[i];
    }
}

void alya::Hex27Map::init()
{
    ho[12] = 16;
    ho[13] = 17;
    ho[14] = 18;
    ho[15] = 19;
    ho[16] = 12;
    ho[17] = 13;
    ho[18] = 14;
    ho[19] = 15;
    ho[20] = 24;
    ho[21] = 22;
    ho[22] = 21;
    ho[24] = 20;
}

void alya::Pen18Map::init()
{
    ho[9] = 12;
    ho[10] = 13;
    ho[11] = 14;
    ho[12] = 9;
    ho[13] = 10;
    ho[14] = 11;
}

void alya::Tet20Map::init()
{
    ho[10] = 11;
    ho[11] = 10;
    ho[12] = 15;
    ho[13] = 14;
    ho[14] = 13;
    ho[15] = 12;
    ho[16] = 17;
    ho[17] = 19;
    ho[19] = 16;
}
