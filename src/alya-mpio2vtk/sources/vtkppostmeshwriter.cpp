/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include "vtkppostmeshwriter.h"

alya::VTKPPostMeshWriter::VTKPPostMeshWriter(AlyaPtr<alya::PostMeshReader> meshReader, AlyaPtr_MPIOComm communicator, AlyaPtr<ArgumentManager> argumentManager):
    VTKMeshWriter(dynamic_pointer_cast<MeshReader>(meshReader), communicator, argumentManager), postMeshReader(meshReader), more(argumentManager->getMore()), isBoundingBox(argumentManager->getIsboundingbox()), boundingBox(argumentManager->getBoundingbox())
{

}

bool alya::VTKPPostMeshWriter::write()
{
    if (!convertMesh()){
        return false;
    }
    if (!createGrid()){
        return false;
    }
    if (!createOutputDir()){
        return false;
    }
    return writeFiles();
}

bool alya::VTKPPostMeshWriter::convertMesh()
{
    if (communicator->isIOWorker()){
        //COORD
        points = vtkSmartPointer<vtkPoints>::New();
        Message::Info(communicator->getRank(), "Converting points", 2);
        if (!convertPoints(points)){
            return false;
        }
        disableSubdomain=!isValidSubdomain();
        if (disableSubdomain){
            //Message::Info("Excluding subdomain: "+ to_string(communicator->getRank()+1), 2);
            points = vtkSmartPointer<vtkPoints>::New();
        }
        communicator->Barrier();
        //LTYPE+LNODS
        if (meshReader->getIntSize()==4){
            types=new int[meshReader->getLtype4()->getLines()];
        }else{
            types=new int[meshReader->getLtype8()->getLines()];
        }
        cellArray = vtkSmartPointer<vtkCellArray>::New();
        if (!disableSubdomain){
            Message::Info(communicator->getRank(), "Converting cells", 2);
            if (!convertCells(types, cellArray)){
                return false;
            }
        }
        pointArrayDouble.clear();
        pointArrayInt.clear();
        pointArrayLong.clear();
        cellArrayDouble.clear();
        cellArrayInt.clear();
        cellArrayLong.clear();
        if (more){
            for (auto file : postMeshReader->getPostMeshFloat8()){
                STOP_ON_ERROR(file->read())
                if (AlyaString::Equal(file->getHeader()->getResultsOn(),MPIOHEADER_RES_POINT)){
                    pointArrayDouble.push_back(convertData(file));
                }else if (AlyaString::Equal(file->getHeader()->getResultsOn(),MPIOHEADER_RES_ELEM)){
                    cellArrayDouble.push_back(convertData(file));
                }
            }
            for (auto file : postMeshReader->getPostMeshInteger4()){
                STOP_ON_ERROR(file->read())
                if (AlyaString::Equal(file->getHeader()->getResultsOn(),MPIOHEADER_RES_POINT)){
                    pointArrayInt.push_back(convertData(file));
                }else if (AlyaString::Equal(file->getHeader()->getResultsOn(),MPIOHEADER_RES_ELEM)){
                    cellArrayInt.push_back(convertData(file));
                }
            }
            for (auto file : postMeshReader->getPostMeshInteger8()){
                STOP_ON_ERROR(file->read())
                if (AlyaString::Equal(file->getHeader()->getResultsOn(),MPIOHEADER_RES_POINT)){
                    pointArrayLong.push_back(convertData(file));
                }else if (AlyaString::Equal(file->getHeader()->getResultsOn(),MPIOHEADER_RES_ELEM)){
                    cellArrayLong.push_back(convertData(file));
                }
            }
        }
    }
    return true;
}

bool alya::VTKPPostMeshWriter::createGrid()
{
    if (communicator->isIOWorker()){
        Message::Info(communicator->getRank(), "Creating unstructured grid", 2);
        unstructuredGrid = vtkSmartPointer<vtkUnstructuredGrid>::New();
        unstructuredGrid->SetPoints(points);
        unstructuredGrid->SetCells(types, cellArray);
        if (more){
            for (auto array: pointArrayDouble){
                unstructuredGrid->GetPointData()->AddArray(array);
            }
            for (auto array: pointArrayInt){
                unstructuredGrid->GetPointData()->AddArray(array);
            }
            for (auto array: pointArrayLong){
                unstructuredGrid->GetPointData()->AddArray(array);
            }
            for (auto array: cellArrayDouble){
                unstructuredGrid->GetCellData()->AddArray(array);
            }
            for (auto array: cellArrayInt){
                unstructuredGrid->GetCellData()->AddArray(array);
            }
            for (auto array: cellArrayLong){
                unstructuredGrid->GetCellData()->AddArray(array);
            }
        }
    }
    return true;
}

bool alya::VTKPPostMeshWriter::createIterationDir(string iteration)
{
    string it=outputDir+DIR_SEP+iteration;
    if (communicator->isMasterOrSequential()){
        const int dir_err = mkdir(it.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
        if (-1 == dir_err)
        {
            Message::Error("Cannot create the output directory");
            return false;
        }
    }
    return true;
}

bool alya::VTKPPostMeshWriter::writeFiles(int iteration)
{
    if(communicator->isIOWorker()){
        Message::Info(communicator->getRank(), "Writing vtk files", 2);
        vtkSmartPointer<vtkXMLPUnstructuredGridWriter> pwriter = vtkSmartPointer<vtkXMLPUnstructuredGridWriter>::New();
        string outputFileName=caseName;
        if (iteration!=-1){
            //createIterationDir(AlyaString::Integer8(iteration));
            //outputFileName;
            outputFileName+="_";
            outputFileName+=AlyaString::Integer8(iteration);
            pwriter->SetUseSubdirectory(true);
        }
        outputFileName+=".pvtu";
        outputFileName=outputDir+DIR_SEP+outputFileName;
        pwriter->SetInputData(unstructuredGrid);
        pwriter->SetFileName(outputFileName.c_str());
        pwriter->SetNumberOfPieces(communicator->getSize());
        pwriter->SetStartPiece(communicator->getRank());
        pwriter->SetEndPiece(communicator->getRank());
        //pwriter->SetDataModeToAscii();
        pwriter->Write();
    }
    return true;
}

bool alya::VTKPPostMeshWriter::isValidSubdomain()
{
    if (!isBoundingBox){
        return true;
    }
    bool test=false;
    for (int64_t i=0; i <points->GetNumberOfPoints(); i++){
        double point[3];
        points->GetPoint(i, point);
        if (boundingBox->isInside(point[0], point[1], point[2])){
            //Message::Info("Subdomain: "+ to_string(communicator->getRank()+1) + " valid", 2);
            test=true;
            break;
        }
    }
    return test;
}

vtkSmartPointer<vtkDoubleArray> alya::VTKPPostMeshWriter::convertData(AlyaPtr<MPIOFile<double> > file)
{
    return convert(file, vtkSmartPointer<vtkDoubleArray>::New());
}

vtkSmartPointer<vtkIntArray> alya::VTKPPostMeshWriter::convertData(AlyaPtr<MPIOFile<int32_t> > file)
{
    return convert(file, vtkSmartPointer<vtkIntArray>::New());
}

vtkSmartPointer<vtkLongArray> alya::VTKPPostMeshWriter::convertData(AlyaPtr<MPIOFile<int64_t> > file)
{
    return convert(file, vtkSmartPointer<vtkLongArray>::New());
}

template<typename F, typename T>
vtkSmartPointer<T> alya::VTKPPostMeshWriter::convert(AlyaPtr<MPIOFile<F> > file, vtkSmartPointer<T> array)
{
    array->SetNumberOfComponents(file->getColumns());
    array->SetName(AlyaString::Format5(file->getHeader()->getObject()).c_str());
    if (!disableSubdomain){
        if (file->getColumns()==1){
            for (int64_t i = 0; i < file->getLines(); i++){
                array->InsertNextValue(file->getData(i));
            }
        }else{
            F* val=new F[file->getColumns()];
            for (int64_t i = 0; i < file->getLines(); i++){
                for (int64_t j=0; j< file->getColumns(); j++){
                    val[j]=file->getData(i, j);
                }
        #if VTK_MAJOR_VERSION < 8
                array->InsertNextTupleValue(val);
        #else
                array->InsertNextTypedTuple(val);
        #endif
            }
            delete val;
        }
    }
    return array;
}
