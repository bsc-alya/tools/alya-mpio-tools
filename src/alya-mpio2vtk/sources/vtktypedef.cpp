/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include <vtktypedef.h>

map <unsigned int, VTKCellType> alya::VtkTypeDef::Types2vtk;

void alya::VtkTypeDef::Init()
{
     /* 2D elements */
    Types2vtk[ALYA_TRI03]=VTK_TRIANGLE;
    Types2vtk[ALYA_TRI06]=VTK_QUADRATIC_TRIANGLE;
    Types2vtk[ALYA_TRI10]=VTK_LAGRANGE_TRIANGLE;
    Types2vtk[ALYA_QUA04]=VTK_QUAD;
    Types2vtk[ALYA_QUA09]=VTK_BIQUADRATIC_QUAD;
     /* 3D elements */
    Types2vtk[ALYA_TET04]=VTK_TETRA;
    Types2vtk[ALYA_TET10]=VTK_QUADRATIC_TETRA;
    Types2vtk[ALYA_TET20]=VTK_LAGRANGE_TETRAHEDRON;
    Types2vtk[ALYA_PYR05]=VTK_PYRAMID;
    Types2vtk[ALYA_PEN06]=VTK_WEDGE;
    Types2vtk[ALYA_PEN18]=VTK_BIQUADRATIC_QUADRATIC_WEDGE;
    Types2vtk[ALYA_HEX08]=VTK_HEXAHEDRON;
    Types2vtk[ALYA_HEX27]=VTK_TRIQUADRATIC_HEXAHEDRON;
    Types2vtk[ALYA_BAR3D]=VTK_LINE;
    Types2vtk[ALYA_SHELL]=VTK_TRIANGLE;

}
