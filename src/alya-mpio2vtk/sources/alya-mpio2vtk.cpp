/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include <iostream>
#include <argumentmanager.h>
#include <message.h>
#include <pointer.h>
#include <meshreader.h>
#include <postmeshreader.h>
#include <postreader.h>
#include <mpiocomm.h>
#include <mpiowrapper.h>
#include <vtkmeshwriter.h>
#include <vtkppostmeshwriter.h>
#include <vtkppostwriter.h>
#include <vtkMPIController.h>

using namespace std;
using namespace alya;

int main(int argc, char* argv[])
{
    Message::InitTime();
    MPIO::Init(argc, argv);
    AlyaPtr_MPIOComm communicator(new MPIOComm());
    int rank=communicator->getRank();
    Message::IAM(rank, "Alya MPI-IO 2 VTK converter");
    if (communicator->isSequential()){
       Message::Info(rank, "Sequential version");
    }else{
       Message::Info(rank, "Parallel version: "+to_string(communicator->getSize())+" processes");
    }
    Message::Separator(rank);
    Message::Info(rank, "Reading arguments", 1);
    AlyaPtr<ArgumentManager> argumentManager(new ArgumentManager(argc, argv, communicator));
    if (argumentManager->getHelp()){
        argumentManager->usage();
        MPIO::Finalize();
        return RETURN_OK;

    }
    if (!argumentManager->getValid()){
        Message::Critical(rank, "Invalid arguments");
        argumentManager->usage();
        MPIO::Abort();
        return RETURN_ERR;
    }
    if (argumentManager->getConvertType()==CONVERT_TYPE_MESH){
        Message::Info(rank, "Reading mesh", 1);
        AlyaPtr<MeshReader> meshReader(new MeshReader(argumentManager->getCaseName(), communicator));
        if (!meshReader->read()){
            Message::Critical(rank, "Could not read the mesh");
            MPIO::Abort();
            return RETURN_ERR;
        }
        Message::Info(rank, "Converting mesh", 1);
        AlyaPtr<VTKMeshWriter> meshWriter(new VTKMeshWriter(meshReader, meshReader->getCommunicator(), argumentManager));
        if (!meshWriter->write()){
            Message::Critical(rank, "Could not write the vtk file");
            MPIO::Abort();
            return RETURN_ERR;
        }
    }else if (argumentManager->getConvertType()==CONVERT_TYPE_POST){
        Message::Info(rank, "Reading mesh", 1);
        AlyaPtr<PostMeshReader> meshReader(new PostMeshReader(argumentManager->getCaseName(), communicator, true));
        if (!meshReader->read()){
            Message::Critical(rank, "Could not read the mesh");
            MPIO::Abort();
            return RETURN_ERR;
        }
        Message::Info(rank, "Converting mesh", 1);
#ifndef NO_MPI
        vtkSmartPointer<vtkMPIController> controller = vtkMPIController::New();
        controller->Initialize(&argc, &argv, 1);
        vtkSmartPointer<vtkMPIController> subcontroller = controller->PartitionController(meshReader->getCommunicator()->isIOWorker(), meshReader->getCommunicator()->getRank());
        controller->SetGlobalController(subcontroller);
#endif
        AlyaPtr<VTKPPostMeshWriter> meshWriter(new VTKPPostMeshWriter(meshReader, meshReader->getCommunicator(), argumentManager));
        if (!meshWriter->write()){
            Message::Critical(rank, "Could not write the vtk files");
#ifndef NO_MPI
            controller->Finalize(1);
#endif
            MPIO::Abort();
            return RETURN_ERR;
        }
#ifndef NO_MPI
        controller->Finalize(1);
#endif
    }else if (argumentManager->getConvertType()==CONVERT_TYPE_ALL){
        Message::Info(rank, "Reading mesh", 1);
        AlyaPtr<PostReader> meshReader(new PostReader(argumentManager->getCaseName(), communicator, true));
        if (!meshReader->readMeshOnly()){
            Message::Critical(rank, "Could not read the mesh");
            MPIO::Abort();
            return RETURN_ERR;
        }
        Message::Info(rank, "Converting mesh", 1);
#ifndef NO_MPI
        vtkSmartPointer<vtkMPIController> controller = vtkMPIController::New();
        controller->Initialize(&argc, &argv, 1);
        vtkSmartPointer<vtkMPIController> subcontroller = controller->PartitionController(meshReader->getCommunicator()->isIOWorker(), meshReader->getCommunicator()->getRank());
        controller->SetGlobalController(subcontroller);
#endif
        AlyaPtr<VTKPPostWriter> meshWriter(new VTKPPostWriter(meshReader, meshReader->getCommunicator(), argumentManager,
                                                              argumentManager->getIterations(), argumentManager->getRangeMin(), argumentManager->getRangeMax()));
        if (!meshWriter->write()){
            Message::Critical(rank, "Could not write the vtk files");
#ifndef NO_MPI
            controller->Finalize(1);
#endif
            MPIO::Abort();
            return RETURN_ERR;
        }
#ifndef NO_MPI
        controller->Finalize(1);
#endif
    }
    Message::DONE(rank);
    Message::PrintTime(rank);
    MPIO::Finalize();
    return RETURN_OK;
}
