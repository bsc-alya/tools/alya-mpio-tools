/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include "vtkppostwriter.h"

alya::VTKPPostWriter::VTKPPostWriter(AlyaPtr<alya::PostReader> meshReader, AlyaPtr_MPIOComm communicator, AlyaPtr<ArgumentManager> argumentManager,  vector<int> iterations, int minIteration, int maxIteration):
VTKPPostMeshWriter(dynamic_pointer_cast<PostMeshReader>(meshReader), communicator, argumentManager), postReader(meshReader), iterations(iterations), minIteration(minIteration), maxIteration(maxIteration){

}

bool alya::VTKPPostWriter::write()
{
    if (!convertMesh()){
        return false;
    }
    if (!createOutputDir()){
        return false;
    }
    for (const auto &time:postReader->getTime(iterations, minIteration, maxIteration)){
        int ittim=time.first;
        Message::Info(communicator->getRank(), "Generating files, step: "+to_string(ittim), 1);
        if (!createGrid()){
            return false;
        }
        if (!postReader->readPostsByIteration(ittim)){
            return false;
        }
        if (!pushData()){
            return false;
        }
        if (!writeFiles(ittim)){
            return false;
        }
    }
    return writePvd();
}

bool alya::VTKPPostWriter::writePvd()
{
    Message::Info(communicator->getRank(), "Generating vtk master file", 1);
    if (communicator->isMasterOrSequential()){
        ofstream pvdfile;
        std::remove((outputDir+DIR_SEP+caseName+".pvd").c_str());
        pvdfile.open(outputDir+DIR_SEP+caseName+".pvd");
        pvdfile<<"<VTKFile type=\"Collection\" version=\"0.1\" byte_order=\"LittleEndian\">"<<endl;
        pvdfile<<"\t<Collection>"<<endl;
        for (const auto &time:postReader->getTime(iterations, minIteration, maxIteration)){
            pvdfile<<"\t\t<DataSet timestep=\""<<time.second<<"\" file=\'"<<caseName+"_"+AlyaString::Integer8(time.first)+".pvtu\'/>"<<endl;
        }
        pvdfile<<"\t</Collection>"<<endl;
        pvdfile<<"</VTKFile>"<<endl;
        pvdfile.close();
    }
    return true;
}


bool alya::VTKPPostWriter::pushData()
{
    Message::Info(communicator->getRank(), "Converting field data", 2);
    if (communicator->isIOWorker()){
        for (auto file : postReader->getPostFloat8()){
            if (!postReader->readFile(file)){
                return false;
            }
            if (!pushData(file)){
                return false;
            }
        }
        for (auto file : postReader->getPostInteger4()){
            if (!postReader->readFile(file)){
                return false;
            }
            if (!pushData(file)){
                return false;
            }
        }
        for (auto file : postReader->getPostInteger8()){
            if (!postReader->readFile(file)){
                return false;
            }
            if (!pushData(file)){
                return false;
            }
        }
    }
    return true;
}

bool alya::VTKPPostWriter::pushData(AlyaPtr<MPIOFile<double> > file)
{
    vtkSmartPointer<vtkDoubleArray> array=convertData(file);
    return addArray(file, array);
}

bool alya::VTKPPostWriter::pushData(AlyaPtr<MPIOFile<int32_t> > file)
{
    vtkSmartPointer<vtkIntArray> array=convertData(file);
    return addArray(file, array);
}

bool alya::VTKPPostWriter::pushData(AlyaPtr<MPIOFile<int64_t> > file)
{
    vtkSmartPointer<vtkLongArray> array=convertData(file);
    return addArray(file, array);
}


template<typename F, typename T>
bool alya::VTKPPostWriter::addArray(AlyaPtr<MPIOFile<F> > file, vtkSmartPointer<T> array)
{
    if (AlyaString::Equal(file->getHeader()->getResultsOn(),MPIOHEADER_RES_POINT)){
        unstructuredGrid->GetPointData()->AddArray(array);
    }else if (AlyaString::Equal(file->getHeader()->getResultsOn(),MPIOHEADER_RES_ELEM)){
        unstructuredGrid->GetCellData()->AddArray(array);
    }
    return true;
}
