/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include "boundingbox.h"

alya::BoundingBox::BoundingBox::BoundingBox()
{
    this->xmin=0;
    this->xmax=0;
    this->ymin=0;
    this->ymax=0;
    this->zmin=0;
    this->zmax=0;
}

alya::BoundingBox::BoundingBox(double xmin, double xmax, double ymin, double ymax, double zmin, double zmax): xmin(xmin), xmax(xmax), ymin(ymin), ymax(ymax), zmin(zmin), zmax(zmax)
{

}

void alya::BoundingBox::setCoord(double xmin, double xmax, double ymin, double ymax, double zmin, double zmax)
{
    this->xmin=xmin;
    this->xmax=xmax;
    this->ymin=ymin;
    this->ymax=ymax;
    this->zmin=zmin;
    this->zmax=zmax;
}

bool alya::BoundingBox::isInside(double x, double y, double z)
{
    return (x>=xmin && x<=xmax && y>=ymin && y<=ymax && z>=zmin && z<=zmax);
}

double alya::BoundingBox::getZmax() const
{
    return zmax;
}

void alya::BoundingBox::setZmax(const double &value)
{
    zmax = value;
}

double alya::BoundingBox::getZmin() const
{
    return zmin;
}

void alya::BoundingBox::setZmin(const double &value)
{
    zmin = value;
}

double alya::BoundingBox::getYmax() const
{
    return ymax;
}

void alya::BoundingBox::setYmax(const double &value)
{
    ymax = value;
}

double alya::BoundingBox::getYmin() const
{
    return ymin;
}

void alya::BoundingBox::setYmin(const double &value)
{
    ymin = value;
}

void alya::BoundingBox::setXmin(const double &value)
{
    xmin = value;
}

double alya::BoundingBox::getXmax() const
{
    return xmax;
}

void alya::BoundingBox::setXmax(const double &value)
{
    xmax = value;
}

double alya::BoundingBox::getXmin() const
{
    return xmin;
}
