/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#ifndef VTKPOSTWRITER_H
#define VTKPOSTWRITER_H

#include <vtkppostmeshwriter.h>
#include <postreader.h>
#include <iostream>
#include <fstream>
#include <cstdio>
#include <mpiowrapper.h>

using namespace std;

namespace alya{

    class VTKPPostWriter : public VTKPPostMeshWriter
    {
    public:
        VTKPPostWriter(AlyaPtr<alya::PostReader>, AlyaPtr_MPIOComm communicator, AlyaPtr<ArgumentManager> argumentManager, vector<int> iterations, int minIteration=0, int maxIteration=0);
        bool write();
    protected:
        bool pushData();
        bool writePvd();
        bool pushData(AlyaPtr<MPIOFile<double> >);
        bool pushData(AlyaPtr<MPIOFile<int32_t> >);
        bool pushData(AlyaPtr<MPIOFile<int64_t> >);
        template<typename F, typename T>
        bool addArray(AlyaPtr<MPIOFile<F> >, vtkSmartPointer<T>);
        AlyaPtr<PostReader> postReader;
        vector<int> iterations;
        int minIteration, maxIteration;
    };

}

#endif // VTKPOSTWRITER_H
