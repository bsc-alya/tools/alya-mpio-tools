/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#ifndef VTKPPOSTMESHWRITER_H
#define VTKPPOSTMESHWRITER_H

#include <vtkmeshwriter.h>
#include <postmeshreader.h>
#include <vtkXMLPUnstructuredGridWriter.h>
#include <vtkDoubleArray.h>
#include <vtkIntArray.h>
#include <vtkLongArray.h>
#include <vtkCellData.h>
#include <vtkPointData.h>
#include <vtkDenseArray.h>
#include <map>


namespace alya{

    class VTKPPostMeshWriter : public VTKMeshWriter
    {
    public:
        VTKPPostMeshWriter(AlyaPtr<PostMeshReader> meshReader, AlyaPtr_MPIOComm communicator, AlyaPtr<ArgumentManager> argumentManager);
        bool write();
    protected:
        bool convertMesh();
        bool createGrid();
        bool createIterationDir(string iteration);
        bool writeFiles(int iteration=-1);
        bool isValidSubdomain();
        vtkSmartPointer<vtkDoubleArray> convertData(AlyaPtr<MPIOFile<double> >);
        vtkSmartPointer<vtkIntArray> convertData(AlyaPtr<MPIOFile<int32_t> >);
        vtkSmartPointer<vtkLongArray> convertData(AlyaPtr<MPIOFile<int64_t> >);
        template<typename F, typename T>
        vtkSmartPointer<T> convert(AlyaPtr<MPIOFile<F> >, vtkSmartPointer<T>);
        vector<vtkSmartPointer<vtkDoubleArray> > pointArrayDouble;
        vector<vtkSmartPointer<vtkIntArray> > pointArrayInt;
        vector<vtkSmartPointer<vtkLongArray> > pointArrayLong;
        vector<vtkSmartPointer<vtkDoubleArray> > cellArrayDouble;
        vector<vtkSmartPointer<vtkIntArray> > cellArrayInt;
        vector<vtkSmartPointer<vtkLongArray> > cellArrayLong;
        AlyaPtr<PostMeshReader> postMeshReader;
        bool more;
        bool disableSubdomain;
        bool isBoundingBox;
        AlyaPtr<BoundingBox> boundingBox;

    };

}

#endif // VTKPPOSTMESHWRITER_H
