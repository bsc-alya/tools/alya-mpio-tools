/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#ifndef VTKMESHWRITER_H
#define VTKMESHWRITER_H

#include <alya-mpio2vtk_config.h>
#include <alyatypedef.h>
#include <vtktypedef.h>
#include <highordermap.h>
#include <meshreader.h>
#include <pointer.h>
#include <mpiocomm.h>
#include <argumentmanager.h>

#include <vtkVersion.h>
#include <vtkSmartPointer.h>
/* 2D elements*/
#include <vtkTriangle.h>
#include <vtkQuadraticTriangle.h>
#include <vtkLagrangeTriangle.h>
#include <vtkQuad.h>
#include <vtkBiQuadraticQuad.h>
#include <vtkLine.h>
/* 3D elements*/
#include <vtkTetra.h>
#include <vtkQuadraticTetra.h>
#include <vtkLagrangeTetra.h>
#include <vtkPyramid.h>
#include <vtkWedge.h>
#include <vtkBiQuadraticQuadraticWedge.h>
#include <vtkHexahedron.h>
#include <vtkTriQuadraticHexahedron.h>
#include <vtkCellArray.h>
#include <vtkDataSetMapper.h>
#include <vtkActor.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkXMLUnstructuredGridWriter.h>
#include <vtkUnstructuredGrid.h>
#include <vtkPointData.h>
#include <vtkVertexGlyphFilter.h>

#include <sys/types.h>
#include <sys/stat.h>

#include <map>


namespace alya{

    class VTKMeshWriter
    {
    public:
        VTKMeshWriter(AlyaPtr<MeshReader> meshReader, AlyaPtr_MPIOComm communicator, AlyaPtr<ArgumentManager> argumentManager);
        ~VTKMeshWriter();
        bool write();

    protected:
        bool convertMesh();
        bool createGrid();
        bool writeFiles();
        bool createOutputDir();
        bool convertPoints(vtkSmartPointer<vtkPoints> points);

        /* New 2D point converter - same as 3D */
        bool convertPoints2D(vtkSmartPointer<vtkPoints> points);

        bool convertPoints3D(vtkSmartPointer<vtkPoints> points);
        bool convertCells(int types[], vtkSmartPointer<vtkCellArray> cellArray);

        template<typename T>
        bool convertCell(int types[], vtkSmartPointer<vtkCellArray> cellArray, int64_t i, vtkSmartPointer<T> cell);

        template<typename T>
        bool convertHOCell(int types[], vtkSmartPointer<vtkCellArray> cellArray, int64_t i, vtkSmartPointer<T> cell);

        AlyaPtr<MeshReader> meshReader;
        AlyaPtr_MPIOComm communicator;
        string caseName;
        string outputDir;
        vtkSmartPointer<vtkPoints> points;
        vtkSmartPointer<vtkCellArray> cellArray;
        vtkSmartPointer<vtkUnstructuredGrid> unstructuredGrid;
        int *types;
        AlyaPtr<ArgumentManager> argumentManager;
        Hex27Map hex27Map;
        Pen18Map pen18Map;
        Tet20Map tet20Map;
    };

}

#endif // VTKMESHWRITER_H
