#ifndef HIGHORDERMAP_H
#define HIGHORDERMAP_H

#include <map>
#include <cstdint>

using namespace std;

namespace alya{

class HighOrderMap
    {
    public:
        int64_t get(int64_t i);
    protected:
        map<int64_t, int64_t> ho;
    };

class Hex27Map : public HighOrderMap
    {
    public:
        void init();
    };

class Pen18Map : public HighOrderMap
    {
    public:
        void init();
    };

class Tet20Map : public HighOrderMap
    {
    public:
        void init();
    };


}

#endif // HIGHORDERMAP_H
