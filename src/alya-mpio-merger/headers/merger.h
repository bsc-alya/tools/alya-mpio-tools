/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#ifndef MERGER_H
#define MERGER_H

#include <alya-mpio-merger_config.h>
#include <mpiocomm.h>
#include <mpioheader.h>
#include <mpiofile.h>
#include <mpiopartition.h>
#include <pointer.h>
#include <message.h>
#include <alyastring.h>
#include <alyareturn.h>
#include <argumentmanager.h>


#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/trim_all.hpp>
#include <boost/tokenizer.hpp>

using namespace std;

namespace alya {

    class Merger
    {
    public:
        Merger(AlyaPtr_MPIOComm communicator, AlyaPtr<ArgumentManager> argumentManager);
        bool process();
    private:
        AlyaPtr_MPIOComm communicator;
        bool init();
        bool computePartition();
        bool merge();
        bool inv();
        bool inv2();
        string fileName;
        string output;
        string invName, invName2;
        string caseName;
        string resultsOn;
        string type;
        int64_t typeSize;
        int64_t invTypeSize;
        AlyaPtr<MPIOFile<int32_t> > fileI4;
        AlyaPtr<MPIOFile<int64_t> > fileI8;
        AlyaPtr<MPIOFile<int32_t> > invI4;
        AlyaPtr<MPIOFile<int64_t> > invI8;
        AlyaPtr<MPIOFile<int32_t> > invI4_2;
        AlyaPtr<MPIOFile<int64_t> > invI8_2;
        AlyaPtr<MPIOFile<double> > fileD;
        AlyaPtr<MPIOPartition> partition;
        bool toField;
        bool useInv2;
        int64_t tag1, tag2;
        int secondInv;
    };

}



#endif // HEADERMANAGER_H
