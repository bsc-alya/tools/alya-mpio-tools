/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include "argumentmanager.h"

alya::ArgumentManager::ArgumentManager(int argc, char **argv, AlyaPtr_MPIOComm communicator):communicator(communicator)
{
    help=false;
    valid=true;
    toField=false;
    me=string(argv[0]);
    string arg;
    tag1=0;
    tag2=0;
    output="";
    secondInv=NOINV;
    if (argc<2){
        valid=false;
        return;
    }
    for (int i=1; i<argc-1; i++){
        string str=string(argv[i]);
        if (ARGUMENT(str, "-h", "--help")){
            help=true;
            return;
        }else if (ARGUMENT(str, "-f", "--field")){
            tag1=atol(argv[++i]);
            tag2=atol(argv[++i]);
            toField=true;
            Message::Info(communicator->getRank(), "Generating a XFIEL, number "+to_string(tag1)+", step "+to_string(tag2), 2);
        }else if (ARGUMENT(str, "-o", "--output")){
            output=string(argv[++i]);
            Message::Info(communicator->getRank(), "Output file: "+output, 2);
        }else if (ARGUMENT(str, "-b", "--lbinv")){
            secondInv=LBINV;
            Message::Info(communicator->getRank(), "Content will be redistributed using LBINV", 2);
        }else if (ARGUMENT(str, "-e", "--leinv")){
            secondInv=LEINV;
            Message::Info(communicator->getRank(), "Content will be redistributed using LEINV", 2);
        }else if (ARGUMENT(str, "-n", "--lninv")){
            secondInv=LNINV;
            Message::Info(communicator->getRank(), "Content will be redistributed using LNINV", 2);
        }
    }
    file=string(argv[argc-1]);
    Message::Info(communicator->getRank(), "Merging "+file, 2);
}

void alya::ArgumentManager::usage()
{
    Message::Info(communicator->getRank(), "Usage");
    Message::Info(communicator->getRank(), string(me.c_str())+" [OPTIONS] file");
    Message::Info(communicator->getRank(), "Options:");
    Message::Info(communicator->getRank(), "-h     | --help              : Usage");
    Message::Info(communicator->getRank(), "-f     | --field [int] [int] : Convert to field (tag1 tag2)");
    Message::Info(communicator->getRank(), "-o     | --output [string]   : Output file name");
    Message::Info(communicator->getRank(), "-b     | --lbinv             : Redistribute content using LBINV");
    Message::Info(communicator->getRank(), "-e     | --leinv             : Redistribute content using LEINV");
    Message::Info(communicator->getRank(), "-n     | --lninv             : Redistribute content using LNINV");
}

string alya::ArgumentManager::getFile() const
{
    return file;
}

bool alya::ArgumentManager::getToField() const
{
    return toField;
}

int alya::ArgumentManager::getSecondInv() const
{
    return secondInv;
}

string alya::ArgumentManager::getOutput() const
{
    return output;
}

int64_t alya::ArgumentManager::getTag2() const
{
    return tag2;
}

int64_t alya::ArgumentManager::getTag1() const
{
    return tag1;
}

bool alya::ArgumentManager::getHelp() const
{
    return help;
}

bool alya::ArgumentManager::getValid() const
{
    return valid;
}

