/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include "merger.h"


alya::Merger::Merger(AlyaPtr<alya::MPIOComm> communicator, AlyaPtr<ArgumentManager> argumentManager):
communicator(communicator), fileName(argumentManager->getFile()),
  output(argumentManager->getOutput()), toField(argumentManager->getToField()),
   tag1(argumentManager->getTag1()), tag2(argumentManager->getTag2()), secondInv(argumentManager->getSecondInv())
{

}

bool alya::Merger::process()
{
    Message::Info(communicator->getRank(),"Initializing",2);
    STOP_ON_ERROR(init())
    Message::Info(communicator->getRank(),"Retrieving partition",2);
    STOP_ON_ERROR(computePartition())
    Message::Info(communicator->getRank(),"Processing INV file",2);
    STOP_ON_ERROR(inv())
    useInv2=inv2();
    if (useInv2){
        Message::Info(communicator->getRank(),"Processing INV2 file",2);
    }
    Message::Info(communicator->getRank(),"Processing file",2);
    STOP_ON_ERROR(merge())
    return true;
}

bool alya::Merger::init()
{
    AlyaPtr_MPIOHeader header=AlyaPtr_MPIOHeader(new MPIOHeader(communicator, fileName));
    header->open(READ);
    header->read();
    header->close();
    if (header->getNsubd()<2){
        Message::Error(communicator->getRank(),"File does not contain subdomains and won't be merged");
        return false;
    }
    resultsOn=header->getResultsOn();
    type=header->getType();
    typeSize=header->getIntSize();
    string object=AlyaString::Format5(header->getObject());
    unsigned long index=fileName.find(object)-1;
    caseName=fileName.substr(0, index);
    return true;
}

bool alya::Merger::computePartition()
{
    string partitionFile=caseName+".post.alyapar";
    partition=AlyaPtr<MPIOPartition>(new MPIOPartition(communicator, partitionFile));
    STOP_ON_ERROR(partition->computePartition())
    communicator=partition->getCommunicator();
    return true;
}

bool alya::Merger::merge()
{
    string mergeFileName=fileName+".merged";
    if (output.compare("")!=0){
        mergeFileName=output;
    }
    if (mergeFileName.compare(fileName)==0){
        Message::Critical(communicator->getRank(),"The input and output files are the same!");
        return false;
    }
    if (AlyaString::Equal(type, MPIOHEADER_TYPE_REAL)){
        fileD=AlyaPtr<MPIOFile<double> >(new MPIOFile<double>(communicator, fileName));
        STOP_ON_ERROR(fileD->open(READ))
        fileD->setPartition(partition);
        STOP_ON_ERROR(fileD->read())
        STOP_ON_ERROR(fileD->close())
        if (invTypeSize==4){
            STOP_ON_ERROR(fileD->merge(invI4))
        }else{
            STOP_ON_ERROR(fileD->merge(invI8))
        }
        AlyaPtr<double> data=fileD->getData();
        AlyaPtr_MPIOComm communicator2=communicator->split(communicator->isMasterOrSequential()?MPIO_WORKER:MPIO_NOT_WORKER);
        if (toField){
            mergeFileName=caseName+"-XFIEL."+AlyaString::Integer8(tag1)+"."+AlyaString::Integer8(tag2)+".mpio.bin";
        }
        AlyaPtr<MPIOFile<double> >fileMerge=AlyaPtr<MPIOFile<double> >(new MPIOFile<double>(communicator2, mergeFileName));
        if (!toField){
            STOP_ON_ERROR(fileMerge->setHeader(fileD->getHeader(), fileD->getLines(), 1))
        }else{
            STOP_ON_ERROR(fileMerge->setHeaderAsField(fileD->getHeader(), fileD->getLines(), tag1, tag2, 1))
        }
        STOP_ON_ERROR(fileMerge->open(WRITE))
        STOP_ON_ERROR(fileMerge->initWriting(false))
        fileMerge->setData(data);
        STOP_ON_ERROR(fileMerge->write())
        STOP_ON_ERROR(fileMerge->close())
    }else if (AlyaString::Equal(type, MPIOHEADER_TYPE_INTEGER)){
        if (typeSize==4){
            fileI4=AlyaPtr<MPIOFile<int32_t> >(new MPIOFile<int32_t>(communicator, fileName));
            STOP_ON_ERROR(fileI4->open(READ))
            fileI4->setPartition(partition);
            STOP_ON_ERROR(fileI4->read())
            if (useInv2){
                STOP_ON_ERROR(fileI4->merge(invI4, invI4_2))
            }else{
                STOP_ON_ERROR(fileI4->merge(invI4))
            }
            STOP_ON_ERROR(fileI4->close())
            AlyaPtr<int32_t> data=fileI4->getData();
            AlyaPtr_MPIOComm communicator2=communicator->split(communicator->isMasterOrSequential()?MPIO_WORKER:MPIO_NOT_WORKER);
            AlyaPtr<MPIOFile<int32_t> >fileMerge=AlyaPtr<MPIOFile<int32_t> >(new MPIOFile<int32_t>(communicator2, mergeFileName));
            STOP_ON_ERROR(fileMerge->setHeader(fileI4->getHeader(), fileI4->getLines(), 1))
            STOP_ON_ERROR(fileMerge->open(WRITE))
            fileMerge->setData(data);
            STOP_ON_ERROR(fileMerge->initWriting(false))
            STOP_ON_ERROR(fileMerge->write())
            STOP_ON_ERROR(fileMerge->close())
        }else if (typeSize==8){
            fileI8=AlyaPtr<MPIOFile<int64_t> >(new MPIOFile<int64_t>(communicator, fileName));
            STOP_ON_ERROR(fileI8->open(READ))
            fileI8->setPartition(partition);
            STOP_ON_ERROR(fileI8->read())
            if (useInv2){
                STOP_ON_ERROR(fileI8->merge(invI8, invI8_2))
            }else{
                STOP_ON_ERROR(fileI8->merge(invI8))
            }
            STOP_ON_ERROR(fileI8->close())
            AlyaPtr<int64_t> data=fileI8->getData();
            AlyaPtr_MPIOComm communicator2=communicator->split(communicator->isMasterOrSequential()?MPIO_WORKER:MPIO_NOT_WORKER);
            AlyaPtr<MPIOFile<int64_t> >fileMerge=AlyaPtr<MPIOFile<int64_t> >(new MPIOFile<int64_t>(communicator2, mergeFileName));
            STOP_ON_ERROR(fileMerge->setHeader(fileI8->getHeader(), fileI8->getLines(), 1))
            STOP_ON_ERROR(fileMerge->open(WRITE))
            fileMerge->setData(data);
            STOP_ON_ERROR(fileMerge->initWriting(false))
            STOP_ON_ERROR(fileMerge->write())
            STOP_ON_ERROR(fileMerge->close())
        }
    }
    return true;
}

bool alya::Merger::inv()
{
    if (AlyaString::Equal(resultsOn, MPIOHEADER_RES_POINT)){
        invName=caseName+"-LNINV.post.mpio.bin";
    }else if (AlyaString::Equal(resultsOn, MPIOHEADER_RES_ELEM)){
        invName=caseName+"-LEINV.post.mpio.bin";
    }else if (AlyaString::Equal(resultsOn, MPIOHEADER_RES_BOUND)){
        invName=caseName+"-LBINV.post.mpio.bin";
    }else{
        Message::Error("Cannot merge this file because there is no corresponding INV");
        return false;
    }
    AlyaPtr_MPIOHeader invHeader=AlyaPtr_MPIOHeader(new MPIOHeader(communicator, invName));
    STOP_ON_ERROR(invHeader->open(READ))
    STOP_ON_ERROR(invHeader->read())
    STOP_ON_ERROR(invHeader->close())
    invTypeSize=invHeader->getIntSize();
    if (invTypeSize==4){
        invI4=AlyaPtr<MPIOFile<int32_t> >(new MPIOFile<int32_t>(communicator, invName));
        STOP_ON_ERROR(invI4->open(READ))
        invI4->setPartition(partition);
        STOP_ON_ERROR(invI4->read())
        STOP_ON_ERROR(invI4->close())
        STOP_ON_ERROR(invI4->gather())
    }else if (invTypeSize==8){
        invI8=AlyaPtr<MPIOFile<int64_t> >(new MPIOFile<int64_t>(communicator, invName));
        STOP_ON_ERROR(invI8->open(READ))
        invI8->setPartition(partition);
        STOP_ON_ERROR(invI8->read())
        STOP_ON_ERROR(invI8->close())
        STOP_ON_ERROR(invI8->gather())
    }
    return true;
}

bool alya::Merger::inv2()
{
    if (secondInv==LNINV){
        invName2=caseName+"-LNINV.post.mpio.bin";
    }else if (secondInv==LEINV){
        invName2=caseName+"-LEINV.post.mpio.bin";
    }else if (secondInv==LBINV){
        invName2=caseName+"-LBINV.post.mpio.bin";
    }else{
        return false;
    }
    AlyaPtr_MPIOHeader invHeader=AlyaPtr_MPIOHeader(new MPIOHeader(communicator, invName2));
    STOP_ON_ERROR(invHeader->open(READ))
    STOP_ON_ERROR(invHeader->read())
    STOP_ON_ERROR(invHeader->close())
    if (invTypeSize==4){
        invI4_2=AlyaPtr<MPIOFile<int32_t> >(new MPIOFile<int32_t>(communicator, invName2));
        STOP_ON_ERROR(invI4_2->open(READ))
        invI4_2->setPartition(partition);
        STOP_ON_ERROR(invI4_2->read())
        STOP_ON_ERROR(invI4_2->close())
        STOP_ON_ERROR(invI4_2->gather())
    }else if (invTypeSize==8){
        invI8_2=AlyaPtr<MPIOFile<int64_t> >(new MPIOFile<int64_t>(communicator, invName2));
        STOP_ON_ERROR(invI8_2->open(READ))
        invI8_2->setPartition(partition);
        STOP_ON_ERROR(invI8_2->read())
        STOP_ON_ERROR(invI8_2->close())
        STOP_ON_ERROR(invI8_2->gather())
    }
    return true;
}


