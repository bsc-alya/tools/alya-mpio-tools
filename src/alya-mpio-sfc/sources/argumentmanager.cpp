/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include "argumentmanager.h"

alya::ArgumentManager::ArgumentManager(int argc, char **argv, AlyaPtr_MPIOComm communicator):communicator(communicator)
{
    help=false;
    caseName="";
    subdomains=0;
    post=false;
    me=string(argv[0]);
    if (argc<4){
        valid=false;
        return;
    }else{
        for (int i=1; i<argc; i++){
            string str=string(argv[i]);
            if (ARGUMENT(str, "-h", "--help")){
                help=true;
                return;
            }else if (ARGUMENT(str, "-s", "--subdomains")){
                subdomains=atoi(argv[++i]);
                Message::Info(communicator->getRank(), "Subdomains: "+to_string(subdomains) , 2);
            }else if (ARGUMENT(str, "-p", "--post")){
                post=true;
                Message::Info(communicator->getRank(), "Partitionning merged post-process files", 2);
            }else{
                caseName=str;
                Message::Info(communicator->getRank(), "Case: "+caseName, 2);
                valid=true;
            }
        }
    }
    if (caseName.compare("")==0){
        valid=false;
        Message::Error(communicator->getRank(), "You did not specify any case!");
        return;
    }
    if (subdomains<2){
        valid=false;
        Message::Error(communicator->getRank(), "The number of subdomains must be greater than 1");
        return;
    }
    if (subdomains<communicator->getSize()){
        valid=false;
        Message::Error(communicator->getRank(), "The number of subdomains must be greater or equal to the number of MPI processes");
        return;
    }
}

void alya::ArgumentManager::usage()
{
    Message::Info(communicator->getRank(), "Usage");
    Message::Info(communicator->getRank(), string(me.c_str())+" [OPTIONS] CASE NAME");
    Message::Info(communicator->getRank(), "Options:");
    Message::Info(communicator->getRank(), "-h  | --help              Print usage");
    Message::Info(communicator->getRank(), "-s  | --subdomains        Number of subdomains");
    Message::Info(communicator->getRank(), "-p  | --post              Partition post-process files");
}

string alya::ArgumentManager::getCaseName() const
{
    return caseName;
}

int alya::ArgumentManager::getSubdomains() const
{
    return subdomains;
}

bool alya::ArgumentManager::getPost() const
{
    return post;
}



bool alya::ArgumentManager::getHelp() const
{
    return help;
}

bool alya::ArgumentManager::getValid() const
{
    return valid;
}
