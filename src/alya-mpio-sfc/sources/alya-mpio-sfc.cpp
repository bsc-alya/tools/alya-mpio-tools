/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include <iostream>
#include <argumentmanager.h>
#include <message.h>
#include <pointer.h>
#include <postreader.h>
#include <partitioner.h>
#include <mpiocomm.h>
#include <mpiowrapper.h>

using namespace std;
using namespace alya;


template<typename T>
int partition(AlyaPtr_MPIOComm communicator, AlyaPtr<Partitioner<T>> partitioner){
    int rank=communicator->getRank();
    Message::Info(rank, "Reading mesh files", 2);
    if (!partitioner->readMesh()){
        return RETURN_ERR;
    }
    Message::Info(rank, "Partitioning mesh", 2);
    //TODO show partition results
    if (!partitioner->partitionMesh()){
        return RETURN_ERR;
    }
    Message::Info(rank, "Writing partition file", 2);
    if (!partitioner->writePartition()){
        return RETURN_ERR;
    }
    return RETURN_OK;
}


int main(int argc, char* argv[])
{
    Message::InitTime();
    MPIO::Init(argc, argv);
    AlyaPtr_MPIOComm communicator(new MPIOComm());
    int rank=communicator->getRank();
    Message::IAM(rank, "Alya MPI-IO SFC partitioner");
    Message::Separator(rank);
    if (communicator->isSequential()){
       Message::Info(rank, "Sequential version");
       Message::Error(rank, "This program does not work sequentially due to a bug in Gempa");
       MPIO::Abort();
       return RETURN_ERR;
    }else{
       Message::Info(rank, "Parallel version: "+to_string(communicator->getSize())+" processes");
    }
    Message::Separator(rank);
    Message::Info(rank, "Reading arguments", 1);
    AlyaPtr<ArgumentManager> argumentManager(new ArgumentManager(argc, argv, communicator));
    if (argumentManager->getHelp()){
        argumentManager->usage();
        MPIO::Finalize();
        return RETURN_OK;
    }
    if (!argumentManager->getValid()){
        Message::Critical(rank, "Invalid arguments");
        argumentManager->usage();
        MPIO::Abort();
        return RETURN_ERR;
    }
    Message::Separator(rank);
    Message::Info(rank, "Detecting integer size", 1);
    string lnodsFileName=argumentManager->getCaseName()+LNODS_SUFFIX;
    if (argumentManager->getPost()){
        lnodsFileName+=POST_SUFFIX;
    }
    lnodsFileName+=MPIO_EXT;
    AlyaPtr_MPIOHeader lnodsHeader(new MPIOHeader(communicator, lnodsFileName));
    if (!lnodsHeader->open(READ)){
        MPIO::Abort();
        return RETURN_ERR;
    }
    if (!lnodsHeader->read()){
        MPIO::Abort();
        return RETURN_ERR;
    }
    if (!lnodsHeader->close()){
        MPIO::Abort();
        return RETURN_ERR;
    }
    int returnCode=RETURN_ERR;
    int intSize=lnodsHeader->getIntSize();
    Message::Separator(rank);
    if (intSize==MPIO_TYPESIZE_4){
        AlyaPtr<Partitioner<int32_t>> partitioner(new Partitioner<int32_t>(communicator, argumentManager));
        returnCode=partition(communicator, partitioner);
    }else if (intSize==MPIO_TYPESIZE_8){
        AlyaPtr<Partitioner<int64_t>> partitioner(new Partitioner<int64_t>(communicator, argumentManager));
        returnCode=partition(communicator, partitioner);
    }
    if (returnCode != RETURN_OK){
        MPIO::Abort();
        return returnCode;
    }
    Message::DONE(rank);
    Message::PrintTime(rank);
    MPIO::Finalize();
    return RETURN_OK;
}
