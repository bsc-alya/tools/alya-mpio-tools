/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include "partitioner.h"


template<typename T>
alya::Partitioner<T>::Partitioner(AlyaPtr_MPIOComm communicator, AlyaPtr<ArgumentManager> argumentManager):
argumentManager(argumentManager), communicator(communicator)
{
    caseName=argumentManager->getCaseName();
    subdomainNumber=argumentManager->getSubdomains();
    processNumber=communicator->getSize();
    post=argumentManager->getPost();
    if (post){
        meshExtension=string(POST_SUFFIX)+string(MPIO_EXT);
    }
    else{
        meshExtension=string(MPIO_EXT);
    }
    gempaPartitioner=AlyaPtr<gempa::Partitioner>(new gempa::Partitioner());
}


template<typename T>
bool alya::Partitioner<T>::partitionMesh()
{
    STOP_ON_ERROR(computeNodeCommunications());
    STOP_ON_ERROR(redistributeCoord());
    STOP_ON_ERROR(computeGaussCenter());
    STOP_ON_ERROR(configureGempa());
    STOP_ON_ERROR(runGempa());
    return true;
}


template<typename T>
bool alya::Partitioner<T>::readHeader(AlyaPtr<alya::MPIOHeader> header)
{
    STOP_ON_ERROR_MSG(header->open(READ), Message::Error("Can not open "+header->getFileName()+" (header)"))
    STOP_ON_ERROR_MSG(header->read(), Message::Error("Can not read "+header->getFileName()+" (header)"))
    STOP_ON_ERROR_MSG(header->close(), Message::Error("Can not close "+header->getFileName()+" (header)"))
    return true;
}


template<typename T>
template<typename U>
bool alya::Partitioner<T>::readFile(AlyaPtr<alya::MPIOFile<U> > file)
{
    Message::Debug(communicator->getRank(), "Reading "+file->getFileName()+" file");
    STOP_ON_ERROR_MSG(file->open(READ), Message::Critical(communicator->getRank(), "Can not open "+file->getFileName()+" file!"))
    STOP_ON_ERROR_MSG(file->read(), Message::Critical(communicator->getRank(), "Can not read "+file->getFileName()+" file!"))
    STOP_ON_ERROR_MSG(file->close(), Message::Critical(communicator->getRank(), "Can not close "+file->getFileName()+" file!"))
    return true;
}

template<typename T>
bool alya::Partitioner<T>::readMesh()
{
    if (communicator->isIOWorker()){
        string coordName = caseName + COORD_SUFFIX + meshExtension;
        string lnodsName = caseName + LNODS_SUFFIX + meshExtension;
        lnodsFile=AlyaPtr<MPIOFile<T> >(new MPIOFile<T> (communicator, lnodsName));
        STOP_ON_ERROR(readFile(lnodsFile))
        coordFile=AlyaPtr<MPIOFile<double> >(new MPIOFile<double> (communicator, coordName));
        STOP_ON_ERROR(readFile(coordFile))
    }
    return true;
}

template<typename T>
bool alya::Partitioner<T>::computeGaussCenter()
{
    int64_t columns=lnodsFile->getColumns();
    int64_t dimension=coordFile->getColumns();
    int64_t lines=lnodsFile->getLines();
    if (communicator->isIOWorker()){
        gaussCenters=AlyaPtr<double>(new double[lines*dimension]);
        for (int64_t i=0; i<lines; i++){
            for (int64_t k=0; k<dimension; k++){
                gaussCenters.get()[i*dimension+k]=0.0;
            }
            int64_t n=0;
            for (int64_t j=0; j<columns; j++){
                T node=lnodsFile->getData(i, j);
                if (node==0){
                    break;
                }
                n++;
                T localNode=lninvinv[node];
                for (int64_t k=0; k<dimension; k++){
                    gaussCenters.get()[i*dimension+k]+=coordRedis.get()[localNode*dimension+k];
                }
            }
            for (int64_t k=0; k<dimension; k++){
                gaussCenters.get()[i*dimension+k]/=n;
            }
        }
    }
    return true;
}

template<typename T>
bool alya::Partitioner<T>::configureGempa()
{
    if (communicator->isIOWorker()){
        gempaPartitioner->sdim=coordFile->getColumns();
        gempaPartitioner->N=lnodsFile->getLines();
        gempaPartitioner->maxIter=100;
        gempaPartitioner->printLB=true;
        gempaPartitioner->npart=subdomainNumber;
        gempaPartitioner->targLB=0.99;
        gempaPartitioner->isParallel=!communicator->isSequential();
        gempaPartitioner->isWeighted=false;
        gempaPartitioner->doCorr=false;
        gempaPartitioner->coords.resize(static_cast<unsigned long>(gempaPartitioner->N * gempaPartitioner->sdim));
        for (int64_t i=0; i<gempaPartitioner->N*gempaPartitioner->sdim; i++){
            gempaPartitioner->coords[static_cast<unsigned long>(i)]=gaussCenters.get()[i];
        }
    }
    return true;
}

template<typename T>
bool alya::Partitioner<T>::runGempa()
{
    if (communicator->isIOWorker()){
        gempaPartitioner->part();
        balance=gempaPartitioner->checkBalance();
        ordered=gempaPartitioner->checkOrdering();
        partition=AlyaPtr<T>(new T[lnodsFile->getLines()]);
        for (int64_t i=0; i<lnodsFile->getLines(); i++){
            partition.get()[i]=gempaPartitioner->partition[static_cast<unsigned long>(i)];
        }
    }
    return true;
}


template<typename T>
bool alya::Partitioner<T>::computeNodeCommunications()
{
    if (communicator->isParallelIOWorker()){
        //Computing offsets and total node number (without redistribution)
        computeNodeOffset(coordFile->getPartition());
        int64_t offset=nodeOffset[static_cast<unsigned long>(communicator->getRank())];
        totalNodeNumberBefore=coordFile->getHeader()->getLines();
        //Init values
        vector<std::set<T>> nodesToAskVec;
        nodesRecvcount=AlyaPtr<int64_t>(new int64_t[processNumber]);
        nodesSendcount=AlyaPtr<int64_t>(new int64_t[processNumber]);
        nodeNumber=0;
        for (int64_t i=0; i<processNumber; i++){
            nodesRecvcount.get()[i]=0;
            nodesSendcount.get()[i]=0;
            nodesToAskVec.push_back(set<T>());
        }
        lnodsGlobalNumber=lnodsFile->getCount();
        for(int64_t i=0; i<lnodsGlobalNumber; i++){
            int64_t n = lnodsFile->getData(i);
            if (n==0){
                continue;
            }
            int64_t s=getSubdomain(n);
            if (s<0){
                return false;
            }
            unsigned long index=static_cast<unsigned long>(s);
            if (nodesToAskVec[index].find(n) != nodesToAskVec[index].end()){
                continue;
            }
            nodesRecvcount.get()[index]+=1;
            nodesToAskVec[index].insert(n);
            nodeNumber++;
        }
        communicator->Alltoall(nodesRecvcount, 1, nodesSendcount, 1);
        nodesLocalNumber=0;
        for(int64_t i=0; i<processNumber; i++){
            nodesLocalNumber+=nodesSendcount.get()[i];
        }
        lninv=AlyaPtr<T>(new T[nodeNumber]);
        int64_t k=0;
        k=0;
        for(unsigned long i=0; i<static_cast<unsigned long>(nodesToAskVec.size()); i++){
            for (const T &n : nodesToAskVec[i]){
                lninv.get()[k++]=n;
                lninvinv[n]=k;
            }
        }
        nodesLocal=AlyaPtr<T>(new T[nodesLocalNumber]);
        communicator->Alltoallv(lninv, vector<int64_t>(nodesRecvcount.get(), nodesRecvcount.get()+subdomainNumber),
                                nodesLocal, vector<int64_t>(nodesSendcount.get(), nodesSendcount.get()+subdomainNumber));
        for (int64_t i=0; i<nodesLocalNumber; i++){
            nodesLocal.get()[i]=nodesLocal.get()[i]-offset;
        }
        totalNodeNumberAfter=nodeNumber;
        communicator->Sum(&totalNodeNumberAfter);
        communicator->Bcast(&totalNodeNumberAfter);
    }
    return true;
}

template<typename T>
bool alya::Partitioner<T>::redistributeCoord()
{
    if(communicator->isParallelIOWorker()){
        int64_t partitionNumber=0;
        int64_t columns=coordFile->getColumns();
        int64_t lines;
        AlyaPtr<T> local;
        partitionNumber=nodeNumber;
        local=nodesLocal;
        lines=nodesLocalNumber;
        vector<int64_t> sendcount;
        vector<int64_t> recvcount;
        for(int64_t i=0; i<processNumber; i++){
            sendcount.push_back(nodesSendcount.get()[i]*columns);
            recvcount.push_back(nodesRecvcount.get()[i]*columns);
        }
        AlyaPtr<double> datain=coordFile->getData();
        AlyaPtr<double> datainReorder(new double[lines*columns]);
        for (int64_t i=0; i<lines; i++){
            for (int64_t j=0; j<columns; j++){
                datainReorder.get()[i*columns+j]=datain.get()[local.get()[i]*columns+j];
            }
        }
        coordRedis=AlyaPtr<double>(new double[columns*partitionNumber]);
        communicator->Alltoallv(datainReorder, sendcount, coordRedis, recvcount);
    }else if(communicator->isSequential()){
        coordRedis=coordFile->getData();
    }
    return true;
}


template<typename T>
int64_t alya::Partitioner<T>::getSubdomain(int64_t node)
{
    int64_t subdomain=(node*processNumber)/totalNodeNumberBefore;
    while (!((node<nodeOffset[static_cast<unsigned long>(subdomain)+1])&&(node>=nodeOffset[static_cast<unsigned long>(subdomain)]))){
        if (node<nodeOffset[static_cast<unsigned long>(subdomain)]){
            subdomain--;
        }
        else{
            subdomain++;
        }
    }
    if (subdomain<0||subdomain>=processNumber){
        return -1;
    }
    return subdomain;
}


template<typename T>
bool alya::Partitioner<T>::computeNodeOffset(vector<int64_t> partition)
{
    int64_t offset=1;
    for (unsigned long i=0; i<partition.size(); i++){
        nodeOffset.push_back(offset);
        offset+=partition[i];
    }
    nodeOffset.push_back(offset);
    return true;
}


template<typename T>
bool alya::Partitioner<T>::writePartition()
{
    if (communicator->isIOWorker()){
        string partitionName = caseName + MPIRA_SUFFIX + meshExtension;
        AlyaPtr<MPIOFile<T> > partitionFile=AlyaPtr<MPIOFile<T> >(new MPIOFile<T> (communicator, partitionName));
        STOP_ON_ERROR_MSG(partitionFile->setHeader(MPIOHEADER_OBJ_MPIRA, MPIOHEADER_RES_ELEM, 1, lnodsFile->getHeader()->getLines(), vector<string>(), 1), Message::Critical("File "+partitionName+": Cannot set the header"))
        STOP_ON_ERROR_MSG(partitionFile->open(WRITE), Message::Critical("File "+partitionName+": Cannot open"));
        partitionFile->setPartition(gempaPartitioner->N);
        STOP_ON_ERROR_MSG(partitionFile->initWriting(false), Message::Critical("File "+partitionName+": Cannot start writing"))
        partitionFile->setData(partition);
        STOP_ON_ERROR_MSG(partitionFile->write(), Message::Critical("File "+partitionName+": Cannot write"))
        STOP_ON_ERROR_MSG(partitionFile->close(), Message::Critical("File "+partitionName+": Cannot close"))
    }
    return true;
}


template class alya::Partitioner<int32_t>;

template class alya::Partitioner<int64_t>;

