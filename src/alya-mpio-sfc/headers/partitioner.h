/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#ifndef PARTITIONER_H
#define PARTITIONER_H

#include <set>
#include <map>
#include <mpioreaderdef.h>
#include <mpiofile.h>
#include <argumentmanager.h>
#include <message.h>
#include <sstream>
#include <iomanip>
#include <alyareturn.h>
#include <alya-mpio-sfc_config.h>
#include <Partitioner.h>

using namespace std;

namespace alya {

    template<typename T> class Partitioner
    {
    public:
        Partitioner(AlyaPtr_MPIOComm communicator, AlyaPtr<ArgumentManager> argumentManager);
        bool readMesh();
        bool partitionMesh();
        bool writePartition();
    protected:
        template<typename U>
        bool readFile(std::shared_ptr<alya::MPIOFile<U> > file);
        bool readHeader(std::shared_ptr<alya::MPIOHeader> header);
        bool computeNodeCommunications();
        bool redistributeCoord();
        bool computeGaussCenter();
        bool configureGempa();
        bool runGempa();
        int64_t getSubdomain(int64_t node);
        bool computeNodeOffset(vector<int64_t> partition);
    private:
        AlyaPtr<ArgumentManager> argumentManager;
        AlyaPtr_MPIOComm communicator;

        bool post;
        string caseName;
        string meshExtension;

        int64_t processNumber;
        int64_t subdomainNumber;

        //files
        AlyaPtr<MPIOFile<double>> coordFile;
        AlyaPtr<MPIOFile<T> > lnodsFile;

        //nodes
        int64_t totalNodeNumberBefore;
        vector<int64_t> nodeOffset;
        int64_t nodeNumber;
        int64_t totalNodeNumberAfter;
        AlyaPtr<T> lninv;
        map<T, T> lninvinv;
        AlyaPtr<T> nodesLocal;
        int64_t nodesLocalNumber;
        AlyaPtr<int64_t> nodesRecvcount;
        AlyaPtr<int64_t> nodesSendcount;

        //lnods
        int64_t lnodsGlobalNumber;
        AlyaPtr<T> lnodsLocal;

        //coord
        AlyaPtr<double> coordRedis;

        //gauss center
        AlyaPtr<double> gaussCenters;

        //gempa partitioner
        AlyaPtr<gempa::Partitioner> gempaPartitioner;
        double balance;
        bool ordered;
        AlyaPtr<T> partition;

    };



}



#endif // PARTITIONER_H
