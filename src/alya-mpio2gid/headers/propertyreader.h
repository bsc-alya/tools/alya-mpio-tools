/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#ifndef PROPERTYREADER_H
#define PROPERTYREADER_H

#include <alya-mpio2gid_config.h>
#include <string>
#include <vector>
#include <streampointer.h>
#include <mpiocomm.h>
#include <alyafilereader.h>

namespace alya{

    namespace property {
        enum States{init, data, subdomain, end};
    }

    class PropertyReader
    {
    public:
        PropertyReader(AlyaPtr_MPIOComm communicator, AlyaPtr<AlyaFileReader> fileReader);
        bool read();
        int getFormat() const;
        int getMark() const;
        bool getEliminateBoundaryNodes() const;
        bool getMultipleFiles() const;
        bool getBoundary() const;
        bool getAllSubdomain() const;

        vector<int64_t> getSubdomains() const;

    private:
        bool readData();
        bool readSubdomain();
        bool broadcast();

        AlyaPtr_MPIOComm communicator;
        AlyaPtr<AlyaFileReader> fileReader;

        alya::property::States state;
        int64_t sectionLineNumber;
        string currentFileName;
        int64_t currentFileLineNumber;

        int format;
        int mark;
        bool eliminateBoundaryNodes;
        bool multipleFiles;
        bool boundary;
        bool allSubdomain;
        vector<int64_t> subdomains;
    };

}

#endif // PROPERTYREADER_H
