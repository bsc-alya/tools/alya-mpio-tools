/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#ifndef GIDWRITER_H
#define GIDWRITER_H

#include <map>
#include <postreader.h>
#include <streampointer.h>
#include <argumentmanager.h>
#include <sstream>
#include <iomanip>
#include <alyareturn.h>
#include <alyatypedef.h>
#include <propertyreader.h>
#include <gidtype.h>
#include <gidresult.h>
#include <postfilelist.h>

#include <alya-mpio2gid_config.h>

using namespace std;

#define PRINT_LINE(n)   *output<<n+1<<"\t";

namespace alya {

    class GidWriter
    {
    public:
        GidWriter(AlyaPtr<alya::PostReader> reader, AlyaPtr_MPIOComm communicator, AlyaPtr<ArgumentManager> argumentManager, AlyaPtr<PropertyReader> propertyReader);
        bool write();
    private:
        bool checkOptions();

        bool readInv();
        bool gatherInv();
        bool gatherInv4();
        bool gatherInv8();

        AlyaPtr<MPIOFile<int32_t> > getInv4(string resultsOn);
        AlyaPtr<MPIOFile<int64_t> > getInv8(string resultsOn);

        bool computeDimensions();

        bool computeElementMark(int64_t element);

        bool computeBoundaryMark(int64_t bound);

        bool computeElementTypes();

        bool computeBoundaryTypes();

        bool writeMesh();

        bool writeBoundaries();

        bool writeElements(AlyaPtr<GidType>);

        bool writeBoundaries(AlyaPtr<GidType>);

        bool writeCoords();

        bool writePosts();

        template<typename T>
        bool writePost(AlyaPtr<MPIOFile<T> >);

        template<typename T>
        bool writePostNodes(AlyaPtr<MPIOFile<T> >);

        template<typename T>
        bool writePostElements(AlyaPtr<MPIOFile<T> >);

        template<typename T>
        bool writePostBoundaries(AlyaPtr<MPIOFile<T> >);

        template<typename T>
        bool writeFile(AlyaPtr_ofstream output, AlyaPtr<MPIOFile<T> >);

        template<typename T>
        bool writeElementFile(AlyaPtr_ofstream output, AlyaPtr<MPIOFile<T> >, AlyaPtr<GidType> gidt, map<int64_t, vector<int64_t> > mapping);

        bool mergeOrGather(AlyaPtr<MPIOFile<double> >);
        bool mergeOrGather(AlyaPtr<MPIOFile<int32_t> >);
        bool mergeOrGather(AlyaPtr<MPIOFile<int64_t> >);
        bool mergeOrGather(AlyaPtr<MPIOFile<double> >, AlyaPtr<MPIOFile<int32_t> >);
        bool mergeOrGather(AlyaPtr<MPIOFile<double> >, AlyaPtr<MPIOFile<int64_t> >);
        bool mergeOrGather(AlyaPtr<MPIOFile<int32_t> >, AlyaPtr<MPIOFile<int32_t> >);
        bool mergeOrGather(AlyaPtr<MPIOFile<int64_t> >, AlyaPtr<MPIOFile<int64_t> >);

        bool convertMesh();
        bool convertMesh4();
        bool convertMesh8();
        bool convertMeshExtended();
        bool convertPost();
        bool discardPost(AlyaPtr<MPIOHeader>);



        AlyaPtr_ofstream openMeshFile();
        AlyaPtr_ofstream openResFile();
        bool closeFile(AlyaPtr_ofstream);
        string format(double value);
        string format(int32_t value);
        string format(int64_t value);
        AlyaPtr<PostReader> reader;
        AlyaPtr<ArgumentManager> argumentManager;
        AlyaPtr<PropertyReader> propertyReader;
        AlyaPtr_MPIOComm communicator;
        string caseName;
        unsigned int dimension;
        int intSize;
        bool merge;
        int outputFormat;
        int mark;
        bool multipleFiles;
        bool boundary;
        bool allSubdomain;
        map<int64_t, int64_t> elementMarks;
        map<int64_t, int64_t> boundaryMarks;
        vector<int64_t> subdomains;
        vector<AlyaPtr<GidType> > typeId;
        vector<AlyaPtr<GidType> > typbId;
        map<int64_t, vector<int64_t> > elements;
        map<int64_t, vector<int64_t> > boundaries;
        AlyaPtr_ofstream meshFile;
        AlyaPtr_ofstream resFile;
        AlyaPtr<int64_t> elementTypes8;
        AlyaPtr<int64_t> boundaryTypes8;
        AlyaPtr<PostFileList> postfileList;
    };



}



#endif // GIDWRITER_H
