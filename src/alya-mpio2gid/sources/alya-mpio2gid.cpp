/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include <iostream>
#include <argumentmanager.h>
#include <message.h>
#include <pointer.h>
#include <postreader.h>
#include <gidwriter.h>
#include <mpiocomm.h>
#include <mpiowrapper.h>
#include <propertyreader.h>

using namespace std;
using namespace alya;

int main(int argc, char* argv[])
{
    Message::InitTime();
    MPIO::Init(argc, argv);
    AlyaPtr_MPIOComm communicator(new MPIOComm());
    int rank=communicator->getRank();
    Message::IAM(rank, "Alya MPI-IO 2 GID converter");
    Message::Separator(rank);
    if (communicator->isSequential()){
       Message::Info(rank, "Sequential version");
    }else{
       Message::Info(rank, "Parallel version: "+to_string(communicator->getSize())+" processes");
    }
    Message::Info(rank, "-----------------------------------------");
    Message::Info(rank, "Reading arguments", 1);
    AlyaPtr<ArgumentManager> argumentManager(new ArgumentManager(argc, argv, communicator));
    if (argumentManager->getHelp()){
        argumentManager->usage();
        MPIO::Finalize();
        return RETURN_OK;

    }
    if (!argumentManager->getValid()){
        Message::Critical(rank, "Invalid arguments");
        argumentManager->usage();
        MPIO::Abort();
        return RETURN_ERR;
    }
    AlyaPtr<AlyaFileReader> fileReader(new AlyaFileReader(communicator->isMasterOrSequential()));
    if (!fileReader->init(argumentManager->getCaseName()+".post.alyadat")){
        Message::Critical(communicator->getRank(), "Cannot open domain file");
        argumentManager->usage();
        MPIO::Abort();
        return RETURN_ERR;
    }
    AlyaPtr<PropertyReader> propertyReader(new PropertyReader(communicator, fileReader));
    propertyReader->read();
    Message::Info(rank, "Setting partitions", 1);
    AlyaPtr<PostReader> meshReader(new PostReader(argumentManager->getCaseName(), communicator, true));
    if (!meshReader->setPartitions()){
        Message::Critical(rank, "Could not set the partitions");
        MPIO::Abort();
        return RETURN_ERR;
    }
    AlyaPtr<GidWriter> gidWriter(new GidWriter(meshReader, meshReader->getCommunicator(), argumentManager, propertyReader));
    if (!gidWriter->write()){
        Message::Critical(rank, "Could not write the GID files");
        MPIO::Abort();
        return RETURN_ERR;
    }
    Message::DONE(rank);
    Message::PrintTime(rank);
    MPIO::Finalize();
    return RETURN_OK;
}
