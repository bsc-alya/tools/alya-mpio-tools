/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include "propertyreader.h"

alya::PropertyReader::PropertyReader(std::shared_ptr<alya::MPIOComm> communicator, std::shared_ptr<alya::AlyaFileReader> fileReader):communicator(communicator), fileReader(fileReader)
{
    if (fileReader->getCurrentFile()!=nullptr){
        Message::Info(communicator->getRank(),"GID property file: "+fileReader->getCurrentFile()->getFileName(),1);
    }
    format=-1;
    mark=-1;
    eliminateBoundaryNodes=false;
    multipleFiles=false;
    boundary=false;
    allSubdomain=false;
}

bool alya::PropertyReader::read()
{
    subdomains.clear();
    if (communicator->isMasterOrSequential()){
        state=alya::property::init;
        sectionLineNumber=0;
        while (fileReader->readLine()){
            currentFileLineNumber=fileReader->getCurrentFile()->getLineNumber();
            currentFileName=fileReader->getCurrentFile()->getFileName();
            switch(state){
                case alya::property::init:
                {
                    string temp=fileReader->nextString();
                    if (AlyaString::Equal(temp,"DATA")){
                        Message::Info(communicator->getRank(),"Reading data", 1);
                        state=alya::property::data;
                    }
                    break;
                }
                case alya::property::data:
                {
                    if (!readData()){
                        return false;
                    }
                    break;
                }
                case alya::property::subdomain:
                {
                    if (!readSubdomain()){
                        return false;
                    }
                    break;
                }
                default:
                    break;
            }
        }
        broadcast();
        return true;
    }
    else{
        broadcast();
        return true;
    }
}

bool alya::PropertyReader::readData()
{
    string temp=fileReader->nextString();
    if (!AlyaString::Equal(temp, "END_DATA")){
        if (AlyaString::Equal(temp, "FORMAT")){
            temp=fileReader->nextString();
            if (AlyaString::Equal(temp, "gid")){
                format=FORMAT_GID;
                Message::Info("Format: GID", 2);
            }else{
                format=FORMAT_OTHER;
                Message::Info("Format: Other", 2);
            }
        }
        else if (AlyaString::Equal(temp, "MARK_ELEMENTS")){
             temp=fileReader->nextString();
             if (AlyaString::Equal(temp, "type")){
                 mark=MARK_TYPE;
                 Message::Info("Mark elements: Type", 2);
             }else{
                 mark=MARK_OTHER;
                 Message::Info("Mark elements: Other", 2);
             }
        }
        else if (AlyaString::Equal(temp, "ELIMINATE_BOUNDARY_NODES")){
             temp=fileReader->nextString();
             if (AlyaString::IsEnabled(temp)){
                 eliminateBoundaryNodes=true;
                 Message::Info("Eliminate boundary nodes: Enabled", 2);
             }else{
                 eliminateBoundaryNodes=false;
                 Message::Info("Eliminate boundary nodes: Disabled", 2);
             }
        }
        else if (AlyaString::Equal(temp, "MULTIPLE_FILE")){
             temp=fileReader->nextString();
             if (AlyaString::IsEnabled(temp)){
                 multipleFiles=true;
                 Message::Info("Multiple files: Enabled", 2);
             }else{
                 multipleFiles=false;
                 Message::Info("Multiple files: Disabled", 2);
             }
        }
        else if (AlyaString::Equal(temp, "BOUNDARY")){
             temp=fileReader->nextString();
             if (AlyaString::IsEnabled(temp)){
                 boundary=true;
                 Message::Info("Boundary: Enabled", 2);
             }else{
                 boundary=false;
                 Message::Info("Boundary: Disabled", 2);
             }
        }
        else if (AlyaString::Equal(temp, "SUBDOMAIN")){
            if (fileReader->containField("ALL")){
                allSubdomain=true;
                Message::Info("Export all the subdomains: Enabled", 2);
            }
            else{
                allSubdomain=false;
                state=alya::property::subdomain;
            }
        }
        else{
            return true;
        }
    }else{
        state=alya::property::init;
    }
    return true;
}

bool alya::PropertyReader::readSubdomain()
{
    string temp=fileReader->nextString();
    if (!AlyaString::Equal(temp, "END_SUBDOMAINS")){
        temp=fileReader->nextString();
        subdomains.push_back(atol(temp.c_str()));
    }else{
        state=alya::property::data;
    }
    return true;
}

bool alya::PropertyReader::broadcast()
{
    communicator->Bcast(&format);
    communicator->Bcast(&mark);
    communicator->Bcast(&eliminateBoundaryNodes);
    communicator->Bcast(&multipleFiles);
    communicator->Bcast(&boundary);
    communicator->Bcast(&allSubdomain);
    //communicator->Bcast(subdomains);
    return true;
}

vector<int64_t> alya::PropertyReader::getSubdomains() const
{
    return subdomains;
}

bool alya::PropertyReader::getAllSubdomain() const
{
    return allSubdomain;
}

bool alya::PropertyReader::getBoundary() const
{
    return boundary;
}

bool alya::PropertyReader::getMultipleFiles() const
{
    return multipleFiles;
}

bool alya::PropertyReader::getEliminateBoundaryNodes() const
{
    return eliminateBoundaryNodes;
}

int alya::PropertyReader::getMark() const
{
    return mark;
}

int alya::PropertyReader::getFormat() const
{
    return format;
}
