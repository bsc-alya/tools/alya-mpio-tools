/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include "gidresult.h"

ostream &alya::operator<<(ostream &out, alya::GidResult &gidResult)
{
    if (AlyaString::Equal(gidResult.resultOn, MPIOHEADER_RES_POINT)){
        return gidResult.printNode(out);
    }else{
        return gidResult.printElement(out);
    }
}

ostream &alya::operator<<(ostream &out, std::shared_ptr<alya::GidResult> &gidResult)
{
    out << gidResult.get();
    return out;
}

alya::GidResult::GidResult(string name, unsigned int dimension, string resultOn, double time):
    name(name), dimension(dimension), resultOn(resultOn), type(nullptr), time(time)
{

}

alya::GidResult::GidResult(string name, unsigned int dimension, string resultOn, AlyaPtr<GidType> type, double time):
    name(name), dimension(dimension), resultOn(resultOn), type(type), time(time)
{

}

string alya::GidResult::getName() const
{
    return name;
}

unsigned int alya::GidResult::getDimension() const
{
    return dimension;
}

string alya::GidResult::getResultOn() const
{
    return resultOn;
}

double alya::GidResult::getTime() const
{
    return time;
}

string alya::GidResult::Axis(unsigned int i)
{
    switch(i){
        case 0: return "X";
        case 1: return "Y";
        case 2: return "Z";
    }
    return "";
}

ostream &alya::GidResult::printNode(ostream &out)
{
    out << "Result " + AlyaString::Format5(name) + " ALYA " + to_string(time)+ " ";
    if (dimension>1){
        out << "Vector ";
    }else{
        out << "Scalar ";
    }
    out << "OnNodes";
    out << endl;
    out << "ComponentNames ";
    if (dimension>1){
       for(unsigned int i=0; i<dimension; i++){
           out << AlyaString::Format5(name) << "_" << GidResult::Axis(i);
           if (i<dimension-1){
               out<<",";
           }else{
               out<<endl;
           }
       }
    }else{
        out << AlyaString::Format5(name)<<endl;
    }
    return out;
}

ostream &alya::GidResult::printElement(ostream &out)
{
    out << "GaussPoints GP Elemtype " << type->getShape() << endl;
    out << "Number of Gauss Points:   1" << endl;
    out << "Natural Coordinates: Internal" << endl;
    out << "End GaussPoints" << endl;
    out << "Result " + AlyaString::Format5(name) + " ALYA " + to_string(time)+ " ";
    if (dimension>1){
        out << "Vector ";
    }else{
        out << "Scalar ";
    }
    out << "OnGaussPoints GP";
    out << endl;
    out << "ComponentNames ";
    if (dimension>1){
       for(unsigned int i=0; i<dimension; i++){
           out << AlyaString::Format5(name) << "_" << GidResult::Axis(i);
           if (i<dimension-1){
               out<<",";
           }else{
               out<<endl;
           }
       }
    }else{
        out << AlyaString::Format5(name)<<endl;
    }
    return out;
}
