/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include "gidwriter.h"


alya::GidWriter::GidWriter(AlyaPtr<alya::PostReader> reader, AlyaPtr_MPIOComm communicator, AlyaPtr<ArgumentManager> argumentManager, AlyaPtr<PropertyReader> propertyReader):
reader(reader), argumentManager(argumentManager), propertyReader(propertyReader), communicator(communicator)
{
    AlyaTypeDef::Init();
    caseName=argumentManager->getCaseName();
    merge=propertyReader->getEliminateBoundaryNodes();
    outputFormat=propertyReader->getFormat();
    mark=propertyReader->getMark();
    multipleFiles=propertyReader->getMultipleFiles();
    boundary=propertyReader->getBoundary();
    allSubdomain=propertyReader->getAllSubdomain();
    subdomains=propertyReader->getSubdomains();
}

bool alya::GidWriter::write()
{
    if (communicator->isIOWorker()){
        Message::Info(communicator->getRank(), "Checking options", 1);
        STOP_ON_ERROR(checkOptions())
        Message::Info(communicator->getRank(), "Creating and opening output files", 2);
        meshFile=openMeshFile();
        resFile=openResFile();
        Message::Info(communicator->getRank(), "Processing mesh", 1);
        Message::Info(communicator->getRank(), "Reading INV files", 2);
        STOP_ON_ERROR(readInv())
        Message::Info(communicator->getRank(), "Computing dimensions", 2);
        STOP_ON_ERROR(computeDimensions())
        Message::Info(communicator->getRank(), "Computing types", 2);
        STOP_ON_ERROR(computeElementTypes())
        if (boundary && !reader->getNoBoundaries()){
            Message::Info(communicator->getRank(), "Computing boundary types", 2);
            STOP_ON_ERROR(computeBoundaryTypes())
        }
        Message::Info(communicator->getRank(), "Writing coordinates and elements", 2);
        STOP_ON_ERROR(writeMesh())
        if (boundary && !reader->getNoBoundaries()){
            Message::Info(communicator->getRank(), "Writing boundaries", 2);
            STOP_ON_ERROR(writeBoundaries())
        }
        Message::Info(communicator->getRank(), "Processing results", 1);
        Message::Info(communicator->getRank(), "Reading list of post-processed files", 2);
        postfileList=AlyaPtr<PostFileList>(new PostFileList(caseName+".post.alyafil", communicator));
        postfileList->read();
        Message::Info(communicator->getRank(), "Writing result file", 2);
        STOP_ON_ERROR(writePosts());
        Message::Info(communicator->getRank(), "Closing output files", 1);
        STOP_ON_ERROR(closeFile(meshFile))
        STOP_ON_ERROR(closeFile(resFile))
    }
    return true;
}

bool alya::GidWriter::checkOptions()
{
    if (outputFormat!=FORMAT_GID){
        Message::Critical(communicator->getRank(), "FORMAT field must be set to gid");
        return false;
    }
    if (mark!=MARK_TYPE){
        Message::Critical(communicator->getRank(), "MARK field must be set to types");
        return false;
    }
    if (!merge){
        Message::Critical(communicator->getRank(), "ELIMINATE BOUNDARY OPTION field must be enabled");
        return false;
    }
    if (multipleFiles){
        Message::Critical(communicator->getRank(), "MULTIPLE FILES field must be disabled");
        return false;
    }
    return true;
}

bool alya::GidWriter::computeDimensions()
{
    STOP_ON_ERROR(reader->readCoord())
    mergeOrGather(reader->getCoord());
    if (communicator->isIOWorker()){
        dimension=static_cast<unsigned int>(reader->getCoord()->getColumns());
    }
    return true;
}

bool alya::GidWriter::computeElementMark(int64_t element)
{
    if (mark==MARK_TYPE){
        elementMarks.insert(std::pair<int64_t, int64_t>(element, elementTypes8.get()[element]));
    }else{
        return false;
    }
    return true;
}

bool alya::GidWriter::computeBoundaryMark(int64_t bound)
{
    if (mark==MARK_TYPE){
        boundaryMarks.insert(std::pair<int64_t, int64_t>(bound, boundaryTypes8.get()[bound]));
    }else{
        return false;
    }
    return true;
}

bool alya::GidWriter::computeElementTypes()
{
    AlyaPtr<int32_t> types4;
    int64_t lastType=-1;
    STOP_ON_ERROR(reader->readLtype())
    if (intSize==4){
        STOP_ON_ERROR(mergeOrGather(reader->getLtype4()))
        types4=reader->getLtype4()->getData();
        elementTypes8=AlyaPtr<int64_t>(new int64_t[reader->getLtype4()->getLines()]);
        for (int64_t i=0; i<reader->getLtype4()->getLines(); i++){
            elementTypes8.get()[i]=static_cast<int64_t>(types4.get()[i]);
            if (lastType!=elementTypes8.get()[i]){
                lastType=elementTypes8.get()[i];
                if (elements.count(lastType)==0){
                    elements.insert(std::pair<int64_t, vector<int64_t> >(lastType, vector<int64_t>()));
                    typeId.push_back(AlyaPtr<GidType>(new GidType(caseName, dimension, lastType, false)));
                }
            }
            elements[lastType].push_back(i);
            computeElementMark(i);
        }
    }else if (intSize==8){
        STOP_ON_ERROR(mergeOrGather(reader->getLtype8()))
        elementTypes8=reader->getLtype8()->getData();
        for (int64_t i=0; i<reader->getLtype8()->getLines(); i++){
            if (lastType!=elementTypes8.get()[i]){
                lastType=elementTypes8.get()[i];
                if (elements.count(lastType)==0){
                    elements.insert(std::pair<int64_t, vector<int64_t> >(lastType, vector<int64_t>()));
                    typeId.push_back(AlyaPtr<GidType>(new GidType(caseName, dimension, lastType, false)));
                }
            }
            elements[lastType].push_back(i);
            computeElementMark(i);
        }
    }
    return true;
}

bool alya::GidWriter::computeBoundaryTypes()
{
    AlyaPtr<int32_t> typesBound4;
    int64_t lastTypb=-1;
    STOP_ON_ERROR(reader->readLtypb())
    if (intSize==4){
        STOP_ON_ERROR(mergeOrGather(reader->getLtypb4()))
        typesBound4=reader->getLtypb4()->getData();
        boundaryTypes8=AlyaPtr<int64_t>(new int64_t[reader->getLtypb4()->getLines()]);
        for (int64_t i=0; i<reader->getLtypb4()->getLines(); i++){
            boundaryTypes8.get()[i]=static_cast<int64_t>(typesBound4.get()[i]);
            if (lastTypb!=boundaryTypes8.get()[i]){
                lastTypb=boundaryTypes8.get()[i];
                if (boundaries.count(lastTypb)==0){
                    boundaries.insert(std::pair<int64_t, vector<int64_t> >(lastTypb, vector<int64_t>()));
                    typbId.push_back(AlyaPtr<GidType>(new GidType(caseName, dimension, lastTypb, true)));
                }
            }
            boundaries[lastTypb].push_back(i);
            computeBoundaryMark(i);
        }
    }else if (intSize==8){
        STOP_ON_ERROR(mergeOrGather(reader->getLtypb8()))
        boundaryTypes8=reader->getLtypb8()->getData();
        for (int64_t i=0; i<reader->getLtypb8()->getLines(); i++){
            if (lastTypb!=boundaryTypes8.get()[i]){
                lastTypb=boundaryTypes8.get()[i];
                if (boundaries.count(lastTypb)==0){
                    boundaries.insert(std::pair<int64_t, vector<int64_t> >(lastTypb, vector<int64_t>()));
                    typbId.push_back(AlyaPtr<GidType>(new GidType(caseName, dimension, lastTypb, true)));
                }
            }
            boundaries[lastTypb].push_back(i);
            computeBoundaryMark(i);
        }
    }
    return true;
}

bool alya::GidWriter::writeMesh()
{
    STOP_ON_ERROR(reader->readLnods())
    if (intSize==4){
        STOP_ON_ERROR(mergeOrGather(reader->getLnods4(), reader->getLninv4()))
    }else if(intSize==8){
        STOP_ON_ERROR(mergeOrGather(reader->getLnods8(), reader->getLninv8()))
    }
    bool initCoord=false;
    if (communicator->isMasterOrSequential()){
        for (AlyaPtr<GidType> gidt: typeId){
            *meshFile<<*gidt;
            if (!initCoord){
                STOP_ON_ERROR(writeCoords())
                initCoord=true;
            }
            *meshFile<<"elements"<<endl;
            STOP_ON_ERROR(writeElements(gidt))
            *meshFile<<"end elements"<<endl<<endl;
        }
    }
    return true;
}

bool alya::GidWriter::writeBoundaries()
{
    STOP_ON_ERROR(reader->readLnodb())
    if (intSize==4){
        STOP_ON_ERROR(mergeOrGather(reader->getLnodb4(), reader->getLninv4()))
    }else if(intSize==8){
        STOP_ON_ERROR(mergeOrGather(reader->getLnodb8(), reader->getLninv8()))
    }
    if (communicator->isMasterOrSequential()){
        for (AlyaPtr<GidType> gidt: typbId){
            *meshFile<<*gidt;
            *meshFile<<"elements"<<endl;
            STOP_ON_ERROR(writeBoundaries(gidt))
            *meshFile<<"end elements"<<endl<<endl;
        }
    }
    return true;
}

bool alya::GidWriter::writeElements(std::shared_ptr<alya::GidType> type)
{
    AlyaPtr_ofstream output = meshFile;
    if (communicator->isMasterOrSequential()){
        if (!output->is_open()){
            return false;
        }
        int64_t t=type->getType();
        int64_t n=static_cast<int64_t>(type->getNodes());
        for (int64_t i=0; i<static_cast<int64_t>((elements.at(t)).size()); i++){
            int64_t e=(elements.at(t))[static_cast<unsigned long>(i)];
            PRINT_LINE(e)
            for (int64_t j=0; j<n; j++){
                if (intSize==4){
                    *output<<format(reader->getLnods4()->getData(e,j));
                }else if (intSize==8){
                    *output<<format(reader->getLnods8()->getData(e,j));
                }
                *output<<"\t";
            }
            *output<<format(elementMarks.at(e))<<endl;
        }
    }
    return true;
}

bool alya::GidWriter::writeBoundaries(std::shared_ptr<alya::GidType> type)
{
    AlyaPtr_ofstream output = meshFile;
    if (communicator->isMasterOrSequential()){
        if (!output->is_open()){
            return false;
        }
        int64_t t=type->getType();
        int64_t n=static_cast<int64_t>(type->getNodes());
        for (int64_t i=0; i<static_cast<int64_t>((boundaries.at(t)).size()); i++){
            int64_t e=(boundaries.at(t))[static_cast<unsigned long>(i)];
            PRINT_LINE(e)
            for (int64_t j=0; j<n; j++){
                if (intSize==4){
                    *output<<format(reader->getLnodb4()->getData(e,j));
                }else if (intSize==8){
                    *output<<format(reader->getLnodb8()->getData(e,j));
                }
                *output<<"\t";
            }
            *output<<format(boundaryMarks.at(e))<<endl;
        }
    }
    return true;
}

bool alya::GidWriter::writeCoords()
{
    *meshFile<<"coordinates"<<endl;
    STOP_ON_ERROR(writeFile(meshFile, reader->getCoord()))
    *meshFile<<"end coordinates"<<endl;
    return true;
}

bool alya::GidWriter::writePosts()
{
    *resFile<<"GiD Post Results File 1.0"<<endl<<endl;
    STOP_ON_ERROR(reader->readPostMesh())
    for (auto file : reader->getPostMeshFloat8()){
        if (postfileList->isPartOf(file->getFileName())){
            STOP_ON_ERROR(writePost(file))
        }
    }
    for (auto file : reader->getPostMeshInteger4()){
        if (postfileList->isPartOf(file->getFileName())){
            STOP_ON_ERROR(writePost(file))
        }
    }
    for (auto file : reader->getPostMeshInteger8()){
        if (postfileList->isPartOf(file->getFileName())){
            STOP_ON_ERROR(writePost(file))
        }
    }
    STOP_ON_ERROR(reader->findSteps());
    STOP_ON_ERROR(reader->readPosts());
    for (auto file : reader->getPostFloat8()){
        if (postfileList->isPartOf(file->getFileName())){
            STOP_ON_ERROR(writePost(file))
        }
    }
    for (auto file : reader->getPostInteger4()){
        if (postfileList->isPartOf(file->getFileName())){
            STOP_ON_ERROR(writePost(file))
        }
    }
    for (auto file : reader->getPostInteger8()){
        if (postfileList->isPartOf(file->getFileName())){
            STOP_ON_ERROR(writePost(file))
        }
    }
    return true;
}

bool alya::GidWriter::readInv()
{
    STOP_ON_ERROR(reader->readInv())
    intSize=reader->getIntSize();
    return gatherInv();
}

bool alya::GidWriter::gatherInv()
{
    if (merge){
        if (intSize==4){
            return gatherInv4();
        }else if(intSize==8){
            return gatherInv8();
        }
    }
    return true;
}

bool alya::GidWriter::gatherInv4()
{
    STOP_ON_ERROR(reader->getLninv4()->gather())
    STOP_ON_ERROR(reader->getLeinv4()->gather())
    if (boundary && !reader->getNoBoundaries()){
        STOP_ON_ERROR(reader->getLbinv4()->gather())
    }
    return true;
}

bool alya::GidWriter::gatherInv8()
{
    STOP_ON_ERROR(reader->getLninv8()->gather())
    STOP_ON_ERROR(reader->getLeinv8()->gather())
    if (boundary && !reader->getNoBoundaries()){
        STOP_ON_ERROR(reader->getLbinv8()->gather())
    }
    return true;
}

template<typename T>
bool alya::GidWriter::writeFile(AlyaPtr_ofstream output, AlyaPtr<MPIOFile<T> > file)
{
    if (communicator->isMasterOrSequential()){
        Message::Info(communicator->getRank(), "Writing "+file->getFileName(), 3);
        if (!output->is_open()){
            return false;
        }
        for (int64_t i=0; i<file->getLines(); i++){
            PRINT_LINE(i)
            for (int64_t j=0; j<file->getColumns(); j++){
                *output<<format(file->getData(i,j));
                if (j<file->getColumns()-1){
                    *output<<"\t";
                }
            }
            *output<<endl;
        }
    }
    return true;
}

template<typename T>
bool alya::GidWriter::writeElementFile(AlyaPtr_ofstream output, AlyaPtr<MPIOFile<T> > file, AlyaPtr<GidType> gidt, map<int64_t, vector<int64_t> > mapping)
{
    if (communicator->isMasterOrSequential()){
        Message::Info(communicator->getRank(), "Writing "+file->getFileName(), 3);
        if (!output->is_open()){
            return false;
        }
        int64_t t=gidt->getType();
        for (int64_t a=0; a<static_cast<int64_t>((mapping.at(t)).size()); a++){
            int64_t e=(mapping.at(t))[static_cast<unsigned long>(a)];
            PRINT_LINE(e)
            for (int64_t j=0; j<file->getColumns(); j++){
                *output<<format(file->getData(e,j));
                *output<<"\t";
            }
            *output<<endl;
        }
    }
    return true;
}

template<typename T>
bool alya::GidWriter::writePost(AlyaPtr<MPIOFile<T> > file)
{
    if (AlyaString::Equal(file->getHeader()->getResultsOn(), MPIOHEADER_RES_POINT)){
        return writePostNodes(file);
    }else if (AlyaString::Equal(file->getHeader()->getResultsOn(), MPIOHEADER_RES_ELEM)){
        return writePostElements(file);
    }else if (AlyaString::Equal(file->getHeader()->getResultsOn(), MPIOHEADER_RES_BOUND)){
        return writePostBoundaries(file);
    }
    return true;
}

template<typename T>
bool alya::GidWriter::writePostNodes(AlyaPtr<MPIOFile<T> > file)
{
    AlyaPtr<GidResult> res=AlyaPtr<GidResult>(
                new GidResult(file->getHeader()->getObject(), static_cast<unsigned int>(file->getHeader()->getColumns()),
                              file->getHeader()->getResultsOn(), file->getHeader()->getTime()));

    STOP_ON_ERROR(reader->readFile(file));
    STOP_ON_ERROR(mergeOrGather(file));
    if (communicator->isMasterOrSequential()){
        *resFile<<*res;
        *resFile<<"Values"<<endl;
        STOP_ON_ERROR(writeFile(resFile, file));
        *resFile<<"End values"<<endl<<endl;
    }
    return true;
}

template<typename T>
bool alya::GidWriter::writePostElements(AlyaPtr<MPIOFile<T> > file)
{
    STOP_ON_ERROR(reader->readFile(file));
    STOP_ON_ERROR(mergeOrGather(file));
    for (AlyaPtr<GidType> gidt: typeId){
        AlyaPtr<GidResult> res=AlyaPtr<GidResult>(
                    new GidResult(file->getHeader()->getObject(), static_cast<unsigned int>(file->getHeader()->getColumns()),
                                  file->getHeader()->getResultsOn(), gidt, file->getHeader()->getTime()));
        if (communicator->isMasterOrSequential()){
            *resFile<<*res;
            *resFile<<"Values"<<endl;
            STOP_ON_ERROR(writeElementFile(resFile, file, gidt, elements));
            *resFile<<"End values"<<endl<<endl;
        }
    }
    return true;
}

template<typename T>
bool alya::GidWriter::writePostBoundaries(AlyaPtr<MPIOFile<T> > file)
{
    STOP_ON_ERROR(reader->readFile(file));
    STOP_ON_ERROR(mergeOrGather(file));
    for (AlyaPtr<GidType> gidt: typbId){
        AlyaPtr<GidResult> res=AlyaPtr<GidResult>(
                    new GidResult(file->getHeader()->getObject(), static_cast<unsigned int>(file->getHeader()->getColumns()),
                                  file->getHeader()->getResultsOn(), gidt, file->getHeader()->getTime()));
        if (communicator->isMasterOrSequential()){
            *resFile<<*res;
            *resFile<<"Values"<<endl;
            STOP_ON_ERROR(writeElementFile(resFile, file, gidt, boundaries));
            *resFile<<"End values"<<endl<<endl;
        }
    }
    return true;
}

bool alya::GidWriter::mergeOrGather(AlyaPtr<MPIOFile<double> > file)
{
    if (merge){
        if (reader->getIntSize()==4){
            AlyaPtr<MPIOFile<int32_t> > inv=getInv4(file->getHeader()->getResultsOn());
            if (inv!=nullptr){
                file->merge(inv);
            }else{
                file->gather();
            }
        }else if (reader->getIntSize()==8){
            AlyaPtr<MPIOFile<int64_t> > inv=getInv8(file->getHeader()->getResultsOn());
            if (inv!=nullptr){
                file->merge(inv);
            }else{
                file->gather();
            }
        }
    }
    else{
        file->gather();
    }
    return true;
}

bool alya::GidWriter::mergeOrGather(AlyaPtr<MPIOFile<int32_t> > file)
{
    if (merge){
        AlyaPtr<MPIOFile<int32_t> > inv=getInv4(file->getHeader()->getResultsOn());
        if (inv!=nullptr){
            file->merge(inv);
        }else{
            file->gather();
        }
    }
    else{
        file->gather();
    }
    return true;
}

bool alya::GidWriter::mergeOrGather(AlyaPtr<MPIOFile<int64_t> > file)
{
    if (merge){
        AlyaPtr<MPIOFile<int64_t> > inv=getInv8(file->getHeader()->getResultsOn());
        if (inv!=nullptr){
            file->merge(inv);
        }else{
            file->gather();
        }
    }
    else{
        file->gather();
    }
    return true;
}

bool alya::GidWriter::mergeOrGather(AlyaPtr<MPIOFile<double> > file, AlyaPtr<MPIOFile<int32_t> > inv2)
{
    if (merge){
        if (reader->getIntSize()==4){
            AlyaPtr<MPIOFile<int32_t> > inv=getInv4(file->getHeader()->getResultsOn());
            if (inv!=nullptr){
                file->merge(inv, inv2);
            }else{
                file->gather();
            }
        }
        else{
            return false;
        }
    }
    else{
        file->gather();
    }
    return true;
}

bool alya::GidWriter::mergeOrGather(AlyaPtr<MPIOFile<double> > file, AlyaPtr<MPIOFile<int64_t> > inv2)
{
    if (merge){
        if (reader->getIntSize()==8){
            AlyaPtr<MPIOFile<int64_t> > inv=getInv8(file->getHeader()->getResultsOn());
            if (inv!=nullptr){
                file->merge(inv, inv2);
            }else{
                file->gather();
            }
        }
        else{
            return false;
        }
    }
    else{
        file->gather();
    }
    return true;
}

bool alya::GidWriter::mergeOrGather(AlyaPtr<MPIOFile<int32_t> > file, AlyaPtr<MPIOFile<int32_t> > inv2)
{
    if (merge){
        AlyaPtr<MPIOFile<int32_t> > inv=getInv4(file->getHeader()->getResultsOn());
        if (inv!=nullptr){
            file->merge(inv, inv2);
        }else{
            file->gather();
        }
    }
    else{
        file->gather();
    }
    return true;
}

bool alya::GidWriter::mergeOrGather(AlyaPtr<MPIOFile<int64_t> > file, AlyaPtr<MPIOFile<int64_t> > inv2)
{
    if (merge){
        AlyaPtr<MPIOFile<int64_t> > inv=getInv8(file->getHeader()->getResultsOn());
        if (inv!=nullptr){
            file->merge(inv, inv2);
        }else{
            file->gather();
        }
    }
    else{
        file->gather();
    }
    return true;
}

AlyaPtr<alya::MPIOFile<int32_t> > alya::GidWriter::getInv4(string resultsOn)
{
    if (resultsOn.compare(MPIOHEADER_RES_POINT)==0){
        return reader->getLninv4();
    }else if (resultsOn.compare(MPIOHEADER_RES_ELEM)==0){
        return reader->getLeinv4();
    }else if (resultsOn.compare(MPIOHEADER_RES_BOUND)==0&&!reader->getNoBoundaries()){
        return reader->getLbinv4();
    }else{
        return nullptr;
    }
}

AlyaPtr<alya::MPIOFile<int64_t> > alya::GidWriter::getInv8(string resultsOn)
{
    if (resultsOn.compare(MPIOHEADER_RES_POINT)==0){
        return reader->getLninv8();
    }else if (resultsOn.compare(MPIOHEADER_RES_ELEM)==0){
        return reader->getLeinv8();
    }else if (resultsOn.compare(MPIOHEADER_RES_BOUND)==0&&!reader->getNoBoundaries()){
        return reader->getLbinv8();
    }else{
        return nullptr;
    }
}

AlyaPtr_ofstream alya::GidWriter::openMeshFile()
{
    AlyaPtr_ofstream file(new ofstream());
    file->open(caseName+".post.msh");
    return file;
}

AlyaPtr_ofstream alya::GidWriter::openResFile()
{
    AlyaPtr_ofstream file(new ofstream());
    file->open(caseName+".post.res");
    return file;
}

bool alya::GidWriter::closeFile(AlyaPtr_ofstream file)
{
    file->close();
    return true;
}

string alya::GidWriter::format(double value)
{
    stringstream stream;
    stream<<setprecision(FLOAT_PRECISION*2)<<std::scientific<<value;
    string s=stream.str();
    //find e:
    unsigned long e=0;
    string rad;
    string radsign;
    string exp;
    string expsign;
    bool shift=false;
    if (s[0]=='-'){
        radsign="-";
        s=s.substr(1, s.size()-1);
    }else{
        radsign=" ";
    }
    for (unsigned long i=0; i<s.size(); i++){
        if (s[i]=='e'){
            e=i;
            break;
        }
    }
    rad=s.substr(0, e);
    if (rad[0]!='0'){
        shift=true;
        string radtmp="0."+rad.substr(0,1)+rad.substr(2,rad.size()-2);
        rad=radtmp;
    }
    rad=rad.substr(0,1+1+FLOAT_PRECISION);
    expsign=s[e+1];
    exp=s.substr(e+2, s.substr().size()-(e+2));
    if (shift){
        int eint=atoi(exp.c_str());
        if (expsign[0]=='+'){
            eint++;
        }else if (--eint==0){
            expsign='+';
        }
        exp=to_string(eint);
    }
    while (exp.size()<EXP_SIZE){
        exp.insert(0, "0");
    }
    while (exp.size()>EXP_SIZE){
        if (exp[0]=='0'){
            exp=exp.substr(1, exp.size()-1);
        }else{
            break;
        }
    }
    s=radsign+rad+"E"+expsign+exp;
    return s;
}

string alya::GidWriter::format(int32_t value)
{
    return to_string(value);
}

string alya::GidWriter::format(int64_t value)
{
    return to_string(value);
}

template bool alya::GidWriter::writeFile(AlyaPtr_ofstream output, AlyaPtr<alya::MPIOFile<double> > file);

template bool alya::GidWriter::writeFile(AlyaPtr_ofstream output, AlyaPtr<alya::MPIOFile<int32_t> > file);

template bool alya::GidWriter::writeFile(AlyaPtr_ofstream output, AlyaPtr<alya::MPIOFile<int64_t> > file);

template bool alya::GidWriter::writeElementFile(AlyaPtr_ofstream output, AlyaPtr<alya::MPIOFile<double> > file, AlyaPtr<alya::GidType> gidt, map<int64_t, vector<int64_t> > mapping);

template bool alya::GidWriter::writeElementFile(AlyaPtr_ofstream output, AlyaPtr<alya::MPIOFile<int32_t> > file, AlyaPtr<alya::GidType> gidt, map<int64_t, vector<int64_t> > mapping);

template bool alya::GidWriter::writeElementFile(AlyaPtr_ofstream output, AlyaPtr<alya::MPIOFile<int64_t> > file, AlyaPtr<alya::GidType> gidt, map<int64_t, vector<int64_t> > mapping);

template bool alya::GidWriter::writePost(AlyaPtr<alya::MPIOFile<double> > file);

template bool alya::GidWriter::writePost(AlyaPtr<alya::MPIOFile<int32_t> > file);

template bool alya::GidWriter::writePost(AlyaPtr<alya::MPIOFile<int64_t> > file);

template bool alya::GidWriter::writePostNodes(AlyaPtr<alya::MPIOFile<double> > file);

template bool alya::GidWriter::writePostNodes(AlyaPtr<alya::MPIOFile<int32_t> > file);

template bool alya::GidWriter::writePostNodes(AlyaPtr<alya::MPIOFile<int64_t> > file);

template bool alya::GidWriter::writePostElements(AlyaPtr<alya::MPIOFile<double> > file);

template bool alya::GidWriter::writePostElements(AlyaPtr<alya::MPIOFile<int32_t> > file);

template bool alya::GidWriter::writePostElements(AlyaPtr<alya::MPIOFile<int64_t> > file);

template bool alya::GidWriter::writePostBoundaries(AlyaPtr<alya::MPIOFile<double> > file);

template bool alya::GidWriter::writePostBoundaries(AlyaPtr<alya::MPIOFile<int32_t> > file);

template bool alya::GidWriter::writePostBoundaries(AlyaPtr<alya::MPIOFile<int64_t> > file);




