/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include "postfilelist.h"


alya::PostFileList::PostFileList(string fileName, std::shared_ptr<MPIOComm> communicator): fileName(fileName), communicator(communicator)
{

}

bool alya::PostFileList::read()
{
    if (communicator->isMasterOrSequential()){
        AlyaPtr_ifstream file=AlyaPtr_ifstream(new ifstream());
        file->open(fileName);
        if (!file->is_open()){
            return false;
        }
        postFiles.clear();
        string line;
        while (getline(*file, line)){
            postFiles.push_back(line);
            Message::Info(communicator->getRank(), "Adding "+line,3);
        }
        file->close();
        count=static_cast<int64_t>(postFiles.size());
        communicator->Bcast(&count);
        for (string str: postFiles){
            communicator->Bcast(&str);
        }
    }else if (communicator->isParallelIOWorker()){
        communicator->Bcast(&count);
        for (int64_t i=0; i<count; i++){
            string str;
            communicator->Bcast(&str);
            postFiles.push_back(str);
        }
    }
    return true;
}

bool alya::PostFileList::isPartOf(string file)
{
    for (string str: postFiles){
        if (str.compare(file)==0){
            return true;
        }
    }
    return false;
}
