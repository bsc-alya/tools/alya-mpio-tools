/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include "gidtype.h"

std::ostream &alya::operator << (std::ostream &out, const alya::GidType &gidType)
{
    out << "MESH " + gidType.caseName + "_";
    if (gidType.boundaries){
        out << "BOU_";
    }
    out << gidType.name + " dimension " + to_string(gidType.dimension) + " Elemtype " + gidType.shape + " Nnode " + to_string(gidType.nodes) <<endl;
    return out;
}

std::ostream &alya::operator << (std::ostream &out, const AlyaPtr<alya::GidType> &gidType)
{
    out << gidType.get();
    return out;
}


alya::GidType::GidType(string caseName, unsigned int dimension, int64_t type, bool boundaries)
{
    this->caseName=caseName;
    this->dimension=dimension;
    this->type=type;
    this->boundaries=boundaries;
    for (auto& i : AlyaTypeDef::String2types){
        if (i.second==this->type){
            name=i.first;
            break;
        }
    }
    nodes=AlyaTypeDef::Types2nodes[static_cast<unsigned int>(this->type)];
    shape=AlyaTypeDef::Types2shapes[static_cast<unsigned int>(this->type)];
}

string alya::GidType::getShape() const
{
    return shape;
}

string alya::GidType::getName() const
{
    return name;
}

string alya::GidType::getCaseName() const
{
    return caseName;
}

bool alya::GidType::getBoundaries() const
{
    return boundaries;
}

unsigned int alya::GidType::getNodes() const
{
    return nodes;
}

int64_t alya::GidType::getType() const
{
    return type;
}

unsigned int alya::GidType::getDimension() const
{
    return dimension;
}
