/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include <iostream>
#include <argumentmanager.h>
#include <message.h>
#include <pointer.h>
#include <mpiocomm.h>
#include <mpiowrapper.h>
#include "subdomains.h"
#include "redis.h"

using namespace std;
using namespace alya;


template<typename T>
int redistribute(AlyaPtr_MPIOComm communicator, AlyaPtr<Redis<T>> redis){
    int rank=communicator->getRank();
    Message::Info(rank, "Creating output directory", 2);
    redis->createOutputDirectory();
    communicator->Barrier();
    Message::Info(rank, "Restributing mesh files", 2);
    STOP_ON_ERROR(redis->meshRedistribution());
    Message::Info(rank, "Restributing other files", 2);
    STOP_ON_ERROR(redis->fileRedistribution());
    return RETURN_OK;
}

int main(int argc, char* argv[])
{
    Message::InitTime();
    MPIO::Init(argc, argv);
    AlyaPtr_MPIOComm communicator(new MPIOComm());
    int rank=communicator->getRank();
    Message::IAM(rank, "Alya MPI-IO redistribution");
    Message::Separator(rank);
    if (communicator->isSequential()){
        Message::Info(rank, "Sequential version");
        Message::Critical(rank, "You cannot run the redistribution sequentially");
        MPIO::Abort();
        return RETURN_ERR;
    }else{
        Message::Info(rank, "Parallel version: "+to_string(communicator->getSize())+" processes");
    }
    Message::Separator(rank);
    Message::Info(rank, "Reading arguments", 1);
    AlyaPtr<ArgumentManager> argumentManager(new ArgumentManager(argc, argv, communicator));
    if (argumentManager->getHelp()){
        argumentManager->usage();
        MPIO::Finalize();
        return RETURN_OK;

    }
    if (!argumentManager->getValid()){
        Message::Critical(rank, "Invalid arguments");
        argumentManager->usage();
        MPIO::Abort();
        return RETURN_ERR;
    }
    Message::Info(rank, "Finding subdomain number", 1);
    AlyaPtr<Subdomains> subdomains(new Subdomains(communicator, argumentManager));
    int64_t subdomainNumber=subdomains->getSubdomainNumber();
    Message::Info(rank, "Partition contains " + to_string(subdomainNumber) + " subdomains", 2);
    if (subdomainNumber <1){
        Message::Critical(rank, "An error has been detected with the partition");
        MPIO::Abort();
        return RETURN_ERR;
    }else if (subdomainNumber <2){
        Message::Critical(rank, "Only one subdomain detected, cannot redistribute!");
        MPIO::Abort();
        return RETURN_ERR;
    }
    communicator->Barrier();
    if (communicator->getSize()<subdomainNumber){
        Message::Critical(rank, "You must redistribute with " + to_string(subdomainNumber) + " MPI processes");
        MPIO::Abort();
        return RETURN_ERR;
    }else if (communicator->getSize()>subdomainNumber){
        Message::Info(rank, "Reducing the communicator size to " + to_string(subdomainNumber), 2);
        communicator=communicator->split(communicator->getRank()<subdomainNumber?MPIO_WORKER:MPIO_NOT_WORKER);
        if (!communicator->isIOWorker()){
           MPIO::Finalize();
           return RETURN_OK;
       }
    }
    rank=communicator->getRank();
    communicator->Barrier();
    Message::Info(rank, "Redistributing elements", 1);
    int intSize=subdomains->getIntSize();
    int returnCode=RETURN_ERR;
    if(intSize==MPIO_TYPESIZE_4){
        AlyaPtr<Redis<int32_t>> redis(new Redis<int32_t>(communicator, argumentManager));
        returnCode=redistribute(communicator, redis);
    }else if(intSize==MPIO_TYPESIZE_8){
        AlyaPtr<Redis<int64_t>> redis(new Redis<int64_t>(communicator, argumentManager));
        returnCode=redistribute(communicator, redis);
    }
    if (returnCode != RETURN_OK){
        MPIO::Abort();
        return returnCode;
    }
    Message::DONE(rank);
    Message::PrintTime(rank);
    MPIO::Finalize();
    return RETURN_OK;
}
