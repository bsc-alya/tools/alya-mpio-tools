#include "redis.h"


template<typename T>
alya::Redis<T>::Redis(std::shared_ptr<MPIOComm> communicator, std::shared_ptr<ArgumentManager> argumentManager):
communicator(communicator), argumentManager(argumentManager)
{
    caseName=argumentManager->getCaseName();
    post=argumentManager->getPost();
    if (post){
        meshExtension=string(POST_SUFFIX)+string(MPIO_EXT);
    }
    else{
        meshExtension=string(MPIO_EXT);
    }
    subdomainNumber=communicator->getSize();
    for (int i=0; i<subdomainNumber; i++){
        boundaryPartition.push_back(0);
    }
}

template<typename T>
bool alya::Redis<T>::computeElementCommunications()
{

    if (communicator->isIOWorker()){
        //Reading MPIRA
        mpirankFile=AlyaPtr<MPIOFile<T> >(new MPIOFile<T> (communicator, caseName+MPIRA_SUFFIX+meshExtension));
        STOP_ON_ERROR(readFile(mpirankFile));
        int64_t offset=mpirankFile->getOffset();
        //Init values
        elementsSendcount=AlyaPtr<int64_t>(new int64_t[subdomainNumber]);
        elementsRecvcount=AlyaPtr<int64_t>(new int64_t[subdomainNumber]);
        for (int64_t i=0; i<subdomainNumber; i++){
            elementsSendcount.get()[i]=0;
            elementsRecvcount.get()[i]=0;
        }
        vector<vector<T>> elementsVec(static_cast<unsigned long>(subdomainNumber), vector<T>());
        //Compute counts and elements to send
        for(int64_t i=0; i<mpirankFile->getCount(); i++){
            elementsSendcount.get()[static_cast<unsigned long>(mpirankFile->getData(i)-1)]+=1;
            elementsVec[static_cast<unsigned long>(mpirankFile->getData(i)-1)].push_back(i);
        }
        //Exchange counts
        communicator->Alltoall(elementsSendcount, 1, elementsRecvcount, 1);
        //Compute final element number for this subdomain
        elementNumber=0;
        for(int64_t i=0; i<subdomainNumber; i++){
            elementNumber+=elementsRecvcount.get()[i];
        }
        leinv=AlyaPtr<T>(new T[elementNumber]);
        //Format elements to send
        AlyaPtr<T>elementsGlobal(new T[mpirankFile->getCount()]);
        elementsLocal=AlyaPtr<T>(new T[mpirankFile->getCount()]);
        int64_t k=0;
        for(unsigned long i=0; i<static_cast<unsigned long>(elementsVec.size()); i++){
            for(unsigned long j=0; j<static_cast<unsigned long>(elementsVec[i].size()); j++){
                elementsGlobal.get()[k]=elementsVec[i][j]+1+offset;
                elementsLocal.get()[k++]=elementsVec[i][j];
            }
        }
        //Exchange elements
        communicator->Alltoallv(elementsGlobal, vector<int64_t>(elementsSendcount.get(), elementsSendcount.get()+subdomainNumber),
                                leinv, vector<int64_t>(elementsRecvcount.get(), elementsRecvcount.get()+subdomainNumber));
        //Exchange sizes
        totalElementNumber=elementNumber;
        communicator->Sum(&totalElementNumber);
        communicator->Bcast(&totalElementNumber);
    }
    return true;
}

template<typename T>
bool alya::Redis<T>::computeNodeCommunications()
{
    if (communicator->isIOWorker()){
        //Reading LNODS
        lnodsFile=AlyaPtr<MPIOFile<T> >(new MPIOFile<T> (communicator, caseName+LNODS_SUFFIX+meshExtension));
        STOP_ON_ERROR(readFile(lnodsFile));
        //Reading COORD
        coordFile=AlyaPtr<MPIOFile<double> >(new MPIOFile<double> (communicator, caseName+COORD_SUFFIX+meshExtension));
        STOP_ON_ERROR(readFile(coordFile));
        //Computing offsets and total node number (without redistribution)
        computeNodeOffset(coordFile->getPartition());
        int64_t offset=nodeOffset[static_cast<unsigned long>(communicator->getRank())];
        totalNodeNumberBefore=coordFile->getHeader()->getLines();
        //Init values
        AlyaPtr<int64_t>lnodsSendcount(new int64_t[subdomainNumber]);
        AlyaPtr<int64_t>lnodsRecvcount(new int64_t[subdomainNumber]);
        for (int64_t i=0; i<subdomainNumber; i++){
            lnodsSendcount.get()[i]=0;
            lnodsRecvcount.get()[i]=0;
        }
        vector<vector<T>> lnodsToSendVec(static_cast<unsigned long>(subdomainNumber), vector<T>());
        int64_t lnodsSendcountTotal=0;
        //Compute counts and lnods to send
        for(int64_t i=0; i<lnodsFile->getLines(); i++){
            for(int64_t j=0; j<lnodsFile->getColumns(); j++){
                T data=lnodsFile->getData(i, j);
                /*if (data==0){
                    break;
                }*/
                lnodsSendcount.get()[static_cast<unsigned long>(mpirankFile->getData(i)-1)]+=1;
                lnodsSendcountTotal+=1;
                lnodsToSendVec[static_cast<unsigned long>(mpirankFile->getData(i)-1)].push_back(data);
            }
        }
        //Exchange counts
        communicator->Alltoall(lnodsSendcount, 1, lnodsRecvcount, 1);
        lnodsGlobalNumber=0;
        for(int64_t i=0; i<subdomainNumber; i++){
            lnodsGlobalNumber+=lnodsRecvcount.get()[i];
        }
        lnodsGlobal=AlyaPtr<T>(new T[lnodsGlobalNumber]);
        //Format lnods to send
        AlyaPtr<T>lnodsToSend(new T[lnodsSendcountTotal]);
        int64_t k=0;
        for(unsigned long i=0; i<static_cast<unsigned long>(lnodsToSendVec.size()); i++){
            for(unsigned long j=0; j<static_cast<unsigned long>(lnodsToSendVec[i].size()); j++){
                lnodsToSend.get()[k++]=lnodsToSendVec[i][j];
            }
        }
        //Exchange lnods
        communicator->Alltoallv(lnodsToSend, vector<int64_t>(lnodsSendcount.get(), lnodsSendcount.get()+subdomainNumber),
                                lnodsGlobal, vector<int64_t>(lnodsRecvcount.get(), lnodsRecvcount.get()+subdomainNumber));
        vector<std::set<T>> nodesToAskVec;
        nodesRecvcount=AlyaPtr<int64_t>(new int64_t[subdomainNumber]);
        nodesSendcount=AlyaPtr<int64_t>(new int64_t[subdomainNumber]);
        nodeNumber=0;
        for (int64_t i=0; i<subdomainNumber; i++){
            nodesRecvcount.get()[i]=0;
            nodesSendcount.get()[i]=0;
            nodesToAskVec.push_back(set<T>());
        }
        for(int64_t i=0; i<lnodsGlobalNumber; i++){
            int64_t n = lnodsGlobal.get()[i];
            if (n==0){
                continue;
            }
            int64_t s=getSubdomain(n);
            if (s<0){
                return false;
            }
            unsigned long index=static_cast<unsigned long>(s);
            if (nodesToAskVec[index].find(n) != nodesToAskVec[index].end()){
                continue;
            }
            nodesRecvcount.get()[index]+=1;
            nodesToAskVec[index].insert(n);
            nodeNumber++;
        }
        communicator->Alltoall(nodesRecvcount, 1, nodesSendcount, 1);
        nodesLocalNumber=0;
        for(int64_t i=0; i<subdomainNumber; i++){
            nodesLocalNumber+=nodesSendcount.get()[i];
        }
        lninv=AlyaPtr<T>(new T[nodeNumber]);
        k=0;
        for(unsigned long i=0; i<static_cast<unsigned long>(nodesToAskVec.size()); i++){
            for (const T &n : nodesToAskVec[i]){
                lninv.get()[k++]=n;
                lninvinv[n]=k;
            }
        }
        nodesLocal=AlyaPtr<T>(new T[nodesLocalNumber]);
        communicator->Alltoallv(lninv, vector<int64_t>(nodesRecvcount.get(), nodesRecvcount.get()+subdomainNumber),
                                nodesLocal, vector<int64_t>(nodesSendcount.get(), nodesSendcount.get()+subdomainNumber));
        for (int64_t i=0; i<nodesLocalNumber; i++){
            nodesLocal.get()[i]=nodesLocal.get()[i]-offset;
        }
        totalNodeNumberAfter=nodeNumber;
        communicator->Sum(&totalNodeNumberAfter);
        communicator->Bcast(&totalNodeNumberAfter);
    }
    return true;
}


template<typename T>
bool alya::Redis<T>::writeLeinv()
{
    if (communicator->isIOWorker()){
        AlyaPtr<MPIOFile<T> > leinvFile(new MPIOFile<T> (communicator, string(REDIS_OUTPUT_DIRECTORY)+"/"+caseName+LEINV_SUFFIX+string(POST_SUFFIX)+string(MPIO_EXT)));
        STOP_ON_ERROR(leinvFile->setHeader(MPIOHEADER_OBJ_LEINV, MPIOHEADER_RES_ELEM, 1, totalElementNumber));
        STOP_ON_ERROR(leinvFile->open(WRITE));
        leinvFile->setPartition(elementNumber);
        STOP_ON_ERROR(leinvFile->initWriting(false));
        leinvFile->setData(leinv);
        STOP_ON_ERROR(leinvFile->write());
        STOP_ON_ERROR(leinvFile->close());
        elementPartition=leinvFile->getPartition();
    }
    return true;
}

template<typename T>
bool alya::Redis<T>::writeLninv()
{
    if (communicator->isIOWorker()){
        AlyaPtr<MPIOFile<T> > lninvFile(new MPIOFile<T> (communicator, string(REDIS_OUTPUT_DIRECTORY)+"/"+caseName+LNINV_SUFFIX+string(POST_SUFFIX)+string(MPIO_EXT)));
        STOP_ON_ERROR(lninvFile->setHeader(MPIOHEADER_OBJ_LNINV, MPIOHEADER_RES_POINT, 1, totalNodeNumberAfter));
        STOP_ON_ERROR(lninvFile->open(WRITE));
        lninvFile->setPartition(nodeNumber);
        STOP_ON_ERROR(lninvFile->initWriting(false));
        lninvFile->setData(lninv);
        STOP_ON_ERROR(lninvFile->write());
        STOP_ON_ERROR(lninvFile->close());
        nodePartition=lninvFile->getPartition();
    }
    return true;
}

template<typename T>
bool Redis<T>::writeLnods()
{
    if (communicator->isIOWorker()){
        lnodsLocal=AlyaPtr<T>(new T[lnodsGlobalNumber]);
        for(int64_t i=0; i<lnodsGlobalNumber; i++){
            T n=lnodsGlobal.get()[i];
            if (n==0){
                lnodsLocal.get()[i]=0;
            }else{
                lnodsLocal.get()[i]=lninvinv[n];
            }
        }
        AlyaPtr<MPIOFile<T> > lnodsFile(new MPIOFile<T> (communicator, string(REDIS_OUTPUT_DIRECTORY)+"/"+caseName+LNODS_SUFFIX+string(POST_SUFFIX)+string(MPIO_EXT)));
        STOP_ON_ERROR(lnodsFile->setHeader(MPIOHEADER_OBJ_LNODS, MPIOHEADER_RES_ELEM, this->lnodsFile->getColumns(), totalElementNumber));
        STOP_ON_ERROR(lnodsFile->open(WRITE));
        lnodsFile->setPartition(elementNumber);
        STOP_ON_ERROR(lnodsFile->initWriting(false));
        lnodsFile->setData(lnodsLocal);
        STOP_ON_ERROR(lnodsFile->write());
        STOP_ON_ERROR(lnodsFile->close());
    }
    return true;
}

template<typename T>
bool Redis<T>::writeLtype()
{
    STOP_ON_ERROR(redistributeFile(caseName+LTYPE_SUFFIX+meshExtension));
    return true;
}

template<typename T>
bool Redis<T>::writeCoord()
{
    STOP_ON_ERROR(redistributeFile(caseName+COORD_SUFFIX+meshExtension));
    return true;
}

template<typename T>
bool Redis<T>::writePartition()
{
    fstream partition;
    string alyapar=".alyapar";
    alyapar=POST_SUFFIX+alyapar;
    partition.open(string(REDIS_OUTPUT_DIRECTORY)+"/"+caseName+alyapar, ios::out);
    if (!partition){
        return false;
    }
    partition<<to_string(subdomainNumber)<<endl;
    for (unsigned long i=0; i<static_cast<unsigned long>(subdomainNumber); i++){
        partition<<to_string(i+1)<<" "<<to_string(elementPartition[i])<<" "<<to_string(nodePartition[i])<<" "<<to_string(boundaryPartition[i])<<endl;
    }
    partition.close();
    return true;
}

template<typename T>
bool Redis<T>::findfileNames()
{
    set<string> excludedObjects EXCLUDED_OBJECTS;
    DIR *dir = opendir(".");
    dirent *entry;
    while((entry = readdir(dir))!=nullptr)
    {
        bool isMpio=AlyaString::HasSuffix(entry->d_name, string(MPIO_EXT));
        bool isPost=AlyaString::HasSuffix(entry->d_name, string(POST_SUFFIX)+string(MPIO_EXT));
        if((post && !isPost) || (!post && isPost)){
            continue;
        }else if(!isMpio){
            continue;
        }
        string str=string(entry->d_name);
        if(!(str.find(caseName) != string::npos)){
            continue;
        }
        bool excluded=false;
        for (auto excludedObject : excludedObjects){
            if (str.find(excludedObject) != string::npos){
                excluded=true;
                break;
            }
        }
        if (excluded){
            continue;
        }
        fileNames.push_back(str);
    }
    closedir(dir);
    sort(fileNames.begin(), fileNames.end());
    return true;
}

template<typename T>
bool Redis<T>::redistributeFile(string fileName)
{
    if(communicator->isIOWorker()){
        AlyaPtr<MPIOHeader>header=AlyaPtr<MPIOHeader>(new MPIOHeader(communicator, fileName));
        STOP_ON_ERROR(readHeader(header));
        if(AlyaString::Equal(header->getResultsOn(), MPIOHEADER_RES_BOUND)){
            return false;
        }
        if(AlyaString::Equal(header->getType(), MPIOHEADER_TYPE_REAL)){
            return redistributeFile<double>(fileName);
        }else if(AlyaString::Equal(header->getType(), MPIOHEADER_TYPE_INTEGER)){
            return redistributeFile<T>(fileName);
        }
    }
    return true;
}

template<typename T>
template<typename U>
bool Redis<T>::redistributeFile(string fileName)
{
    if(communicator->isIOWorker()){
        AlyaPtr<MPIOFile<U> >file=AlyaPtr<MPIOFile<U> >(new MPIOFile<U>(communicator, fileName));
        STOP_ON_ERROR(readFile(file));
        AlyaPtr<MPIOFile<U> >redisFile=AlyaPtr<MPIOFile<U> >(new MPIOFile<U>(communicator, string(REDIS_OUTPUT_DIRECTORY)+"/"+regex_replace(fileName, regex(meshExtension), string(POST_SUFFIX)+string(MPIO_EXT))));
        string resultsOn=file->getHeader()->getResultsOn();
        int64_t lineNumber=0;
        int64_t partitionNumber=0;
        int64_t columns=file->getColumns();
        int64_t lines=file->getLines();
        AlyaPtr<T> local;
        vector<int64_t> sendcount;
        vector<int64_t> recvcount;
        if (AlyaString::Equal(resultsOn, MPIOHEADER_RES_ELEM)){
            lineNumber=totalElementNumber;
            partitionNumber=elementNumber;
            local=elementsLocal;
            for(int64_t i=0; i<subdomainNumber; i++){
                sendcount.push_back(elementsSendcount.get()[i]*columns);
                recvcount.push_back(elementsRecvcount.get()[i]*columns);
            }
        }else if(AlyaString::Equal(resultsOn, MPIOHEADER_RES_POINT)){
            lineNumber=totalNodeNumberAfter;
            partitionNumber=nodeNumber;
            local=nodesLocal;
            lines=nodesLocalNumber;
            for(int64_t i=0; i<subdomainNumber; i++){
                sendcount.push_back(nodesSendcount.get()[i]*columns);
                recvcount.push_back(nodesRecvcount.get()[i]*columns);
            }
        }
        AlyaPtr<U> datain=file->getData();
        AlyaPtr<U> datainReorder(new U[lines*columns]);
        for (int64_t i=0; i<lines; i++){
            for (int64_t j=0; j<columns; j++){
                datainReorder.get()[i*columns+j]=datain.get()[local.get()[i]*columns+j];
            }
        }
        AlyaPtr<U> dataout(new U[columns*partitionNumber]);
        communicator->Alltoallv(datainReorder, sendcount, dataout, recvcount);
        STOP_ON_ERROR(redisFile->setHeader(file->getHeader()->getObject(), resultsOn, columns, lineNumber));
        STOP_ON_ERROR(redisFile->open(WRITE));
        redisFile->setPartition(partitionNumber);
        STOP_ON_ERROR(redisFile->initWriting(false));
        redisFile->setData(dataout);
        STOP_ON_ERROR(redisFile->write());
        STOP_ON_ERROR(redisFile->close());
    }
    return true;
}


template<typename T>
int64_t alya::Redis<T>::getSubdomain(int64_t node)
{
    int64_t subdomain=(node*subdomainNumber)/totalNodeNumberBefore;
    while (!((node<nodeOffset[static_cast<unsigned long>(subdomain)+1])&&(node>=nodeOffset[static_cast<unsigned long>(subdomain)]))){
        if (node<nodeOffset[static_cast<unsigned long>(subdomain)]){
            subdomain--;
        }
        else{
            subdomain++;
        }
    }
    if (subdomain<0||subdomain>=subdomainNumber){
        return -1;
    }
    return subdomain;
}

template<typename T>
bool alya::Redis<T>::computeNodeOffset(vector<int64_t> partition)
{
    int64_t offset=1;
    for (unsigned long i=0; i<partition.size(); i++){
        nodeOffset.push_back(offset);
        offset+=partition[i];
    }
    nodeOffset.push_back(offset);
    return true;
}

template<typename T>
void alya::Redis<T>::createOutputDirectory()
{
    if (communicator->isMasterOrSequential()){
        const int dir_err = mkdir(REDIS_OUTPUT_DIRECTORY, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
        if (-1 == dir_err){
            Message::Error(communicator->getRank(), "Unable to create the output directory");
            MPIO::Abort();
        }
    }
}

template<typename T>
bool Redis<T>::meshRedistribution()
{
    if(communicator->isIOWorker()){
        STOP_ON_ERROR(computeElementCommunications());
        STOP_ON_ERROR(writeLeinv());
        STOP_ON_ERROR(computeNodeCommunications());
        STOP_ON_ERROR(writeLninv());
        STOP_ON_ERROR(writePartition())
        STOP_ON_ERROR(writeLnods());
        STOP_ON_ERROR(writeLtype());
        STOP_ON_ERROR(writeCoord());
    }
    return true;
}

template<typename T>
bool Redis<T>::fileRedistribution()
{
    if(communicator->isIOWorker()){
        findfileNames();
        for (auto fileName: fileNames){
            redistributeFile(fileName);
        }
    }
    return true;
}

template<typename T>
template<typename U>
bool alya::Redis<T>::readFile(AlyaPtr<alya::MPIOFile<U> > file)
{
    Message::Debug(communicator->getRank(), "Reading "+file->getFileName()+" file");
    STOP_ON_ERROR_MSG(file->open(READ), Message::Critical(communicator->getRank(), "Can not open "+file->getFileName()+" file!"));
    STOP_ON_ERROR_MSG(file->read(), Message::Critical(communicator->getRank(), "Can not read "+file->getFileName()+" file!"));
    STOP_ON_ERROR_MSG(file->close(), Message::Critical(communicator->getRank(), "Can not close "+file->getFileName()+" file!"));
    return true;
}

template<typename T>
bool alya::Redis<T>::readHeader(AlyaPtr<alya::MPIOHeader > header)
{
    Message::Debug(communicator->getRank(), "Reading "+header->getFileName()+" header");
    STOP_ON_ERROR_MSG(header->open(READ), Message::Critical(communicator->getRank(), "Can not open "+header->getFileName()+" header!"));
    STOP_ON_ERROR_MSG(header->read(), Message::Critical(communicator->getRank(), "Can not read "+header->getFileName()+" header!"));
    STOP_ON_ERROR_MSG(header->close(), Message::Critical(communicator->getRank(), "Can not close "+header->getFileName()+" header!"));
    return true;
}


template class alya::Redis<int32_t>;

template class alya::Redis<int64_t>;

