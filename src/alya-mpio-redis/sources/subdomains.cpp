#include "subdomains.h"


alya::Subdomains::Subdomains(AlyaPtr_MPIOComm communicator, AlyaPtr<ArgumentManager> argumentManager):
    communicator(communicator), argumentManager(argumentManager)
{
    caseName=argumentManager->getCaseName();
    post=argumentManager->getPost();
    if (post){
        meshExtension=string(POST_SUFFIX)+string(MPIO_EXT);
    }
    else{
        meshExtension=string(MPIO_EXT);
    }
    intSize=0;
    mpirankFileName=caseName+MPIRA_SUFFIX+meshExtension;
}


bool alya::Subdomains::readHeader(AlyaPtr<alya::MPIOHeader> header)
{
    STOP_ON_ERROR_MSG(header->open(READ), Message::Error("Can not open "+header->getFileName()+" (header)"));
    STOP_ON_ERROR_MSG(header->read(), Message::Error("Can not read "+header->getFileName()+" (header)"));
    STOP_ON_ERROR_MSG(header->close(), Message::Error("Can not close "+header->getFileName()+" (header)"));
    return true;
}


template<typename T>
bool alya::Subdomains::readFile(AlyaPtr<alya::MPIOFile<T> > file)
{
    Message::Debug(communicator->getRank(), "Reading "+file->getFileName()+" file");
    STOP_ON_ERROR_MSG(file->open(READ), Message::Critical(communicator->getRank(), "Can not open "+file->getFileName()+" file!"));
    STOP_ON_ERROR_MSG(file->read(), Message::Critical(communicator->getRank(), "Can not read "+file->getFileName()+" file!"));
    STOP_ON_ERROR_MSG(file->close(), Message::Critical(communicator->getRank(), "Can not close "+file->getFileName()+" file!"));
    return true;
}


bool alya::Subdomains::findIntegerSize(){
    if (communicator->isIOWorker()){
        mpirankHeader = AlyaPtr_MPIOHeader(new MPIOHeader(communicator, mpirankFileName));
        STOP_ON_ERROR(readHeader(mpirankHeader));
        intSize=mpirankHeader->getIntSize();
    }
    return true;
}

int64_t Subdomains::getSubdomainNumber()
{
    if (!findIntegerSize()){
        Message::Error(communicator->getRank(), "Cannot find integer size");
        return -1;
    }
    int64_t maxLocal=0;
    if (communicator->isIOWorker()){
        if (intSize == MPIO_TYPESIZE_4){
            mpirankFileI4=AlyaPtr<MPIOFile<int32_t> >(new MPIOFile<int32_t> (communicator, mpirankFileName));
            if (!readFile(mpirankFileI4)){
                return -1;
            }
            maxLocal=mpirankFileI4->getMaxValue();
        }else if (intSize == MPIO_TYPESIZE_8){
            mpirankFileI8=AlyaPtr<MPIOFile<int64_t> >(new MPIOFile<int64_t> (communicator, mpirankFileName));
            if (!readFile(mpirankFileI8)){
                return -1;
            }
            maxLocal=mpirankFileI8->getMaxValue();
        }
        subdomainNumber=maxLocal;
        communicator->Max(&subdomainNumber);
        communicator->Bcast(&subdomainNumber);

    }
    return subdomainNumber;
}

int alya::Subdomains::getIntSize() const
{
    return intSize;
}
