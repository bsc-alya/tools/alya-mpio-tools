#ifndef REDIS_H
#define REDIS_H


#include "argumentmanager.h"
#include <mpiofile.h>
#include <mpioreaderdef.h>
#include <alyareturn.h>
#include <alyastring.h>
#include <set>
#include <map>
#include <iostream>
#include <fstream>
#include <regex>
#include <dirent.h>

using namespace std;
using namespace alya;

#define REDIS_OUTPUT_DIRECTORY "redistribution"
#define EXCLUDED_OBJECTS {"COORD", "LNODS", "LTYPE", "LEINV", "LNINV", "LMAST"}

namespace alya
{

    template<typename T> class Redis
    {
    public:
        Redis(AlyaPtr_MPIOComm communicator, AlyaPtr<ArgumentManager> argumentManager);
        void createOutputDirectory();
        bool meshRedistribution();
        bool fileRedistribution();
    private:
        bool computeElementCommunications();
        bool computeNodeCommunications();
        bool writeLeinv();
        bool writeLninv();
        bool writeLnods();
        bool writeLtype();
        bool writeCoord();
        bool writePartition();
        bool findfileNames();

        bool readHeader(AlyaPtr<MPIOHeader> header);
        template<typename U>
        bool readFile(AlyaPtr<MPIOFile<U> > file);
        template<typename U>
        bool redistributeFile(string fileName);
        bool redistributeFile(string fileName);
        int64_t getSubdomain(int64_t node);
        bool computeNodeOffset(vector<int64_t> partition);

    private:
        AlyaPtr_MPIOComm communicator;
        AlyaPtr<ArgumentManager> argumentManager;
        string caseName;
        string meshExtension;
        bool post;

        //files
        AlyaPtr<MPIOFile<T>> mpirankFile;
        AlyaPtr<MPIOFile<T>> lnodsFile;
        AlyaPtr<MPIOFile<double>> coordFile;
        vector<string> fileNames;

        //subdomain
        int64_t subdomainNumber;

        //elements
        int64_t elementNumber;
        int64_t totalElementNumber;
        AlyaPtr<T> leinv;
        AlyaPtr<T> elementsLocal;
        AlyaPtr<int64_t> elementsSendcount;
        AlyaPtr<int64_t> elementsRecvcount;

        //nodes
        int64_t totalNodeNumberBefore;
        vector<int64_t> nodeOffset;
        int64_t nodeNumber;
        int64_t totalNodeNumberAfter;
        AlyaPtr<T> lninv;
        map<T, T> lninvinv;
        AlyaPtr<T> nodesLocal;
        int64_t nodesLocalNumber;
        AlyaPtr<int64_t> nodesRecvcount;
        AlyaPtr<int64_t> nodesSendcount;

        //lnods
        int64_t lnodsGlobalNumber;
        AlyaPtr<T> lnodsGlobal;
        AlyaPtr<T> lnodsLocal;

        //partition
        vector<int64_t> elementPartition;
        vector<int64_t> nodePartition;
        vector<int64_t> boundaryPartition;
    };

}

#endif // REDIS_H
