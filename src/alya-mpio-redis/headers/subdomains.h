#ifndef SUBDOMAINS_H
#define SUBDOMAINS_H

#include <argumentmanager.h>
#include <mpiofile.h>
#include <mpioreaderdef.h>
#include <alyareturn.h>

using namespace std;
using namespace alya;

namespace alya
{

    class Subdomains
    {
    public:
        Subdomains(AlyaPtr_MPIOComm communicator, AlyaPtr<ArgumentManager> argumentManager);
        int64_t getSubdomainNumber();

        int getIntSize() const;

    private:
        bool findIntegerSize();
        bool readHeader(std::shared_ptr<alya::MPIOHeader> header);
        template<typename T>
        bool readFile(std::shared_ptr<alya::MPIOFile<T> > file);
    private:
        AlyaPtr_MPIOComm communicator;
        AlyaPtr<ArgumentManager> argumentManager;
        string caseName;
        string meshExtension;
        bool post;
        string mpirankFileName;
        int intSize;
        AlyaPtr_MPIOHeader mpirankHeader;
        AlyaPtr<MPIOFile<int32_t>> mpirankFileI4;
        AlyaPtr<MPIOFile<int64_t>> mpirankFileI8;
        int64_t subdomainNumber;
    };

}

#endif // SUBDOMAINS_H
