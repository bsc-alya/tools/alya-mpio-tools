/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#ifndef PARTITIONER_H
#define PARTITIONER_H

#include <mpioreaderdef.h>
#include <mpiofile.h>
#include <argumentmanager.h>
#include <message.h>
#include <sstream>
#include <iomanip>
#include <alyareturn.h>
#include <metis.h>
#include <alya-mpio-metis_config.h>
#include <cstddef> /* NULL */

using namespace std;

namespace alya {

    class Partitioner
    {
    public:
        Partitioner(AlyaPtr_MPIOComm communicator, AlyaPtr<ArgumentManager> argumentManager);
        ~Partitioner();
        bool readMesh();
        bool partitionMesh();
        bool writePartition();
    protected:
        template<typename T>
        bool readFile(std::shared_ptr<alya::MPIOFile<T> > file);
        bool readHeader(std::shared_ptr<alya::MPIOHeader> header);
        void deallocate();
    private:
        AlyaPtr<ArgumentManager> argumentManager;
        AlyaPtr_MPIOComm communicator;
        int subdomains;
        bool post;
        string caseName;
        string meshExtension;
        int intSize;
        AlyaPtr<MPIOHeader> coordHeader;
        AlyaPtr<MPIOFile<int32_t> > lnods4;
        AlyaPtr<MPIOFile<int64_t> > lnods8;
        idx_t ne, nn, *eptr, nparts, objval, *epart, *npart;
        vector<idx_t> eind;
        idx_t options[METIS_NOPTIONS];
        bool allocated;
    };



}



#endif // PARTITIONER_H
