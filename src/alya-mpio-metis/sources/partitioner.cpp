/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include "partitioner.h"


alya::Partitioner::Partitioner(AlyaPtr_MPIOComm communicator, AlyaPtr<ArgumentManager> argumentManager):
argumentManager(argumentManager), communicator(communicator)
{
    caseName=argumentManager->getCaseName();
    subdomains=argumentManager->getSubdomains();
    post=argumentManager->getPost();
    if (post){
        meshExtension=string(POST_SUFFIX)+string(MPIO_EXT);
    }
    else{
        meshExtension=string(MPIO_EXT);
    }
    intSize=0;
    allocated=false;
}

alya::Partitioner::~Partitioner()
{
   if (allocated){
       deallocate();
   }
}


bool alya::Partitioner::readHeader(AlyaPtr<alya::MPIOHeader> header)
{
    STOP_ON_ERROR_MSG(header->open(READ), Message::Error("Can not open "+header->getFileName()+" (header)"))
    STOP_ON_ERROR_MSG(header->read(), Message::Error("Can not read "+header->getFileName()+" (header)"))
    STOP_ON_ERROR_MSG(header->close(), Message::Error("Can not close "+header->getFileName()+" (header)"))
    return true;
}


template<typename T>
bool alya::Partitioner::readFile(AlyaPtr<alya::MPIOFile<T> > file)
{
    Message::Debug(communicator->getRank(), "Reading "+file->getFileName()+" file");
    STOP_ON_ERROR_MSG(file->open(READ), Message::Critical(communicator->getRank(), "Can not open "+file->getFileName()+" file!"))
    STOP_ON_ERROR_MSG(file->read(), Message::Critical(communicator->getRank(), "Can not read "+file->getFileName()+" file!"))
    STOP_ON_ERROR_MSG(file->close(), Message::Critical(communicator->getRank(), "Can not close "+file->getFileName()+" file!"))
    return true;
}


bool alya::Partitioner::readMesh()
{
    if (communicator->isIOWorker()){
        string coordName = caseName + COORD_SUFFIX + meshExtension;
        coordHeader=AlyaPtr<MPIOHeader>(new MPIOHeader(communicator, coordName));
        STOP_ON_ERROR(readHeader(coordHeader));
        string lnodsName = caseName + LNODS_SUFFIX + meshExtension;
        AlyaPtr<MPIOHeader> header = AlyaPtr<MPIOHeader>(new MPIOHeader(communicator, lnodsName));
        STOP_ON_ERROR(readHeader(header));
        intSize = header->getIntSize();
        if (intSize == MPIO_TYPESIZE_4){
            lnods4=AlyaPtr<MPIOFile<int32_t> >(new MPIOFile<int32_t> (communicator, lnodsName));
            STOP_ON_ERROR(readFile(lnods4))
        }else if (intSize == MPIO_TYPESIZE_8){
            lnods8=AlyaPtr<MPIOFile<int64_t> >(new MPIOFile<int64_t> (communicator, lnodsName));
            STOP_ON_ERROR(readFile(lnods8))
        }
    }
    return true;
}

bool alya::Partitioner::partitionMesh()
{
    METIS_SetDefaultOptions(options);
    options[METIS_OPTION_NUMBERING]=0;
    options[METIS_OPTION_CONTIG]=1;
    options[METIS_OPTION_PTYPE]=METIS_PTYPE_KWAY;
    if (allocated){
        deallocate();
    }
    if (communicator->isIOWorker()){
        allocated = true;
        nn = static_cast<idx_t>(coordHeader->getLines());
        int totalElementNodes=0;
        if (intSize == MPIO_TYPESIZE_4){
            ne = static_cast<idx_t>(lnods4->getLines());
            eptr = new idx_t[lnods4->getLines()+1];
            eptr[0]=0;
            for (int64_t i=0; i<lnods4->getLines(); i++){
                int elementNodes=0;
                for (int64_t j=0; j<lnods4->getColumns(); j++){
                    int32_t node=lnods4->getData(i, j);
                    if (node==0){
                        break;
                    }
                    elementNodes++;
                    eind.push_back(node-1);
                }
                totalElementNodes+=elementNodes;
                eptr[i+1]=totalElementNodes;
             }
        }else if (intSize == MPIO_TYPESIZE_8){
            ne = static_cast<idx_t>(lnods8->getLines());
            eptr = new idx_t[lnods8->getLines()+1];
            eptr[0]=0;
            for (int64_t i=0; i<lnods8->getLines(); i++){
                int elementNodes=0;
                for (int64_t j=0; j<lnods8->getColumns(); j++){
                    int64_t node=lnods8->getData(i, j);
                    if (node==0){
                        break;
                    }
                    elementNodes++;
                    eind.push_back(node-1);
                }
                totalElementNodes+=elementNodes;
                eptr[i+1]=totalElementNodes;
             }
        }
        nparts = static_cast<idx_t>(subdomains);
        epart = new idx_t[ne];
        npart = new idx_t[nn];
        int return_code = METIS_PartMeshNodal(&ne, &nn, eptr, eind.data(), NULL, NULL, &nparts, NULL, options, &objval, epart, npart);
        switch(return_code){
            case(METIS_OK): return true;
            case(METIS_ERROR_INPUT): Message::Error(communicator->getRank(), "Metis: error input"); return false;
            case(METIS_ERROR_MEMORY): Message::Error(communicator->getRank(), "Metis: error memory"); return false;
            case(METIS_ERROR): Message::Error(communicator->getRank(), "Metis: error"); return false;
            default: Message::Error(communicator->getRank(), "Metis: unknown error"); return false;
        }
    }
    return true;
}

bool alya::Partitioner::writePartition()
{
    if (communicator->isIOWorker()){
        string partitionName = caseName + MPIRA_SUFFIX + meshExtension;
        if (intSize==4){
            AlyaPtr<MPIOFile<int32_t> >int4file=AlyaPtr<MPIOFile<int32_t> >(new MPIOFile<int32_t> (communicator, partitionName));
            STOP_ON_ERROR_MSG(int4file->setHeader(MPIOHEADER_OBJ_MPIRA, MPIOHEADER_RES_ELEM, 1, lnods4->getLines()), Message::Critical("File "+partitionName+": Cannot set the header"))
            STOP_ON_ERROR_MSG(int4file->open(WRITE), Message::Critical("File "+partitionName+": Cannot open"))
            STOP_ON_ERROR_MSG(int4file->initWriting(true), Message::Critical("File "+partitionName+": Cannot start writing"))
            for(int64_t i=0; i<lnods4->getLines(); i++){
                STOP_ON_ERROR_MSG(int4file->write(epart[i]+1), Message::Critical("File "+partitionName+": Cannot write"))
            }
            STOP_ON_ERROR_MSG(int4file->close(), Message::Critical("File "+partitionName+": Cannot close"))
        }else if (intSize==8){
            AlyaPtr<MPIOFile<int64_t> >int8file=AlyaPtr<MPIOFile<int64_t> >(new MPIOFile<int64_t> (communicator, partitionName));
            STOP_ON_ERROR_MSG(int8file->setHeader(MPIOHEADER_OBJ_MPIRA, MPIOHEADER_RES_ELEM, 1, lnods8->getLines()), Message::Critical("File "+partitionName+": Cannot set the header"))
            STOP_ON_ERROR_MSG(int8file->open(WRITE), Message::Critical("File "+partitionName+": Cannot open"))
            STOP_ON_ERROR_MSG(int8file->initWriting(true), Message::Critical("File "+partitionName+": Cannot start writing"))
            for(int64_t i=0; i<lnods8->getLines(); i++){
                STOP_ON_ERROR_MSG(int8file->write(epart[i]+1), Message::Critical("File "+partitionName+": Cannot write"))
            }
            STOP_ON_ERROR_MSG(int8file->close(), Message::Critical("File "+partitionName+": Cannot close"))
        }
    }
    return true;
}

void alya::Partitioner::deallocate()
{
    if (allocated){
        delete[] eptr;
        delete[] epart;
        delete[] npart;
    }
    allocated = false;
}



