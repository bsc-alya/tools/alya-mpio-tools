/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#ifndef HEADERMANAGER_H
#define HEADERMANAGER_H

#include <alya-mpio-header-manager_config.h>
#include <mpiocomm.h>
#include <mpioheader.h>
#include <mpiofile.h>
#include <pointer.h>
#include <message.h>
#include <alyastring.h>
#include <alyareturn.h>
#include <argumentmanager.h>

using namespace std;

namespace alya {

    class HeaderManager
    {
    public:
        HeaderManager(AlyaPtr_MPIOComm communicator, string fileName, AlyaPtr<ArgumentManager> arguments);
        bool processHeader();
    private:
        bool update();
        bool printHeader();
        AlyaPtr_MPIOComm communicator;
        string fileName;
        AlyaPtr<ArgumentManager> arguments;
        AlyaPtr<alya::MPIOHeader> header;
    };

}



#endif // HEADERMANAGER_H
