/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include "argumentmanager.h"

alya::ArgumentManager::ArgumentManager(int argc, char **argv, AlyaPtr_MPIOComm communicator):communicator(communicator)
{
    help=false;
    valid=true;
    me=string(argv[0]);
    string arg;
    for (int i=1; i<argc; i++){
        string str=string(argv[i]);
        if (i<argc-1){
           arg=string(argv[i+1]);
        }
        if (ARGUMENT(str, "-h", "--help")){
            help=true;
            break;
        } else if (argc<4){
            valid=false;
            return;
        }
        else if (ARGUMENT(str, "-o", "--object")){
            object=arg;
            i++;
        }
        else if (ARGUMENT(str, "-d", "--dimension")){
            dimension=arg;
            i++;
        }
        else if (ARGUMENT(str, "-r", "--resultsOn")){
            resultsOn=arg;
            i++;
        }
        else if (ARGUMENT(str, "--type", "--type")){
            type=arg;
            i++;
        }
        else if (ARGUMENT(str, "--size", "--size")){
            size=arg;
            i++;
        }
        else if (ARGUMENT(str, "-p", "--parallel")){
            parallel=arg;
            i++;
        }
        else if (ARGUMENT(str, "-f", "--filter")){
            filter=arg;
            i++;
        }
        else if (ARGUMENT(str, "--sorting", "--sorting")){
            sorting=arg;
            i++;
        }
        else if (ARGUMENT(str, "--id", "--id")){
            id=arg;
            i++;
        }
        else if (ARGUMENT(str, "-c", "--columns")){
            columns=arg;
            i++;
        }
        else if (ARGUMENT(str, "-l", "--lines")){
            lines=arg;
            i++;
        }
        else if (ARGUMENT(str, "--ittim", "--ittim")){
            ittim=arg;
            i++;
        }
        else if (ARGUMENT(str, "-n", "--nsubd")){
            nsubd=arg;
            i++;
        }
        else if (ARGUMENT(str, "-d", "--divi")){
            divi=arg;
            i++;
        }
        else if (ARGUMENT(str, "-t1", "--tag1")){
            tag1=arg;
            i++;
        }
        else if (ARGUMENT(str, "-t2", "--tag2")){
            tag2=arg;
            i++;
        }
        else if (ARGUMENT(str, "--time", "--time")){
            time=arg;
            i++;
        }else{
            file=str;
        }
    }
}

void alya::ArgumentManager::usage()
{
    Message::Info(communicator->getRank(), "Usage");
    Message::Info(communicator->getRank(), string(me.c_str())+" [OPTIONS] file");
    Message::Info(communicator->getRank(), "Options:");
    Message::Info(communicator->getRank(), "-h     | --help              : Usage");
    Message::Info(communicator->getRank(), "-o     | --object    [string]: Modify field: Object");
    Message::Info(communicator->getRank(), "-d     | --dimension [string]: Modify field: Dimension");
    Message::Info(communicator->getRank(), "-r     | --resultson [string]: Modify field: ResultsOn");
    Message::Info(communicator->getRank(), "--type               [string]: Modify field: Type");
    Message::Info(communicator->getRank(), "--size               [string]: Modify field: Size");
    Message::Info(communicator->getRank(), "-p     | --parallel  [string]: Modify field: Parallel");
    Message::Info(communicator->getRank(), "-f     | --filter    [string]: Modify field: Filter");
    Message::Info(communicator->getRank(), "--sorting            [string]: Modify field: Sorting");
    Message::Info(communicator->getRank(), "--id                 [string]: Modify field: Id");
    Message::Info(communicator->getRank(), "-c     | --columns   [string]: Modify field: Columns");
    Message::Info(communicator->getRank(), "-l     | --lines     [string]: Modify field: Lines");
    Message::Info(communicator->getRank(), "--ittim              [string]: Modify field: Ittim");
    Message::Info(communicator->getRank(), "-n     | --nsubd     [string]: Modify field: Nsubd");
    Message::Info(communicator->getRank(), "-d     | --divi      [string]: Modify field: Divi");
    Message::Info(communicator->getRank(), "-t1    | --tag1      [string]: Modify field: Tag 1");
    Message::Info(communicator->getRank(), "-t2    | --tag2      [string]: Modify field: Tag 2");
    Message::Info(communicator->getRank(), "--time               [string]: Modify field: Time");

}

bool alya::ArgumentManager::getHelp() const
{
    return help;
}

string alya::ArgumentManager::getFile() const
{
    return file;
}

string alya::ArgumentManager::getTime() const
{
    return time;
}

string alya::ArgumentManager::getTag2() const
{
    return tag2;
}

string alya::ArgumentManager::getTag1() const
{
    return tag1;
}

string alya::ArgumentManager::getDivi() const
{
    return divi;
}

string alya::ArgumentManager::getNsubd() const
{
    return nsubd;
}

string alya::ArgumentManager::getIttim() const
{
    return ittim;
}

string alya::ArgumentManager::getLines() const
{
    return lines;
}

string alya::ArgumentManager::getColumns() const
{
    return columns;
}

string alya::ArgumentManager::getId() const
{
    return id;
}

string alya::ArgumentManager::getSorting() const
{
    return sorting;
}

string alya::ArgumentManager::getFilter() const
{
    return filter;
}

string alya::ArgumentManager::getParallel() const
{
    return parallel;
}

string alya::ArgumentManager::getSize() const
{
    return size;
}

string alya::ArgumentManager::getType() const
{
    return type;
}

string alya::ArgumentManager::getResultsOn() const
{
    return resultsOn;
}

string alya::ArgumentManager::getDimension() const
{
    return dimension;
}

string alya::ArgumentManager::getObject() const
{
    return object;
}

bool alya::ArgumentManager::getValid() const
{
    return valid;
}
