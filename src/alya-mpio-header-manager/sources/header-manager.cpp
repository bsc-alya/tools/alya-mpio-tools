/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include "header-manager.h"


alya::HeaderManager::HeaderManager(AlyaPtr<alya::MPIOComm> communicator, string fileName, AlyaPtr<alya::ArgumentManager> arguments):
communicator(communicator), fileName(fileName), arguments(arguments)
{

}

bool alya::HeaderManager::update()
{
    if (communicator->isMasterOrSequential()){
        if (!arguments->getObject().empty()){
            header->setObject(arguments->getObject());
        }
        if (!arguments->getDimension().empty()){
            header->setDimension(arguments->getDimension());
        }
        if (!arguments->getResultsOn().empty()){
            header->setResultsOn(arguments->getResultsOn());
        }
        if (!arguments->getType().empty()){
            header->setType(arguments->getType());
        }
        if (!arguments->getSize().empty()){
            header->setSize(arguments->getSize());
        }
        if (!arguments->getParallel().empty()){
            header->setParallel(arguments->getParallel());
        }
        if (!arguments->getFilter().empty()){
            header->setFilter(arguments->getFilter());
        }
        if (!arguments->getSorting().empty()){
            header->setSorting(arguments->getSorting());
        }
        if (!arguments->getId().empty()){
            header->setId(arguments->getId());
        }
        if (!arguments->getColumns().empty()){
            header->setColumns(stoll(arguments->getColumns()));
        }
        if (!arguments->getLines().empty()){
            header->setLines(stoll(arguments->getLines()));
        }
        if (!arguments->getIttim().empty()){
            header->setIttim(stoll(arguments->getIttim()));
        }
        if (!arguments->getNsubd().empty()){
            header->setNsubd(stoll(arguments->getNsubd()));
        }
        if (!arguments->getDivi().empty()){
            header->setDivi(stoll(arguments->getDivi()));
        }
        if (!arguments->getTag1().empty()){
            header->setTag1(stoll(arguments->getTag1()));
        }
        if (!arguments->getTag2().empty()){
            header->setTag2(stoll(arguments->getTag2()));
        }
        if (!arguments->getTime().empty()){
            header->setTime(stod(arguments->getTime()));
        }
    }
    return true;
}

bool alya::HeaderManager::processHeader()
{
    header = AlyaPtr<MPIOHeader>(new MPIOHeader(communicator, fileName));
    STOP_ON_ERROR_MSG(header->open(READWRITE),Message::Error("Can not open "+header->getFileName()+" (header)"))
    STOP_ON_ERROR_MSG(header->read(),Message::Error("Problem while reading "+header->getFileName()+" (header)"))
    Message::Info(communicator->getRank(),"Initial Header");
    Message::Info(communicator->getRank(),"--------------------");
    printHeader();
    update();
    STOP_ON_ERROR_MSG(header->write(),Message::Error("Problem while writing "+header->getFileName()+" (header)"))
    STOP_ON_ERROR_MSG(header->close(),Message::Error("Can not close "+header->getFileName()+" (header)"))
    STOP_ON_ERROR_MSG(header->open(READ),Message::Error("Can not open "+header->getFileName()+" (header)"))
    STOP_ON_ERROR_MSG(header->read(),Message::Error("Problem while reading "+header->getFileName()+" (header)"))
    Message::Info(communicator->getRank(),"");
    Message::Info(communicator->getRank(),"Final Header");
    Message::Info(communicator->getRank(),"--------------------");
    printHeader();
    STOP_ON_ERROR_MSG(header->close(),Message::Error("Can not close "+header->getFileName()+" (header)"))
    return true;
}

bool alya::HeaderManager::printHeader()
{
    if (communicator->isMasterOrSequential()){
        cout << "#OBJECT:     " << header->getObject() << endl;
        cout << "#DIMENSION:  " << header->getDimension() << endl;
        cout << "#RESULTS ON: " << header->getResultsOn() << endl;
        cout << "#TYPE:       " << header->getType() << endl;
        cout << "#SIZE:       " << header->getSize() << endl;
        cout << "#PARALLEL:   " << header->getParallel() << endl;
        cout << "#FILTER:     " << header->getFilter() << endl;
        cout << "#SORTING:    " << header->getSorting() << endl;
        cout << "#ID:         " << header->getId() << endl;
        cout << "#COLUMNS:    " << header->getColumns() << endl;
        cout << "#LINES:      " << header->getLines() << endl;
        cout << "#STEP:       " << header->getIttim() << endl;
        cout << "#DOMAINS:    " << header->getNsubd() << endl;
        cout << "#DIVISION:   " << header->getDivi() << endl;
        cout << "#TAG1:       " << header->getTag1() << endl;
        cout << "#TAG2:       " << header->getTag2() << endl;
        cout << "#TIME:       " << header->getTime() << endl;
    }
    return true;
}

