/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include <mpiotypedef.h>

map <int, unsigned int> alya::MPIOTypeDef::Vtk2types;

void alya::MPIOTypeDef::Init()
{
    Vtk2types[VTK_TRIANGLE]=						ALYA_TRI03;
    Vtk2types[VTK_QUADRATIC_TRIANGLE]=				ALYA_TRI06;
    Vtk2types[VTK_QUAD]=							ALYA_QUA04;
    Vtk2types[VTK_BIQUADRATIC_QUAD]=				ALYA_QUA09;
    Vtk2types[VTK_TETRA]=							ALYA_TET04;
    Vtk2types[VTK_QUADRATIC_TETRA]=					ALYA_TET10;
    Vtk2types[VTK_PYRAMID]=							ALYA_PYR05;
    Vtk2types[VTK_WEDGE]=							ALYA_PEN06;
    Vtk2types[VTK_BIQUADRATIC_QUADRATIC_WEDGE]=		ALYA_PEN18;
    Vtk2types[VTK_HEXAHEDRON]=						ALYA_HEX08;
    Vtk2types[VTK_TRIQUADRATIC_HEXAHEDRON]=			ALYA_HEX27;
}
