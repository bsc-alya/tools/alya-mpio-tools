/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include "argumentmanager.h"

alya::ArgumentManager::ArgumentManager(int argc, char **argv, AlyaPtr_MPIOComm communicator):communicator(communicator)
{
    help=false;
    me=string(argv[0]);
    caseName="";
    vtkInput="";
    if (argc<3){
        valid=false;
        return;
    }else{
        for (int i=1; i<argc; i++){
            string str=string(argv[i]);
            if (ARGUMENT(str, "-h", "--help")){
                help=true;
                return;
            }else if (ARGUMENT(str, "-i", "--input")){
                vtkInput=string(argv[++i]);
                Message::Info(communicator->getRank(), "VTK input file: " + vtkInput, 2);
                valid=true;
            }else if (ARGUMENT(str, "-o", "--output")){
                caseName=string(argv[++i]);
                Message::Info(communicator->getRank(), "Case: " + caseName, 2);
                valid=true;
            }
        }
    }
    if (caseName.compare("")==0){
        valid=false;
        Message::Error(communicator->getRank(), "You did not specify any case!");
        return;
    }
    if (vtkInput.compare("")==0){
        valid=false;
        Message::Error(communicator->getRank(), "You did not specify any vtk input file!");
        return;
    }
}

void alya::ArgumentManager::usage()
{
    Message::Info(communicator->getRank(), "Usage");
    Message::Info(communicator->getRank(), string(me.c_str())+" -i [VTK_INPUT_FILE] -o [OUTPUT_CASE_NAME]");
    Message::Info(communicator->getRank(), "Options:");
    Message::Info(communicator->getRank(), "-h | --help                    Print help");
    Message::Info(communicator->getRank(), "-i | --input                   VTK input file");
    Message::Info(communicator->getRank(), "-o | --output                  Output case name");
}

string alya::ArgumentManager::getCaseName() const
{
    return caseName;
}

string alya::ArgumentManager::getVtkInput() const
{
    return vtkInput;
}

bool alya::ArgumentManager::getHelp() const
{
    return help;
}

bool alya::ArgumentManager::getValid() const
{
    return valid;
}
