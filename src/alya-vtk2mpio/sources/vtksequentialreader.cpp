#include "vtksequentialreader.h"

alya::VTKSequentialReader::VTKSequentialReader(AlyaPtr_MPIOComm communicator, AlyaPtr<ArgumentManager> argumentManager):communicator(communicator), argumentManager(argumentManager)
{
    input=argumentManager->getVtkInput();
    caseName=argumentManager->getCaseName();
    AlyaTypeDef::Init();
    MPIOTypeDef::Init();
}

bool alya::VTKSequentialReader::readFile()
{
    if(communicator->isMasterOrSequential()){
        Message::Info(communicator->getRank(), "Reading vtk files", 2);
        sreader = vtkSmartPointer<vtkXMLUnstructuredGridReader>::New();
        sreader->SetFileName(input.c_str());
        sreader->Update();
        grid=vtkSmartPointer<vtkUnstructuredGrid>(sreader->GetOutput());
        points=vtkSmartPointer<vtkPoints>(grid->GetPoints());
    }
    return true;
}

bool alya::VTKSequentialReader::writeCoordinates()
{
    if(communicator->isMasterOrSequential()){
        Message::Info(communicator->getRank(), "Write coordinates", 2);
        int64_t lines=points->GetNumberOfPoints();
        string fileName = caseName + COORD_SUFFIX + VTK_SUFFIX + MPIO_EXT;
        AlyaPtr<MPIOFile<double> > file(new MPIOFile<double>(communicator, fileName));
        STOP_ON_ERROR_MSG(file->setHeader(MPIOHEADER_OBJ_COORD, MPIOHEADER_RES_POINT, 3, lines), Message::Critical("File " + fileName + ": Cannot set the header"))
        STOP_ON_ERROR_MSG(file->open(WRITE), Message::Critical("File " + fileName + ": Cannot open"))
        STOP_ON_ERROR_MSG(file->initWriting(false), Message::Critical("File " + fileName + ": Cannot start writing"))
        for (int64_t i=0; i<lines; i++){
            double* point = new double[3];
            points->GetPoint(i, point);
            for (int64_t j=0; j<3; j++){
                file->write(point[j]);
            }
        }
        STOP_ON_ERROR_MSG(file->close(), Message::Critical("File " + fileName + ": Cannot close"))
    }
    return true;
}

bool alya::VTKSequentialReader::writeTypes()
{
    if(communicator->isMasterOrSequential()){
        Message::Info(communicator->getRank(), "Write types", 2);
        int64_t lines=grid->GetNumberOfCells();
        string fileName = caseName + LTYPE_SUFFIX + VTK_SUFFIX + MPIO_EXT;
        AlyaPtr<MPIOFile<int32_t> > file(new MPIOFile<int32_t>(communicator, fileName));
        STOP_ON_ERROR_MSG(file->setHeader(MPIOHEADER_OBJ_LTYPE, MPIOHEADER_RES_POINT, 1, lines), Message::Critical("File " + fileName + ": Cannot set the header"))
        STOP_ON_ERROR_MSG(file->open(WRITE), Message::Critical("File " + fileName + ": Cannot open"))
        STOP_ON_ERROR_MSG(file->initWriting(false), Message::Critical("File " + fileName + ": Cannot start writing"))
        for (int64_t i=0; i<lines; i++){
            int32_t type=static_cast<int32_t>(MPIOTypeDef::Vtk2types[grid->GetCellType(i)]);
            maxNodes=max(maxNodes, static_cast<int64_t>(AlyaTypeDef::Types2nodes[static_cast<unsigned int>(type)]));
            file->write(type);
        }
        STOP_ON_ERROR_MSG(file->close(), Message::Critical("File " + fileName + ": Cannot close"))
    }
    return true;
}

bool alya::VTKSequentialReader::writePointArrays()
{
    if(communicator->isMasterOrSequential()){
        int64_t arrayNumber = grid->GetPointData()->GetNumberOfArrays();
        string resulton = MPIOHEADER_RES_POINT;
        vtkSmartPointer<vtkDataArray> array;
        int64_t lines=points->GetNumberOfPoints();
        for (int64_t i=0; i<arrayNumber; i++){
            string name = string(grid->GetPointData()->GetArrayName(i));
            int64_t components = grid->GetPointData()->GetNumberOfComponents();
            array=vtkSmartPointer<vtkDataArray>(grid->GetPointData()->GetArray(i));
            writeArray(array, name, resulton, components, lines);
        }
    }
    return true;
}

bool alya::VTKSequentialReader::writeArray(vtkSmartPointer<vtkDataArray> array, string name, string resulton, int64_t columns, int64_t lines)
{
    if(communicator->isMasterOrSequential()){
        Message::Info(communicator->getRank(), "Write " + name, 2);
        string fileName = caseName + "-" + name + VTK_SUFFIX + MPIO_EXT;
        AlyaPtr<MPIOFile<double> > file(new MPIOFile<double>(communicator, fileName));
        STOP_ON_ERROR_MSG(file->setHeader("VTK0000", resulton, columns, lines), Message::Critical("File " + fileName + ": Cannot set the header"))
        STOP_ON_ERROR_MSG(file->open(WRITE), Message::Critical("File " + fileName + ": Cannot open"))
        STOP_ON_ERROR_MSG(file->initWriting(false), Message::Critical("File " + fileName + ": Cannot start writing"))
        for (int64_t i=0; i<lines; i++){
            double* val;
            val=array->GetTuple(i);
            for (int64_t j=0; j<columns; j++){
                file->write(val[j]);
            }
        }
        STOP_ON_ERROR_MSG(file->close(), Message::Critical("File " + fileName + ": Cannot close"))
    }
    return true;
}

//bool alya::VTKSequentialReader::writeElements()
//{
//    if(communicator->isMasterOrSequential()){
//        Message::Info(communicator->getRank(), "Write types", 2);
//        int64_t size=grid->GetNumberOfCells();
//        string fileName = caseName + LTYPE_SUFFIX + VTK_SUFFIX + MPIO_EXT;
//        AlyaPtr<MPIOFile<int32_t> > file(new MPIOFile<int32_t>(communicator, fileName));
//        STOP_ON_ERROR_MSG(file->setHeader(MPIOHEADER_OBJ_LTYPE, MPIOHEADER_RES_POINT, maxNodes, size), Message::Critical("File " + fileName + ": Cannot set the header"))
//        STOP_ON_ERROR_MSG(file->open(WRITE), Message::Critical("File " + fileName + ": Cannot open"))
//        STOP_ON_ERROR_MSG(file->initWriting(false), Message::Critical("File " + fileName + ": Cannot start writing"))
//        for (int64_t i=0; i<size; i++){
//            for (int64_t j=0; j<maxNodes, j++){
//
//            }
//            file->write(static_cast<int32_t>(MPIOTypeDef::Vtk2types[grid->GetCellType(i)]));
//        }
//        STOP_ON_ERROR_MSG(file->close(), Message::Critical("File " + fileName + ": Cannot close"))
//
//}
