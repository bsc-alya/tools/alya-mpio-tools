cmake_minimum_required(VERSION 3.1)
find_package(VTK REQUIRED)
include(boost)
include(mpi)

project(alya-vtk2mpio)

file(GLOB_RECURSE SRC_FILES ${CMAKE_CURRENT_LIST_DIR}/*.cpp)
set(HEADER_DIR ${CMAKE_CURRENT_LIST_DIR}/headers)
file(GLOB HEADER_FILES ${HEADER_DIR}/*.h)

include_directories(${HEADER_DIR} ${alya-mpio-def_INCLUDE_DIRS} ${alya-mpio-utils_INCLUDE_DIRS} ${alya-mpio_INCLUDE_DIRS} ${alya-mpio-reader_INCLUDE_DIRS} ${VTK_INCLUDE_PATH})

add_definitions(-D__BUILD_VERSION__="${ALYATOOLS_VERSION}")

add_executable(${PROJECT_NAME} ${SRC_FILES} ${HEADER_FILES})

set_mpi()
set_boost()

target_link_libraries(${PROJECT_NAME} ${VTK_LIBRARIES} alya-mpio-def alya-mpio-utils alya-mpio alya-mpio-reader)

set_target_properties(${PROJECT_NAME} PROPERTIES OUTPUT_NAME "vtk2mpio" VERSION ${ALYATOOLS_VERSION} SOVERSION ${ALYATOOLS_VERSION_MAJOR})

install(TARGETS ${PROJECT_NAME} DESTINATION bin)
