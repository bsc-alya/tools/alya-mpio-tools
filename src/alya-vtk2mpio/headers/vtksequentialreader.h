#ifndef VTKREADER_H
#define VTKREADER_H

#include <vtkVersion.h>
#include <vtkSmartPointer.h>
#include <vtkXMLUnstructuredGridReader.h>
#include <vtkUnstructuredGrid.h>
#include <vtkDoubleArray.h>
#include <vtkIntArray.h>
#include <vtkLongArray.h>
#include <vtkCellData.h>
#include <vtkPointData.h>
#include <vtkDenseArray.h>

#include "alya-vtk2mpio_config.h"
#include "mpiocomm.h"
#include "pointer.h"
#include "mpiofile.h"
#include "alyareturn.h"
#include "argumentmanager.h"
#include "mpiotypedef.h"

using namespace std;

namespace alya{

    class VTKSequentialReader
    {
    public:
        VTKSequentialReader(AlyaPtr_MPIOComm communicator, AlyaPtr<ArgumentManager> argumentManager);
        bool readFile();
        bool writeCoordinates();
        bool writeTypes();
        //bool writeElements();
        bool writePointArrays();
        bool writeArray(vtkSmartPointer<vtkDataArray> array, string name, string resulton, int64_t columns, int64_t lines);
    private:
        AlyaPtr_MPIOComm communicator;
        vtkSmartPointer<vtkXMLUnstructuredGridReader> sreader;
        vtkSmartPointer<vtkUnstructuredGrid> grid;
        vtkSmartPointer<vtkPoints> points;
        vtkSmartPointer<vtkCellArray> cells;
        vtkSmartPointer<vtkCellTypes> types;
        AlyaPtr<ArgumentManager> argumentManager;
        string input;
        string caseName;
        int64_t maxNodes;
    };

}

#endif // VTKREADER_H
