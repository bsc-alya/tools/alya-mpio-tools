/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include "postreader.h"


alya::PostReader::PostReader
(string caseName, AlyaPtr<alya::MPIOComm> communicator, bool readPartitions):PostMeshReader(caseName, communicator, readPartitions)
{
    AlyaPtr<PostFiles> postFiles(new PostFiles(caseName));
    allPosts=postFiles->getPostFileNames();
}

bool alya::PostReader::read()
{
    STOP_ON_ERROR(setPartitions())
    STOP_ON_ERROR(readMesh())
    STOP_ON_ERROR(readInv())
    STOP_ON_ERROR(readPostMesh())
    STOP_ON_ERROR(findSteps())
    STOP_ON_ERROR(readPosts())
    return true;
}

bool alya::PostReader::readMeshOnly(bool all)
{
    STOP_ON_ERROR(setPartitions())
    STOP_ON_ERROR(readMesh());
    STOP_ON_ERROR(readInv())
    if (all){
        STOP_ON_ERROR(readPostMesh())
    }
    STOP_ON_ERROR(findSteps())
    return true;
}

bool alya::PostReader::readPostsByIteration(int iteration)
{
    postInteger4.clear();
    postInteger8.clear();
    postFloat8.clear();
    return readPosts(iteration);
}

bool alya::PostReader::readPosts(vector<int> iterations)
{
    postInteger4.clear();
    postInteger8.clear();
    postFloat8.clear();
    for (auto iteration : iterations){
        STOP_ON_ERROR(readPosts(iteration))
    }
    return true;
}


bool alya::PostReader::readPosts()
{
    for (const auto &iteration : time){
        STOP_ON_ERROR(readPosts(iteration.first))
    }
    return true;
}

bool alya::PostReader::findSteps()
{
    if (communicator->isIOWorker()){
        for (string post: allPosts){
            AlyaPtr<MPIOHeader> header;
            header=AlyaPtr<MPIOHeader>(new MPIOHeader(communicator, post));
            if(!readHeader(header)){
                continue;
            }
            if (postsBySteps.count(header->getIttim())==0){
                postsBySteps[header->getIttim()]=vector<string>();
                time[header->getIttim()]=header->getTime();
            }
            (postsBySteps[header->getIttim()]).push_back(post);
        }
    }
    return true;
}

bool alya::PostReader::readPosts(int iteration)
{
    if (communicator->isIOWorker()){
        if (postsBySteps.count(iteration)==0){
            Message::Warning(communicator->getRank(), "Iteration not found. Skipping");
            return true;
        }
        for (auto fileName : postsBySteps[iteration]){
            AlyaPtr<MPIOHeader> header;
            header=AlyaPtr<MPIOHeader>(new MPIOHeader(communicator, fileName));
            STOP_ON_ERROR(readHeader(header))
            Message::Debug(communicator->getRank(), "Reading "+fileName+" header");
            if (header->getMpioType()==MPIO_TYPE_INTEGER&&
                header->getIntSize()==MPIO_TYPESIZE_4){
                AlyaPtr<MPIOFile<int32_t> >file(new MPIOFile<int32_t> (communicator, header, fileName));
                postInteger4.push_back(file);
                //STOP_ON_ERROR(readFile(file))
            }else if (header->getMpioType()==MPIO_TYPE_INTEGER&&
                header->getIntSize()==MPIO_TYPESIZE_8){
                AlyaPtr<MPIOFile<int64_t> >file(new MPIOFile<int64_t> (communicator, header, fileName));
                postInteger8.push_back(file);
                //STOP_ON_ERROR(readFile(file))
            }else if (header->getMpioType()==MPIO_TYPE_FLOAT&&
                header->getIntSize()==MPIO_TYPESIZE_8){
                AlyaPtr<MPIOFile<double> >file(new MPIOFile<double> (communicator, header, fileName));
                postFloat8.push_back(file);
                //STOP_ON_ERROR(readFile(file))
            }else{
                Message::Warning(communicator->getRank(), "Type/size of "+fileName+" not recognized. Skipping...");
            }
        }
    }
    return true;
}

map<int, double> alya::PostReader::PostReader::getTime() const
{
    return time;
}

map<int, double> alya::PostReader::getTime(vector<int> iterations, int minIteration, int maxIteration) const
{
    map<int, double> time2;
    if (iterations.size()==0&&minIteration==0&&maxIteration==0){
        return getTime();
    }else if(iterations.size()==0){
        for (auto i: time){
            if (i.first>=minIteration&&i.first<=maxIteration){
                time2[i.first]=time.at(i.first);
            }
        }
    }else{
        for (auto i: iterations){
            if (time.count(i)>0){
                time2[i]=time.at(i);
            }
        }
    }
    return time2;
}

vector<AlyaPtr<alya::MPIOFile<double> > > alya::PostReader::getPostFloat8() const
{
    return postFloat8;
}

vector<AlyaPtr<alya::MPIOFile<int32_t> > > alya::PostReader::getPostInteger4() const
{
    return postInteger4;
}

vector<AlyaPtr<alya::MPIOFile<int64_t> > > alya::PostReader::getPostInteger8() const
{
    return postInteger8;
}
