/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include "meshreader.h"


alya::MeshReader::MeshReader(string caseName, AlyaPtr_MPIOComm communicator):
    caseName(caseName), communicator(communicator)
{
        AlyaPtr<MeshFiles> meshFiles(new MeshFiles(caseName));
        coordName=meshFiles->getCoordFileName();
        ltypeName=meshFiles->getLtypeFileName();
        lnodsName=meshFiles->getLnodsFileName();
        lnodbName=meshFiles->getLnodbFileName();
        lelboName=meshFiles->getLelboFileName();
        ltypbName=meshFiles->getLtypbFileName();
        intSize=0;
        noBoundaries=false;
        lelbo=true;
        lnodb=true;
        ltypb=true;
}

bool alya::MeshReader::read()
{
    STOP_ON_ERROR(setPartitions())
    return readMesh();
}

bool alya::MeshReader::readMesh()
{
    STOP_ON_ERROR(readCoord())
    STOP_ON_ERROR(readLtype())
    STOP_ON_ERROR(readLnods())
    STOP_ON_ERROR(readLelbo())
    STOP_ON_ERROR(readLnodb())
    STOP_ON_ERROR(readLtypb())
    return true;
}

bool alya::MeshReader::readCoord()
{
    if (communicator->isIOWorker()){
        AlyaPtr<MPIOHeader> header;
        //coords
        header=AlyaPtr<MPIOHeader>(new MPIOHeader(communicator, coordName));
        STOP_ON_ERROR(readHeader(header))
        if(header->getMpioType()!=MPIO_TYPE_FLOAT){
            Message::Error("The coordinate file must contain float values");
            return false;
        }else if (header->getIntSize()!=MPIO_TYPESIZE_8){
            Message::Error("The coordinate file should contain 8 bytes values");
            return false;
        }
        coord=AlyaPtr<MPIOFile<double> >(new MPIOFile<double> (communicator, header, coordName));
        STOP_ON_ERROR(readFile(coord))
    }
    return true;
}

bool alya::MeshReader::readLtype()
{
    if (communicator->isIOWorker()){
        AlyaPtr<MPIOHeader> header;
        header=AlyaPtr<MPIOHeader>(new MPIOHeader(communicator, ltypeName));
        STOP_ON_ERROR(readHeader(header))
        if(header->getMpioType()!=MPIO_TYPE_INTEGER){
            Message::Error("The element type file must contain integer values");
            return false;
        }if (header->getIntSize()==MPIO_TYPESIZE_4){
            if (intSize==8){
                Message::Error("Mixed integers 4 and 8 bytes in the same geometry");
                return false;
            }
            intSize=4;
            ltype4=AlyaPtr<MPIOFile<int32_t> >(new MPIOFile<int32_t> (communicator, header, ltypeName));
            STOP_ON_ERROR(readFile(ltype4))
        }else if(header->getIntSize()==MPIO_TYPESIZE_8){
            if (intSize==4){
                Message::Error("Mixed integers 4 and 8 bytes in the same geometry");
                return false;
            }
            intSize=8;
            ltype8=AlyaPtr<MPIOFile<int64_t> >(new MPIOFile<int64_t> (communicator, header, ltypeName));
            STOP_ON_ERROR(readFile(ltype8))
        }else{
            Message::Error("Wrong integer size");
            return false;
        }
    }
    return true;
}

bool alya::MeshReader::readLnods()
{
    if (communicator->isIOWorker()){
        AlyaPtr<MPIOHeader> header;
        header=AlyaPtr<MPIOHeader>(new MPIOHeader(communicator, lnodsName));
        STOP_ON_ERROR(readHeader(header))
        if(header->getMpioType()!=MPIO_TYPE_INTEGER){
            Message::Error("The node file must contain integer values");
            return false;
        }if (header->getIntSize()==MPIO_TYPESIZE_4){
            if (intSize==8){
                Message::Error("Mixed integers 4 and 8 bytes in the same geometry");
                return false;
            }
            intSize=4;
            lnods4=AlyaPtr<MPIOFile<int32_t> >(new MPIOFile<int32_t> (communicator, header, lnodsName));
            STOP_ON_ERROR(readFile(lnods4))
        }else if(header->getIntSize()==MPIO_TYPESIZE_8){
            if (intSize==4){
                Message::Error("Mixed integers 4 and 8 bytes in the same geometry");
                return false;
            }
            intSize=8;
            lnods8=AlyaPtr<MPIOFile<int64_t> >(new MPIOFile<int64_t> (communicator, header, lnodsName));
            STOP_ON_ERROR(readFile(lnods8))
        }else{
            Message::Error("Wrong integer size");
            return false;
        }
    }
    return true;
}

bool alya::MeshReader::readLelbo()
{
    if (!existFile(lelboName)){
        lelbo=false;
        return true;
    }
    if (communicator->isIOWorker()){
        AlyaPtr<MPIOHeader> header;
        header=AlyaPtr<MPIOHeader>(new MPIOHeader(communicator, lelboName));
        STOP_ON_ERROR(readHeader(header))
        if(header->getMpioType()!=MPIO_TYPE_INTEGER){
            Message::Error("The element type file must contain integer values");
            return false;
        }if (header->getIntSize()==MPIO_TYPESIZE_4){
            if (intSize==8){
                Message::Error("Mixed integers 4 and 8 bytes in the same geometry");
                return false;
            }
            intSize=4;
            lelbo4=AlyaPtr<MPIOFile<int32_t> >(new MPIOFile<int32_t> (communicator, header, lelboName));
            STOP_ON_ERROR(readFile(lelbo4))
        }else if(header->getIntSize()==MPIO_TYPESIZE_8){
            if (intSize==4){
                Message::Error("Mixed integers 4 and 8 bytes in the same geometry");
                return false;
            }
            intSize=8;
            lelbo8=AlyaPtr<MPIOFile<int64_t> >(new MPIOFile<int64_t> (communicator, header, lelboName));
            STOP_ON_ERROR(readFile(lelbo8))
        }else{
            Message::Error("Wrong integer size");
            return false;
        }
    }
    return true;
}

bool alya::MeshReader::readLnodb()
{
    if (!existFile(lnodbName)){
        noBoundaries=true;
        lnodb=false;
        return true;
    }
    if (communicator->isIOWorker()){
        AlyaPtr<MPIOHeader> header;
        header=AlyaPtr<MPIOHeader>(new MPIOHeader(communicator, lnodbName));
        STOP_ON_ERROR(readHeader(header))
        if(header->getMpioType()!=MPIO_TYPE_INTEGER){
            Message::Error("The node file must contain integer values");
            return false;
        }if (header->getIntSize()==MPIO_TYPESIZE_4){
            if (intSize==8){
                Message::Error("Mixed integers 4 and 8 bytes in the same geometry");
                return false;
            }
            intSize=4;
            lnodb4=AlyaPtr<MPIOFile<int32_t> >(new MPIOFile<int32_t> (communicator, header, lnodbName));
            STOP_ON_ERROR(readFile(lnodb4))
        }else if(header->getIntSize()==MPIO_TYPESIZE_8){
            if (intSize==4){
                Message::Error("Mixed integers 4 and 8 bytes in the same geometry");
                return false;
            }
            intSize=8;
            lnodb8=AlyaPtr<MPIOFile<int64_t> >(new MPIOFile<int64_t> (communicator, header, lnodbName));
            STOP_ON_ERROR(readFile(lnodb8))
        }else{
            Message::Error("Wrong integer size");
            return false;
        }
    }
    return true;
}

bool alya::MeshReader::readLtypb()
{
    if (!existFile(ltypbName)){
        ltypb=false;
        return true;
    }
    if (communicator->isIOWorker()){
        AlyaPtr<MPIOHeader> header;
        header=AlyaPtr<MPIOHeader>(new MPIOHeader(communicator, ltypbName));
        STOP_ON_ERROR(readHeader(header))
        if(header->getMpioType()!=MPIO_TYPE_INTEGER){
            Message::Error("The element type file must contain integer values");
            return false;
        }if (header->getIntSize()==MPIO_TYPESIZE_4){
            if (intSize==8){
                Message::Error("Mixed integers 4 and 8 bytes in the same geometry");
                return false;
            }
            intSize=4;
            ltypb4=AlyaPtr<MPIOFile<int32_t> >(new MPIOFile<int32_t> (communicator, header, ltypbName));
            STOP_ON_ERROR(readFile(ltypb4))
        }else if(header->getIntSize()==MPIO_TYPESIZE_8){
            if (intSize==4){
                Message::Error("Mixed integers 4 and 8 bytes in the same geometry");
                return false;
            }
            intSize=8;
            ltypb8=AlyaPtr<MPIOFile<int64_t> >(new MPIOFile<int64_t> (communicator, header, ltypbName));
            STOP_ON_ERROR(readFile(ltypb8))
        }else{
            Message::Error("Wrong integer size");
            return false;
        }
    }
    return true;
}

bool alya::MeshReader::setPartitions()
{
    partition=AlyaPtr<MPIOPartition>(new MPIOPartition(communicator));
    STOP_ON_ERROR(partition->computePartition())
    communicator=partition->getCommunicator();
    return true;
}

bool alya::MeshReader::readHeader(AlyaPtr<alya::MPIOHeader> header)
{
    STOP_ON_ERROR_MSG(header->open(READ),Message::Error("Can not open "+header->getFileName()+" (header)"))
    STOP_ON_ERROR_MSG(header->read(),Message::Error("Can not read "+header->getFileName()+" (header)"))
    STOP_ON_ERROR_MSG(header->close(),Message::Error("Can not close "+header->getFileName()+" (header)"))
    return true;
}

template<typename T>
bool alya::MeshReader::readFile(AlyaPtr<alya::MPIOFile<T> > file)
{
    Message::Debug(communicator->getRank(), "Reading "+file->getFileName()+" file");
    STOP_ON_ERROR_MSG(file->open(READ),Message::Critical(communicator->getRank(), "Can not open "+file->getFileName()+" file!"))
    file->setPartition(partition);
    STOP_ON_ERROR_MSG(file->read(),Message::Critical(communicator->getRank(), "Can not read "+file->getFileName()+" file!"))
    STOP_ON_ERROR_MSG(file->close(), Message::Critical(communicator->getRank(), "Can not close "+file->getFileName()+" file!"))
    return true;
}

bool alya::MeshReader::existFile(string name)
{
    struct stat buffer;
    return (stat (name.c_str(), &buffer) == 0);
}

bool alya::MeshReader::getLtypb() const
{
    return ltypb;
}

bool alya::MeshReader::getNoBoundaries() const
{
    return noBoundaries;
}

AlyaPtr<alya::MPIOFile<int64_t> > alya::MeshReader::getLnodb8() const
{
    return lnodb8;
}

AlyaPtr<alya::MPIOFile<int64_t> > alya::MeshReader::getLelbo8() const
{
    return lelbo8;
}

AlyaPtr<alya::MPIOFile<int32_t> > alya::MeshReader::getLnodb4() const
{
    return lnodb4;
}

AlyaPtr<alya::MPIOFile<int32_t> > alya::MeshReader::getLelbo4() const
{
    return lelbo4;
}

bool alya::MeshReader::isLnodb() const
{
    return lnodb;
}

bool alya::MeshReader::isLelbo() const
{
    return lelbo;
}

int alya::MeshReader::MeshReader::getIntSize() const
{
    return intSize;
}

AlyaPtr_MPIOComm alya::MeshReader::getCommunicator() const
{
    return communicator;
}

AlyaPtr<alya::MPIOFile<double> > alya::MeshReader::getCoord() const
{
    return coord;
}

AlyaPtr<alya::MPIOFile<int32_t> > alya::MeshReader::getLnods4() const
{
    return lnods4;
}

AlyaPtr<alya::MPIOFile<int32_t> > alya::MeshReader::getLtype4() const
{
    return ltype4;
}

AlyaPtr<alya::MPIOFile<int64_t> > alya::MeshReader::getLnods8() const
{
    return lnods8;
}

AlyaPtr<alya::MPIOFile<int32_t> > alya::MeshReader::getLtypb4() const
{
    return ltypb4;
}

AlyaPtr<alya::MPIOFile<int64_t> > alya::MeshReader::getLtypb8() const
{
    return ltypb8;
}

AlyaPtr<alya::MPIOFile<int64_t> > alya::MeshReader::getLtype8() const
{
    return ltype8;
}

template bool alya::MeshReader::readFile(AlyaPtr<alya::MPIOFile<double> > file);

template bool alya::MeshReader::readFile(AlyaPtr<alya::MPIOFile<int32_t> > file);

template bool alya::MeshReader::readFile(AlyaPtr<alya::MPIOFile<int64_t> > file);
