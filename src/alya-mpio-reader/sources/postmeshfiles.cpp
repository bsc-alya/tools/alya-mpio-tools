/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include "postmeshfiles.h"

alya::PostMeshFiles::PostMeshFiles(string caseName):MeshFiles()
{
    this->caseName=caseName;
    coordFileName=caseName+COORD_SUFFIX+POST_SUFFIX+MPIO_EXT;
    ltypeFileName=caseName+LTYPE_SUFFIX+POST_SUFFIX+MPIO_EXT;
    lnodsFileName=caseName+LNODS_SUFFIX+POST_SUFFIX+MPIO_EXT;
    lnodbFileName=caseName+LNODB_SUFFIX+POST_SUFFIX+MPIO_EXT;
    ltypbFileName=caseName+LTYPB_SUFFIX+POST_SUFFIX+MPIO_EXT;
    leinvFileName=caseName+LEINV_SUFFIX+POST_SUFFIX+MPIO_EXT;
    lninvFileName=caseName+LNINV_SUFFIX+POST_SUFFIX+MPIO_EXT;
    lbinvFileName=caseName+LBINV_SUFFIX+POST_SUFFIX+MPIO_EXT;
    lelchFileName=caseName+LELCH_SUFFIX+POST_SUFFIX+MPIO_EXT;
    lnochFileName=caseName+LNOCH_SUFFIX+POST_SUFFIX+MPIO_EXT;
    lbochFileName=caseName+LBOCH_SUFFIX+POST_SUFFIX+MPIO_EXT;
    lesetFileName=caseName+LESET_SUFFIX+POST_SUFFIX+MPIO_EXT;
    lbsetFileName=caseName+LBSET_SUFFIX+POST_SUFFIX+MPIO_EXT;
    lnsetFileName=caseName+LNSET_SUFFIX+POST_SUFFIX+MPIO_EXT;
    codboFileName=caseName+CODBO_SUFFIX+POST_SUFFIX+MPIO_EXT;
    codnoFileName=caseName+CODNO_SUFFIX+POST_SUFFIX+MPIO_EXT;
    lesubFileName=caseName+LESUB_SUFFIX+POST_SUFFIX+MPIO_EXT;
    lmateFileName=caseName+LMATE_SUFFIX+POST_SUFFIX+MPIO_EXT;
    lmastFileName=caseName+LMAST_SUFFIX+POST_SUFFIX+MPIO_EXT;

    DIR *dir = opendir(".");
    dirent *entry;
    while((entry = readdir(dir))!=nullptr)
    {
        if(AlyaString::HasSuffix(entry->d_name, string(POST_SUFFIX)+string(MPIO_EXT)))
        {
            string str=string(entry->d_name);
            if (isAddPostMeshFile(str)){
                postMeshFileNames.push_back(str);
            }
        }
    }
    closedir(dir);
    sort(postMeshFileNames.begin(), postMeshFileNames.end());
}

string alya::PostMeshFiles::getLeinvFileName() const
{
    return leinvFileName;
}

string alya::PostMeshFiles::getLninvFileName() const
{
    return lninvFileName;
}

string alya::PostMeshFiles::getLbinvFileName() const
{
    return lbinvFileName;
}

bool alya::PostMeshFiles::isGeoPostMeshFile(string s)
{
    return (s.compare(coordFileName)==0 ||
            s.compare(lnodsFileName)==0 ||
            s.compare(lnodbFileName)==0 ||
            s.compare(ltypeFileName)==0 ||
            s.compare(lelboFileName)==0 ||
            s.compare(ltypbFileName)==0);
}

bool alya::PostMeshFiles::isInvPostMeshFile(string s)
{
    return (s.compare(leinvFileName)==0 ||
            s.compare(lninvFileName)==0 ||
            s.compare(lbinvFileName)==0);
}

bool alya::PostMeshFiles::isAddPostMeshFile(string s)
{
    return (s.compare(lmateFileName)==0 ||
            s.compare(lmastFileName)==0 ||
            s.compare(lesubFileName)==0 ||
            s.compare(lelchFileName)==0 ||
            s.compare(lnochFileName)==0 ||
            s.compare(lbochFileName)==0 ||
            s.compare(lesetFileName)==0 ||
            s.compare(lbsetFileName)==0 ||
            s.compare(lnsetFileName)==0 ||
            s.compare(codboFileName)==0 ||
            s.compare(codnoFileName)==0);
}

bool alya::PostMeshFiles::isPostMeshFile(string s)
{
    return isGeoPostMeshFile(s)||isInvPostMeshFile(s)||isAddPostMeshFile(s);
}

vector<string> alya::PostMeshFiles::getPostMeshFileNames() const
{
    return postMeshFileNames;
}
