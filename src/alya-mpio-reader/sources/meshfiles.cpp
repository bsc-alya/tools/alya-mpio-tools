/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include "meshfiles.h"


alya::MeshFiles::MeshFiles()
{

}

alya::MeshFiles::MeshFiles(string caseName): caseName(caseName)
{
    coordFileName=caseName+COORD_SUFFIX+MPIO_EXT;
    ltypeFileName=caseName+LTYPE_SUFFIX+MPIO_EXT;
    lnodsFileName=caseName+LNODS_SUFFIX+MPIO_EXT;
    lnodbFileName=caseName+LNODB_SUFFIX+MPIO_EXT;
    lelboFileName=caseName+LELBO_SUFFIX+MPIO_EXT;
    ltypbFileName=caseName+LTYPB_SUFFIX+MPIO_EXT;
}

string alya::MeshFiles::getLtypeFileName() const
{
    return ltypeFileName;
}

string alya::MeshFiles::getLtypbFileName() const
{
    return ltypbFileName;
}

string alya::MeshFiles::getLelboFileName() const
{
    return lelboFileName;
}

string alya::MeshFiles::getLnodsFileName() const
{
    return lnodsFileName;
}

string alya::MeshFiles::getLnodbFileName() const
{
    return lnodbFileName;
}

string alya::MeshFiles::getCoordFileName() const
{
    return coordFileName;
}
