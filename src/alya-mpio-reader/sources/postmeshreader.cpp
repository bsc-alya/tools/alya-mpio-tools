/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include "postmeshreader.h"

alya::PostMeshReader::PostMeshReader(string caseName, AlyaPtr<alya::MPIOComm> communicator, bool readPartitions):
MeshReader(caseName,communicator), readPartitions(readPartitions)
{
    meshFiles= AlyaPtr<PostMeshFiles>(new PostMeshFiles(caseName));
    coordName=meshFiles->getCoordFileName();
    ltypeName=meshFiles->getLtypeFileName();
    lnodsName=meshFiles->getLnodsFileName();
    lnodbName=meshFiles->getLnodbFileName();
    ltypbName=meshFiles->getLtypbFileName();
    lelboName=meshFiles->getLelboFileName();
    lninvName=meshFiles->getLninvFileName();
    leinvName=meshFiles->getLeinvFileName();
    lbinvName=meshFiles->getLbinvFileName();
    lbinv=true;
}

bool alya::PostMeshReader::read(bool all)
{
    STOP_ON_ERROR(setPartitions())
    STOP_ON_ERROR(readMesh())
    STOP_ON_ERROR(readInv())
    if (all){
        STOP_ON_ERROR(readPostMesh())
    }
    return true;
}

bool alya::PostMeshReader::readPostMesh()
{
    if (communicator->isIOWorker()){
        for (auto fileName : meshFiles->getPostMeshFileNames()){
            AlyaPtr<MPIOHeader> header;
            header=AlyaPtr<MPIOHeader>(new MPIOHeader(communicator, fileName));
            STOP_ON_ERROR(readHeader(header))
            Message::Debug(communicator->getRank(), "Reading "+fileName+" header");
            if (header->getMpioType()==MPIO_TYPE_INTEGER&&
                header->getIntSize()==MPIO_TYPESIZE_4){
                AlyaPtr<MPIOFile<int32_t> >file(new MPIOFile<int32_t> (communicator, header, fileName));
                postMeshInteger4.push_back(file);
                //STOP_ON_ERROR(readFile(file))
            }else if (header->getMpioType()==MPIO_TYPE_INTEGER&&
                header->getIntSize()==MPIO_TYPESIZE_8){
                AlyaPtr<MPIOFile<int64_t> >file(new MPIOFile<int64_t> (communicator, header, fileName));
                postMeshInteger8.push_back(file);
                //STOP_ON_ERROR(readFile(file))
            }else if (header->getMpioType()==MPIO_TYPE_FLOAT&&
                header->getIntSize()==MPIO_TYPESIZE_8){
                AlyaPtr<MPIOFile<double> >file(new MPIOFile<double> (communicator, header, fileName));
                postMeshFloat8.push_back(file);
                //STOP_ON_ERROR(readFile(file))
            }else{
                Message::Warning(communicator->getRank(), "Type/size of "+fileName+" not recognized. Skipping...");
            }
        }
    }
    return true;
}

bool alya::PostMeshReader::readInv()
{
    STOP_ON_ERROR(readLninv())
    STOP_ON_ERROR(readLeinv())
    STOP_ON_ERROR(readLbinv())
            return true;
}

bool alya::PostMeshReader::readLninv()
{
    string name=lninvName;
    AlyaPtr<MPIOHeader> header=AlyaPtr<MPIOHeader>(new MPIOHeader(communicator, name));
    STOP_ON_ERROR(read(header))
    lninv4=AlyaPtr<MPIOFile<int32_t> >(new MPIOFile<int32_t> (communicator, header, name));
    lninv8=AlyaPtr<MPIOFile<int64_t> >(new MPIOFile<int64_t> (communicator, header, name));
    return read(header, lninv4, lninv8);
}

bool alya::PostMeshReader::readLeinv()
{
    string name=leinvName;
    AlyaPtr<MPIOHeader> header=AlyaPtr<MPIOHeader>(new MPIOHeader(communicator, name));
    STOP_ON_ERROR(read(header))
    leinv4=AlyaPtr<MPIOFile<int32_t> >(new MPIOFile<int32_t> (communicator, header, name));
    leinv8=AlyaPtr<MPIOFile<int64_t> >(new MPIOFile<int64_t> (communicator, header, name));
    return read(header, leinv4, leinv8);
}

bool alya::PostMeshReader::readLbinv()
{
    string name=lbinvName;
    if (!existFile(name)){
        lbinv=false;
        noBoundaries=true;
        return true;
    }
    AlyaPtr<MPIOHeader> header=AlyaPtr<MPIOHeader>(new MPIOHeader(communicator, name));
    STOP_ON_ERROR(read(header))
    lbinv4=AlyaPtr<MPIOFile<int32_t> >(new MPIOFile<int32_t> (communicator, header, name));
    lbinv8=AlyaPtr<MPIOFile<int64_t> >(new MPIOFile<int64_t> (communicator, header, name));
    return read(header, lbinv4, lbinv8);
}



std::shared_ptr<alya::MPIOFile<int32_t> > alya::PostMeshReader::getLeinv4() const
{
    return leinv4;
}

std::shared_ptr<alya::MPIOFile<int32_t> > alya::PostMeshReader::getLbinv4() const
{
    return lbinv4;
}

std::shared_ptr<alya::MPIOFile<int32_t> > alya::PostMeshReader::getLninv4() const
{
    return lninv4;
}

std::shared_ptr<alya::MPIOFile<int64_t> > alya::PostMeshReader::getLeinv8() const
{
    return leinv8;
}

std::shared_ptr<alya::MPIOFile<int64_t> > alya::PostMeshReader::getLbinv8() const
{
    return lbinv8;
}

std::shared_ptr<alya::MPIOFile<int64_t> > alya::PostMeshReader::getLninv8() const
{
    return lninv8;
}

bool alya::PostMeshReader::read(std::shared_ptr<alya::MPIOHeader> header)
{
    if (communicator->isIOWorker()){
        STOP_ON_ERROR(readHeader(header))
    }
    return true;
}

bool alya::PostMeshReader::read(std::shared_ptr<alya::MPIOHeader> header, std::shared_ptr<alya::MPIOFile<int32_t> > file4, std::shared_ptr<alya::MPIOFile<int64_t> > file8)
{
    if (communicator->isIOWorker()){
        if(header->getMpioType()!=MPIO_TYPE_INTEGER){
            Message::Error("File must contain integer values");
            return false;
        }if (header->getIntSize()==MPIO_TYPESIZE_4){
            if (intSize==8){
                Message::Error("Mixed integers 4 and 8 bytes in the same geometry");
                return false;
            }
            intSize=4;
            STOP_ON_ERROR(readFile(file4))
        }else if(header->getIntSize()==MPIO_TYPESIZE_8){
            if (intSize==4){
                Message::Error("Mixed integers 4 and 8 bytes in the same geometry");
                return false;
            }
            intSize=8;
            STOP_ON_ERROR(readFile(file8))
        }else{
            Message::Error("Wrong integer size");
            return false;
        }
    }
    return true;
}


bool alya::PostMeshReader::setPartitions(){

    string partitionFile=caseName+PART_EXT;
    partition=AlyaPtr<MPIOPartition>(new MPIOPartition(communicator, partitionFile));
    STOP_ON_ERROR(partition->computePartition())
    communicator=partition->getCommunicator();
    return true;
}

vector<std::shared_ptr<alya::MPIOFile<int32_t> > > alya::PostMeshReader::getPostMeshInteger4() const
{
    return postMeshInteger4;
}

vector<std::shared_ptr<alya::MPIOFile<int64_t> > > alya::PostMeshReader::getPostMeshInteger8() const
{
    return postMeshInteger8;
}

vector<std::shared_ptr<alya::MPIOFile<double> > > alya::PostMeshReader::getPostMeshFloat8() const
{
    return postMeshFloat8;
}

bool alya::PostMeshReader::isLbinv() const
{
    return lbinv;
}
