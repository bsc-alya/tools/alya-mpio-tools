/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include "postfiles.h"

alya::PostFiles::PostFiles(string caseName): PostMeshFiles(caseName)
{
    DIR *dir = opendir(".");
    dirent *entry;
    while((entry = readdir(dir)) != nullptr)
    {
        if(AlyaString::HasSuffix(entry->d_name, string(POST_SUFFIX)+string(MPIO_EXT)))
        {
            string str=string(entry->d_name);
            if (str.find(caseName) != std::string::npos){
                if (!isPostMeshFile(str)){
                    postFileNames.push_back(str);
                }
            }
        }
    }
    closedir(dir);
    sort(postFileNames.begin(), postFileNames.end());
}

vector<string> alya::PostFiles::getPostFileNames() const
{
    return postFileNames;
}


