/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#ifndef MPIOREADERDEF_H
#define MPIOREADERDEF_H

#ifndef __STDC_IEC_559__
#warning "Requires IEEE 754 floating point!"
#endif

#ifndef __BUILD_VERSION__
#define __BUILD_VERSION__ "Unknown"
#endif

#include <mpioheaderdef.h>

#define MPIO_EXT                    ".mpio.bin"

#define POST_SUFFIX                 ".post"

#define PART_EXT                    POST_SUFFIX+".alyapar"

#define RST_SUFFIX                  ".rst"

#define COORD_SUFFIX                "-COORD"
#define LTYPE_SUFFIX                "-LTYPE"
#define LNODS_SUFFIX                "-LNODS"
#define LNODB_SUFFIX                "-LNODB"
#define LTYPB_SUFFIX                "-LTYPB"
#define LELBO_SUFFIX                "-LELBO"

#define LESUB_SUFFIX                "-LESUB"
#define LMATE_SUFFIX                "-LMATE"
#define LMAST_SUFFIX                "-LMAST"

#define LNINV_SUFFIX                "-LNINV"
#define LEINV_SUFFIX                "-LEINV"
#define LBINV_SUFFIX                "-LBINV"

#define LELCH_SUFFIX                "-LELCH"
#define LNOCH_SUFFIX                "-LNOCH"
#define LBOCH_SUFFIX                "-LBOCH"

#define LESET_SUFFIX                "-LESET"
#define LBSET_SUFFIX                "-LBSET"
#define LNSET_SUFFIX                "-LNSET"

#define CODBO_SUFFIX                "-CODBO"
#define CODNO_SUFFIX                "-CODNO"

#define XFIEL_SUFFIX                "-XFIEL"

#define MPIRA_SUFFIX                "-MPIRA"

#define PARTITION_DIM               4
#define PARTITION_ELEME             0
#define PARTITION_POINT             1
#define PARTITION_BOUND             2
#define PARTITION_WHATE             3





#endif // MPIOREADERDEF_H
