/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#ifndef POSTMESHREADER_H
#define POSTMESHREADER_H

#include <meshreader.h>
#include <postmeshfiles.h>
#include <pointer.h>
#include <mpiopartition.h>

#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/trim_all.hpp>
#include <boost/tokenizer.hpp>

using namespace std;

namespace alya {

    class PostMeshReader : public MeshReader
    {
    public:
        PostMeshReader(string caseName, AlyaPtr_MPIOComm communicator, bool readPartitions=false);
        bool read(bool all=true);
        bool readPostMesh();
        AlyaPtr<MPIOFile<int32_t> > getLninv4() const;
        AlyaPtr<MPIOFile<int32_t> > getLeinv4() const;
        AlyaPtr<MPIOFile<int32_t> > getLbinv4() const;
        AlyaPtr<MPIOFile<int64_t> > getLninv8() const;
        AlyaPtr<MPIOFile<int64_t> > getLeinv8() const;
        AlyaPtr<MPIOFile<int64_t> > getLbinv8() const;

        bool isLbinv() const;
        bool readInv();
        bool readLninv();
        bool readLeinv();
        bool readLbinv();
        bool setPartitions();

        vector<AlyaPtr<MPIOFile<int32_t> > > getPostMeshInteger4() const;
        vector<AlyaPtr<MPIOFile<int64_t> > > getPostMeshInteger8() const;
        vector<AlyaPtr<MPIOFile<double> > > getPostMeshFloat8() const;

    protected:
        bool read(AlyaPtr<MPIOHeader>);
        bool read(AlyaPtr<MPIOHeader>, AlyaPtr<MPIOFile<int32_t> > file4, AlyaPtr<MPIOFile<int64_t> > file8);

        AlyaPtr<PostMeshFiles> meshFiles;

        vector<AlyaPtr<MPIOFile<int32_t> > > postMeshInteger4;
        vector<AlyaPtr<MPIOFile<int64_t> > > postMeshInteger8;
        vector<AlyaPtr<MPIOFile<double> > > postMeshFloat8;
        AlyaPtr<MPIOFile<int32_t> > lninv4;
        AlyaPtr<MPIOFile<int32_t> > leinv4;
        AlyaPtr<MPIOFile<int32_t> > lbinv4;
        AlyaPtr<MPIOFile<int64_t> > lninv8;
        AlyaPtr<MPIOFile<int64_t> > leinv8;
        AlyaPtr<MPIOFile<int64_t> > lbinv8;
        string lninvName;
        string leinvName;
        string lbinvName;
        bool lbinv;
        bool readPartitions;
        AlyaPtr_ifstream partitionStream;
        string line;
        int subdomains;


    };
}

#endif // POSTMESHREADER_H
