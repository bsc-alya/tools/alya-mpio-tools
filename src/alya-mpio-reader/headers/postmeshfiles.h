/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#ifndef POSTMESHFILES_H
#define POSTMESHFILES_H

#include <meshfiles.h>
#include <vector>
#include <mpioreaderdef.h>
#include <alyastring.h>
#include <pointer.h>
#include <dirent.h>
#include <algorithm>

namespace alya{

    class PostMeshFiles : public MeshFiles
    {
    public:
        PostMeshFiles(string caseName);

        string getLeinvFileName() const;
        string getLninvFileName() const;
        string getLbinvFileName() const;

        bool isGeoPostMeshFile(string s);
        bool isInvPostMeshFile(string s);
        bool isAddPostMeshFile(string s);
        bool isPostMeshFile(string s);

        vector<string> getPostMeshFileNames() const;

    protected:
        string leinvFileName;
        string lninvFileName;
        string lbinvFileName;
        string lelchFileName;
        string lnochFileName;
        string lbochFileName;
        string lesetFileName;
        string lbsetFileName;
        string lnsetFileName;
        string lmateFileName;
        string lmastFileName;
        string lesubFileName;
        string codboFileName;
        string codnoFileName;


        vector<string> postMeshFileNames;
    };

}

#endif // POSTMESHFILES_H
