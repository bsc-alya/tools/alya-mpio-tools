/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#ifndef POSTREADER_H
#define POSTREADER_H

#include <postmeshreader.h>
#include <vector>
#include <map>
#include <postfiles.h>

using namespace std;

namespace alya{

    class PostReader : public PostMeshReader
    {
    public:
        PostReader(string caseName, AlyaPtr_MPIOComm communicator, bool readPartitions=false);
        bool read();
        bool readMeshOnly(bool all=true);
        bool readPostsByIteration(int iteration);
        bool readPosts(vector<int> iterations);
        bool readPosts();
        bool findSteps();
        vector<AlyaPtr<MPIOFile<int32_t> > > getPostInteger4() const;
        vector<AlyaPtr<MPIOFile<int64_t> > > getPostInteger8() const;
        vector<AlyaPtr<MPIOFile<double> > > getPostFloat8() const;

        map<int, double> getTime() const;
        map<int, double> getTime(vector<int> iterations, int minIteration=0, int maxIteration=0) const;

    protected:
        bool readPosts(int iteration);
        vector<AlyaPtr<MPIOFile<int32_t> > > postInteger4;
        vector<AlyaPtr<MPIOFile<int64_t> > > postInteger8;
        vector<AlyaPtr<MPIOFile<double> > > postFloat8;
        map<int, vector<string> > postsBySteps;
        map<int, double> time;
        vector<string> allPosts;
    };

}

#endif // POSTREADER_H
