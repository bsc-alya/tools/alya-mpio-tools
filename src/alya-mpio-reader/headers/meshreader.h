/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#ifndef MESHREADER_H
#define MESHREADER_H

#include <mpioheaderdef.h>
#include <mpioreaderdef.h>
#include <meshfiles.h>
#include <mpiocomm.h>
#include <mpioheader.h>
#include <mpiofile.h>
#include <pointer.h>
#include <message.h>
#include <alyastring.h>
#include <alyareturn.h>
#include <sys/stat.h>

namespace alya{

    class MeshReader
    {
    public:
        MeshReader(string caseName, AlyaPtr_MPIOComm communicator);
        bool read();
        AlyaPtr<MPIOFile<double> > getCoord() const;
        AlyaPtr<MPIOFile<int32_t> > getLtype4() const;
        AlyaPtr<MPIOFile<int32_t> > getLnods4() const;
        AlyaPtr<MPIOFile<int64_t> > getLtype8() const;
        AlyaPtr<MPIOFile<int64_t> > getLnods8() const;
        AlyaPtr<MPIOFile<int32_t> > getLtypb4() const;
        AlyaPtr<MPIOFile<int32_t> > getLelbo4() const;
        AlyaPtr<MPIOFile<int32_t> > getLnodb4() const;
        AlyaPtr<MPIOFile<int64_t> > getLelbo8() const;
        AlyaPtr<MPIOFile<int64_t> > getLnodb8() const;
        AlyaPtr<MPIOFile<int64_t> > getLtypb8() const;
        AlyaPtr_MPIOComm getCommunicator() const;
        int getIntSize() const;
        bool isLelbo() const;
        bool isLnodb() const;
        bool getNoBoundaries() const;
        bool readMesh();
        bool readCoord();
        bool readLtype();
        bool readLnods();
        bool readLelbo();
        bool readLnodb();
        bool readLtypb();
        bool setPartitions();

        template<typename T>
        bool readFile(AlyaPtr<MPIOFile<T> > file);

        bool getLtypb() const;

    protected:
        bool readHeader(AlyaPtr<MPIOHeader> header);
        bool existFile(string name);
        string caseName;
        AlyaPtr_MPIOComm communicator;
        string coordName;
        string ltypeName;
        string ltypbName;
        string lnodsName;
        string lelboName;
        string lnodbName;
        AlyaPtr<MPIOFile<double> > coord;
        AlyaPtr<MPIOFile<int32_t> > ltype4;
        AlyaPtr<MPIOFile<int32_t> > lnods4;
        AlyaPtr<MPIOFile<int32_t> > lelbo4;
        AlyaPtr<MPIOFile<int32_t> > lnodb4;
        AlyaPtr<MPIOFile<int32_t> > ltypb4;
        AlyaPtr<MPIOFile<int64_t> > ltype8;
        AlyaPtr<MPIOFile<int64_t> > lnods8;
        AlyaPtr<MPIOFile<int64_t> > lelbo8;
        AlyaPtr<MPIOFile<int64_t> > lnodb8;
        AlyaPtr<MPIOFile<int64_t> > ltypb8;
        int intSize;
        AlyaPtr<MPIOPartition> partition;
        bool lelbo;
        bool lnodb;
        bool ltypb;
        bool noBoundaries;
    };

}

#endif // MESHCONVERTER_H
