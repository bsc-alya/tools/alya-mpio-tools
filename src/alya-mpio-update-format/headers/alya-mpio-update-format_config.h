/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#ifndef ALYAMPIOUPDATEFORMAT_CONFIG_H
#define ALYAMPIOUPDATEFORMAT_CONFIG_H

#ifndef __BUILD_VERSION__
#define __BUILD_VERSION__ "Unknown"
#endif

#include <mpioheaderdef.h>

#define MPIO_EXT       ".mpio.bin"
#define MAGIC           MPIOHEADER_MAGICNUMBER
#define FORMAT          MPIOHEADER_FORMAT
#define VERSION3        "V000300"
#define VERSION4        MPIOHEADER_VERSION
#define NADA            0
#define HEADER3_SIZE    232
#define HEADER4_SIZE    MPIOHEADER_HEADERSIZE_BYTES
#define HEADER3_PART2   ((10*8)+8+8) //options+align+time
#define HEADER3_PART1   (HEADER3_SIZE-HEADER3_PART2)
#define VERSION_OFFSET  (8+8)

#define RETURN_OK       0
#define RETURN_ERR      1

#define BUFFERDATA      1000000000

#endif // ALYAMPIOUPDATEFORMAT
