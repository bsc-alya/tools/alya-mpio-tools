/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include "update.h"


alya::Update::Update(vector<string> files)
{
    Message::Info(2);
    for (string file: files){
        Message::Info(-1);
        Message::Info("Opening "+file);
        Message::Info();
        magic=0;
        format="0";
        version="0";
        open(file);
        readHead();
        if (magic!=MAGIC){
            Message::Info("Invalid magic number: "+to_string(magic));
            //continue;
        }
        if (format.compare(FORMAT)!=0){
            Message::Info("Invalid file format: "+format);
            continue;
        }
        if (version.compare(VERSION3)==0){
            Message::Info("Format version: "+string(VERSION3));
            Message::Info("Updating to version: "+string(VERSION4));
            update3to4(file);
            close();
            std::rename(string(file+".tmp").c_str(), file.c_str());
            open(file);
            readHead();
            if (version.compare(VERSION4)==0){
                Message::Info("File updated...");
            }else{
                Message::Error("Update has failed");
            }
            continue;
        }
        if (version.compare(VERSION4)==0){
            Message::Info("File already up-to-date...");
        }
        close();
    }
    Message::Info(-2);
}

bool alya::Update::readHead()
{
    ifileSTD->seekg(0);
    magic=readInteger8STD();
    format=readStringSTD();
    version=readStringSTD();
    return true;
}

bool alya::Update::open(string file)
{
    ifileSTD=AlyaPtr_ifstream(new ifstream(file.c_str(), std::ios::binary));
    fileSizeSTD=AlyaFile::Size(file);
    if (ifileSTD->is_open()){
        return true;
    }else{
        return false;
    }
}

bool alya::Update::close()
{
    if (ifileSTD->is_open()) ifileSTD->close();
    return true;
}

bool alya::Update::update3to4(string file)
{
    int64_t totalDataToRead=fileSizeSTD-HEADER3_SIZE;
    char* header3_part1=new char[HEADER3_PART1];
    char* header3_part2=new char[HEADER3_PART2];
    char new_version[MPIOHEADER_STRING_SIZE]=VERSION4;
    int64_t nul=NADA;
    ifileSTD->seekg(0);
    ifileSTD->read(header3_part1, HEADER3_PART1);
    ifileSTD->read(header3_part2, HEADER3_PART2);
    ofileSTD=AlyaPtr_ofstream(new ofstream(string(file+".tmp").c_str(), std::ios::binary));
    if (!ofileSTD->is_open()){
        return false;
    }
    ofileSTD->seekp(0);
    ofileSTD->write(header3_part1, HEADER3_PART1);;
    ofileSTD->write(reinterpret_cast<CHAR>(&nul), sizeof(nul));//divi
    ofileSTD->write(reinterpret_cast<CHAR>(&nul), sizeof(nul));//tag1
    ofileSTD->write(reinterpret_cast<CHAR>(&nul), sizeof(nul));//tag2
    ofileSTD->write(header3_part2, HEADER3_PART2);
    while(totalDataToRead>0){
        int64_t subDataToRead=min(totalDataToRead, static_cast<int64_t>(BUFFERDATA));
        totalDataToRead-=subDataToRead;
        char* data3=        new char[subDataToRead];
        ifileSTD->read(data3, subDataToRead);
        ofileSTD->write(data3, subDataToRead);
        delete[] data3;
    }
    ofileSTD->seekp(VERSION_OFFSET, ios_base::beg);
    ofileSTD->write(const_cast<CHAR>(new_version), MPIOHEADER_STRING_SIZE);//version 4
    if (ofileSTD->is_open()) ofileSTD->close();
    delete[] header3_part1;
    delete[] header3_part2;
    return true;
}

int64_t alya::Update::readInteger8STD()
{
    int64_t buf;
    ifileSTD->read(reinterpret_cast<CHAR>(&buf), sizeof(buf));
    return buf;
}

string alya::Update::readStringSTD()
{
    char buf[MPIOHEADER_STRING_SIZE];
    ifileSTD->read(const_cast<CHAR>(buf), sizeof(buf));
    string str(buf);
    return str;
}
