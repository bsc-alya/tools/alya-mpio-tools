/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include "filelist.h"

alya::FileList::FileList(string path): path(path)
{
    struct stat info;

    if( stat( path.c_str(), &info ) != 0 ){
        Message::Error("Cannot access to "+path);
        return;
    }else if(! (info.st_mode & S_IFDIR) ){  // S_ISDIR() doesn't exist on my windows
        Message::Error(path+" is not a directory");
        return;
    }
    DIR *dir = opendir(path.c_str());
    dirent *entry;
    while((entry = readdir(dir))!=NULL)
    {
        if(AlyaString::HasSuffix(entry->d_name, string(MPIO_EXT)))
        {
            string str=string(entry->d_name);
            files.push_back(path+sep+str);
        }
    }
}

vector<string> alya::FileList::getFiles() const
{
    return files;
}
