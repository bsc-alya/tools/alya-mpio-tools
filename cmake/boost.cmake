if (SUBMODULE_BOOST)
  set(Boost_LIBRARIES "Boost::boost")
else()
  find_package(Boost REQUIRED)
endif()

function(include_directories_boost)
  if (NOT SUBMODULE_BOOST)
    include_directories(${Boost_INCLUDE_DIRS})
  endif()
endfunction()

function(target_link_libraries_boost)
  target_link_libraries(${PROJECT_NAME} ${Boost_LIBRARIES})
endfunction()

function(set_boost)
  include_directories_boost()
  target_link_libraries_boost()
endfunction()
