/*****************************************************************************\
 *                             Alya MPI-IO tools                             *
 *                              alya-mpio-tools                              *
 *        Libraries and Tools for reading and converting Alya files          *
 *****************************************************************************
 *     ___     This library is free software; you can redistribute it and/or *
 *    /  __         modify it under the terms of the GNU GPL as published    *
 *   /  /  _____    by the Free Software Foundation; either version 3        *
 *  /  /  /     \   of the License, or (at your option) any later version.   *
 * (  (  ( B S C )                                                           *
 *  \  \  \_____/   This library is distributed in hope that it will be      *
 *   \  \__         useful but WITHOUT ANY WARRANTY; without even the        *
 *    \___          implied warranty of MERCHANTABILITY or FITNESS FOR A     *
 *                  PARTICULAR PURPOSE. See the GNU GPL for more details.    *
 *                                                                           *
 * You should have received a copy of the GNU Lesser General Public License  *
 * along with this library; if not, write to the Free Software Foundation,   *
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA          *
 * The GNU LEsser General Public License is contained in the file COPYING.   *
 *                                 ---------                                 *
 *   Barcelona Supercomputing Center - Centro Nacional de Supercomputacion   *
\*****************************************************************************/

/*Author: Damien Dosimont <damien.dosimont[at]bsc.es>                        */
/*Date:   14/06/2017                                                         */



#include <iostream>
#include "alyastring.h"


using namespace std;

#define TEST(result) if (!(result)){\
                        cout<<"FAILED!"<<endl;\
                        return 1;\
                     }else{\
                        cout<<"SUCCESS!"<<endl;\
                     }
int main()
//int main(int argc, char* argv[])
{
    cout<<"Testing alya::AlyaString"<<endl;
    cout<<"Testing alya::AlyaString::Equal(string a, string b)"<<endl;
    cout<<"a=ABCDE, b=ABCDE"<<endl;
    TEST(alya::AlyaString::Equal("ABCDE", "ABCDE"));
    cout<<"a=ABCDE, b=ABCDF"<<endl;
    TEST(!alya::AlyaString::Equal("ABCDE", "ABCDF"));
    cout<<"a=ABcDE, b=AbcDef"<<endl;
    TEST(alya::AlyaString::Equal("ABcDE", "AbcDef"));
    cout<<"a=ABCDE, b=AbCD"<<endl;
    TEST(!alya::AlyaString::Equal("ABCDE", "AbCD"));
    cout<<"Testing alya::AlyaString::IsNumber(string s)"<<endl;
    cout<<"s=1"<<endl;
    TEST(alya::AlyaString::IsNumber("1"));
    cout<<"s=a"<<endl;
    TEST(!alya::AlyaString::IsNumber("a"));
    cout<<"s=0.5"<<endl;
    TEST(!alya::AlyaString::IsNumber("0.5"));
    cout<<"Testing alya::AlyaString::IsEnabled(string s)"<<endl;
    cout<<"s=ON"<<endl;
    TEST(alya::AlyaString::IsEnabled("ON"));
    cout<<"s=YES"<<endl;
    TEST(alya::AlyaString::IsEnabled("YES"));
    cout<<"s=on"<<endl;
    TEST(alya::AlyaString::IsEnabled("on"));
    cout<<"s=yes"<<endl;
    TEST(alya::AlyaString::IsEnabled("yes"));
    cout<<"s=no"<<endl;
    TEST(!alya::AlyaString::IsEnabled("no"));
    cout<<"s=OFF"<<endl;
    TEST(!alya::AlyaString::IsEnabled("OFF"));
    cout<<"Testing alya::AlyaString::Format5(string s)"<<endl;
    cout<<"s=ABCDEGFH"<<endl;
    TEST(alya::AlyaString::Format5("ABCDEGFH").compare("ABCDE")==0);
    cout<<"s=ABC"<<endl;
    TEST(alya::AlyaString::Format5("ABC").compare("ABC")==0);
    cout<<"Testing alya::AlyaString::Integer8(int i)"<<endl;
    cout<<"i=1"<<endl;
    TEST(alya::AlyaString::Integer8(1).compare("00000001")==0);
    cout<<"Testing alya::AlyaString::Format5Integer8(string s, int i)"<<endl;
    cout<<"s=ABCDEF, i=3"<<endl;
    TEST(alya::AlyaString::Format5Integer8("ABCDEF", 3).compare("ABCDE-00000003")==0);
    cout<<"Testing alya::AlyaString::HasSuffix(const string &s, const string &suffix)"<<endl;
    cout<<"s=file.suf, suffix=suf"<<endl;
    TEST(alya::AlyaString::HasSuffix("file.suf", "suf"));
    cout<<"s=file.saf, suffix=suf"<<endl;
    TEST(!alya::AlyaString::HasSuffix("file.saf", "suf"));
}
