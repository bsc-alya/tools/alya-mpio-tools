# alya-mpio-tools

Alya MPI-IO tools: Libraries and Tools for reading and converting Alya files

## License

Copyright: Barcelona Supercomputing Center - Centro Nacional de Supercomputacion, 2017

Alya-mpio-tools is free software; you can redistribute it and/or modify it under the terms of the GNU GPL as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.   
                                                         
Alya-mpio-tools is distributed in hope that it will be useful but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU GPL for more details.    
                                                                          
You should have received a copy of the GNU General Public License along with this library. If not, see [http://www.gnu.org/licenses](http://www.gnu.org/licenses).

## Documentation

You will find the documentation [here](https://github.com/dosimont/alya-mpio-tools/wiki).
